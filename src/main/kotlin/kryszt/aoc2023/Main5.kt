package kryszt.aoc2023

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines

object Main5 : DayChallenge {
    private const val SEED_TO_SOIL: String = "seed-to-soil map:"
    private const val SOIL_TO_FERTILIZER: String = "soil-to-fertilizer map:"
    private const val FERTILIZER_TO_WATER: String = "fertilizer-to-water map:"
    private const val WATER_TO_LIGHT: String = "water-to-light map:"
    private const val LIGHT_TO_TEMPERATURE: String = "light-to-temperature map:"
    private const val TEMPERATURE_TO_HUMIDITY: String = "temperature-to-humidity map:"
    private const val HUMIDITY_TO_LOCATION: String = "humidity-to-location map:"

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val task = readTaskLines("")
        val seeds: List<LongRange> = task.parseSingleSeeds()
        val mappings: Mappings = task.parseMappings()
        mappings
            .translate(seeds)
            .minOfOrNull { it.first }
            .alsoPrint()
    }

    override fun taskTwo() {
        val task = readTaskLines("")
        val seeds: List<LongRange> = task.parseSeedsRanges()
        val mappings: Mappings = task.parseMappings()
        mappings
            .translate(seeds)
            .alsoPrintOnEach()
            .minOfOrNull { it.first }
            .alsoPrint()
    }

    private fun List<String>.parseSingleSeeds(): List<LongRange> =
        this[0].drop(7).trim().split(" ").map { it.toLong().asRange() }

    private fun List<String>.parseSeedsRanges(): List<LongRange> =
        this[0].drop(7).trim().split(" ").map { it.toLong() }
            .windowed(size = 2, step = 2)
            .map { it[0]..<it[0] + it[1] }

    private fun List<String>.parseMappings(): Mappings = Mappings(
        seedToSoil = this.parseGroup(SEED_TO_SOIL),
        soilToFertilizer = this.parseGroup(SOIL_TO_FERTILIZER),
        fertilizerToWater = this.parseGroup(FERTILIZER_TO_WATER),
        waterToLight = this.parseGroup(WATER_TO_LIGHT),
        lightToTemperature = this.parseGroup(LIGHT_TO_TEMPERATURE),
        temperatureToHumidity = this.parseGroup(TEMPERATURE_TO_HUMIDITY),
        humidityToLocation = this.parseGroup(HUMIDITY_TO_LOCATION),
    )

    private fun List<String>.parseGroup(header: String): MappingGroup =
        this
            .dropWhile { it != header }
            .drop(1)
            .takeWhile { it.isNotEmpty() }
            .map { it.toMapping() }
            .let { MappingGroup(it) }

    private data class Mappings(
        val seedToSoil: MappingGroup,
        val soilToFertilizer: MappingGroup,
        val fertilizerToWater: MappingGroup,
        val waterToLight: MappingGroup,
        val lightToTemperature: MappingGroup,
        val temperatureToHumidity: MappingGroup,
        val humidityToLocation: MappingGroup,
    ) {
        private val translateOrder: List<MappingGroup> = listOf(
            seedToSoil,
            soilToFertilizer,
            fertilizerToWater,
            waterToLight,
            lightToTemperature,
            temperatureToHumidity,
            humidityToLocation,
        )

        fun translate(values: List<LongRange>): List<LongRange> {
            var current = values
            current.alsoPrint()
            translateOrder.forEach { mapping ->
                current = mapping.translate(current)
                current.alsoPrint()
            }
            return current
        }
    }

    private data class MappingGroup(val mappings: List<Mapping>) {
        val breakPoints: List<Long> =
            mappings
                .flatMap { listOf(it.range.first, it.range.last + 1) }
                .plus(0)
                .distinct()
                .sorted()

        private fun findDiff(value: Long): Long =
            mappings
                .find { it.contains(value) }
                ?.diff
                ?: 0

        fun translate(values: List<LongRange>): List<LongRange> =
            values
                .flatMap { translate(it) }
                .mergeContinuous()

        fun translate(values: LongRange): List<LongRange> = splitIntoTargets(values)
            .map { it.translate(findDiff(it.first)) }

        private fun splitIntoTargets(values: LongRange): List<LongRange> {
            val innerBreaks = breakPoints.filter { it > values.first && it <= values.last }
            if (innerBreaks.isEmpty()) return listOf(values)
            val allPoints = listOf(values.first) + innerBreaks + listOf(values.last + 1)
            return allPoints
                .windowed(size = 2, step = 1)
                .map { it[0]..<it[1] }
        }
    }

    private data class Mapping(val target: Long, val source: Long, val size: Long) {
        val range: LongRange = source..<source + size
        val diff = target - source

        fun contains(value: Long): Boolean = value in range
    }

    private fun String.toMapping(): Mapping {
        val (target, source, range) = this.trim().split(" ").map { it.toLong() }
        return Mapping(target, source, range)
    }

    private fun List<LongRange>.mergeContinuous(): List<LongRange> {
        if (this.size < 2) return this

        val ranges = mutableListOf<LongRange>()

        this.sortedBy { it.first }.forEach {
            if (ranges.lastOrNull()?.last?.plus(1) == it.first) {
                val prev = ranges.removeLast()
                val merged = prev.first..it.last
                ranges.add(merged)
            } else {
                ranges.add(it)
            }
        }
        return ranges
    }

    private fun Long.asRange(): LongRange = this..this
}