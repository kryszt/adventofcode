package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main12 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines("")
            .map { it.parseTask() }
            .map { it to it.unknownSpaces to countPossibleMatches(it.pattern, it.lines, it.unknownSpaces) }
            .sumOf { it.second }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines("")
            .map { it.parseFoldedTask() }
            .map { task ->
                task to countPossibleMatches(task.pattern, task.lines, task.unknownSpaces)
            }
            .sumOf { it.second }
            .alsoPrint()
    }

    private fun String.parseTask(): Task {
        val (pattern, numbers) = this.split(" ").let { it[0] to it[1].split(",").map(String::toInt) }
        return Task(
            pattern
                .trimEmpty()
                .trimToFirst(numbers.first().asFirstItem())
                .trimToLast(numbers.last().asLastItem()),
            numbers
        )
    }

    private fun String.parseFoldedTask(): Task {
        val (pattern, numbers) = this.split(" ").let { it[0] to it[1].split(",").map(String::toInt) }
        val unfoldedPattern = List(size = 5) { pattern }.joinToString(separator = "?")
        val unfoldedNumbers = List(size = 5) { numbers }.flatten()
        return Task(
            unfoldedPattern
                .trimEmpty()
                .trimToFirst(numbers.first().asFirstItem())
                .trimToLast(numbers.last().asLastItem()),
            unfoldedNumbers
        )
    }

    private fun String.trimEmpty() = this.dropWhile { it == '.' }.dropLastWhile { it == '.' }

    private fun String.trimToFirst(item: String): String {
        var position = 0
        while (!this.canPlace(item, position)) {
            position++
        }
        return this.drop(position)
    }

    private fun String.trimToLast(item: String): String {
        var position = this.length - item.length
        while (!this.canPlace(item, position)) {
            position--
        }
        return this.take(position + item.length)
    }

    private fun Int.asFirstItem(): String = asItem().breaksAfter()
    private fun Int.asLastItem(): String = asItem().breaksBefore()
    private fun Int.asItem(): String = "#".repeat(this)
    private fun String.breaksAfter(count: Int = 1) = this + (".".repeat(count))
    private fun String.breaksBefore(count: Int = 1) = (".".repeat(count)) + this

    private data class Task(val pattern: String, val lines: List<Int>) {
        val unknownSpaces = pattern.length - (lines.sum() + lines.size - 1)
    }

    private fun countPossibleMatches(pattern: String, items: List<Int>, spaces: Int): Long {
        val calculation = Calculation(pattern, spaces, items.joinToString(","))
        val found = calculationCache[calculation]
        if (found != null) return found

        val item = items.first()
        val rest = items.drop(1)
        val isLast = rest.isEmpty()
        var total = 0L

        (0..spaces).forEach { position ->
            val check = if (isLast) {
//                item.asItem().breaksAfter(spaces - position)
                item.asItem().breaksBefore(position).breaksAfter(spaces - position)
            } else {
                item.asItem().breaksBefore(position).breaksAfter(1)
            }

            if (pattern.canPlace(check, 0)) {
                if (isLast) {
                    total++
                } else {
                    total += countPossibleMatches(pattern.drop(check.length), rest, spaces - position)
                }
            }
        }
        calculationCache[calculation] = total
        return total
    }

    private fun String.canPlace(item: String, position: Int): Boolean {
        if ((item.length + position) > this.length) return false
        item.forEachIndexed { index, c ->
            val patternIndex = index + position
            if (this[patternIndex] != c && this[patternIndex] != '?') return false
        }
        return true
    }

    private val calculationCache = mutableMapOf<Calculation, Long>()

    private data class Calculation(val pattern: String, val spaces: Int, val items: String)

}