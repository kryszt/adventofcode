package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main2 : DayChallenge {

    private val limits = mapOf(
        "red" to 12,
        "green" to 13,
        "blue" to 14
    )

    private val colors = listOf("red", "green", "blue")

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines()
            .map { it.parseGame() }
            .filter { it.isValid(limits) }
            .sumOf { it.id }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines()
            .map { it.parseGame() }
            .sumOf { it.power() }
            .alsoPrint()
    }

    private data class Game(val id: Int, val rounds: List<Round>)

    private data class Round(val takes: Map<String, Int>)

    private fun String.parseGame(): Game {
        val (idPart, gamePart) = this.split(":").let { it[0] to it[1] }
        return Game(idPart.parseGameId(), gamePart.parseRounds())
    }

    private fun String.parseGameId(): Int =
        drop(5)
            .toInt()

    private fun String.parseRounds(): List<Round> =
        trim()
            .split(";")
            .map { it.parseRound() }

    private fun String.parseRound(): Round =
        trim()
            .split(",")
            .associate { it.parseTake() }
            .let { Round(it) }

    private fun String.parseTake(): Pair<String, Int> =
        trim()
            .split(" ")
            .let { it[1] to it[0].toInt() }

    private fun Game.isValid(limits: Map<String, Int>): Boolean =
        rounds
            .all { it.isValid(limits) }

    private fun Round.isValid(limits: Map<String, Int>): Boolean =
        takes
            .all { (k, v) ->
                val limit = limits[k] ?: 0
                v <= limit
            }

    private fun Game.power(): Int = this.rounds.power()

    private fun List<Round>.power(): Int =
        colors
            .map { this.maxColor(it) }
            .fold(initial = 1, operation = { a, b -> a * b })

    private fun List<Round>.maxColor(color: String): Int =
        this.maxOfOrNull { it.takes[color] ?: 0 } ?: 0
}
