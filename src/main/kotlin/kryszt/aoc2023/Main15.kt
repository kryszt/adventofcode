package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskFile
import kryszt.tools.alsoPrint

object Main15 : DayChallenge {
    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readCommands()
            .sumOf { it.hash }
            .alsoPrint()
    }

    override fun taskTwo() {
        val boxes = mutableMapOf<Int, Box>()

        readCommands()
            .forEach { command ->
                val box = boxes.getOrPut(command.labelHash) { Box(command.labelHash + 1) }
                box.command(command)
            }
        boxes
            .alsoPrint()
            .values
            .sumOf { it.focusingPower() }
            .alsoPrint()
    }

    private fun readCommands(): List<Command> =
        readTaskFile("")
            .split(",")
            .map { it.parseCommand() }


    private sealed interface Command {
        val label: String
        val hash: Int
        val labelHash: Int

        data class Put(
            override val label: String,
            override val hash: Int,
            val lens: Int,
            override val labelHash: Int = label.hash()
        ) : Command

        data class Remove(
            override val label: String,
            override val hash: Int,
            override val labelHash: Int = label.hash()
        ) : Command
    }

    private data class Lens(val label: String, val focal: Int)

    private class Box(val index: Int) {
        private val lenses = mutableListOf<Lens>()
        private fun List<Lens>.focusingPower() = this.mapIndexed { index, lens -> (index + 1) * lens.focal }.sum()


        fun focusingPower(): Int =
            index * lenses.focusingPower()

        fun command(command: Command) {
            when (command) {
                is Command.Put -> put(command.label, command.lens)
                is Command.Remove -> remove(command.label)
            }
        }

        fun put(label: String, lens: Int) {
            val newLens = Lens(label, lens)

            val index = lenses.indexOfFirst { it.label == label }
            if (index >= 0) {
                lenses[index] = newLens
            } else {
                lenses.add(newLens)
            }
        }

        fun remove(label: String) {
            val index = lenses.indexOfFirst { it.label == label }
            if (index >= 0) lenses.removeAt(index)
        }
    }


    private fun String.parseCommand(): Command {
        val label = this.takeLabel()
        val hash = this.hash()
        if (this.endsWith("-")) return Command.Remove(label, hash)
        return Command.Put(label, hash, this.takeLens())
    }

    private fun String.takeLabel(): String = this.takeWhile { it != '-' && it != '=' }
    private fun String.takeLens(): Int = this.takeLastWhile { it != '=' }.toInt()

    private fun String.hash(): Int =
        this.fold(initial = 0, operation = { current, c ->
            current.plus(c.code).times(17).mod(256)
        })
}