package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint
import kryszt.tools.dividesBy

object Main17 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val board = readTaskLines("")
            .readBoard()
        solve(board, 1..3).alsoPrint()
    }

    override fun taskTwo() {
        val board = readTaskLines("")
            .readBoard()
        solve(board, 4..10).alsoPrint()
    }

    private fun solve(
        board: Board,
        stepsRange: IntRange,
        start: List<Walk> = listOf(
            Walk(0, Position(Point(0, 0), Point.Right)),
            Walk(0, Position(Point(0, 0), Point.Down))
        )
    ): Int? {
        var places = start

        var steps = 0
        while (places.isNotEmpty()) {
            steps++
            if (steps.dividesBy(1)) println("step: $steps, places: ${places.size}")
            places = places.flatMap { it.next(board, stepsRange) }.distinct()
            board.addLowest(places)
            places = places.filter { board.isLowest(it) }.distinct()
        }

        return board.lowestForLast()
    }

    private fun List<String>.readBoard(): Board =
        this
            .map { line ->
                line.map { it - '0' }.toIntArray()
            }
            .toTypedArray()
            .let { Board(it) }

    private class Board(val tiles: Array<IntArray>) {
        private val lowestCosts = mutableMapOf<Position, Int>()

        fun addLowest(walks: List<Walk>) {
            walks.forEach {
                if (isNewLowest(it.position, it.cost)) {
                    lowestCosts[it.position] = it.cost
                }
            }
        }

        fun isNewLowest(position: Position, cost: Int): Boolean {
            val currentLowest = lowestCosts[position] ?: return true
            return cost < currentLowest
        }

        private fun lastPoint(): Point {
            val y = tiles.lastIndex
            val x = tiles[y].lastIndex
            return Point(x, y)
        }

        private fun lowest(point: Point): Int? = Point
            .neighbours
            .map { Position(point, it) }
            .mapNotNull { lowestCosts[it] }
            .minOrNull()

        fun lowestForLast(): Int? {
            return lowest(lastPoint())
        }

        fun contains(point: Point): Boolean = point.y in tiles.indices && point.x in tiles[point.y].indices
        fun cost(point: Point): Int = tiles[point.y][point.x]
        fun isLowest(it: Walk): Boolean {
            val currentLowest = lowestCosts[it.position] ?: return true
            return it.cost <= currentLowest
        }
    }

    private fun Walk.next(board: Board, stepsRange: IntRange): List<Walk> {
        val next = mutableListOf<Walk>()
        val initialPosition = this.position
        var currentCost = this.cost
        repeat(stepsRange.last) { i ->
            val steps = i + 1
            val moved = initialPosition.move(steps)
            if (board.contains(moved.place)) {
                currentCost += board.cost(moved.place)
                if (stepsRange.contains(steps)) {
                    val positionLeft = moved.turnLeft()
                    if (board.isNewLowest(positionLeft, currentCost)) {
                        next.add(Walk(currentCost, positionLeft))
                    }
                    val positionRight = moved.turnRight()
                    if (board.isNewLowest(positionRight, currentCost)) {
                        next.add(Walk(currentCost, positionRight))
                    }
                }
            }
        }
        return next
    }

    private data class Position(val place: Point, val direction: Point) {
        fun move(steps: Int): Position = copy(place = place.plus(direction.times(steps)))
        fun turnLeft(): Position = copy(direction = direction.left())
        fun turnRight(): Position = copy(direction = direction.right())
    }

    private data class Walk(val cost: Int, val position: Position)

}