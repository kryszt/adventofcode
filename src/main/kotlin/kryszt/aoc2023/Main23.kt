package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint
import kryszt.tools.alsoPrintOnEach

object Main23 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val board = readTaskLines("").readBoard()
        findLongest(board, findAllDirectedPaths(board))
            .alsoPrintOnEach()
            .totalLength()
            .alsoPrint()
    }

    override fun taskTwo() {
        val board = readTaskLines("").readBoard()
        findLongest(board, findAllPaths(board))
            .alsoPrintOnEach()
            .totalLength()
            .alsoPrint()
    }

    private fun findLongest(board: Board, allPaths: List<Path>): List<Path> {
        println()
        val first = allPaths.find { it.start == board.start } ?: error("No starting path?")
        return findLongest(listOf(first), board.end, allPaths)
    }

    private fun findLongest(currentPath: List<Path>, end: Point, allPaths: List<Path>): List<Path> {
        val currentEnd = currentPath.last().end
        if (currentEnd == end) return currentPath
        val nextSegments = allPaths.filter { it.start == currentEnd && !currentPath.contains(it.end) }
        if (nextSegments.isEmpty()) return emptyList()

        return nextSegments
            .map { next -> findLongest(currentPath.plus(next), end, allPaths) }
            .maxBy { it.totalLength() }
    }

    private fun List<Path>.contains(point: Point): Boolean = this.any { it.start == point || it.end == point }
    private fun List<Path>.totalLength(): Int = this.sumOf { it.length }

    private fun List<String>.readBoard(): Board = Board(
        maze = this,
        start = Point(first().indexOf('.'), 0),
        end = Point(last().indexOf('.'), lastIndex)
    )

    private fun findAllDirectedPaths(board: Board): List<Path> =
        board.pathPoints
            .flatMap {
                Point.crossNeighbours.mapNotNull { direction ->
                    findPath(it, direction, board) { board, point, walkDirection ->
                        board.canWalk(point, walkDirection)
                    }
                }
            }

    private fun findAllPaths(board: Board): List<Path> =
        board.pathPoints
            .flatMap {
                Point.crossNeighbours.mapNotNull { direction ->
                    findPath(it, direction, board) { board, point, _ ->
                        board.isPath(point)
                    }
                }
            }

    private fun findPath(
        start: Point,
        startDirection: Point,
        board: Board,
        pointCheck: (Board, Point, Point) -> Boolean
    ): Path? {
        var position = start.plus(startDirection).takeIf { pointCheck(board, it, startDirection) } ?: return null
        var steps = 1
        var direction = startDirection

        val endpoints = board.pathPoints
        while (!endpoints.contains(position)) {
            val nextDirection = direction.nextDirections().find { d ->
                pointCheck(board, position.plus(d), d)
            } ?: return null
            steps++
            position = position.plus(nextDirection)
            direction = nextDirection
        }
        return Path(start, position, steps)
    }

    private class Board(val maze: List<String>, val start: Point, val end: Point) {
        val left = 0
        val right = maze.first().lastIndex
        val top = 0
        val bottom = maze.lastIndex

        val crossings: List<Point> by lazy { findCrossings() }

        val pathPoints: Set<Point> by lazy {
            crossings.plus(start).plus(end).toSet()
        }

        private fun findCrossings(): List<Point> {
            val crossings = mutableListOf<Point>()
            for (x in left..right) {
                for (y in top..bottom) {
                    val point = Point(x, y)
                    if (isPath(point)) {
                        if (point.crossNeighbours().count { isPath(it) } > 2) {
                            crossings.add(point)
                        }
                    }
                }
            }
            return crossings
        }

        fun canWalk(point: Point, direction: Point): Boolean = onBoard(point) && cell(point).canPass(direction)
        fun isPath(point: Point): Boolean = onBoard(point) && (cell(point).isPath() || cell(point).isSlope())

        private fun onBoard(point: Point): Boolean = point.y in maze.indices && point.x in maze[point.y].indices
        private fun cell(point: Point): Char = maze[point.y][point.x]
    }

    private data class Path(val start: Point, val end: Point, val length: Int)

    private fun Char.isPath() = this == '.'
    private fun Char.isSlope() = when (this) {
        '<', '>', '^', 'v' -> true
        else -> false
    }

    private fun Point.nextDirections(): Sequence<Point> = sequenceOf(this, this.left(), this.right())

    private fun Char.canPass(direction: Point): Boolean = when (this) {
        '.' -> true
        '#' -> false
        '>' -> direction == Point.Right
        '<' -> direction == Point.Left
        '^' -> direction == Point.Up
        'v' -> direction == Point.Down
        else -> error("Not a board: $this")
    }
}