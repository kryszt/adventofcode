package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main9 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines("")
            .map { line -> line.split(" ").map { it.toInt() } }
            .sumOf { it.extrapolateNext() }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines("")
            .map { line -> line.split(" ").map { it.toInt() } }
            .sumOf { it.extrapolatePrev() }
            .alsoPrint()
    }

    private fun List<Int>.extrapolateNext(): Int {
        if (all { it == 0 }) return 0

        return last() + differences().extrapolateNext()
    }

    private fun List<Int>.extrapolatePrev(): Int {
        if (all { it == 0 }) return 0

        return first() - differences().extrapolatePrev()
    }

    private fun List<Int>.differences(): List<Int> =
        windowed(size = 2, step = 1)
            .map { it[1] - it[0] }
}