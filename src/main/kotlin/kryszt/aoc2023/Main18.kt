package kryszt.aoc2023

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines
import kotlin.math.max
import kotlin.math.min

object Main18 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solve { it.parseOrder() }
    }

    override fun taskTwo() {
        solve { it.parseFromColor() }
    }

    private fun solve(parse: (String) -> Order) {
        val lines = readTaskLines("")
            .map(parse)
            .makeLines(Point(0, 0))

        val rows = lines.makeRows().also { it.checkContinuous() }
        val board = Board(rows)

        board.markAreas(lines)
        board.calculateEnclosed().alsoPrint()
    }


    private fun List<Row>.checkContinuous() {
        this.windowed(size = 2, step = 1) { (top, bottom) ->
            require(top.bottom == (bottom.top - 1))
        }
        this.forEach { row ->
            row.areas.forEach { area ->
                require(area.yRange == row.yRange)
            }
            row.areas.windowed(size = 2, step = 1) { (left, right) ->
                require(left.right == right.left - 1)
            }
        }
    }

    private fun List<Order>.makeLines(start: Point): List<Line> {
        val lines = mutableListOf<Line>()
        var position = start
        forEach { order ->
            val nextPosition = position.plus(order.direction.times(order.steps))
            lines.add(Line(position, nextPosition))
            position = nextPosition

        }
        return lines
    }

    private fun List<Line>.makeRows(): List<Row> {
        val xPoints = this.filter { it.isVertical }.map { it.left }.distinct()
        val yPoints = this.filterNot { it.isVertical }.map { it.top }.distinct()

        val xRanges = xPoints.makeInnerRanges().plus(xPoints.map { it..it }).sortedBy { it.first }
        val yRanges = yPoints.makeInnerRanges().plus(yPoints.map { it..it }).sortedBy { it.first }

        return yRanges
            .map { yRange ->
                Row(
                    yRange = yRange,
                    areas = xRanges.map { xRange ->
                        Area(xRange, yRange)
                    })
            }

    }

    @Suppress("ConvertCallChainIntoSequence")
    private fun List<Int>.makeInnerRanges(): List<IntRange> =
        this
            .sorted()
            .windowed(size = 2, step = 1)
            .filter { it[1] - it[0] > 1 }
            .map { it[0].plus(1)..it[1].minus(1) }

    private fun String.parseOrder(): Order = Order(
        direction = this.first().asDirection(),
        steps = this.drop(2).takeWhile { it.isDigit() }.toInt(),
    )

    private fun String.extractColor(): String =
        this.dropLast(1).takeLastWhile { it != '#' }


    private fun String.parseFromColor(): Order =
        extractColor()
            .let { colorString ->
                Order(
                    direction = colorString.last().asDirection(),
                    steps = colorString.take(5).toInt(16),
                )
            }

    private fun Char.asDirection(): Point = when (this) {
        'U', '3' -> Point.Up
        'D', '1' -> Point.Down
        'L', '2' -> Point.Left
        'R', '0' -> Point.Right
        else -> error("Unsupported direction: $this")
    }

    private data class Order(val direction: Point, val steps: Int)
    private data class Line(val start: Point, val end: Point) {
        val top = min(start.y, end.y)
        val bottom = max(start.y, end.y)
        val left = min(start.x, end.x)
        val right = max(start.x, end.x)
        val isVertical = start.x == end.x
        val isHorizontal = start.y == end.y
        val xRange = left..right
    }

    private data class Board(val rows: List<Row>) {

        fun markAreas(lines: List<Line>) {
            rows.forEach { markAreas(lines, it) }
        }

        private fun markAreas(lines: List<Line>, row: Row) {
            val relevantVertical = lines.filter { it.isVertical && it.top < row.top && it.bottom > row.bottom }
            val relevantHorizontal = lines.filter { it.isHorizontal && it.top == row.top && it.bottom == row.bottom }
            val relevant = relevantVertical.plus(relevantHorizontal).sortedBy { it.left }

            var closed = false
            relevant.forEach { line ->
                if (line.isVertical) {
                    closed = !closed
                    //mark line:
                    row.areas.find { it.left == line.left }?.closed = true
                    //mark all after:
                    row.areas.filter { it.left > line.right }.forEach { it.closed = closed }
                } else {
                    //mark line:
                    row.areas.filter { line.xRange.contains(it.xRange) }
                        .forEach { it.closed = true }
                    if (line.changesClosed(lines)) {
                        closed = !closed
                    }
                    row.areas
                        .filter { it.left > line.right }
                        .forEach { it.closed = closed }
                }
            }
        }

        private fun Line.changesClosed(all: List<Line>): Boolean {
            if (this.isVertical) return true
            val connected = findConnected(all)
            return connected.first.top != connected.second.top
                    && connected.first.bottom != connected.second.bottom
        }

        private fun Line.findConnected(all: List<Line>): Pair<Line, Line> {
            return if (this.isVertical) {
                Pair(
                    all.find { it.isHorizontal && this.top == it.top && (this.left == it.left || this.left == it.right) }!!,
                    all.find { it.isHorizontal && this.bottom == it.bottom && (this.left == it.left || this.left == it.right) }!!
                )
            } else {
                Pair(
                    all.find { it.isVertical && this.left == it.left && (this.top == it.top || this.top == it.bottom) }!!,
                    all.find { it.isVertical && this.right == it.right && (this.top == it.top || this.top == it.bottom) }!!
                )
            }
        }

        fun calculateEnclosed(): Long {
            return rows.sumOf { row -> row.areas.sumOf { it.area() } }
        }
    }

    private data class Area(val xRange: IntRange, val yRange: IntRange, var closed: Boolean = false) {
        val left = xRange.first
        val right = xRange.last


        fun area(): Long {
            return if (!closed) return 0L else xRange.width() * yRange.width()
        }
    }

    private data class Row(val yRange: IntRange, val areas: List<Area>) {
        val top = yRange.first
        val bottom = yRange.last
    }
}