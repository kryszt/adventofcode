package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint

object Main10 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val board = readTaskLines("")
        val start = board.findStart()
        visitAll(board, start).size.div(2).alsoPrint()
    }

    override fun taskTwo() {
        val board = readTaskLines("")
        val start = board.findStart()
        val startPipe = board.startPipe(start)
        val loop = visitAll(board, start)
        board.countEnclosed(loop, startPipe).alsoPrint()
    }

    private fun visitAll(
        board: List<String>,
        start: Point,
    ): Set<Point> {
        val startDirections = board.startNeighbours(start)
        val visited = mutableSetOf(start)
        var current: List<Point> = startDirections.map { it.plus(start) }
        while (current.isNotEmpty()) {
            visited.addAll(current)
            current = current.flatMap { board.neighbours(it) }.filterNot { visited.contains(it) }
        }
        return visited
    }

    private fun List<String>.countEnclosed(loop: Set<Point>, startPipe: Char): Int = this
        .mapIndexed { y, line ->
            line.countEnclosed(y, loop, startPipe)
        }
        .sum()

    private fun String.countEnclosed(y: Int, loop: Set<Point>, startPipe: Char): Int {
        val reducer = EncloseStateReducer()
        var state: EncloseState = EncloseState.Open
        var enclosed = 0
        this.forEachIndexed { x, sign ->
            val c = if (sign == 'S') startPipe else sign
            val inLoop = loop.contains(Point(x, y))
            if (!inLoop && state == EncloseState.Closed) {
                enclosed++
            } else if (inLoop) {
                state = reducer.nextState(state, c)
            }
        }
        return enclosed
    }

    private fun List<String>.findStart(): Point {
        this.forEachIndexed { y, line ->
            val x = line.indexOf('S')
            if (x >= 0) return Point(x, y)
        }
        error("'S' not found")
    }

    private fun List<String>.neighbours(point: Point): List<Point> =
        this.get(point)!!.connected().map { it + point }

    private fun List<String>.startPipe(start: Point): Char {
        var options = setOf('|', '-', 'F', 'L', '7', 'J')
        if (this.get(start.oneLeft())?.connected()?.contains(Point.Right) == true) {
            options = options.intersect(setOf('-', '7', 'J'))
        }
        if (this.get(start.oneRight())?.connected()?.contains(Point.Left) == true) {
            options = options.intersect(setOf('-', 'F', 'L'))
        }
        if (this.get(start.oneUp())?.connected()?.contains(Point.Down) == true) {
            options = options.intersect(setOf('|', 'L', 'J'))
        }
        if (this.get(start.oneDown())?.connected()?.contains(Point.Up) == true) {
            options = options.intersect(setOf('|', '7', 'F'))
        }
        check(options.size == 1) { "Something is wrong, should have only one option" }
        return options.first()
    }

    private fun List<String>.startNeighbours(start: Point): List<Point> =
        startPipe(start).connected()

    private fun List<String>.get(point: Point): Char? =
        this.getOrNull(point.y)?.getOrNull(point.x)

    private fun Char.connected(): List<Point> = pipeNeighbours[this].orEmpty()

    private val pipeNeighbours = mapOf(
        '|' to listOf(Point.Up, Point.Down),
        '-' to listOf(Point.Left, Point.Right),
        'L' to listOf(Point.Up, Point.Right),
        'J' to listOf(Point.Up, Point.Left),
        '7' to listOf(Point.Down, Point.Left),
        'F' to listOf(Point.Down, Point.Right),
    )

    private enum class EncloseState {
        Open, ClosingUp, ClosingDown, Closed, OpeningUp, OpeningDown
    }

    private class EncloseStateReducer {
        fun nextState(current: EncloseState, sign: Char): EncloseState =
            when (current) {
                EncloseState.Open -> when (sign) {
                    '|' -> EncloseState.Closed
                    'F' -> EncloseState.ClosingDown
                    'L' -> EncloseState.ClosingUp
                    else -> error("$sign not allowed in $current")
                }

                EncloseState.Closed -> when (sign) {
                    '|' -> EncloseState.Open
                    'F' -> EncloseState.OpeningDown
                    'L' -> EncloseState.OpeningUp
                    else -> error("$sign not allowed in $current")
                }


                EncloseState.ClosingUp -> when (sign) {
                    'J' -> EncloseState.Open
                    '7' -> EncloseState.Closed
                    '-' -> current
                    else -> error("$sign not allowed in $current")
                }

                EncloseState.ClosingDown -> when (sign) {
                    'J' -> EncloseState.Closed
                    '7' -> EncloseState.Open
                    '-' -> current
                    else -> error("$sign not allowed in $current")
                }

                EncloseState.OpeningUp -> when (sign) {
                    'J' -> EncloseState.Closed
                    '7' -> EncloseState.Open
                    '-' -> current
                    else -> error("$sign not allowed in $current")
                }

                EncloseState.OpeningDown -> when (sign) {
                    'J' -> EncloseState.Open
                    '7' -> EncloseState.Closed
                    '-' -> current
                    else -> error("$sign not allowed in $current")
                }
            }
    }
}