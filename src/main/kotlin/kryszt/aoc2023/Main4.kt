package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main4 : DayChallenge {


    private val whitespaces = "\\s+".toRegex()

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines("")
            .sumOf { it.asGame().score() }
            .alsoPrint()
    }

    override fun taskTwo() {
        val games = readTaskLines("")
            .map { GameCounter(it.asGame()) }
            .onEachIndexed { index, gameCounter -> check(index == gameCounter.game.id - 1) }

        games.forEachIndexed { index, gameCounter ->
            repeat(gameCounter.game.winningCount) { repeat ->
                games[index + repeat + 1].add(gameCounter.count)
            }
        }

        games
            .sumOf { it.count }
            .alsoPrint()
    }

    private fun String.asGame(): Game {
        val id = this.drop(5).takeWhile { it != ':' }.trim().toInt()
        val winning = this.dropWhile { it != ':' }.drop(1).takeWhile { it != '|' }.splitNumbers().toSet()
        val selected = this.dropWhile { it != '|' }.drop(1).splitNumbers()

        return Game(id, selected.count { winning.contains(it) })
    }

    private fun String.splitNumbers(): List<Int> =
        this
            .trim()
            .split(whitespaces)
            .map { it.toInt() }

    private data class Game(val id: Int, val winningCount: Int)
    private data class GameCounter(val game: Game, var count: Int = 1) {
        fun add(change: Int) {
            count += change
        }
    }

    private fun Game.score(): Int =
        winningCount
            .takeIf { it > 0 }
            ?.let { 1 shl (it - 1) }
            ?: 0
}