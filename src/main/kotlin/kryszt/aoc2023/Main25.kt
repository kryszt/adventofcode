package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import java.util.*

object Main25 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val connections = readTaskLines("")
            .readConnections()
        val graph = Graph(connections)
        val pathsCount = findPathCounts(graph)
        val otherSize = pathsCount[3]!!
        val thisSize = graph.vertices.size - otherSize
        (otherSize * thisSize).alsoPrint()
    }

    override fun taskTwo() {
    }

    private fun findPathCounts(graph: Graph): Map<Int, Int> {
        val vertices = graph.vertices.toList()
        val counters = mutableMapOf<Int, Int>()
        for (j in 1..vertices.lastIndex) {
            val paths = findPathsCount(graph, vertices[0], vertices[j])
            counters[paths] = counters[paths]?.plus(1) ?: 1
        }
        return counters
    }

    private fun findPathsCount(graph: Graph, start: String, target: String): Int {
        var count = 0
        var currentGraph = graph
        while (true) {
            val path = findShortestPath(currentGraph, start, target)
            if (path.isEmpty()) return count
            count++
            currentGraph = currentGraph.removePath(path)
        }
    }

    private fun findShortestPath(graph: Graph, start: String, target: String): List<String> {
        val queue = LinkedList(listOf(start))
        val visited = mutableSetOf<String>().apply { add(start) }
        val parents = mutableMapOf<String, String>()

        while (queue.isNotEmpty()) {
            val v = queue.poll()
            val next = graph.connected(v).filterNot { visited.contains(it) }
            queue.addAll(next)
            visited.addAll(next)
            parents.putAll(next.map { it to v })
            if (visited.contains(target)) return makePath(target, parents)
        }
        return emptyList()
    }

    private fun makePath(target: String, parents: Map<String, String>): List<String> {
        val path = mutableListOf<String>()
        var current: String? = target
        while (current != null) {
            path.add(current)
            current = parents[current]
        }
        return path
    }

    private fun List<String>.readConnections(): Map<String, Set<String>> {
        val result = mutableMapOf<String, Set<String>>()
        forEach { line ->
            val from = line.takeWhile { it != ':' }
            val to = line.takeLastWhile { it != ':' }.trim().split(" ").map { it.trim() }
            result.addConnections(from, to)
        }
        return result.also { it.check() }
    }

    private fun MutableMap<String, Set<String>>.addConnections(from: String, to: List<String>) {
        this[from] = this[from].orEmpty().plus(to)
        to.forEach { target ->
            this[target] = this[target].orEmpty().plus(from)
        }
    }

    private class Graph(private val links: Map<String, Set<String>>) {

        val vertices: Collection<String> = links.keys

        fun connected(v: String): Set<String> = links[v].orEmpty()

        fun removePath(path: List<String>): Graph {
            return Graph(links.removePath(path))
        }
    }

    private fun Map<String, Set<String>>.removePath(path: List<String>): Map<String, Set<String>> {
        var updated = this
        path.windowed(size = 2, step = 1)
            .forEach { (start, end) ->
                updated = updated.removeLink(start, end)
            }
        return updated
    }

    private fun Map<String, Set<String>>.removeLink(from: String, to: String): Map<String, Set<String>> =
        this
            .plus(from to this[from]!!.minus(to))
            .plus(to to this[to]!!.minus(from))

    private fun Map<String, Set<String>>.check() {
        entries.forEach {
            require(it.value.size > 3) { "At least 3 connections required, failed for $it" }
        }

        entries.forEach { (source, targets) ->
            targets.forEach { target ->
                check(this[target].orEmpty().contains(source)) { "Missing two-way connection from $source to $target" }
            }
        }
    }
}