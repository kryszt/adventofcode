package kryszt.aoc2023

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines

object Main8 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val (turns, directions) = readTaskLines("").read()
        Solver(turns, directions).solve().alsoPrint()
    }

    override fun taskTwo() {
        val (turns, directions) = readTaskLines("").read()
        Solver2(turns, directions).solve().alsoPrint()
    }

    private fun List<String>.read(): Pair<String, Map<String, Pair<String, String>>> =
        Pair(
            this[0],
            this.drop(2).parseDirections()
        )

    private fun List<String>.parseDirections(): Map<String, Pair<String, String>> =
        this.associate { it.parseLine() }

    private fun String.parseLine(): Pair<String, Pair<String, String>> =
        Pair(
            this.substring(0..2),
            Pair(
                this.substring(7..9),
                this.substring(12..14),
            )
        )

    private class Solver(
        private val turns: String,
        private val directions: Map<String, Pair<String, String>>
    ) {
        fun solve(): Int {
            val turnSequence = turns.asRepeatingSequence().iterator()
            var current = "AAA"
            var steps = 0
            while (current != "ZZZ") {
                steps++
                val turn = turnSequence.next()
                val nextDirections = directions[current]!!
                current = if (turn == 'L') nextDirections.first else nextDirections.second
            }
            return steps
        }
    }

    private class Solver2(
        private val turns: String,
        private val directions: Map<String, Pair<String, String>>
    ) {
        fun solve(): Long {
            val start = directions.keys.filter { it[2] == 'A' }
            val ends = directions.keys.filter { it[2] == 'Z' }
            val endMet = mutableMapOf<String, List<Long>>()

            val turnSequence = turns.asRepeatingSequence().iterator()
            var current = start
            var steps = 0L
            while (!endMet.hasAll(ends.size)) {
                steps++
                val turn = turnSequence.next()
                current = current.map {
                    val nextDirections = directions[it]!!
                    val next = if (turn == 'L') nextDirections.first else nextDirections.second
                    if (next[2] == 'Z') endMet.append(next, steps)
                    next
                }
            }
            return endMet.expectedSteps(turns.length)
        }

        private fun Map<String, List<Long>>.hasAll(expected: Int): Boolean =
            this.size == expected
                    && this.all { it.value.size > 1 }

        private fun MutableMap<String, List<Long>>.append(key: String, value: Long) {
            this[key] = this[key].orEmpty().plus(value)
        }

        private fun Map<String, List<Long>>.expectedSteps(loopSize: Int): Long =
            this
                .values
                .map { (it[1] - it[0]) }
                .onEach { check(it.dividesBy(loopSize)) }
                .map { it / loopSize }
                .multiply()
                .times(loopSize)
    }
}