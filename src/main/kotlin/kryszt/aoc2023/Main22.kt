package kryszt.aoc2023

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines

object Main22 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        buildFloor()
            .countRemovable()
            .alsoPrint()
    }

    override fun taskTwo() {
        buildFloor()
            .countMaxCollapse()
            .alsoPrint()
    }

    private fun buildFloor(): Floor {
        val bricks = readTaskLines("")
            .map { it.parseBrick() }
            .sortedBy { it.bottom }
        val floor = Floor()
        bricks.forEach {
            floor.place(it)
        }
        return floor
    }

    private fun String.parseBrick(): Brick =
        split("~").let { (start, end) -> Brick(start.parsePoint3D(), end.parsePoint3D()) }

    private fun String.parsePoint3D(): Point3D = split(",").map { it.toInt() }.let { (x, y, z) -> Point3D(x, y, z) }

    private class Floor {

        private val columns = mutableMapOf<Point, Brick>()
        private val basedOn = mutableMapOf<Brick, List<Brick>>()
        private val supportOf = mutableMapOf<Brick, List<Brick>>()
        private val allBricks = mutableListOf<Brick>()

        fun place(brick: Brick) {
            val brickSupports = findSupports(brick.areaPoints)
            val placeHeight = brickSupports.maxHeight() + 1
            val placed = brick.moveToZ(placeHeight)

            allBricks.add(placed)
            basedOn[placed] = brickSupports
            brick.areaPoints.forEach { columns[it] = placed }
            brickSupports.forEach {
                supportOf[it] = supportOf[it]?.plus(placed) ?: listOf(placed)
            }
        }

        fun countMaxCollapse(): Int = allCollapses()
            .values.sumOf { it.size }

        private fun allCollapses(): Map<Brick, Set<Brick>> {
            return allBricks.associateWith { collapses(it) }
        }

        private fun collapses(brick: Brick): Set<Brick> {
            val collapsed = mutableSetOf(brick)
            var candidates = supportOf[brick].orEmpty()
            while (candidates.isNotEmpty()) {
                val willCollapse = candidates.filter { candidate ->
                    basedOn[candidate]?.minus(collapsed).isNullOrEmpty()
                }
                collapsed.addAll(willCollapse)
                candidates = candidates.flatMap { supportOf[it].orEmpty() }.distinct()
            }
            return collapsed.minus(brick)
        }

        fun countRemovable(): Int {
            return allBricks.count {
                canRemove(it)
            }
        }

        private fun canRemove(brick: Brick): Boolean {
            val supportedByCurrent = supportOf[brick]?.takeUnless { it.isEmpty() } ?: return true

            return supportedByCurrent.all {
                (basedOn[it]?.size ?: 0) > 1
            }
        }

        fun findSupports(area: List<Point>): List<Brick> {
            val base = area
                .mapNotNull { columns[it] }
                .takeUnless { it.isEmpty() }
                ?.distinct()
                ?: return emptyList()
            val max = base.maxOf { it.top }
            return base.filter { it.top == max }
        }

        private fun List<Brick>.maxHeight(): Int =
            this.maxOfOrNull { it.top } ?: 0

    }

    private data class Brick(val start: Point3D, val end: Point3D) {
        val bottom = start.z
        val top = end.z

        val areaPoints: List<Point> = (start.x..end.x)
            .flatMap { x ->
                (start.y..end.y).map { y ->
                    Point(x, y)
                }
            }

        fun moveToZ(newZ: Int) = moveZBy(newZ - bottom)

        fun moveZBy(dz: Int) = Brick(start = start.moveBy(dz = dz), end = end.moveBy(dz = dz))
    }
}