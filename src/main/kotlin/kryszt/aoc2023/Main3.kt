package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint

object Main3 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val content = readTaskLines("")
        val symbols = findAllSymbols(content)
        val numbers = findAllNumbers(content)

        numbers
            .filter { part ->
                symbols.any { symbol -> areNeighbours(symbol, part) }
            }
            .sumOf { it.value }
            .alsoPrint()
    }

    override fun taskTwo() {
        val content = readTaskLines("")
        val gears = findAllSymbols(content).filter { it.char == '*' }
        val numbers = findAllNumbers(content)

        gears
            .associateWith { gear -> numbers.filter { part -> areNeighbours(gear, part) } }
            .filter { it.value.size == 2 }
            .map { it.value[0].value * it.value[1].value }
            .sum()
            .alsoPrint()
    }

    private fun findAllSymbols(lines: List<String>): List<Symbol> {
        val symbols = mutableListOf<Symbol>()
        lines.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                if (c.isSymbol()) {
                    val point = Point(x, y)
                    symbols.add(Symbol(c, point))
                }
            }
        }
        return symbols
    }

    private fun findAllNumbers(lines: List<String>): List<PartNumber> {
        val found = mutableListOf<PartNumber>()
        var currentValue = 0
        lateinit var start: Point

        lines.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                if (c.isDigit()) {
                    if (currentValue == 0) {
                        start = Point(x, y)
                    }
                    val digitValue = (c - '0')
                    currentValue = currentValue * 10 + digitValue
                } else {
                    if (currentValue > 0) {
                        found += PartNumber(currentValue, start, Point(x - 1, y))
                    }
                    currentValue = 0
                }
            }
            if (currentValue > 0) {
                found += PartNumber(currentValue, start, Point(line.lastIndex, y - 1))
            }
            currentValue = 0
        }
        return found
    }

    private fun areNeighbours(symbol: Symbol, part: PartNumber): Boolean =
        symbol.point.y in (part.start.y - 1)..(part.start.y + 1)
                && symbol.point.x in (part.start.x - 1)..(part.end.x + 1)


    private data class Symbol(val char: Char, val point: Point)

    private data class PartNumber(val value: Int, val start: Point, val end: Point)

    private fun Char.isSymbol(): Boolean = this != '.' && this.isDigit().not()
}