package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main1 : DayChallenge {

    private val digit = "\\d".toRegex()
    private val anyFormDigit = "(?=(one|two|three|four|five|six|seven|eight|nine|\\d))".toRegex()

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }
    override fun taskOne() {
        readTaskLines()
            .sumOf { it.decodeNumber(digit) }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines()
            .sumOf { it.decodeNumber(anyFormDigit) }
            .alsoPrint()
    }

    private fun String.decodeNumber(match: Regex): Int {
        val all = match.findAll(this).toList().flatMap { it.groupValues }.filterNot { it.isEmpty() }
        val first = all.first()
        val last = all.last()
        val value = first.decodeDigit() * 10 + last.decodeDigit()
        println("$this -> $first, $last -> $value")
        return value
    }

    private fun String.decodeDigit(): Int {
        if(this.length == 1 && this[0].isDigit()) return this[0] - '0'
        return digitWords[this] ?: error("Not a digit string: $this")
    }

    private val digitWords = mapOf(
        "one" to 1,
        "two" to 2,
        "three" to 3,
        "four" to 4,
        "five" to 5,
        "six" to 6,
        "seven" to 7,
        "eight" to 8,
        "nine" to 9,
        )
}
