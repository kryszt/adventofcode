package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import kryszt.tools.width

object Main19 : DayChallenge {

    private val groups = listOf("x", "a", "s", "m")

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val (rules, items) = readTaskLines("")
            .parseTask()

        val startRule = rules["in"]!!

        items
            .filter { checkRules(it, startRule, rules) }
            .sumOf { it.total }
            .alsoPrint()
    }

    override fun taskTwo() {
        val (rules, _) = readTaskLines("")
            .parseTask()

        val startRule = rules["in"]!!

        flatten(startRule, emptyList(), rules)
            .filter { it.target == "A" }
            .sumOf { mergeAllConditions(1..4000, it.conditions).conditionSize() }
            .alsoPrint()
    }

    private fun Map<String, IntRange>.conditionSize(): Long = groups
        .map { this[it]!!.width() }
        .reduce { acc, l -> acc * l }

    private fun IntRange.adjustBy(condition: Condition): IntRange {
        val min = this.first.coerceAtLeast(condition.min)
        val max = this.last.coerceAtMost(condition.max)
        return min..max
    }

    private fun mergeAllConditions(initialRange: IntRange, conditions: List<Condition>): Map<String, IntRange> {
        return groups.associateWith { mergeConditions(it, initialRange, conditions) }
    }

    private fun mergeConditions(
        group: String,
        initialRange: IntRange,
        conditions: List<Condition>
    ): IntRange {
        return mergeConditions(initialRange, conditions.filter { it.property == group })
    }

    private fun mergeConditions(initialRange: IntRange, conditions: List<Condition>): IntRange {
        return conditions.fold(initialRange) { acc, c -> acc.adjustBy(c) }
    }

    private fun flatten(ruleLine: RuleLine, preconditions: List<Condition>, rules: Map<String, RuleLine>): List<Rule> {
        return ruleLine.rules.flatMap { flatten(it, preconditions, rules) }
    }

    private fun flatten(rule: Rule, preconditions: List<Condition>, rules: Map<String, RuleLine>): List<Rule> {
        val combinedPreconditions = preconditions.plus(rule.conditions)
        if (rule.isTerminal) return listOf(rule.copy(conditions = combinedPreconditions))

        val subs = rules[rule.target]!!
        return flatten(subs, combinedPreconditions, rules)
    }


    private fun checkRules(item: Item, rule: RuleLine, rules: Map<String, RuleLine>): Boolean {
        var currentRule = rule
        while (true) {
            val nextTarget = currentRule.findTarget(item)
            if (nextTarget == "A") return true
            if (nextTarget == "R") return false
            currentRule = rules[nextTarget]!!
        }
    }

    private fun List<String>.parseTask(): Pair<Map<String, RuleLine>, List<Item>> = Pair(
        this.takeWhile { it.isNotEmpty() }.map { it.parseRules() }.associateBy { it.name },
        this.takeLastWhile { it.isNotEmpty() }.map { it.parseItem() }
    )

    private fun String.parseItem(): Item =
        this
            .drop(1)
            .dropLast(1)
            .split(",")
            .associate { part -> part.split("=").let { it[0] to it[1].toInt() } }
            .let {
                Item(it)
            }


    private fun String.parseRules(): RuleLine {
        val name = this.takeWhile { it != '{' }
        val rulesString = this.dropWhile { it != '{' }.drop(1).dropLast(1)
        val ruleParts = rulesString
            .split(",")
            .map {
                it.parseRule()
            }
        var preconditions = emptyList<Condition>()
        val rules = mutableListOf<Rule>()

        ruleParts.forEach { (condition, target) ->
            val ruleList = if (condition == null) preconditions else preconditions.plus(condition)
            rules.add(Rule(ruleList, target))
            if (condition != null) preconditions = preconditions.plus(condition.invert())
        }
        return RuleLine(name, rules)
    }

    private fun String.parseRule(): Pair<Condition?, String> {
        val parts = this.split(":")
        val target = parts.last()
        if (parts.size < 2) return (null to target)

        return parts[0].parseCondition() to target
    }

    private fun String.parseCondition(): Condition = Condition(
        this.take(1),
        this[1],
        this.drop(2).toInt()
    )

    private fun Char.compareValues(left: Int, right: Int): Boolean = when (this) {
        '<' -> left < right
        '>' -> left > right
        else -> error("Not supported comparison: '$this'")
    }

    private data class Condition(val property: String, val test: Char, val value: Int) {
        val min = if (test == '>') value + 1 else Int.MIN_VALUE
        val max = if (test == '<') value - 1 else Int.MAX_VALUE

        fun check(item: Item): Boolean {
            if (property.isEmpty()) return true
            return test.compareValues(item[property], value)
        }

        fun invert(): Condition = when (test) {
            '>' -> Condition(property, '<', value + 1)
            '<' -> Condition(property, '>', value - 1)
            else -> this
        }
    }

    private data class Rule(val conditions: List<Condition>, val target: String) {
        val isTerminal = (target == "R" || target == "A")
    }

    private data class RuleLine(val name: String, val rules: List<Rule>) {
        fun findTarget(item: Item): String {
            return rules.find { rule -> rule.conditions.all { condition -> condition.check(item) } }!!.target
        }
    }

    private data class Item(private val values: Map<String, Int>) {
        val total = values.values.sum()
        operator fun get(key: String): Int = values[key]!!
    }
}
