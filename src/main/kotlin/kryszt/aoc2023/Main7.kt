package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main7 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines("")
            .map { line -> line.parseHand { it.countCards() } }
            .sortedWith(HandComparator(BasicCardComparator))
            .mapIndexed { index, hand -> hand.bid * (index + 1) }
            .sum()
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines("")
            .map { line -> line.parseHand { it.countCardsWithJokers() } }
            .sortedWith(HandComparator(JokerCardComparator))
            .mapIndexed { index, hand -> hand.bid * (index + 1) }
            .sum()
            .alsoPrint()
    }

    private fun findType(counts: List<Int>): HandType {
        return when {
            counts.contains(5) -> HandType.Five
            counts.contains(4) -> HandType.Four
            counts.contains(3) && counts.contains(2) -> HandType.FullHouse
            counts.contains(3) -> HandType.Three
            counts.count { it == 2 } == 2 -> HandType.TwoPairs
            counts.contains(2) -> HandType.Pair
            else -> HandType.HighCard
        }
    }

    private fun String.countCards(): List<Int> = this.groupBy { it }.map { it.value.size }
    private fun String.countCardsWithJokers(): List<Int> {
        val jokerCount = this.count { it == 'J' }
        val numbers = this.groupBy { it }.mapValues { it.value.size }.minus('J').map { it.value }

        if (jokerCount == 0) return numbers
        if (numbers.isEmpty()) return listOf(jokerCount)

        val max = numbers.max()
        return numbers.minus(max).plus(max + jokerCount)
    }

    private fun String.parseHand(counting: (String) -> List<Int>): Hand {
        val (cards, bidString) = this.split(" ")
        return Hand(
            cards = cards,
            type = findType(counting(cards)),
            bid = bidString.toInt()
        )
    }

    private data class Hand(val cards: String, val type: HandType, val bid: Int)

    private enum class HandType(val rank: Int) {
        Five(7),
        Four(6),
        FullHouse(5),
        Three(4),
        TwoPairs(3),
        Pair(2),
        HighCard(1),
    }

    private object HandTypeComparator : Comparator<HandType> {
        override fun compare(o1: HandType, o2: HandType): Int {
            return o1.rank - o2.rank
        }
    }

    private object BasicCardComparator : CardComparator(cardValues)
    private object JokerCardComparator : CardComparator(cardValuesWithJoker)

    private open class CardComparator(private val cardValues: Map<Char, Int>) : Comparator<Char> {
        override fun compare(o1: Char, o2: Char): Int {
            val o1Value = cardValues[o1] ?: error("Not a card: $o1")
            val o2Value = cardValues[o2] ?: error("Not a card: $o2")
            return o1Value.compareTo(o2Value)
        }
    }

    private class HandComparator(private val cardComparator: CardComparator) : Comparator<Hand> {
        override fun compare(o1: Hand, o2: Hand): Int {
            val typeComparison = HandTypeComparator.compare(o1.type, o2.type)
            if (typeComparison != 0) return typeComparison
            o1.cards.forEachIndexed { index, c ->
                val comparison = cardComparator.compare(c, o2.cards[index])
                if (comparison != 0) return comparison
            }
            return 0
        }
    }

    private val cardValues: Map<Char, Int> =
        "AKQJT98765432".cardPowers()

    private val cardValuesWithJoker: Map<Char, Int> =
        "AKQT98765432J".cardPowers()

    private fun String.cardPowers(): Map<Char, Int> =
        this
            .reversed()
            .mapIndexed { index, c -> c to index }
            .toMap()

}