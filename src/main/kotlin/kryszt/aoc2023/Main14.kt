package kryszt.aoc2023

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines
import kotlin.math.abs

object Main14 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines("")
            .readTask()
            .tilt(Point.Up)
            .totalLoadNorth()
            .alsoPrint()
    }

    override fun taskTwo() {
        val startTask = readTaskLines("").readTask()
        val repeats = 1_000_000_000
        val seenStates = mutableMapOf(
            startTask.rockStateString() to TaskValues(0, startTask.totalLoadNorth())
        )
        var currentState = startTask
        repeat(repeats) { index ->
            currentState = currentState.rotate()
            val rotation = index + 1
            val state = currentState.rockStateString()
            val lastSeen = seenStates[state]
            if (lastSeen != null) {
                val resultIndex = findIndex(repeats, lastSeen.round, rotation)
                seenStates.values.find { it.round == resultIndex }?.load?.alsoPrint()
                return
            } else {
                seenStates[state] = TaskValues(rotation, currentState.totalLoadNorth())
            }
        }
    }

    @Suppress("SameParameterValue")
    private fun findIndex(total: Int, first: Int, next: Int): Int {
        val diff = abs(next - first)
        return (total - first).mod(diff).plus(first)
    }

    private data class TaskValues(val round: Int, val load: Int)

    private fun List<String>.readTask(): Task {
        val large = mutableSetOf<Point>()
        val small = mutableListOf<Point>()

        this.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                when (c) {
                    '#' -> large.add(Point(x, y))
                    'O' -> small.add(Point(x, y))
                }
            }
        }
        return Task(large, small, Rectangle(0, 0, this.first().length, this.size))
    }

    private fun Task.rotate(): Task {
        var result = this
        rotationOrder.forEach { direction ->
            result = result.tilt(direction)
        }
        return result
    }

    private fun Task.tilt(tiltDirection: Point): Task {
        val smallSorted = this.smallRocks.sortForTilt(tiltDirection)
        val movedSmall = mutableSetOf<Point>()

        smallSorted.forEach {
            movedSmall.add(it.move(tiltDirection, this.largeRocks, movedSmall, this.area))
        }
        return this.copy(smallRocks = movedSmall.toList())
    }

    private fun Point.move(direction: Point, large: Set<Point>, small: Set<Point>, area: Rectangle): Point {
        var space = this
        while (true) {
            val moved = space + direction
            if (!area.contains(moved) || large.contains(moved) || small.contains(moved)) {
                return space
            } else {
                space = moved
            }
        }
    }

    private fun Task.totalLoadNorth(): Int =
        smallRocks
            .sumOf { this.area.height - it.y }

    private data class Task(val largeRocks: Set<Point>, val smallRocks: List<Point>, val area: Rectangle)

    private fun Task.rockStateString(): String =
        smallRocks.map { 100 * it.x + it.y }.sorted().joinToString(separator = ",")

    private fun List<Point>.sortForTilt(tilt: Point): List<Point> = when (tilt) {
        Point.Up -> this.sortedBy(Point::y)
        Point.Down -> this.sortedByDescending(Point::y)
        Point.Left -> this.sortedBy(Point::x)
        Point.Right -> this.sortedByDescending(Point::x)
        else -> error("Only main directions are supported, requested: $tilt")
    }

    private val rotationOrder = listOf(Point.Up, Point.Left, Point.Down, Point.Right)
}