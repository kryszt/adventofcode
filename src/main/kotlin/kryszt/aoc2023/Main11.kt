package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.PointLong
import kryszt.tools.alsoPrint
import kotlin.math.abs

object Main11 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solve(2)
    }

    override fun taskTwo() {
        solve(1_000_000)
    }

    private fun solve(multiplier: Long) {
        val image = readTaskLines("")
        val galaxies = image.galaxies()
        val emptyRows = image.emptyRows()
        val emptyColumns = image.emptyColumns()
        val expanded = galaxies.expand(emptyRows, emptyColumns, multiplier - 1)
        expanded.makePairs()
            .sumOf { it.distance() }
            .alsoPrint()
    }

    private fun List<String>.galaxies(): List<PointLong> {
        val galaxies = mutableListOf<PointLong>()
        forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                if (c == '#') galaxies.add(PointLong(x, y))
            }
        }
        return galaxies
    }

    private fun List<String>.emptyRows(): List<Int> = this.indices.filter { y -> this[y].all { it == '.' } }

    private fun List<String>.emptyColumns(): List<Int> {
        val columns = this[0].indices
        return columns.filter { x ->
            this.all { it[x] == '.' }
        }
    }

    private fun List<PointLong>.expand(
        emptyRows: List<Int>,
        emptyColumns: List<Int>,
        multiplier: Long
    ): List<PointLong> =
        this.map { galaxy ->
            val emptyLeft = emptyColumns.count { it < galaxy.x }
            val emptyAbove = emptyRows.count { it < galaxy.y }
            galaxy.move(emptyLeft * multiplier, emptyAbove * multiplier)
        }

    private fun List<PointLong>.makePairs(): List<Pair<PointLong, PointLong>> {
        val result = mutableListOf<Pair<PointLong, PointLong>>()
        this.forEachIndexed { i, galaxy ->
            for (j in (i + 1)..this.lastIndex) {
                val other = this[j]
                result.add(galaxy to other)
            }
        }
        return result
    }

    private fun Pair<PointLong, PointLong>.distance(): Long = abs(first.x - second.x) + abs(first.y - second.y)

}