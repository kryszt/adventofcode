package kryszt.aoc2023

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines

object Main6 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines("")
            .read()
            .map { it.countWinning() }
            .alsoPrint()
            .multiply()
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines("")
            .readAsOne()
            .alsoPrint()
            .countWinning().alsoPrint()
    }

    private fun List<String>.read(): List<Race> {
        val times = this[0].readNumbers()
        val records = this[1].readNumbers()
        check(times.size == records.size) { "Times and records must have the same size" }
        return times.mapIndexed { index, time ->
            Race(time, records[index])
        }
    }

    private fun List<String>.readAsOne(): Race = Race(
        time = this[0].readSingleNumber(),
        record = this[1].readSingleNumber()
    )

    private fun String.readNumbers(): List<Long> =
        this.dropWhile { it != ' ' }.trim().split("\\s+".toRegex()).map { it.toLong() }

    private fun String.readSingleNumber(): Long =
        this.dropWhile { it != ' ' }.trim().replace("\\s+".toRegex(), "").toLong()

    private data class Race(val time: Long, val record: Long)

    private fun Race.countWinning(): Long {
        return if (this.time.isEven()) {
            val max = (this.time / 2).sqr()
            val margin = max - this.record
            val marginCount = margin.sqrt()
            2 * marginCount + 1
        } else {
            val max = (this.time.plus(1).div(2) * this.time.div(2))
            val margin = max - this.record
            val marginCount = margin.sqrt()
            if (marginCount * (marginCount + 1) > margin) {
                2 * marginCount
            } else {
                2 * marginCount + 2
            }
        }
    }
}