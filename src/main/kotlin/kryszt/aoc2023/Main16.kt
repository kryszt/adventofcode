package kryszt.aoc2023

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines

object Main16 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val board = readTaskLines("")
            .readBoard()

        countEnergized(Ray(Point(0, 0), Direction.Right), board)
            .alsoPrint()
    }

    override fun taskTwo() {
        val board = readTaskLines("")
            .readBoard()
        val startRays = board.makeAllStartRays()

        startRays
            .maxOf { countEnergized(it, board) }
            .alsoPrint()
    }

    private fun Board.makeAllStartRays(): List<Ray> {
        val rays = mutableListOf<Ray>()
        (size.left..size.right).forEach { x ->
            //top down
            rays.add(Ray(Point(x, 0), Direction.Down))
            //bottom up
            rays.add(Ray(Point(x, size.bottom), Direction.Up))
        }

        (size.top..size.bottom).forEach { y ->
            //left to right
            rays.add(Ray(Point(0, y), Direction.Right))
            //right to left
            rays.add(Ray(Point(size.right, y), Direction.Left))
        }
        return rays
    }

    private fun countEnergized(first: Ray, board: Board): Int {
        var rays = listOf(first)
        val visited = mutableSetOf<Ray>()

        var turn = 0
        while (rays.isNotEmpty()) {
            turn++
            visited.addAll(rays)
            rays = rays
                .flatMap { it.execute(board) }
                .map { it.move() }
                .distinct()
                .filter { board.size.contains(it.position) }
                .filterNot { visited.contains(it) }
        }

        return visited
            .map { it.position }
            .toSet()
            .size
    }

    private fun List<String>.readBoard(): Board {
        val objects: Map<Point, Char> = flatMapIndexed { y, line ->
            line.mapIndexedNotNull { x, c ->
                if (c != '.') Point(x, y) to c else null
            }
        }.toMap()

        val size = Rectangle(x = 0, y = 0, width = this.first().length, height = this.size)
        return Board(objects, size)
    }

    private data class Board(val objects: Map<Point, Char>, val size: Rectangle)

    private data class Ray(val position: Point, val direction: Direction) {
        fun move(): Ray = copy(position = position + direction)

        fun execute(board: Board): List<Ray> {
            if (!board.size.contains(position)) return emptyList()
            return execute(board.objects.getOrDefault(position, '.'))
        }

        fun execute(type: Char): List<Ray> = when (type) {
            '/', '\\' -> listOf(mirror(type))
            '|', '-' -> splitter(type)
            '.' -> listOf(this)
            else -> error("Not supported space: '$type'")
        }

        fun mirror(type: Char): Ray = copy(direction = direction.mirror(type))
        fun splitter(type: Char): List<Ray> {
            return if (direction.splitsAt(type)) {
                if (type == '|') listOf(copy(direction = Point.Up), copy(direction = Point.Down))
                else listOf(copy(direction = Point.Left), copy(direction = Point.Right))
            } else {
                listOf(this)
            }
        }
    }

    private fun Direction.splitsAt(type: Char): Boolean =
        (isHorizontal() && type == '|')
                || (isVertical() && type == '-')

    fun Direction.mirror(type: Char): Direction = when (type) {
        '\\' -> Direction(this.y, this.x)
        '/' -> Direction(-this.y, -this.x)
        else -> error("Not a mirror: '$type'")
    }

    private fun Direction.isHorizontal(): Boolean = x != 0 && y == 0
    private fun Direction.isVertical(): Boolean = x == 0 && y != 0
}

private typealias Direction = Point