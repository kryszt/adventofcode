package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import kryszt.tools.lcm
import java.util.*

object Main20 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val board = readTaskLines("")
            .parseBoard()
        board.signal(Signal.LOW, 1000)
            .alsoPrint()
            .score()
            .alsoPrint()
    }

    override fun taskTwo() {
        val board = readTaskLines("")
            .parseBoard()
        val multipliers = evaluateFlips(board)
        val endParts = findParts(board)

        endParts
            .map { it.sumOf { flip -> multipliers[flip.name]!! }.toLong() }
            .lcm()
            .alsoPrint()
    }

    private fun findParts(board: Board): List<List<FlipFlop>> {
        return board
            .items
            .values.find { it.outputs.contains("rx") }
            ?.inputs
            ?.map { board.items[it]!!.inputs.also { inputs -> require(inputs.size == 1) }.first() }
            ?.map { board.getFlips(board.items[it]!!.inputs) }
            ?: error("No output to 'rx'")
    }

    private fun evaluateFlips(board: Board): Map<String, Int> {
        var evaluating: List<FlipFlop> = board.getFlips(board.items[BROADCASTER]!!.outputs)
        val multipliers = mutableMapOf<String, Int>()
        var multiplier = 1
        while (evaluating.isNotEmpty()) {
            multipliers.putAll(evaluating.map { it.name to multiplier })
            multiplier *= 2
            evaluating = board.getFlips(evaluating.flatMap { it.outputs })
        }
        return multipliers
    }

    private fun Board.getFlips(ids: List<String>): List<FlipFlop> =
        ids
            .mapNotNull { this.items[it] }
            .filterIsInstance<FlipFlop>()

    private fun List<String>.parseBoard(): Board {
        val flips = mutableMapOf<String, List<String>>()
        val conjunctions = mutableMapOf<String, List<String>>()
        lateinit var broadcastTargets: List<String>

        this.forEach { line ->

            val name = line.parseName()
            val targets = line.parseTargets()
            when {
                line.startsWith('%') -> flips[name] = targets
                line.startsWith('&') -> conjunctions[name] = targets
                name == BROADCASTER -> broadcastTargets = targets
                else -> error("Unknown type: $line")
            }
        }
        val allTypes = mutableMapOf<String, List<String>>().apply {
            putAll(flips)
            putAll(conjunctions)
        }

        val flipObjects = flips
            .mapValues { flip ->
                FlipFlop(flip.key, flip.value, allTypes.findInputs(flip.key))
            }
        val conjunctionObjects = conjunctions
            .mapValues { e ->
                Conjunction(e.key, e.value, allTypes.findInputs(e.key))
            }

        return Board(
            flipObjects
                .plus(conjunctionObjects)
                .plus(
                    BROADCASTER to Broadcast(broadcastTargets)
                )
        )
    }

    private fun Map<String, List<String>>.findInputs(target: String): List<String> =
        this.entries.filter { e -> e.value.contains(target) }.map { it.key }

    private fun String.parseName(): String = this
        .dropWhile { it == '%' || it == '&' }
        .takeWhile { it != ' ' }

    private fun String.parseTargets(): List<String> = this
        .takeLastWhile { it != '>' }
        .split(",")
        .map { it.trim() }

    private interface Item {
        val name: String
        val outputs: List<String>
        val inputs: List<String>
        fun onPulse(pulse: Pulse): List<Pulse>

        fun makePulses(signal: Signal): List<Pulse> {
            return outputs.map { Pulse(name, signal, it) }
        }
    }

    private data class FlipFlop(
        override val name: String,
        override val outputs: List<String>,
        override val inputs: List<String>
    ) : Item {
        var isOn = false
            private set

        override fun onPulse(pulse: Pulse): List<Pulse> {
            if (pulse.signal.isHigh()) return emptyList()

            isOn = !isOn
            val outSignal = if (isOn) Signal.HIGH else Signal.LOW
            return makePulses(outSignal)
        }
    }

    private data class Conjunction(
        override val name: String,
        override val outputs: List<String>,
        override val inputs: List<String>
    ) :
        Item {
        private val inputValues = mutableMapOf<String, Signal>().apply {
            putAll(inputs.map { it to Signal.LOW })
        }

        override fun onPulse(pulse: Pulse): List<Pulse> {
            require(inputValues.contains(pulse.source)) { "Not connected source: ${pulse.source}" }
            inputValues[pulse.source] = pulse.signal

            val allHigh = inputValues.values.all { it.isHigh() }
            val outSignal = if (allHigh) Signal.LOW else Signal.HIGH
            return makePulses(outSignal)
        }
    }

    private data class Broadcast(override val outputs: List<String>) : Item {
        override val name: String = BROADCASTER
        override val inputs: List<String> = emptyList()

        override fun onPulse(pulse: Pulse): List<Pulse> = makePulses(pulse.signal)
    }

    private data class Pulse(val source: String, val signal: Signal, val target: String)

    private enum class Signal {
        LOW, HIGH
    }

    private class Board(val items: Map<String, Item>) {

        fun signal(
            signal: Signal = Signal.LOW,
            repeats: Int = 1,
            interrupt: (Pulse) -> Boolean = { false }
        ): Map<Signal, Long> {
            var lowCount = 0L
            var highCount = 0L

            repeat(repeats) { step ->
                val pulses = LinkedList<Pulse>().also { it.add(Pulse("", signal, BROADCASTER)) }
                while (pulses.isNotEmpty()) {
                    val pulse = pulses.pop().also {
                        if (it.signal.isHigh()) highCount++
                        else lowCount++
                    }
                    if (interrupt(pulse)) {
                        println(step)
                        return emptyMap()
                    }

                    items[pulse.target]?.onPulse(pulse)?.also {
                        pulses.addAll(it)
                    }
                }
            }
            return mapOf(
                Signal.LOW to lowCount,
                Signal.HIGH to highCount,
            )
        }
    }

    private fun Map<Signal, Long>.score(): Long = getOrDefault(Signal.LOW, 0) * getOrDefault(Signal.HIGH, 0)
    private fun Signal.isHigh() = this == Signal.HIGH

    private const val BROADCASTER = "broadcaster"
}