package kryszt.aoc2023

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines

object Main21 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val garden = readTaskLines("").readGarden()
        solve(garden, 64)[64].alsoPrint()
    }

    override fun taskTwo() {
        val steps = 26501365
        val garden = readTaskLines("").readGarden().unlimited()
        val size = garden.area.width
        val initialSteps = steps.mod(size)
        val multiplier: Long = (steps / size).toLong()

        val results = solve(garden, size * 2 + initialSteps)
        val x0 = results[initialSteps]!!
        val x1 = results[initialSteps + size]!!
        val x2 = results[initialSteps + 2 * size]!!
        val step2 = (x2 - x1) - (x1 - x0)
        val step1 = (x1 - x0) - step2

        val total = x0 + step1.times(multiplier) + (step2 * (multiplier + 1) * multiplier) / 2
        println(total)
    }

    private fun solve(garden: Garden, maxSteps: Int, start: Point = garden.start): Map<Int, Int> {
        val counters = mutableMapOf(0 to 0)
        var current = listOf(start)
        repeat(maxSteps) { step ->
            current = current.flatMap { it.crossNeighbours() }.distinct()
                .filter { garden.inArea(it) && !garden.isRock(it) }
            counters[step + 1] = current.size
        }
        return counters
    }

    private fun List<String>.readGarden(): Garden {
        val rocks = mutableSetOf<Point>()
        lateinit var start: Point
        forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                if (c == '#') rocks.add(Point(x, y))
                if (c == 'S') start = Point(x, y)
            }
        }

        val area = Rectangle(x = 0, y = 0, width = this.first().length, height = this.size)
        return Garden(rocks, start, area)
    }

    private data class Garden(
        val rocks: Set<Point>,
        val start: Point,
        val area: Rectangle,
        val limited: Boolean = true
    ) {
        fun unlimited(): Garden = copy(limited = false)

        fun inArea(point: Point): Boolean = !limited || area.contains(point)
        fun isRock(point: Point): Boolean = rocks.contains(adjust(point))

        private fun adjust(point: Point): Point =
            if (limited) point else Point(point.x.mod(area.width), point.y.mod(area.height))
    }
}