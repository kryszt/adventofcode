package kryszt.aoc2023

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines
import java.math.BigInteger
import kotlin.math.sign

object Main24 : DayChallenge {

    private val testArea = 7.0..27.0
    private val realArea = 200000000000000.0..400000000000000.0

    @Suppress("SameParameterValue")
    private fun readArea(type: String?) = when (type) {
        "-test" -> testArea
        else -> realArea
    }

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val testType = ""
        val range = readArea(testType)

        readTaskLines(testType)
            .map { it.parseLine() }
            .allPairs()
            .count {
                val cross = crossPointInFuture(it.first, it.second)
                cross != null && cross.x in range && cross.y in range
            }
            .alsoPrint()
    }

    override fun taskTwo() {
        val lines = readTaskLines("").map { it.parseLine() }
        makeMatrixLines(lines[0], lines[1])
            .plus(makeMatrixLines(lines[0], lines[2]))
            .makeTriangular()
            .makeDiagonal()
            .makePositiveAll()
            .take(3).sumOf { it.last().longValueExact() }
            .alsoPrint()
    }

    private fun makeMatrixLines(l1: Line, l2: Line): List<List<BigInteger>> {
        return listOf(
            listOf(
                //(dy'-dy) X + (dx-dx') Y
                l2.dy - l1.dy, l1.dx - l2.dx, 0,
                // + (y-y') DX + (x'-x) DY
                l1.y - l2.y, l2.x - l1.x, 0,
                // = x' dy' - y' dx' - x dy + y dx
                (l2.x * l2.dy) - (l2.y * l2.dx) - (l1.x * l1.dy) + (l1.y * l1.dx)
            ).map { it.toBigInteger() },

            listOf(
                //(dz'-dz) X + (dx-dx') Z
                l2.dz - l1.dz, 0, l1.dx - l2.dx,
                // + (z-z') DX + (x'-x) DZ
                l1.z - l2.z, 0, l2.x - l1.x,
                // = x' dz' - z' dx' - x dz + z dx
                (l2.x * l2.dz) - (l2.z * l2.dx) - (l1.x * l1.dz) + (l1.z * l1.dx)
            ).map { it.toBigInteger() },
            listOf(
                //(dz-dz') Y + (dy'-dy) Z
                0, l1.dz - l2.dz, l2.dy - l1.dy,
                // + (z'-z) DY + (y-y') DZ
                0, l2.z - l1.z, l1.y - l2.y,
                // = z' dy' - y' dz' - z dy + y dz
                (l2.z * l2.dy) - (l2.y * l2.dz) - (l1.z * l1.dy) + (l1.y * l1.dz)
            ).map { it.toBigInteger() }
        )
    }

    private fun List<List<BigInteger>>.normalizeAll(): List<List<BigInteger>> = map { it.normalize() }
    private fun List<List<BigInteger>>.makePositiveAll(): List<List<BigInteger>> = map { it.makePositive() }

    private fun List<BigInteger>.normalize(): List<BigInteger> {
        val commonDivider = this.gcdBI().takeUnless { it == BigInteger.ZERO } ?: return this
        return this.map { it / commonDivider }
    }

    private fun List<BigInteger>.makePositive(): List<BigInteger> =
        this.map { if (it == BigInteger.ZERO) BigInteger.ZERO else it * (it / it.abs()) }

    private fun List<List<BigInteger>>.sortAbsoluteByPosition(position: Int): List<List<BigInteger>> =
        this.sortedBy { it[position].abs() }

    private fun List<List<BigInteger>>.makeTriangular(): List<List<BigInteger>> {
        var matrix = this.normalizeAll().sortByLeadingZeros()
        for (i in 0..<this.lastIndex) {
            matrix = matrix
                .sortAbsoluteByPosition(i)
                .sortByLeadingZeros()
                .zeroFirstItem(i)
                .normalizeAll()
        }
        return matrix.sortByLeadingZeros()
    }

    private fun List<List<BigInteger>>.makeDiagonal(): List<List<BigInteger>> {
        var matrix = this.normalizeAll().sortByLeadingZeros()
        for (i in (1..this.lastIndex).reversed()) {
            matrix = matrix
                .zeroColumn(i)
                .normalizeAll()
        }
        return matrix.normalizeAll()
    }

    private fun List<List<BigInteger>>.zeroFirstItem(row: Int): List<List<BigInteger>> {
        val unchanged = this.take(row + 1)
        val base = unchanged.last()

        val zeroed = this.drop(row + 1)
            .map { it.zeroFirstItem(base) }

        return unchanged + zeroed
    }

    private fun List<BigInteger>.zeroFirstItem(base: List<BigInteger>): List<BigInteger> {
        require(this.size == base.size) { "Lists must have the same length" }

        val rIndex = base.indexOfFirst { it != BigInteger.ZERO }.takeUnless { it < 0 } ?: base.size
        val xIndex = this.indexOfFirst { it != BigInteger.ZERO }.takeUnless { it < 0 } ?: this.size
        if (xIndex < rIndex) error("$base will not zero first item from $this")
        if (xIndex > rIndex) return this //already 0 at position

        val target = this[xIndex]
        val removing = base[rIndex]
        val common = target.abs().lcm(removing.abs())
        val tMultiplier = common / target
        val sMultiplier = common / removing

        return this.mapIndexed { index, l -> tMultiplier * l - sMultiplier * base[index] }
    }

    private fun List<List<BigInteger>>.zeroColumn(column: Int): List<List<BigInteger>> {
        val unchanged = this.drop(column)
        val base = this[column]

        val zeroed = this.take(column)
            .map { it.zeroColumn(base, column) }

        return zeroed + unchanged
    }

    private fun List<BigInteger>.zeroColumn(base: List<BigInteger>, column: Int): List<BigInteger> {
        require(this.size == base.size) { "Lists must have the same length" }
        if (this[column] == BigInteger.ZERO) return this

        val target = this[column]
        val removing = base[column]
        val common = target.abs().lcm(removing.abs())
        val tMultiplier = common / target
        val sMultiplier = common / removing

        return this.mapIndexed { index, l -> tMultiplier * l - sMultiplier * base[index] }
    }

    private fun List<List<BigInteger>>.sortByLeadingZeros(): List<List<BigInteger>> =
        this.sortedBy { it.leadingZeros() }

    private fun List<BigInteger>.leadingZeros(): Int =
        this.indexOfFirst { it != BigInteger.ZERO }.takeUnless { it < 0 } ?: this.size

    private fun String.parseLine(): Line =
        this.split("@").map { it.parsePoint() }
            .let { (start, direction) -> Line(start, direction) }

    private fun String.parsePoint(): Point3DLong =
        this
            .trim()
            .split(",")
            .map { it.trim().toLong() }
            .let { (x: Long, y: Long, z: Long) -> Point3DLong(x, y, z) }

    private fun crossPoint(line1: Line, line2: Line): PointDouble? {
        if (line1.isParallel(line2)) return null
        val x = (line2.by - line1.by) / (line1.ay - line2.ay)
        val y = x * line1.ay + line1.by
        return PointDouble(x, y)
    }

    private fun crossPointInFuture(line1: Line, line2: Line): PointDouble? {
        val cross = crossPoint(line1, line2) ?: return null
        if (!cross.x.isFinite()) return null
        if (!cross.y.isFinite()) return null
        if (!line1.isInFuture(cross)) return null
        if (!line2.isInFuture(cross)) return null

        return cross
    }

    data class Line(val start: Point3DLong, val direction: Point3DLong) {
        val ay: Double = direction.y.toDouble() / direction.x.toDouble()
        val by: Double = start.y.toDouble() - (ay * start.x.toDouble())

        val x = start.x
        val y = start.y
        val z = start.z
        val dx = direction.x
        val dy = direction.y
        val dz = direction.z
    }

    private fun Line.isInFuture(point: PointDouble): Boolean = (point.x - start.x).sign == direction.x.toDouble().sign
            && (point.y - start.y).sign == direction.y.toDouble().sign

    private fun Line.isParallel(other: Line): Boolean = this.ay == other.by
    private fun <T> List<T>.allPairs(): List<Pair<T, T>> {
        val pairs = mutableListOf<Pair<T, T>>()
        for (i in 0 until this.lastIndex) {
            for (j in i + 1..this.lastIndex) {
                pairs.add(this[i] to this[j])
            }
        }
        return pairs
    }

    private data class PointDouble(val x: Double, val y: Double)
}