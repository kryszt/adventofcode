package kryszt.aoc2023

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main13 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solve(0)
    }

    override fun taskTwo() {
        solve(1)
    }

    private fun solve(expectedMirrorDiff: Int) {
        readTaskLines("")
            .readReflections()
            .sumOf {
                it.findHorizontalReflections().indexOf(expectedMirrorDiff).plus(1).times(100) +
                        it.findVerticalReflections().indexOf(expectedMirrorDiff).plus(1)
            }.alsoPrint()
    }

    private data class Reflections(val rows: List<String>) {
        val columns: List<String> = rows.transform()
    }

    private fun Reflections.findHorizontalReflections(): List<Int> =
        (0..<rows.lastIndex).map { index -> reflectionErrors(index) { this.rows.getOrNull(it) } }

    private fun Reflections.findVerticalReflections(): List<Int> =
        (0..<columns.lastIndex).map { index -> reflectionErrors(index) { this.columns.getOrNull(it) } }

    private fun reflectionErrors(startIndex: Int, getLine: (Int) -> String?): Int {
        var reflectionDiff = 0
        var leftIndex = startIndex
        var rightIndex = startIndex + 1
        var left = getLine(leftIndex)
        var right = getLine(rightIndex)

        while (left != null && right != null) {
            reflectionDiff += left.diffCount(right)
            leftIndex--
            rightIndex++
            left = getLine(leftIndex)
            right = getLine(rightIndex)
        }
        return reflectionDiff
    }

    private fun List<String>.transform(): List<String> =
        this
            .first()
            .indices.map { x ->
                this
                    .joinToString(separator = "", transform = { it[x].toString() })
            }

    private fun List<String>.readReflections(): List<Reflections> {
        var chunk = mutableListOf<String>()
        val chunks = mutableListOf<List<String>>()

        this.forEach { line ->
            if (line.isEmpty()) {
                chunks.add(chunk)
                chunk = mutableListOf()
            } else {
                chunk.add(line)
            }
        }
        chunks.add(chunk)
        return chunks.map { Reflections(it) }
    }

    private fun String.diffCount(other: String): Int {
        check(this.length == other.length) { "Strings must be the same length" }
        return this.indices.count { this[it] != other[it] }
    }
}