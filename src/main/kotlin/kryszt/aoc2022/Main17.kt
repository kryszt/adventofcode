package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskFile
import kryszt.tools.alsoPrint

object Main17 : DayChallenge {

    private const val rocksCount = 2022L

    //first item is at the bottom, making
    private val horizontalLine = listOf(30)
    private val cross = listOf(8, 28, 8)
    private val LShape = listOf(28, 4, 4)
    private val verticalLine = listOf(16, 16, 16, 16)
    private val cube = listOf(24, 24)

    private val bricks = listOf(
        horizontalLine,
        cross,
        LShape,
        verticalLine,
        cube
    )

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        totalHeight(rocksCount).alsoPrint()
    }

    override fun taskTwo() {
        totalHeight(1_000_000_000_000L).alsoPrint()
    }

    private fun totalHeight(rocks: Long): Long {
        val windSource = WindSource(readTaskFile(""))
        val brickSource = BrickSource(bricks, rocks)
        val well = Well()

        val visited: MutableMap<State, StateValue> = mutableMapOf()

        while (brickSource.hasMore()) {
            val state = makeState(windSource, brickSource, well)
            val newStateValue = StateValue(well.height, brickSource.dropped)
            val previous = visited.put(state, newStateValue)
            if (previous != null) {
                return findResult(rocks, state, previous, newStateValue, visited)
            }
            val nextBrick = brickSource.next()
            drop(nextBrick, well, windSource)
        }
        return well.height
    }

    private fun findResult(
        drops: Long,
        currentState: State,
        lastFound: StateValue,
        newStateValue: StateValue,
        all: Map<State, StateValue>
    ): Long {
        println("found repeating $currentState -> $lastFound -> $newStateValue")
        val dh = newStateValue.height - lastFound.height
        val dDrops = newStateValue.dropped - lastFound.dropped

        val repeatedResult = drops.mod(dDrops)
        val repeatedHeight = all.values.find { it.dropped == repeatedResult }!!.height
        val multiplier = drops / dDrops
        return repeatedHeight + (multiplier * dh)
    }

    private fun makeState(windSource: WindSource, brickSource: BrickSource, well: Well): State {
        return State(
            brickIndex = brickSource.index,
            windIndex = windSource.index,
            lines = well.makeState()
        )
    }

    private fun drop(falling: FallingBrick, well: Well, windSource: WindSource) {
        var block = falling

        while (true) {
            //wind
            val wind = windSource.next()
            val windCandidate = block.takeIf { it.canMove(wind) }?.move(wind)?.takeIf { well.canOccupy(it) }
            if (windCandidate != null) {
                block = windCandidate
            }
            val candidate = block.moveDown()
            if (well.canOccupy(candidate)) {
                block = candidate
            } else {
                well.place(block)
                return
            }
        }
    }

    private class Well {
        private var taken: MutableList<Int> = mutableListOf(full)

        var height: Long = 0
            private set

        fun makeState(): List<Int> = taken.toList()

        fun canOccupy(fallingBrick: FallingBrick): Boolean {
            if (fallingBrick.bottom < 0) return true
            return (maxOf(fallingBrick.top, 0)..fallingBrick.bottom)
                .none {
                    fallingBrick.lineAt(it).intersects(taken[it])
                }
        }

        private fun Int.intersects(other: Int) = (this and other) != 0

        fun place(fallingBrick: FallingBrick) {
//          //combine with existing
            (maxOf(fallingBrick.top, 0)..fallingBrick.bottom).forEach { line ->
                taken[line] = taken[line] or fallingBrick.lineAt(line)
            }
            //add new on top
            for (line in -1 downTo fallingBrick.top) {
                height++
                taken.add(0, fallingBrick.lineAt(line))
            }
            taken = taken.takeRelevant()
        }

        private fun List<Int>.takeRelevant(): MutableList<Int> {
            var marked = 0
            return this.takeWhile {
                val isFull = marked != full
                marked = marked or it
                isFull
            }.toMutableList()
        }
    }

    private class WindSource(private val sequence: String) {
        var index: Int = 0
            private set

        fun next(): Char {
            val c = sequence[index]
            index = (index + 1).mod(sequence.length)
            return c
        }
    }

    private class BrickSource(private val sequence: List<List<Int>>, count: Long) {
        private var left = count
        var dropped: Long = 0
            private set
        var index: Int = 0
            private set

        fun hasMore(): Boolean = left > 0

        fun next(): FallingBrick {
            val fb = FallingBrick(-4, sequence[index], index)
            dropped++
            index = index.plus(1).mod(sequence.size)
            left--
            return fb
        }
    }

    private fun Char.isToLeft() = this == '<'

    private data class FallingBrick(val bottom: Int, val lines: List<Int>, val id: Int) {
        val top = bottom - lines.size + 1

        fun lineAt(level: Int): Int = lines[bottom - level]

        fun canMove(move: Char): Boolean = if (move.isToLeft()) canMoveLeft() else canMoveRight()
        fun canMoveLeft(): Boolean = lines.all { (it and left) == 0 }
        fun canMoveRight(): Boolean = lines.all { (it and right) == 0 }
        fun moveDown() = copy(bottom = bottom + 1)
        fun move(move: Char) = if (move.isToLeft()) moveLeft() else moveRight()
        fun moveLeft(): FallingBrick = copy(lines = lines.map { it.shl(1) })
        fun moveRight(): FallingBrick = copy(lines = lines.map { it.shr(1) })

    }

    private data class State(val brickIndex: Int, val windIndex: Int, val lines: List<Int>)
    private data class StateValue(val height: Long, val dropped: Long)

    private const val full: Int = 127
    private const val left: Int = 64
    private const val right: Int = 1

}
