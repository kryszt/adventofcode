package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskFile
import kryszt.tools.alsoPrint

object Main6 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        findMarker(readTaskFile(), 4).plus(4).alsoPrint()
    }

    override fun taskTwo() {
        findMarker(readTaskFile(), 14).plus(14).alsoPrint()
    }

    private fun findMarker(signal: String, size: Int): Int {
        for (i in 0..signal.length - size) {
            if (isMarker(i, signal, size)) return i
        }
        throw IllegalArgumentException("Marker not found")
    }

    private fun isMarker(index: Int, signal: String, size: Int): Boolean {
        for (i in index..index + (size - 2)) {
            for (j in (i + 1) until index + size) {
                if (signal[i] == signal[j]) return false
            }
        }
        return true
    }
}