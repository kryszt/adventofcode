package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main20 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solve(1, 1)
    }

    override fun taskTwo() {
        solve(811589153L, 10)
    }

    private fun solve(multiplier: Long, rounds: Int) {
        val message = readTaskLines("").map { it.toInt() }
        val s = Solver(message, multiplier)

        s.decrypt(rounds)
            .let {
                val node1k = it[1000.mod(it.size)]
                val node2k = it[2000.mod(it.size)]
                val node3k = it[3000.mod(it.size)]

                println("${node1k}, ${node2k}, $node3k")
                (node1k + node2k + node3k)
            }.alsoPrint()
    }

    private class Solver(private val lines: List<Int>, multiplier: Long) {
        private val mutableLines = lines
            .mapIndexed { index, i -> MappedNumber(index, multiplier * i.toLong()) }
            .toMutableList()

        fun decrypt(repeats: Int): List<Long> {
            repeat(repeats) {
                lines.indices.forEach(this::move)
            }
            return mutableLines.moveToFront { it.value == 0L }.map { it.value }
        }

        fun move(position: Int) {
            val index = mutableLines.indexOfFirst { it.originalIndex == position }
            val item = mutableLines.removeAt(index)
            val newIndex = (index + item.value).mod(mutableLines.size)
            mutableLines.add(newIndex, item)
        }

    }

    private data class MappedNumber(val originalIndex: Int, val value: Long)

    private fun <T> List<T>.moveToFront(predicate: (T) -> Boolean): List<T> {
        val index = indexOfFirst(predicate)
        return this.drop(index) + this.take(index)
    }
}