package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import kryszt.tools.parseWith
import java.util.Stack

object Main5 : DayChallenge {
    private val linePattern = "^move (\\d+) from (\\d+) to (\\d+)\$".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val lines = readTaskLines()
        val crates = lines.takeWhile { it.isNotBlank() }.let(this::parseCrates)
        val moves = lines.takeLastWhile { it.isNotBlank() }.map { it.parseMove() }
        moves.forEach(crates::moveByOne)
        crates.tops().joinToString("").alsoPrint()
    }

    override fun taskTwo() {
        val lines = readTaskLines()
        val crates = lines.takeWhile { it.isNotBlank() }.let(this::parseCrates)
        val moves = lines.takeLastWhile { it.isNotBlank() }.map { it.parseMove() }
        moves.forEach(crates::moveAtOnce)
        crates.tops().joinToString("").alsoPrint()
    }

    private data class Move(val count: Int, val from: Int, val to: Int)

    private fun parseCrates(lines: List<String>): Crates {
        val lastLine = lines.last()
        val indexes = mutableListOf<Int>()
        lastLine.forEachIndexed { index, c -> if (c.isDigit()) indexes.add(index) }

        val crates = Crates(indexes.size)

        lines
            .reversed()
            .drop(1)
            .forEach { line ->
                indexes.forEachIndexed { index, inLineIndex ->
                    val c = line.getOrNull(inLineIndex)
                    if (c != null && c.isLetter()) {
                        crates.put(index + 1, c)
                    }
                }
            }

        return crates
    }

    private class Crates(size: Int) {
        private val crates: List<Stack<Char>> = List(size) { Stack() }

        private fun crate(at: Int): Stack<Char> = crates[at - 1]

        fun put(into: Int, content: Char) {
            crate(into).push(content)
        }

        fun take(from: Int): Char = crate(from).pop()

        fun moveByOne(move: Move) {
            repeat(move.count) {
                put(move.to, take(move.from))
            }
        }

        fun moveAtOnce(move: Move) {
            val cache = mutableListOf<Char>()
            repeat(move.count) {
                cache.add(take(move.from))
            }
            cache
                .asReversed()
                .forEach { put(move.to, it) }
        }

        fun tops(): List<Char> = crates.map { it.peek() }

        override fun toString(): String = crates.joinToString("\n") { it.joinToString() }
    }


    private fun String.parseMove(): Move =
        this.parseWith(linePattern) {
            Move(it.group(1).toInt(), it.group(2).toInt(), it.group(3).toInt())
        }
}