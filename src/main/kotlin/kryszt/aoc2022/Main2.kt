package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main2: DayChallenge {
/*
A - Rock (1)
B - Paper (2)
C - Scissors (3)

Part 1:
X - Rock
Y - Paper
Z - Scissors

Part 2:
X - lose
Y - draw
Z - win
 */

    private val scores : Map<String,Int> = mapOf(
        "A X" to 3 + 1,
        "A Y" to 6 + 2,
        "A Z" to 0 + 3,
        "B X" to 0 + 1,
        "B Y" to 3 + 2,
        "B Z" to 6 + 3,
        "C X" to 6 + 1,
        "C Y" to 0 + 2,
        "C Z" to 3 + 3,
    )

    private val answers : Map<String,String> = mapOf(
        "A X" to "A Z",
        "A Y" to "A X",
        "A Z" to "A Y",
        "B X" to "B X",
        "B Y" to "B Y",
        "B Z" to "B Z",
        "C X" to "C Y",
        "C Y" to "C Z",
        "C Z" to "C X",
    )

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines()
            .sumOf { scores[it]!! }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines()
            .map { answers[it]!! }
            .sumOf { scores[it]!! }
            .alsoPrint()
    }
}