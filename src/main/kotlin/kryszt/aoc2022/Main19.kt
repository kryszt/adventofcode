package kryszt.aoc2022

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines
import java.util.*

object Main19 : DayChallenge {

    private val linePattern =
        "Blueprint (\\d+): Each ore robot costs (\\d+) ore. Each clay robot costs (\\d+) ore. Each obsidian robot costs (\\d+) ore and (\\d+) clay. Each geode robot costs (\\d+) ore and (\\d+) obsidian."
            .toPattern()

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val blueprints = readBlueprints()
        blueprints
            .sumOf {
                println("Checking ${it.id}")
                (it.id * quality(it, 24))
            }
            .alsoPrint()
    }

    override fun taskTwo() {
        val blueprints = readBlueprints()
        blueprints
            .take(3)
            .map {
                println("Checking ${it.id}")
                quality(it, 32)
            }
            .reduce(Int::times)
            .alsoPrint()
    }

    private fun quality(blueprint: Blueprint, time: Int): Int {
        var states = PriorityQueue<State>().apply { add(State(0, Cost(), Cost(ore = 1))) }
        var best = 0

        while (states.isNotEmpty()) {
            val state = states.poll()
            if (state.canImprove(best, time)) {
                val next = nextStates2(state, blueprint, time)
//                println("next: ${next.size}")
                best = findAndLogBest(best, next.maxOfOrNull { it.bank.geode } ?: 0) { newBest ->
                    val newQueue = PriorityQueue<State>()
                    states.filterTo(newQueue) { it.canImprove(newBest, time) }
                    states = newQueue
                }
                states.addAll(next.filter { it.canImprove(best, time) })
            }
        }
        return best
    }

    private fun findAndLogBest(oldBest: Int, candidate: Int, onBetter: (newBest: Int) -> Unit): Int {
        if (candidate > oldBest) {
            onBetter(candidate)
        }
        return maxOf(oldBest, candidate)
    }

    private fun State.canImprove(best: Int, time: Int): Boolean {
        val timeLeft = time - this.time
        val maxProd = (0 until timeLeft).sumOf { it + income.geode }
        return this.bank.geode + maxProd > best
    }

    private fun nextStates2(state: State, blueprint: Blueprint, maxTime: Int): List<State> {
        if (state.time == maxTime) return emptyList()
        val nextStates = mutableListOf<State>()

        blueprint
            .upgrades
            .forEach { upgrade ->
                if (state.income.ore >= blueprint.maxCosts.ore && upgrade.update.ore > 0) return@forEach
                if (state.income.clay >= blueprint.maxCosts.clay && upgrade.update.clay > 0) return@forEach
                if (state.income.obsidian >= blueprint.maxCosts.obsidian && upgrade.update.obsidian > 0) return@forEach

                val moveTurns = upgrade.cost.minus(state.bank).needTurns(state.income)?.plus(1)
                if (moveTurns != null && moveTurns + state.time <= maxTime) {
                    val nextState = State(
                        time = state.time + moveTurns,
                        bank = state.bank.plus(state.income.times(moveTurns)).minus(upgrade.cost),
                        income = state.income.plus(upgrade.update)
                    )
                    nextStates.add(nextState)
                }
            }

        val missing = maxTime - state.time
        if (nextStates.isEmpty()) {
            nextStates.add(
                State(
                    maxTime,
                    bank = state.bank.plus(state.income.times(missing)),
                    income = state.income
                )
            )
        }

        return nextStates
    }

    private fun readBlueprints(): List<Blueprint> =
        readTaskLines("")
            .parseWith(linePattern) {
                Blueprint(
                    id = it.intAt(1),
                    upgrades = listOf(
                        Blueprint.Entry(Cost(ore = it.intAt(2)), Cost(ore = 1)),
                        Blueprint.Entry(Cost(ore = it.intAt(3)), Cost(clay = 1)),
                        Blueprint.Entry(Cost(ore = it.intAt(4), clay = it.intAt(5)), Cost(obsidian = 1)),
                        Blueprint.Entry(Cost(ore = it.intAt(6), obsidian = it.intAt(7)), Cost(geode = 1)),
                    )
                )
            }

    private data class Blueprint(
        val id: Int,
        val upgrades: List<Entry>
    ) {
        data class Entry(val cost: Cost, val update: Cost)

        val maxCosts = Cost(
            ore = upgrades.maxOf { it.cost.ore },
            clay = upgrades.maxOf { it.cost.clay },
            obsidian = upgrades.maxOf { it.cost.obsidian },
        )
    }

    private data class State(
        val time: Int,
        val bank: Cost,
        val income: Cost,
    ) : Comparable<State> {
        override fun compareTo(other: State): Int = this.bank.geode.compareTo(other.bank.geode)
    }

    private data class Cost(val ore: Int = 0, val clay: Int = 0, val obsidian: Int = 0, val geode: Int = 0) {
        operator fun plus(other: Cost) =
            Cost(this.ore + other.ore, this.clay + other.clay, this.obsidian + other.obsidian, this.geode + other.geode)

        operator fun minus(other: Cost) =
            Cost(this.ore - other.ore, this.clay - other.clay, this.obsidian - other.obsidian, this.geode - other.geode)

        operator fun times(multiplier: Int) =
            Cost(this.ore * multiplier, this.clay * multiplier, this.obsidian * multiplier, this.geode * multiplier)

        fun needTurns(income: Cost): Int? {
            var max = -1
            max = newMax(max, income, Cost::ore) ?: return null
            max = newMax(max, income, Cost::clay) ?: return null
            max = newMax(max, income, Cost::obsidian) ?: return null
            return max.takeUnless { it < 0 }
        }

        private fun newMax(currentMax: Int, income: Cost, value: Cost.() -> Int): Int? {
            if (this.value() <= 0) return maxOf(0, currentMax)
            if (income.value() == 0) return null
            return maxOf(currentMax, this.value().roofDiv(income.value()))
        }
    }
}
