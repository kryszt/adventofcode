package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.Solver
import kryszt.tools.alsoPrint
import java.lang.IllegalStateException

object Main12 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val (map, start, end) = loadMap()
        val solver = HillSolver(map, endTest = { it == end }, canMoveTest = { from, to -> to - from < 2 })
        solver.solve(listOf(start))
        solver.stepsCount.alsoPrint()
    }

    override fun taskTwo() {
        val (map, _, end) = loadMap()
        val solver =
            HillSolver(map, endTest = { map.getOrNull(it) == 'a' }, canMoveTest = { from, to -> to - from > -2 })
        solver.solve(listOf(end))
        solver.stepsCount.alsoPrint()
    }

    private class HillSolver(
        private val map: Array<CharArray>,
        private val endTest: (Point) -> Boolean,
        private val canMoveTest: (Char, Char) -> Boolean,
    ) : Solver<Point>() {
        private val visited = mutableSetOf<Point>()
        override fun visiting(state: Point) {
            visited.add(state)
        }

        override fun wasVisited(state: Point): Boolean =
            visited.contains(state)

        override fun isEnd(state: Point): Boolean = endTest(state)

        override fun selectMoves(state: Point): Collection<Point> {
            val current = map.getOrNull(state) ?: return emptyList()

            return state
                .crossNeighbours()
                .filter { !wasVisited(it) && (map.test(it) { c -> canMoveTest(current, c) }) }
        }

        override fun allowed(state: Point): Boolean = true
    }

    private fun loadMap(): Triple<Array<CharArray>, Point, Point> {
        val lines = readTaskLines("")
        val map = lines
            .map { it.toCharArray() }
            .toTypedArray()

        val start = lines.find('S')
        val target = lines.find('E')

        map.set(start, 'a')
        map.set(target, 'z')

        return Triple(map, start, target)
    }

    private fun List<String>.find(c: Char): Point {
        this.forEachIndexed { y, s ->
            val x = s.indexOf(c)
            if (x >= 0) return Point(x, y)
        }
        throw IllegalStateException("Not found: $c")
    }

    private fun Array<CharArray>.getOrNull(point: Point): Char? =
        getOrNull(point.y)?.getOrNull(point.x)

    private fun Array<CharArray>.test(point: Point, check: (Char) -> Boolean): Boolean {
        return getOrNull(point.y)?.getOrNull(point.x)?.let(check) ?: false
    }

    private fun Array<CharArray>.set(point: Point, c: Char) {
        this[point.y][point.x] = c
    }
}