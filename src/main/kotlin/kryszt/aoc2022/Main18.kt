package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point3D
import kryszt.tools.alsoPrint

object Main18 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val counter = WallsCounter()
        readTaskLines("")
            .map { line -> line.split(",").let { Point3D(it[0].toInt(), it[1].toInt(), it[2].toInt()) } }
            .forEach(counter::addCube)
        counter.walls.alsoPrint()
    }

    override fun taskTwo() {
        val counter = WallsCounter()
        val cubes = readTaskLines("")
            .map { line -> line.split(",").let { Point3D(it[0].toInt(), it[1].toInt(), it[2].toInt()) } }
        cubes.forEach(counter::addCube)

        val closed = findClosed(counter)
        closed.forEach(counter::addCube)

        counter.walls.alsoPrint()
    }

    private fun findClosed(counter: WallsCounter): Collection<Point3D> {
        val cubes = counter.cubes
        val minX = cubes.minOf { it.x } - 1
        val maxX = cubes.maxOf { it.x } + 1
        val minY = cubes.minOf { it.y } - 1
        val maxY = cubes.maxOf { it.y } + 1
        val minZ = cubes.minOf { it.z } - 1
        val maxZ = cubes.maxOf { it.z } + 1
        val xRange = minX..maxX
        val yRange = minY..maxY
        val zRange = minZ..maxZ

        val start = Point3D(minX, minY, minZ)
        val allCubes = mutableSetOf<Point3D>()

        for (x in xRange) {
            for (y in yRange) {
                for (z in zRange) {
                    allCubes.add(Point3D(x, y, z))
                }
            }
        }
        allCubes.removeAll(cubes)

        var checkCubes = listOf(start)

        while (checkCubes.isNotEmpty()) {
            allCubes.removeAll(checkCubes.toSet())
            checkCubes = checkCubes
                .flatMap { it.crossNeighbours() }
                .distinct()
                .filter { allCubes.contains(it) && it.x in xRange && it.y in yRange && it.z in zRange }
        }
        return allCubes
    }

    private class WallsCounter {
        var walls = 0
        val cubes = mutableSetOf<Point3D>()

        fun addCube(cube: Point3D) {
            val neighboursCount = cube.crossNeighbours().count { cubes.contains(it) }
            walls += cubeWalls - (neighboursCount * 2)
            cubes.add(cube)
        }
    }

    private const val cubeWalls = 6
}