package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import kotlin.math.absoluteValue

object Main10 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solve()
    }

    override fun taskTwo() {
        solve()
    }

    private fun solve() {
        val crt = CRT()
        readTaskLines("")
            .forEach(crt::executeLine)

        println()
        crt.signal.alsoPrint()
    }

    private class CRT {
        private val countOnCycle: List<Int> = listOf(20, 60, 100, 140, 180, 220)
        var signal: Long = 0
        var x = 1
        var cycle: Int = 0

        fun executeLine(line: String) {
            when {
                line == "noop" -> executeNop()
                line.startsWith("addx") -> executeAdd(line.drop(5).toInt())
            }
        }

        private fun executeAdd(v: Int) {
            nextCycle()
            nextCycle()
            x += v
        }

        private fun executeNop() {
            nextCycle()
        }

        private fun nextCycle() {

            val column = cycle % 40
            if (column == 0) {
                println()
            }
            val pixelOn = (column - x).absoluteValue < 2
            val c = if (pixelOn) '#' else ' '
            print(c)


            cycle++
            if (countOnCycle.contains(cycle)) {
                signal += (cycle * x)
            }

        }

    }
}