package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main7 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val fs = FileSystem()
        readTaskLines()
            .forEach(fs::handleLine)
        fs.print()
        fs.findAllDirs()
            .filter { it.size <= 100000 }
            .sumOf { it.size }
            .alsoPrint()
    }

    override fun taskTwo() {
        val discSize = 70_000_000
        val spaceRequired = 30_000_000

        val fs = FileSystem()
        readTaskLines()
            .forEach(fs::handleLine)

        val totalTaken = fs.root.size
        val free = discSize - totalTaken
        val spaceNeeded = spaceRequired - free

        fs.findAllDirs()
            .filter { it.size >= spaceNeeded }
            .minByOrNull { it.size }
            ?.also { println("${it.name} - ${it.size}") }
    }

    private class FileSystem {
        val root: Entry.Dir = Entry.Dir(null, "/")
        var current: Entry.Dir = root


        fun handleLine(line: String) {
            when {
                line == "$ cd /" -> current = root
                line == "$ ls" -> {}
                line == "$ cd .." -> current = current.parent!!
                line.startsWith("$ cd ") -> enter(line.drop(5))
                line.startsWith("dir ") -> addDir(line.drop(4))
                else -> addFile(line.dropWhile { it.isDigit() || it == ' ' }, line.takeWhile { it.isDigit() }.toInt())
            }
        }

        private fun enter(name: String) {
            current = current.files.find { it.name == name } as Entry.Dir
        }

        private fun addDir(name: String) {
            if (current.files.any { it.name == name }) return
            current.files.add(Entry.Dir(current, name))
        }

        private fun addFile(name: String, size: Int) {
            if (current.files.any { it.name == name }) return
            current.files.add(Entry.Fil(current, name, size))
        }

        fun findAllDirs(): List<Entry.Dir> = findAllDirs(root)

        private fun findAllDirs(dir: Entry.Dir): List<Entry.Dir> =
            dir.files
                .mapNotNull { if (it is Entry.Dir) it else null }
                .flatMap { findAllDirs(it) }
                .plus(dir)

        fun print() {
            printDir(root, "")
        }

        fun printDir(dir: Entry.Dir, indent: String) {
            val contentIndent = "$indent  "
            println("$indent - ${dir.name} (dir, size=${dir.size})")
            dir.files.forEach {
                when (it) {
                    is Entry.Fil -> println("$contentIndent - ${it.name} (file, size=${it.size})")
                    is Entry.Dir -> printDir(it, contentIndent)
                }
            }
        }
    }

    private sealed class Entry(val parent: Dir?, val name: String) {
        abstract val size: Int

        class Dir(parent: Dir?, name: String) : Entry(parent, name) {
            val files = mutableListOf<Entry>()

            //TODO - would be nice to cache
            override val size: Int
                get() = files.sumOf { it.size }
        }

        class Fil(parent: Dir, name: String, override val size: Int) : Entry(parent, name)
    }

}
