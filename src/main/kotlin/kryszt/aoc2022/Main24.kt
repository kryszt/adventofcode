package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint
import java.lang.IllegalStateException

object Main24 : DayChallenge {
    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val (blizzards, start, end) = readBlizzards()
        solve(blizzards, start, end.oneUp()).alsoPrint()
    }

    override fun taskTwo() {
        val (blizzards, start, end) = readBlizzards()
        val arrived = solve(blizzards, start, end.oneUp())
        val back = solve(blizzards, end, start.oneDown(), arrived)
        solve(blizzards, start, end.oneUp(), back).alsoPrint()
    }

    private fun solve(blizzards: Blizzards, start: Point, end: Point, startingRound: Int = 0): Int {
        var states: Set<State> = setOf(State(start, startingRound))

        while (states.isNotEmpty() && states.none { it.position == end }) {
            val nextStates = mutableSetOf<State>()
            states.flatMapTo(nextStates) { nextStates(it, blizzards) }
            states = nextStates
        }
        return states.find { it.position == end }?.round?.plus(1) ?: throw IllegalStateException("Solution Not Found")
    }

    private fun nextStates(state: State, blizzards: Blizzards): List<State> {
        val nextRound = state.round + 1
        val candidates = state.position.steps().filter { blizzards.allows(it, nextRound) }.map { State(it, nextRound) }
        return if (state.position.isInitial(blizzards)) return candidates.plus(
            State(
                state.position,
                nextRound
            )
        ) else candidates
    }

    private fun Point.isInitial(blizzards: Blizzards): Boolean = !blizzards.isWithinBounds(this)

    private fun readBlizzards(): Triple<Blizzards, Point, Point> {
        val lines = readTaskLines("")
        val start = Point(lines.first().indexOf('.') - 1, -1)
        val end = Point(lines.last().indexOf('.') - 1, lines.size - 2)

        val w = lines.first().length - 2
        val h = lines.size - 2

        val horizontal = Array<List<Blizzard>>(lines.size - 2) { emptyList() }
        val vertical = Array<List<Blizzard>>(lines.first().length - 2) { emptyList() }

        lines.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                when (c) {
                    '<' -> horizontal[y - 1] = horizontal[y - 1] + Blizzard(x - 1, -1, w)
                    '>' -> horizontal[y - 1] = horizontal[y - 1] + Blizzard(x - 1, +1, w)
                    '^' -> vertical[x - 1] = vertical[x - 1] + Blizzard(y - 1, -1, h)
                    'v' -> vertical[x - 1] = vertical[x - 1] + Blizzard(y - 1, +1, h)
                }

            }
        }
        return Triple(
            Blizzards(
                horizontal = horizontal,
                vertical = vertical
            ),
            start,
            end
        )
    }

    private fun Point.steps(): List<Point> = listOf(
        this,
        this.oneUp(),
        this.oneDown(),
        this.oneLeft(),
        this.oneRight(),
    )

    private data class State(val position: Point, val round: Int)

    private class Blizzards(
        val horizontal: Array<List<Blizzard>>,
        val vertical: Array<List<Blizzard>>,
    ) {
        fun isWithinBounds(position: Point): Boolean =
            position.x in vertical.indices && position.y in horizontal.indices

        fun allows(position: Point, round: Int): Boolean {
            val inHorizontal = horizontal.getOrNull(position.y) ?: return false
            val inVertical = vertical.getOrNull(position.x) ?: return false

            return inHorizontal.none { it.positionAt(round) == position.x } &&
                    inVertical.none { it.positionAt(round) == position.y }
        }
    }

    private class Blizzard(val initial: Int, val direction: Int, val size: Int) {
        fun positionAt(time: Int) = initial.plus(direction.times(time)).mod(size)
    }
}