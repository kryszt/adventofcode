package kryszt.aoc2022

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines
import kotlin.math.abs

object Main15 : DayChallenge {

    private val linePattern = "Sensor at x=(-?[0-9]+), y=(-?[0-9]+): closest beacon is at x=(-?[0-9]+), y=(-?[0-9]+)"
        .toPattern()

    //test value
//    private const val checkLineY = 10
    private const val checkLineY = 2000000

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines("")
            .parseWith(linePattern) {
                BeaconData(
                    position = Point(it.group(1).toInt(), it.group(2).toInt()),
                    closest = Point(it.group(3).toInt(), it.group(4).toInt())
                )
            }
            .mapNotNull { emptyRange(it) }
            .mergeRanges()
            .sumOf { it.width() }
            .alsoPrint()
    }

    private fun List<IntRange>.mergeRanges(): List<IntRange> {
        var current = this
        while (true) {
            val next = current.fold(emptyList<IntRange>(), operation = { c, range -> c.addRange(range) })
            if (next == current) return next
            current = next
        }
    }

    private fun List<IntRange>.addRange(range: IntRange): List<IntRange> {
        var anyUpdated = false
        val updated: List<IntRange> =
            this
                .map { if (it.intersects(range)) it.combine(range).also { anyUpdated = true } else it }
        return if (anyUpdated) updated else updated.plus<IntRange>(range)
    }

    private fun IntRange.combine(other: IntRange): IntRange =
        this.first.coerceAtMost(other.first)..this.last.coerceAtLeast(other.last)

    private fun emptyRange(beacon: BeaconData): IntRange? {
        val distance = beacon.distanceToClosest
        val distanceOnLine = distance - abs(beacon.position.y - checkLineY)
        if (distanceOnLine < 0) return null
        var minX = beacon.position.x - distanceOnLine
        var maxX = beacon.position.x + distanceOnLine

        with(beacon.closest) {
            if (y == checkLineY) {
                if (x == minX) minX += 1
                if (x == maxX) maxX -= 1
            }
        }
        if (minX > maxX) return null

        return minX..maxX
    }

    override fun taskTwo() {
        val beacons = readTaskLines("")
            .parseWith(linePattern) {
                BeaconData(
                    position = Point(it.group(1).toInt(), it.group(2).toInt()),
                    closest = Point(it.group(3).toInt(), it.group(4).toInt())
                )
            }

        val borders = mutableListOf<Pair<BeaconData, BeaconData>>()

        beacons.indices.forEach { i ->
            for (j in (i + 1)..beacons.lastIndex) {
                val o1 = beacons[i]
                val o2 = beacons[j]
                val sDist = o1.position.manhattanDistance(o2.position)
                val rangeDist = o1.distanceToClosest + o2.distanceToClosest
                if (sDist == (rangeDist + 2)) {
                    borders.add(Pair(o1, o2))
                }
            }
        }

        val (first, second) = borders
            .map { findLine(it.first, it.second) }
        val point = findPoint(first, second)
        val value = (point.x.toLong() * 4000000L) + point.y.toLong()
        println(value)
    }

    private fun findLine(o1: BeaconData, o2: BeaconData): Line {
        val p1 = o1.position
        val p2 = o2.position

        val point = if (p2.x < p1.x) {
            p1.move(dx = -(o1.distanceToClosest + 1))
        } else {
            p1.move(dx = +(o1.distanceToClosest + 1))
        }

        val a = if ((p2.x > p1.x && p2.y > p1.y) || (p2.x < p1.x && p2.y < p1.y)) {
            -1
        } else {
            1
        }

        val b = point.y - (a * point.x)

        return Line(a, b)
    }

    private data class Line(val a: Int, val b: Int)

    private fun findPoint(line1: Line, line2: Line): Point {
        val x = (line2.b - line1.b) / (line1.a - line2.a)
        val y = (line1.a * x) + line1.b
        return Point(x, y)
    }

    private data class BeaconData(
        val position: Point,
        val closest: Point,
        val distanceToClosest: Int = closest.manhattanDistance(position)
    )
}