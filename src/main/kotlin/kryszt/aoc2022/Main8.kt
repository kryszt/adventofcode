package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint

object Main8 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }


    override fun taskOne() {
        val task = readTaskLines()
        val forest = task.asForest()
        val visibility = task.asVisibility()
        markAllVisible(forest, visibility)
        visibility.countTrues().alsoPrint()
    }

    override fun taskTwo() {
        val task = readTaskLines()
        val forest = task.asForest()
        maxScenicScore(forest).alsoPrint()
    }

    private fun Array<BooleanArray>.countTrues(): Int =
        sumOf { row -> row.count { it } }

    private fun List<String>.asForest(): Array<IntArray> =
        map { it.map { c -> c - '0' }.toIntArray() }.toTypedArray()

    private fun List<String>.asVisibility(): Array<BooleanArray> {
        val height = this.size
        val width = this[0].length
        return Array(height) { BooleanArray(width) { false } }
    }

    private fun markAllVisible(
        forest: Array<IntArray>,
        visibility: Array<BooleanArray>,
    ) {
        val max = forest[0].lastIndex
        //each row, left to right and right to left
        for (y in forest.indices) {
            //left -> right, x: 0 -> max
            markVisible(forest, visibility, generateSequence(Point(0, y)) { if (it.x < max) it.oneRight() else null })
            //right -> left, x: max -> 0
            markVisible(forest, visibility, generateSequence(Point(max, y)) { if (it.x > 0) it.oneLeft() else null })
        }

        for (x in 0..max) {
            //top -> bottom, y: 0 -> max
            markVisible(forest, visibility, generateSequence(Point(x, 0)) { if (it.y < max) it.oneDown() else null })
            //bottom -> top, y: max -> 0
            markVisible(forest, visibility, generateSequence(Point(x, max)) { if (it.y > 0) it.oneUp() else null })
        }
    }

    private fun markVisible(
        forest: Array<IntArray>,
        visibility: Array<BooleanArray>,
        sequence: Sequence<Point>
    ) {
        var highestOnPath: Int = -1
        sequence.forEach { point ->
            if (highestOnPath == 9) return
            val atPoint = forest[point.y][point.x]
            if (atPoint > highestOnPath) {
                highestOnPath = atPoint
                visibility[point.y][point.x] = true
            }
        }
    }

    private fun maxScenicScore(forest: Array<IntArray>): Int {
        var max = 0
        for (y in forest.indices) {
            for (x in forest[y].indices) {
                val score = scenicScore(Point(x, y), forest)
                if (score > max) {
                    max = score
                }
            }
        }
        return max
    }

    private fun scenicScore(point: Point, forest: Array<IntArray>): Int {
        val max = forest.lastIndex
        val scoreUp = scenicScore(point, forest, Point.Up, point.copy(y = 0))
        val scoreDown = scenicScore(point, forest, Point.Down, point.copy(y = max))

        val scoreLeft = scenicScore(point, forest, Point.Left, point.copy(x = 0))
        val scoreRight = scenicScore(point, forest, Point.Right, point.copy(x = max))

        return scoreUp * scoreDown * scoreLeft * scoreRight
    }

    private fun scenicScore(point: Point, forest: Array<IntArray>, direction: Point, last: Point): Int {
        var count = 0
        var current: Point = point
        val tree = forest[point.y][point.x]

        while (current != last) {
            current += direction
            val atCurrent = forest[current.y][current.x]
            if (atCurrent < tree) {
                count++
            } else {
                return count + 1
            }
        }

        return count
    }

}