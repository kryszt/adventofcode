package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint

object Main23 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val elves = readElves()
        repeat(10) {
            round(elves)
        }
        elves.countEmpty().alsoPrint()
    }

    override fun taskTwo() {
        val elves = readElves()
        var rounds = 1
        while (!round(elves)) {
            rounds++
        }
        rounds.alsoPrint()
    }

    private fun List<Elf>.countEmpty(): Int {
        val w = maxOf { it.position.x } - minOf { it.position.x } + 1
        val h = maxOf { it.position.y } - minOf { it.position.y } + 1
        return (w * h) - size
    }

    private fun readElves(): List<Elf> {
        return readTaskLines("")
            .flatMapIndexed { y: Int, s: String ->
                s.mapIndexedNotNull { x, c ->
                    if (c == '#') Elf(Point(x, y)) else null
                }
            }
    }

    private fun round(elves: List<Elf>): Boolean {
        val positions = mutableSetOf<Point>()
        elves.mapTo(positions, Elf::position)
        elves.forEach { elf ->
            elf.newRound()
            if (elf.shouldMove(positions)) {
                elf.findCandidate(positions)
            }
            elf.rotateConsiderations()
        }
        return elves
            .filterNot { it.candidate == null }
            .groupBy { it.candidate!! }
            .filter { it.value.size == 1 }
            .onEach { it.value.first().move() }
            .isEmpty()
    }

    private class Elf(startPoint: Point) {
        var position: Point = startPoint
            private set
        var candidate: Point? = null
            private set
        private var considerations = listOf(
            Consideration(listOf(Point.UpLeft, Point.Up, Point.UpRight), Point.Up),
            Consideration(listOf(Point.DownLeft, Point.Down, Point.DownRight), Point.Down),
            Consideration(listOf(Point.UpLeft, Point.Left, Point.DownLeft), Point.Left),
            Consideration(listOf(Point.UpRight, Point.Right, Point.DownRight), Point.Right),
        )

        fun newRound() {
            candidate = null
        }

        fun move() {
            position = checkNotNull(candidate)
        }

        fun shouldMove(elfPositions: Set<Point>): Boolean {
            return position.aroundNeighbours().any { elfPositions.contains(it) }
        }

        fun findCandidate(elfPositions: Set<Point>) {
            considerations
                .find { consideration ->
                    consideration.check.map { it.plus(position) }.none { elfPositions.contains(it) }
                }?.also {
                    candidate = it.move.plus(position)
                }
        }

        fun rotateConsiderations() {
            considerations = considerations.drop(1) + considerations.first()
        }
    }

    private class Consideration(val check: List<Point>, val move: Point)
}