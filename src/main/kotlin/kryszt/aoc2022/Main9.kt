package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint
import kotlin.math.absoluteValue
import kotlin.math.sign

object Main9 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val follower = Follower(Point(0, 0), 2)
        readTaskLines()
            .forEach(follower::execute)
        follower.tailVisited.size.alsoPrint()
    }

    override fun taskTwo() {
        val follower = Follower(Point(0, 0), 10)
        readTaskLines()
            .forEach(follower::execute)
        follower.tailVisited.size.alsoPrint()
    }

    private class Follower(start: Point, length: Int) {
        private val rope = Array(length) { start }
        private var head: Point
            get() = rope[0]
            set(value) {
                rope[0] = value
            }
        private val tail: Point
            get() = rope.last()

        val tailVisited = mutableSetOf(start)

        fun execute(line: String) {
            val dir = line[0].asDirection()
            val repeats = line.drop(2).toInt()

            repeat(repeats) {
                moveHead(dir)
                followWithRope()
                saveTailPosition()
            }
        }

        private fun saveTailPosition() {
            tailVisited.add(tail)
        }

        private fun moveHead(dir: Point) {
            head += dir
        }

        private fun followWithRope() {
            for (i in 1..rope.lastIndex) {
                val prev = rope[i - 1]
                val curr = rope[i]
                val moveTailBy = curr.checkMoveTo(prev)
                if (moveTailBy != null) {
                    rope[i] += moveTailBy
                }
            }
        }

        private fun Point.checkMoveTo(other: Point): Point? {
            if (this.isNextTo(other)) return null
            return this.directionTo(other)
        }

        private fun Point.isNextTo(other: Point): Boolean {
            return (this.x - other.x).absoluteValue < 2 && (this.y - other.y).absoluteValue < 2
        }

        private fun Point.directionTo(other: Point): Point {
            val dx = (other.x - this.x).sign
            val dy = (other.y - this.y).sign
            return Point(dx, dy)
        }

        private fun Char.asDirection(): Point = when (this) {
            'U' -> Point.Up
            'D' -> Point.Down
            'L' -> Point.Left
            'R' -> Point.Right
            else -> throw IllegalArgumentException("Unknown direction: $this")
        }
    }
}