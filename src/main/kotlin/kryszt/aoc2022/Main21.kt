package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import kryszt.tools.numberAtEndOption
import java.lang.IllegalArgumentException

object Main21 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val solver = readSolver()
        solver.evaluate("root").alsoPrint()
    }

    private fun readSolver(): Solver = readTaskLines("")
        .map(this::parseYell)
        .associateBy(Yell::id)
        .let(::Solver)


    private class Solver(val items: Map<String, Yell>) {

        private val operations = mapOf(
            '-' to { a: Long, b: Long -> a - b },
            '+' to { a: Long, b: Long -> a + b },
            '*' to { a: Long, b: Long -> a * b },
            '/' to { a: Long, b: Long -> a / b },
        )

        fun evaluate(name: String): Long =
            when (val item = checkNotNull(items[name])) {
                is Yell.AsNumber -> item.number
                is Yell.AsAction -> evaluate(item)
            }

        private fun evaluate(action: Yell.AsAction): Long {
            val operation = checkNotNull(operations[action.sign])
            return operation(evaluate(action.left), evaluate(action.right))
        }

        fun findMonkeys(name: String): List<String> =
            when (val item = checkNotNull(items[name])) {
                is Yell.AsNumber -> listOf(item.id)
                is Yell.AsAction -> findMonkeys(item.left) + findMonkeys(item.right)
            }
    }

    override fun taskTwo() {
        val solver = readSolver()
        val starter = "humn"

        val root = checkNotNull(solver.items["root"]) as Yell.AsAction
        val onLeft = solver.findMonkeys(root.left)
            .toSet()

        val adjustableInLeft = onLeft.contains(starter)

        val adjustableKey = if (adjustableInLeft) root.left else root.right
        val constantKey = if (adjustableInLeft) root.right else root.left

        val expectedValue = solver.evaluate(constantKey)

        adjust(starter, solver.items[adjustableKey]!!, solver, expectedValue)
            .alsoPrint()
    }

    private fun adjust(adjustKey: String, yell: Yell, solver: Solver, expectedValue: Long): Long {
        if (yell is Yell.AsNumber) return expectedValue

        val action = yell as Yell.AsAction

        val inLeft = solver.findMonkeys(action.left)
        val targetIsInLeft = inLeft.contains(adjustKey)

        val constantKey = if (targetIsInLeft) action.right else action.left
        val targetKey = if (targetIsInLeft) action.left else action.right
        val constantValue = solver.evaluate(constantKey)

        val nextExpected = nextExpected(expectedValue, constantValue, action.sign, !targetIsInLeft)

        return adjust(adjustKey, solver.items[targetKey]!!, solver, nextExpected)
    }

    private fun nextExpected(expected: Long, constant: Long, sign: Char, constantOnLeft: Boolean): Long =
        when (sign) {
            '+' -> expected - constant
            '-' -> if (constantOnLeft) constant - expected else expected + constant
            '*' -> expected / constant
            '/' -> if (constantOnLeft) constant / expected else expected * constant
            else -> throw IllegalArgumentException("Sign :$sign")
        }

    private fun parseYell(line: String): Yell {
        val id = line.take(4)
        val number = line.numberAtEndOption()
        if (number != null) return Yell.AsNumber(id, number.toLong())

        val left = line.drop(6).take(4)
        val right = line.takeLast(4)
        val sign = line[11]

        return Yell.AsAction(id, left, right, sign)
    }

    private sealed class Yell {
        abstract val id: String

        data class AsNumber(override val id: String, val number: Long) : Yell()
        data class AsAction(override val id: String, val left: String, val right: String, val sign: Char) : Yell()
    }
}