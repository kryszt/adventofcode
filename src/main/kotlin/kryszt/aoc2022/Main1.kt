package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main1 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val backpacks = BackpackParser().parse(readTaskLines())
        backpacks
            .maxByOrNull { it.total }
            .alsoPrint()
    }

    override fun taskTwo() {
        val backpacks = BackpackParser().parse(readTaskLines())
        backpacks
            .sortedByDescending { it.total }
            .take(3)
            .sumOf { it.total }
            .alsoPrint()
    }

    class BackpackParser {

        private var nextBackpack = mutableListOf<Int>()
        private val elves = mutableListOf<ElfBackpack>()

        fun parse(list: List<String>): List<ElfBackpack> {
            list.forEach {
                if (it.isEmpty()) {
                    closeBackpack()
                } else {
                    nextBackpack.add(it.toInt())
                }
            }

            closeBackpack()
            return elves
        }

        private fun closeBackpack() {
            val backpack = nextBackpack
            nextBackpack = mutableListOf()
            elves.add(ElfBackpack(backpack))
        }
    }

    data class ElfBackpack(val items: List<Int>, val total: Int = items.sum())
}
