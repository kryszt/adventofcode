package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint
import kryszt.tools.printBoard

object Main22 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val lines = readTaskLines("")
        val maze = FlatWrappingMaze(lines.dropLast(2))
        val orders = parseOrders(lines.last())
        val mazeWalker = MazeWalker(maze)
        mazeWalker.execute(orders)
        mazeWalker
            .position
            .let { 1000 * (it.y + 1) + 4 * (it.x + 1) }
            .plus(mazeWalker.direction.directionScore())
            .alsoPrint()
    }

    override fun taskTwo() {
        val lines = readTaskLines("")
        val maze = CubeWrappingMaze(lines.dropLast(2), true)
        val orders = parseOrders(lines.last())
        val mazeWalker = MazeWalker(maze)
        mazeWalker.execute(orders)
        mazeWalker
            .position
            .let { 1000 * (it.y + 1) + 4 * (it.x + 1) }
            .plus(mazeWalker.direction.directionScore())
            .alsoPrint()
    }

    private fun Point.directionScore(): Int = when (this) {
        Point.Right -> 0
        Point.Down -> 1
        Point.Left -> 2
        Point.Up -> 3
        else -> throw IllegalArgumentException("Illegal direction: $this")
    }


    private abstract class WrappingMazeBase(protected val lines: List<String>) {
        val width = lines.maxOf { it.length }
        val height = lines.size
        fun startPoint(): Point = lines.first().indexOf('.').let { Point(it, 0) }
        protected fun Point.normalize() = Point((this.x + width) % width, (this.y + height) % height)
        fun placeAt(point: Point): Char = lines.getOrNull(point.y)?.getOrNull(point.x) ?: ' '
        fun isEmpty(point: Point) = placeAt(point) == '.'
        fun isWall(point: Point) = placeAt(point) == '#'
        abstract fun nextPosition(start: Point, direction: Point): Move
    }

    private class FlatWrappingMaze(lines: List<String>) : WrappingMazeBase(lines) {

        override fun nextPosition(start: Point, direction: Point): Move {
            var point = (start + direction).normalize()
            while (placeAt(point) == ' ') {
                point = (point + direction).normalize()
            }
            return Move(point, direction)
        }
    }

    private class CubeWrappingMaze(lines: List<String>, private val useReal: Boolean) : WrappingMazeBase(lines) {
        private val wallSize: Int = maxOf(lines.size, lines.first().length) / 4
        private val lastInWall = wallSize - 1

        private val groups: Map<Int, Point>
            get() = if (useReal) groupsReal else groupsTest
        private val groupsTest: Map<Int, Point> = mapOf(
            1 to Point(2, 0),
            2 to Point(0, 1),
            3 to Point(1, 1),
            4 to Point(2, 1),
            5 to Point(2, 2),
            6 to Point(3, 2),
        )

        private val groupsReal: Map<Int, Point> = mapOf(
            1 to Point(1, 0),
            2 to Point(2, 0),
            3 to Point(1, 1),
            4 to Point(0, 2),
            5 to Point(1, 2),
            6 to Point(0, 3),
        )

        private val connections: List<Connection>
            get() = if (useReal) connectionsReal else connectionsTest

        private val connectionsTest = listOf(
            Connection(
                1, Point.Left, Wall(Point(0, lastInWall), Point(0, 0)),
                3, Point.Up, Wall(Point(lastInWall, 0), Point(0, 0))
            ),
            Connection(
                1, Point.Up, Wall(Point(0, 0), Point(lastInWall, 0)),
                2, Point.Up, Wall(Point(lastInWall, 0), Point(0, 0))
            ),
            Connection(
                1, Point.Right, Wall(Point(lastInWall, lastInWall), Point(lastInWall, 0)),
                6, Point.Right, Wall(Point(lastInWall, 0), Point(lastInWall, lastInWall))
            ),
            Connection(
                2, Point.Left, Wall(Point(0, 0), Point(0, lastInWall)),
                6, Point.Down, Wall(Point(lastInWall, lastInWall), Point(0, lastInWall))
            ),
            Connection(
                2, Point.Down, Wall(Point(0, lastInWall), Point(lastInWall, lastInWall)),
                5, Point.Down, Wall(Point(lastInWall, lastInWall), Point(0, lastInWall))
            ),
            Connection(
                3, Point.Down, Wall(Point(0, lastInWall), Point(lastInWall, lastInWall)),
                5, Point.Left, Wall(Point(0, lastInWall), Point(0, 0))
            ),
            Connection(
                4, Point.Right, Wall(Point(lastInWall, lastInWall), Point(lastInWall, 0)),
                6, Point.Up, Wall(Point(0, 0), Point(lastInWall, 0))
            ),
        )

        private val connectionsReal = listOf(
            Connection(
                1, Point.Left, Wall(Point(0, 0), Point(0, lastInWall)),
                4, Point.Left, Wall(Point(0, lastInWall), Point(0, 0))
            ),
            Connection(
                1, Point.Up, Wall(Point(0, 0), Point(lastInWall, 0)),
                6, Point.Left, Wall(Point(0, 0), Point(0, lastInWall))
            ),
            Connection(
                2, Point.Up, Wall(Point(0, 0), Point(lastInWall, 0)),
                6, Point.Down, Wall(Point(0, lastInWall), Point(lastInWall, lastInWall))
            ),
            Connection(
                2, Point.Right, Wall(Point(lastInWall, 0), Point(lastInWall, lastInWall)),
                5, Point.Right, Wall(Point(lastInWall, lastInWall), Point(lastInWall, 0))
            ),
            Connection(
                2, Point.Down, Wall(Point(0, lastInWall), Point(lastInWall, lastInWall)),
                3, Point.Right, Wall(Point(lastInWall, 0), Point(lastInWall, lastInWall))
            ),
            Connection(
                3, Point.Left, Wall(Point(0, 0), Point(0, lastInWall)),
                4, Point.Up, Wall(Point(0, 0), Point(lastInWall, 0))
            ),
            Connection(
                5, Point.Down, Wall(Point(0, lastInWall), Point(lastInWall, lastInWall)),
                6, Point.Right, Wall(Point(lastInWall, 0), Point(lastInWall, lastInWall))
            ),
        )

        private val jumps: Map<Move, Move> = calculateJumps(connections, wallSize)

        override fun nextPosition(start: Point, direction: Point): Move {
            return jumps[Move(start, direction)] ?: Move(start.plus(direction), direction)
        }

        private fun calculateJumps(connections: List<Connection>, wallSize: Int): Map<Move, Move> {
            val allJumps = mutableMapOf<Move, Move>()
            connections.forEach {
                allJumps.putAll(calculateJumps(it, wallSize))
            }
            return allJumps
        }

        private fun calculateJumps(connection: Connection, wallSize: Int): Map<Move, Move> {
            val jumps = mutableMapOf<Move, Move>()
            val fromWallDirection = connection.fromWall.step
            val toWallDirection = connection.toWall.step
            repeat(wallSize) { step ->
                val onWall1 = connection.fromWall.from.plus(fromWallDirection.times(step))
                    .plus(groups[connection.fromGroup]!!.times(wallSize))
                val onWall2 = connection.toWall.from.plus(toWallDirection.times(step))
                    .plus(groups[connection.toGroup]!!.times(wallSize))

                jumps[Move(onWall1, connection.fromDirection)] = Move(onWall2, connection.toDirection.times(-1))
                jumps[Move(onWall2, connection.toDirection)] = Move(onWall1, connection.fromDirection.times(-1))
            }
            return jumps
        }

        private data class Wall(val from: Point, val to: Point) {
            val step: Point = to.minus(from).asStep()
        }

        private data class Connection(
            val fromGroup: Int,
            val fromDirection: Point,
            val fromWall: Wall,
            val toGroup: Int,
            val toDirection: Point,
            val toWall: Wall
        )
    }

    private data class Move(val place: Point, val direction: Point)

    private class MazeWalker(private val maze: WrappingMazeBase, starting: Point = maze.startPoint()) {
        var position: Point = starting
            private set
        var direction = Point.Right

        private val visited = mutableSetOf(position)

        fun drawPath() = apply {
            println("${maze.width}x${maze.height}")
            Point(visited.minOf { it.x }, visited.minOf { it.y }).alsoPrint()
            Point(visited.maxOf { it.x }, visited.maxOf { it.y }).alsoPrint()
            visited.printBoard(onEmpty = ' ')
        }

        fun execute(orders: List<Order>) {
            orders.forEach(::execute)
        }

        private fun execute(order: Order) {
            when (order) {
                is Order.Rotate -> direction = order.change(direction)
                is Order.Move -> move(order.steps)
            }
        }

        private fun move(steps: Int) {
            repeat(steps) {
                val candidate = maze.nextPosition(position, direction)
                if (maze.isEmpty(candidate.place)) {
                    position = candidate.place
                    direction = candidate.direction
                    visited.add(position)
                } else if (maze.isWall(candidate.place)) {
                    return
                }
            }
        }
    }

    private fun parseOrders(line: String): List<Order> {
        val orders = mutableListOf<Order>()
        var move = 0
        line.forEach { c ->
            if (c.isDigit()) {
                move = (move * 10) + c.minus('0')
            } else {
                orders.add(Order.Move(move))
                move = 0
                when (c) {
                    'R' -> orders.add(Order.Rotate { it.right() })
                    'L' -> orders.add(Order.Rotate { it.left() })
                    else -> throw IllegalArgumentException("Not supported: $c")
                }
            }
        }
        if (line.last().isDigit()) {
            orders.add(Order.Move(move))
        }
        return orders
    }

    private sealed class Order {
        data class Rotate(val change: (Point) -> Point) : Order()
        data class Move(val steps: Int) : Order()
    }

}