package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLinesSequence
import kryszt.tools.alsoPrint
import kryszt.tools.contains
import java.lang.IllegalArgumentException
import java.util.regex.Matcher

object Main4 : DayChallenge {
    private val linePattern = "^(\\d+)-(\\d+).(\\d+)-(\\d+)\$".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLinesSequence()
            .count { parseLine(it).overlapsFully() }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLinesSequence()
            .count { parseLine(it).hasOverlap() }
            .alsoPrint()
    }

    private fun parseLine(line: String): DoubleAssignment =
        linePattern
            .matcher(line)
            .takeIf { it.matches() }
            ?.parseAssignments()
            ?: throw IllegalArgumentException("Line don't match: '$line'")

    private fun Matcher.parseAssignments(): DoubleAssignment = DoubleAssignment(
        first = IntRange(this.group(1).toInt(), this.group(2).toInt()),
        second = IntRange(this.group(3).toInt(), this.group(4).toInt())
    )

    private data class DoubleAssignment(val first: IntRange, val second: IntRange) {
        fun overlapsFully(): Boolean = first.contains(second) || second.contains(first)

        fun hasOverlap(): Boolean =
            first.contains(second.first)
                    || first.contains(second.last)
                    || second.contains(first.first)
                    || second.contains(first.last)
    }
}