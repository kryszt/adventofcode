package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import java.lang.IllegalArgumentException

object Main25 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines("")
            .sumOf { it.fromSnafuString() }
            .toSnafuString()
            .alsoPrint()
    }

    override fun taskTwo() {
        //there is no second part
    }

    private fun String.fromSnafuString(): Long {
        var v = 0L
        this.forEach { c ->
            v = (v * 5) + c.snafuValue()
        }
        return v
    }

    private fun Long.toSnafuString(): String {
        var left = this
        val digits = mutableListOf<Char>()
        while (left > 0) {
            var digit = left.mod(5)
            left /= 5
            if (digit > 2) {
                left++
                digit -= 5
            }
            digits.add(digit.snafuDigit())
        }
        if (digits.isEmpty()) {
            digits.add('0')
        }
        return digits.reversed().joinToString("")
    }

    private fun Char.snafuValue(): Int = when (this) {
        '=' -> -2
        '-' -> -1
        '0' -> 0
        '1' -> 1
        '2' -> 2
        else -> throw IllegalArgumentException("Not a SNAFU digit: $this")
    }

    private fun Int.snafuDigit(): Char = when (this) {
        -2 -> '='
        -1 -> '-'
        0 -> '0'
        1 -> '1'
        2 -> '2'
        else -> throw IllegalArgumentException("Not a SNAFU digit: $this")
    }

}