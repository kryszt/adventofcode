package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLinesSequence
import kryszt.tools.alsoPrint

object Main3 : DayChallenge {
    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLinesSequence("")
            .sumOf { Rucksack(it).findCommon.priority() }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLinesSequence("")
            .map(::Rucksack)
            .windowed(3, 3)
            .sumOf { commonItem(it).priority() }
            .alsoPrint()
    }

    private fun commonItem(rucksacks: List<Rucksack>): Char {
        require(rucksacks.isNotEmpty())
        var commonSet = rucksacks.first().allTypes
        rucksacks.drop(1).forEach { other ->
            commonSet = commonSet.intersect(other.allTypes)
        }
        check(commonSet.size == 1)

        return commonSet.first()
    }

    private fun Char.priority() = when (this) {
        in 'a'..'z' -> this - 'a' + 1
        in 'A'..'Z' -> this - 'A' + 27
        else -> throw IllegalArgumentException("Only letters, requested: $this")
    }

    private data class Rucksack(val full: String) {
        val left: String = full.take(full.length / 2)
        val right: String = full.takeLast(full.length / 2)
        val findCommon: Char by lazy { left.find { right.contains(it) }!! }

        val allTypes: Set<Char> by lazy { full.toSet() }
    }
}