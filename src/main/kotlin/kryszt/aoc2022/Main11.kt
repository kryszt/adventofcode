package kryszt.aoc2022

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines
import java.lang.IllegalArgumentException

object Main11 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val party = readTaskLines("")
            .filterNot { it.isEmpty() }
            .map { it.trim() }
            .windowed(6, 6)
            .map(::readMonkey)
            .let { monkeys -> Party(monkeys) { it.div(3) } }

        party.makeRounds(20)

        party
            .monkeys
            .sortedByDescending { it.inspected }
            .take(2)
            .map { it.inspected }
            .reduce { acc, i -> acc * i }
            .alsoPrint()
    }

    override fun taskTwo() {
        val party = readTaskLines("")
            .filterNot { it.isEmpty() }
            .map { it.trim() }
            .windowed(6, 6)
            .map(::readMonkey)
            .let { monkeys -> Party(monkeys) { it.mod(monkeys.divisor()) } }

        party.makeRounds(10000)
        party.printParty()

        party
            .monkeys
            .sortedByDescending { it.inspected }
            .take(2)
            .map { it.inspected }
            .reduce { acc, i -> acc * i }
            .alsoPrint()
    }

    private fun List<Monkey>.divisor(): Long = this.map { it.checkDivider.toLong() }.reduce { acc, i -> acc * i }


    private fun readMonkey(lines: List<String>): Monkey {
        check(lines[0].startsWith("Monkey"))
        val initialItems = lines[1].drop("Starting items: ".length).split(", ").map { it.toLong() }
        val operation = readOperation(lines[2])
        val division = lines[3].numberAtEnd()
        val ifPassed = lines[4].numberAtEnd()
        val ifFailed = lines[5].numberAtEnd()
        val targetSelection = { test: Long -> if (test.dividesBy(division)) ifPassed else ifFailed }

        return Monkey(initialItems, division, operation, targetSelection)
    }

    private fun readOperation(line: String): (Long) -> Long {
        val sign = line["Operation: new = old ".length]
        val lastPart = line.lastWord()

        if (lastPart == "old") {
            return when (sign) {
                '+' -> { old: Long -> old + old }
                '*' -> { old: Long -> old * old }
                else -> throw IllegalArgumentException("Illegal sign: $sign")
            }
        }

        val number = lastPart.toLong()
        return when (sign) {
            '+' -> { old: Long -> old + number }
            '*' -> { old: Long -> old * number }
            else -> throw IllegalArgumentException("Illegal sign: $sign")
        }

    }

    private class Party(val monkeys: List<Monkey>, val afterInspection: (Long) -> Long) {

        fun passTo(monkeyId: Int, value: Long) {
            monkeys[monkeyId].items.add(value)
        }


        fun makeRounds(repeats: Int = 1) {
            repeat(repeats) {
                monkeys.forEach { it.inspectAll(this) }
            }
        }

        fun printParty() {
            monkeys.forEachIndexed { index, monkey ->
                println("Monkey $index (inspected ${monkey.inspected}): ${monkey.items}")
            }
        }
    }

    private class Monkey(
        initial: List<Long>,
        val checkDivider: Int,
        private val operation: (Long) -> Long,
        private val targetSelection: (Long) -> Int,//value -> monkeyId
    ) {
        val items = initial.toMutableList()
        var inspected: Long = 0

        fun inspectAll(party: Party) {
            inspected += items.size
            items.forEach { inspect(it, party) }
            items.clear()
        }

        private fun inspect(value: Long, party: Party) {
            val newValue = party.afterInspection(operation(value))
            party.passTo(targetSelection(newValue), newValue)
        }
    }
}