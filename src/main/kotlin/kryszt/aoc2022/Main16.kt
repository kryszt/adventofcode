package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import kryszt.tools.parseWith

object Main16 : DayChallenge {

    private val linePattern = "^Valve ([A-Z]{2}) has flow rate=(\\d+); tunnels? leads? to valves? (.+)\$".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        Solver(readValves())
            .solve()
            .alsoPrint()
    }

    override fun taskTwo() {
        SolverDouble(readValves())
            .solve()
            .alsoPrint()
    }

    private fun readValves(): List<Valve> =
        readTaskLines("")
            .parseWith(linePattern) { matcher ->
                Valve(
                    matcher.group(1),
                    matcher.group(2).toInt(),
                    matcher.group(3).split(", ")
                )
            }


    private abstract class SolverBase<T : Any>(val valves: List<Valve>, val value: T.() -> Int) {
        protected val distances = DistanceFinder(valves).apply { findAll() }

        protected val valvesMap = valves.associateBy(Valve::id)
        protected val startingValve = "AA"
        abstract val startTime: Int
        lateinit var best: T

        fun solve(): Int {
            val initial = makeFirstState()
            best = initial
            visit(initial)
            return best.value()
        }

        fun visit(state: T) {
            checkBest(state)
            val nextMoves = continueWith(state)
            nextMoves.forEach {
                visit(it)
            }
        }

        fun checkBest(bestCandidate: T) {
            if (bestCandidate.value() > best.value()) {
                println("Set best ${bestCandidate.value()}")
                best = bestCandidate
            }
        }

        abstract fun continueWith(state: T): List<T>
        abstract fun makeFirstState(): T
    }


    private class Solver(valves: List<Valve>) : SolverBase<ValveState>(valves, ValveState::value) {
        override val startTime = 30


        override fun continueWith(state: ValveState): List<ValveState> {
            return state
                .closed
                .mapNotNull { target ->
                    val distance = distances.distanceBetween(state.atValve, target)
                    val timeToOpen = distance + 1
                    val timeLeftAfterOpen = state.timeLeft - timeToOpen
                    if (timeLeftAfterOpen <= 0) return@mapNotNull null

                    val targetValve = valvesMap[target]!!

                    val nextState = ValveState(
                        timeLeft = state.timeLeft - timeToOpen,
                        value = state.value + targetValve.rate * timeLeftAfterOpen,
                        atValve = target,
                        closed = state.closed.minus(target),
                    )
                    nextState
                }
        }

        override fun makeFirstState(): ValveState = ValveState(
            timeLeft = startTime,
            value = 0,
            atValve = startingValve,
            closed = valves.filter { it.rate > 0 }.map { it.id }.toSet(),
        )
    }


    private class SolverDouble(valves: List<Valve>) : SolverBase<ValveStateDouble>(valves, ValveStateDouble::value) {
        override val startTime = 26

        override fun continueWith(state: ValveStateDouble): List<ValveStateDouble> {
            return state.closed.mapNotNull { target -> makeNextState(state, target) }
        }

        private fun makeNextState(state: ValveStateDouble, target: String): ValveStateDouble? {
            val distance = distances.distanceBetween(state.targetValve1, target)
            val timeToOpen = distance + 1
            val timeLeftAfterOpen = state.timeLeft - timeToOpen
            if (timeLeftAfterOpen <= 0) return null

            val moveBy = minOf(timeToOpen, state.target2Distance)
            val targetValve = valvesMap[target]!!

            return ValveStateDouble(
                timeLeft = state.timeLeft - moveBy,
                value = state.value + targetValve.rate * timeLeftAfterOpen,
                targetValve1 = target,
                target1Distance = timeToOpen - moveBy,
                targetValve2 = state.targetValve2,
                target2Distance = state.target2Distance - moveBy,
                closed = state.closed.minus(target),
            ).moveArrivedToFirst()
        }

        private fun ValveStateDouble.moveArrivedToFirst(): ValveStateDouble {
            if (this.target1Distance == 0) return this
            return this.copy(
                targetValve1 = this.targetValve2,
                target1Distance = this.target2Distance,
                targetValve2 = this.targetValve1,
                target2Distance = this.target1Distance
            )
        }

        override fun makeFirstState(): ValveStateDouble = ValveStateDouble(
            timeLeft = startTime,
            value = 0,
            targetValve1 = startingValve,
            target1Distance = 0,
            targetValve2 = startingValve,
            target2Distance = 0,
            closed = valves.filter { it.rate > 0 }.map { it.id }.toSet(),
        )
    }

    private class DistanceFinder(private val valves: List<Valve>) {
        private val valvesMap = valves.associateBy(Valve::id)
        val distances = mutableMapOf<String, Map<String, Int>>()

        fun findAll() {
            valves.forEach { distances[it.id] = allFrom(it) }
        }

        fun distanceBetween(valve1: String, valve2: String): Int = distances[valve1]!![valve2]!!

        private fun allFrom(valve: Valve): Map<String, Int> {
            val left = valves.map { it.id }.minus(valve.id).toMutableSet()
            val distances = mutableMapOf<String, Int>()

            var distance = 0
            var steps = listOf(valve.id)
            while (left.isNotEmpty()) {
                steps = steps
                    .onEach { distances[it] = distance }
                    .onEach { left.remove(it) }
                    .flatMap { valvesMap[it]?.connected.orEmpty() }
                    .filter { left.contains(it) }

                distance++
            }
            return distances
        }
    }

    private data class Valve(val id: String, val rate: Int, val connected: List<String>)
    private data class ValveState(
        val timeLeft: Int,
        val value: Int,
        val atValve: String,
        val closed: Set<String>,
    )

    private data class ValveStateDouble(
        val timeLeft: Int,
        val value: Int,
        val targetValve1: String,
        val target1Distance: Int,
        val targetValve2: String,
        val target2Distance: Int,
        val closed: Set<String>,
    )
}