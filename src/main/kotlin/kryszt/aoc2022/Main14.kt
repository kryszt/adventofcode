package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint

object Main14 : DayChallenge {

    private val source = Point(500, 0)

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val walls = parseWalls(readTaskLines(""))
        val bottomLine = walls.maxOf { it.y }

        val scene = Scene(
            walls,
            isFinished = { p -> p.y > bottomLine },
            addOnFinish = false
        )
        while (scene.nextSand(source)) {
            //NOP
        }
        scene.sand.size.alsoPrint()
    }

    override fun taskTwo() {
        val walls = parseWalls(readTaskLines(""))

        val scene = Scene(
            walls,
            isFinished = { p -> p == source },
            addOnFinish = true
        )
        while (scene.nextSand(source)) {
            //NOP
        }
        scene.sand.size.alsoPrint()
    }

    private class Scene(
        private val walls: Set<Point>,
        private val isFinished: (Point) -> Boolean,
        private val addOnFinish: Boolean
    ) {
        val sand = mutableSetOf<Point>()

        private val checks: List<Point> = listOf(Point.Down, Point.DownLeft, Point.DownRight)
        private val bottomLine = walls.maxOf { it.y } + 2

        /**
         * @return true if placed, false if dropped
         */
        fun nextSand(start: Point): Boolean {
            var sandSpace = start
            while (true) {
                val moveTo = moveTo(sandSpace)
                if (moveTo == null) {
                    return if (isFinished(sandSpace)) {
                        if (addOnFinish) {
                            sand.add(sandSpace)
                        }
                        false
                    } else {
                        sand.add(sandSpace)
                        true
                    }
                } else {
                    sandSpace = moveTo
                }
            }
        }

        private fun moveTo(sand: Point): Point? =
            checks
                .map { sand.plus(it) }
                .firstOrNull { hasSpace(it) }

        private fun hasSpace(point: Point): Boolean =
            point.y < bottomLine && !walls.contains(point) && !sand.contains(point)
    }

    private fun parseWalls(lines: List<String>) =
        lines
            .map { parseWalls(it) }
            .reduce { acc, points -> acc.plus(points) }

    private fun parseWalls(line: String): Set<Point> =
        parseWallSegments(line)
            .zipWithNext()
            .map { asWallLine(it.first, it.second) }
            .reduce(operation = { acc, points -> acc.plus(points) })

    private fun asWallLine(p1: Point, p2: Point): Set<Point> {
        val minX = p1.x.coerceAtMost(p2.x)
        val maxX = p1.x.coerceAtLeast(p2.x)
        val minY = p1.y.coerceAtMost(p2.y)
        val maxY = p1.y.coerceAtLeast(p2.y)

        val wallPoints = mutableSetOf<Point>()

        for (x in minX..maxX) {
            for (y in minY..maxY) {
                wallPoints.add(Point(x, y))
            }
        }
        return wallPoints
    }

    private fun parseWallSegments(line: String): List<Point> =
        line
            .split(" -> ")
            .map { it.split(",") }
            .map { (x, y) -> Point(x.toInt(), y.toInt()) }
}