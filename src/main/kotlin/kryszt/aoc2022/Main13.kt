package kryszt.aoc2022

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLinesSequence
import kryszt.tools.alsoPrint

object Main13 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLinesSequence("")
            .filterNot { it.isEmpty() }
            .map { parseLine(it) }
            .chunked(2) { (left, right) -> Pair(left, right) }
            .mapIndexed { index, pair -> if (compareItems(pair.first, pair.second) < 0) index + 1 else 0 }
            .sum()
            .alsoPrint()
    }

    override fun taskTwo() {
        val marker1 = "[[2]]"
        val marker2 = "[[6]]"
        val sorted = readTaskLinesSequence("")
            .plusElement(marker1)
            .plusElement(marker2)
            .filterNot { it.isEmpty() }
            .map { Pair(it, parseLine(it)) }
            .sortedWith { o1, o2 -> compareItems(o1.second, o2.second) }
            .toList()
            .map { it.first }

        val markerIndex1 = sorted.indexOf(marker1)
        val markerIndex2 = sorted.indexOf(marker2)
        val key = (markerIndex1 + 1) * (markerIndex2 + 1)
        println(key)
    }

    private sealed class Item {
        data class Values(val items: List<Item>) : Item() {
            override fun toString(): String {
                return items.joinToString(separator = ",", prefix = "[", postfix = "]")
            }
        }

        data class Value(val int: Int) : Item() {
            override fun toString(): String {
                return int.toString()
            }
        }
    }

    private fun parseLine(line: String): Item {
        val tokenizer = Tokenizer(line)
        require(tokenizer.nextToken() is Token.Open)
        return parseList(tokenizer)
    }

    private fun parseList(tokenizer: Tokenizer): Item.Values {
        val items = mutableListOf<Item>()
        while (true) {
            when (val t = tokenizer.nextToken()) {
                Token.Open -> items.add(parseList(tokenizer))
                Token.Close, null -> return Item.Values(items)
                is Token.Value -> items.add(Item.Value(t.v))
            }
        }
    }

    private sealed class Token {
        object Open : Token()
        object Close : Token()
        data class Value(val v: Int) : Token()
    }

    private class Tokenizer(private val line: String) {
        private var index: Int = 0

        fun nextToken(): Token? {
            if (index >= line.length) return null
            if (line[index] == '[') return Token.Open.also { index++ }
            if (line[index] == ']') return Token.Close.also { index++ }
            if (line[index] == ',') {
                index++
                return nextToken()
            }
            if (line[index].isDigit()) return readInt()
            throw IllegalStateException("Cannot tokenize ${line[index]}")
        }

        private fun readInt(): Token.Value {
            val number = line.drop(index).takeWhile { it.isDigit() }
            index += number.length
            return Token.Value(number.toInt())
        }
    }

    private fun compareItems(left: Item, right: Item): Int {
        if (left is Item.Value && right is Item.Value) return left.int.compareTo(right.int)
        if (left is Item.Values && right is Item.Values) return compareValues(left, right)
        if (left is Item.Value && right is Item.Values) return compareValues(Item.Values(listOf(left)), right)
        if (left is Item.Values && right is Item.Value) return compareValues(left, Item.Values(listOf(right)))

        throw IllegalArgumentException("What? l=$left, r=$right")
    }

    private fun compareValues(left: Item.Values, right: Item.Values): Int {
        val shorter = left.items.size.coerceAtMost(right.items.size)
        for (i in 0 until shorter) {
            val compare = compareItems(left.items[i], right.items[i])
            if (compare != 0) return compare
        }
        return left.items.size - right.items.size
    }
}