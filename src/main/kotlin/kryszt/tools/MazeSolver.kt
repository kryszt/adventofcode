package kryszt.tools

abstract class MazeSolver : Solver<MazeSolver.Path>() {

    val visited = mutableSetOf<Point>()

    fun checkAllMovesDeep(startPoint: Point): Path? = checkAllMovesDeep(Path("", startPoint))

    fun checkAllMovesDeep(currentPath: Path): Path? {
        try {
            visiting(currentPath)
            if (isEnd(currentPath)) {
                return currentPath
            }
            return selectMoves(currentPath).map { checkAllMovesDeep(it) }.minByOrNull { it?.path?.length ?: 0 }
        } finally {
            exiting(currentPath)
        }
    }

    fun checkAllMovesWide(start: Point): Path? = checkAllMovesWide(Path("", start))

    fun checkAllMovesWide(start: Path): Path? = solve(listOf(start))

    override fun visiting(state: Path) {
        visited.add(state.place)
    }

    open fun exiting(path: Path) {
        visited.remove(path.place)
    }

    override fun wasVisited(state: Path): Boolean = wasVisited(state.place)

    fun wasVisited(point: Point): Boolean = visited.contains(point)

    override fun selectMoves(state: Path): List<Path> {
//        println("doors for $path (${PassBase + path.path}}: $doors")
        val possibleMoves = mutableListOf<Path>()
        //up
        (state.place + Point.Up).let { Path(state.path + "U", it) }.takeIf { allowed(it) }?.let { possibleMoves.add(it) }
        (state.place + Point.Down).let { Path(state.path + "D", it) }.takeIf { allowed(it) }?.let { possibleMoves.add(it) }
        (state.place + Point.Left).let { Path(state.path + "L", it) }.takeIf { allowed(it) }?.let { possibleMoves.add(it) }
        (state.place + Point.Right).let { Path(state.path + "R", it) }.takeIf { allowed(it) }?.let { possibleMoves.add(it) }
//        println("Possible: ${possibleMoves.joinToString { it.path }}")
        return possibleMoves
    }

    data class Path(val path: String, val place: Point, val lenght: Int = path.length)

}
