package kryszt.tools

import java.io.BufferedReader
import java.io.FileNotFoundException

object IoUtil {

    private fun Any.getFileNameForClass(nameSuffix: String = ""): String {
        val taskName = javaClass.simpleName.let { n ->
            n.indexOf("Main").takeIf { it >= 0 }?.let { n.substring(it + 4).toInt() }
        }
        val taskYear = javaClass.`package`.name.let { p ->
            p.indexOf(".aoc").takeIf { it >= 0 }?.let { p.substring(it + 4).toInt() }
        }
        return "$taskYear/$taskName$nameSuffix.txt"
    }

    fun forEachLine(source: Any, nameSuffix: String = "", action: (String) -> Unit) {
        readTaskFileLinesAsSequence(source, nameSuffix).forEach(action)
    }

    fun readTaskFile(source: Any, nameSuffix: String = ""): String =
        bufferedReader(source.getFileNameForClass(nameSuffix)).use(BufferedReader::readText).replace("\r\n", "\n")

    fun readTaskFileLines(source: Any, nameSuffix: String = ""): List<String> =
        bufferedReader(source.getFileNameForClass(nameSuffix)).readLines()

    fun readTaskFileLinesAsSequence(source: Any, nameSuffix: String = ""): Sequence<String> =
        bufferedReader(source.getFileNameForClass(nameSuffix)).lineSequence()

    private fun bufferedReader(resourceName: String): BufferedReader =
        javaClass.classLoader
            .getResource(resourceName)
            ?.openStream()
            ?.bufferedReader()
            ?: throw FileNotFoundException(resourceName)

    fun DayChallenge.readTaskLines(nameSuffix: String = ""): List<String> = readTaskFileLines(this, nameSuffix)
    fun DayChallenge.readTaskLinesSequence(nameSuffix: String = ""): Sequence<String> = readTaskFileLinesAsSequence(this, nameSuffix)
    fun DayChallenge.readTestTaskLines(): List<String> = readTaskFileLines(this, "-test")
    fun DayChallenge.readTaskFile(nameSuffix: String = ""): String = readTaskFile(this, nameSuffix)
    fun DayChallenge.readTestTaskFile(): String = readTaskFile(this, "-test")
}
