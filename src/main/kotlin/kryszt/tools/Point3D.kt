package kryszt.tools

import kotlin.math.abs

data class Point3D(val x: Int = 0, val y: Int = 0, val z: Int = 0) {
    fun distance(other: Point3D) = abs(x - other.x) + abs(y - other.y) + abs(z - other.z)
    fun moveBy(dx: Int = 0, dy: Int = 0, dz: Int = 0): Point3D = copy(x = x + dx, y = y + dy, z = z + dz)

    fun vectorTo(other: Point3D): Vector3D =
        Vector3D(
            other.x - x,
            other.y - y,
            other.z - z
        )

    operator fun plus(other: Point3D): Point3D = Point3D(x + other.x, y + other.y, z + other.z)
    operator fun plus(vector: Vector3D): Point3D = Point3D(x + vector.x, y + vector.y, z + vector.z)

    fun crossNeighbours(): List<Point3D> =
        listOf(
            this.moveBy(dx = 1),
            this.moveBy(dx = -1),
            this.moveBy(dy = 1),
            this.moveBy(dy = -1),
            this.moveBy(dz = 1),
            this.moveBy(dz = -1),
        )
}
