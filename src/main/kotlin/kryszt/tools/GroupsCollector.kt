package kryszt.tools

abstract class GroupsCollector<T>(
        private val lines: List<String>,
        private val makeGroup: () -> T
) {
    private val allGroups = mutableListOf<T>()
    private var current: T = makeGroup()

    protected abstract fun handleLine(current: T, line: String)

    fun parseData(): List<T> {
        lines.forEach(::nextLine)
        itemReady()
        return allGroups
    }

    private fun nextLine(line: String) {
        if (line.isEmpty()) {
            itemReady()
        } else {
            handleLine(current, line)
        }
    }

    private fun itemReady() {
        allGroups.add(current)
        current = makeGroup()
    }
}