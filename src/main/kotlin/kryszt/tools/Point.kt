package kryszt.tools

import kotlin.math.abs
import kotlin.math.sign

data class Point(val x: Int = 0, val y: Int = 0) {
    fun move(direction: Point): Point = Point(x + direction.x, y + direction.y)
    fun move(dx: Int = 0, dy: Int = 0): Point = Point(x + dx, y + dy)
    fun left(): Point = Point(y, -x)
    fun right(): Point = Point(-y, x)
    fun revert(): Point = Point(-x, -y)

    fun oneUp(): Point = move(Up)
    fun oneDown(): Point = move(Down)
    fun oneLeft(): Point = move(Left)
    fun oneRight(): Point = move(Right)

    fun manhattanDistance(other: Point) = abs(x - other.x) + abs(y - other.y)
    fun asStep(): Point = copy(x = x.sign, y = y.sign)

    operator fun times(times: Int): Point = Point(x * times, y * times)
    operator fun plus(other: Point): Point = Point(x + other.x, y + other.y)
    operator fun minus(other: Point): Point = Point(x - other.x, y - other.y)

    fun crossNeighbours(): List<Point> =
        crossNeighbours.map {
            it.plus(this)
        }

    fun aroundNeighbours(): List<Point> =
        neighbours.map {
            it.plus(this)
        }

    companion object {
        val comparatorXY: Comparator<Point> = compareBy(Point::x).thenBy(Point::y)

        val Zero = Point(0, 0)

        val Up = Point(0, -1)
        val Down = Point(0, 1)
        val Left = Point(-1, 0)
        val Right = Point(1, 0)

        val UpLeft = Point(-1, -1)
        val UpRight = Point(1, -1)
        val DownLeft = Point(-1, 1)
        val DownRight = Point(1, 1)

        val crossNeighbours = listOf(
            Up,
            Right,
            Down,
            Left
        )
        val neighbours = listOf(
            Up, UpRight,
            Right, DownRight,
            Down, DownLeft,
            Left, UpLeft
        )
    }

}
