package kryszt.tools

abstract class BasicProcessor(val instructions: List<MutableList<String>>) {

    val registers = mutableMapOf<String, Int>()
    var nextInstruction: Long = 0

    var steps = 0

    /**
     * @return True if can make another move
     */
    fun step(): Boolean {
        steps++
        val currentInstruction = instructions[nextInstruction.toInt()]

        val action = currentInstruction[0]
        val args = currentInstruction.subList(1, currentInstruction.size)
        if (executeInstruction(action, args)) {
            nextInstruction++
        }
        return canStep()
    }

    fun canStep(): Boolean {
        return ((nextInstruction >= -1) && (nextInstruction < instructions.size))
    }

    /**
     * @return True if should increase instruction index
     */
    open fun executeInstruction(order: String, args: List<String>): Boolean {
        return true
    }

    fun movePointer(move: Int) {
        nextInstruction += move
    }

    fun isRegister(arg: String): Boolean {
        return arg.toIntOrNull() == null
    }

    fun write(register: String, value: Int) {
        registers[register] = value
    }

    fun valueOrRegister(argument: String): Int {
        return argument.toIntOrNull() ?: registers[argument] ?: 0
    }

}