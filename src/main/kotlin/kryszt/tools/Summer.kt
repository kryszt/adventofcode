package kryszt.tools

class Summer {

    private val counters = mutableMapOf<Char, SummerValue>()

    fun nextChar(char: Char) {
        counters.getOrPut(char, { SummerValue(char) }).count++
    }

    fun sorted(): List<SummerValue> = counters.values.sorted()
}

data class SummerValue(val char: Char, var count: Int = 0) : Comparable<SummerValue> {

    override fun compareTo(other: SummerValue): Int {
        return if (this.count == other.count) {
            char - other.char
        } else {
            other.count - this.count
        }
    }
}