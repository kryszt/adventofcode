package kryszt.tools

import java.util.regex.Pattern

abstract class ActionProcessor(private val actionPatterns: List<ActionPattern>) {

    open fun executeLine(line: String) {
        execute(findAction(line))
    }

    abstract fun execute(action: Action)

    fun findAction(line: String): Action {
        actionPatterns.forEach {
            parsePattern(line, it)?.let { return it }
        }
        throw IllegalArgumentException("Cannot parse [$line]")
    }

    fun parsePattern(line: String, pattern: ActionPattern): Action? {
        return pattern.pattern.matcher(line).takeIf { it.find() }?.let {
            val args = mutableListOf<String>()
            for (i in 1..it.groupCount()) {
                args.add(it.group(i))
            }
            Action(pattern.action, args)
        }
    }

    data class ActionPattern(val action: String, val pattern: Pattern)
    data class Action(val action: String, val args: List<String>)

    companion object {

        const val GroupNumber = "([-]{0,1}[0-9]+)"
        const val GroupAny = "([0-9a-zA-Z]+)"
        const val GroupText = "([a-zA-Z]+)"

    }
}