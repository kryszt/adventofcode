package kryszt.tools

interface LineHandler {
    fun handleLine(line: String)
}