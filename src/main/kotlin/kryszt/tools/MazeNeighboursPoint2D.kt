package kryszt.tools

class MazeNeighboursPoint2D(val points: Set<Point>) : MazeNeighbours<Point> {

    override fun neighbours(place: Point): List<Point> {
        return Point
            .crossNeighbours
            .map { place.plus(it) }
            .filter { points.contains(it) }
    }
}