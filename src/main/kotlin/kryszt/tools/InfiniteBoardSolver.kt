package kryszt.tools

import java.util.concurrent.atomic.AtomicInteger

class InfiniteBoardSolver<T>(
    private val rule: (isAlive: Boolean, aliveNeighbours: Int) -> Boolean,
    private val makeNeighbours: (T) -> List<T>
) {

    fun iterateCells(cells: Set<T>, repeats: Int): Set<T> {
        var aliveCells = cells
        repeat(repeats) {
            aliveCells = iterateCells(aliveCells)
        }
        return aliveCells
    }

    //rules:
    //active - stays active with 2 or 3
    //inactive - becomes active with 3
    fun iterateCells(cells: Set<T>): Set<T> {
        val counts = neighbourCounts(cells)
        val newAlive = mutableSetOf<T>()

        counts.forEach { (place, count) ->
            if (rule(cells.contains(place), count.get())) {
                newAlive.add(place)
            }
        }

        return newAlive
    }

    private fun neighbourCounts(cells: Set<T>): Map<T, AtomicInteger> {
        val neighbourCount = mutableMapOf<T, AtomicInteger>()
        cells.forEach { point ->
            makeNeighbours(point).forEach { n ->
                neighbourCount.getOrPut(n) { AtomicInteger(0) }.incrementAndGet()
            }
        }
        return neighbourCount
    }

}