package kryszt.tools

fun <T : Comparable<T>> ClosedRange<T>.contains(other: ClosedRange<T>): Boolean =
    this.contains(other.start) && this.contains(other.endInclusive)

fun <T : Comparable<T>> ClosedRange<T>.intersects(other: ClosedRange<T>): Boolean =
    other.start <= this.endInclusive && other.endInclusive >= this.start

fun <T : Comparable<T>> ClosedRange<T>.intersection(other: ClosedRange<T>): ClosedRange<T>? {
    val left = maxOf(this.start, other.start)
    val right = minOf(this.endInclusive, other.endInclusive)

    if (left <= right) return left..right
    return null
}

fun ClosedRange<Int>.asIntRange(): IntRange = this.start..this.endInclusive
fun IntRange.width(): Long = (endInclusive - start + 1L)
fun LongRange.width(): Long = (endInclusive - start + 1L)

