package kryszt.tools

data class Vector3D(val x: Int = 0, val y: Int = 0, val z: Int = 0)