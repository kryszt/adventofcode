package kryszt.tools

class MazeTarget<T>(val target: T) : MazeEnd<T> {

    override fun isEnd(place: T) = target == place
}