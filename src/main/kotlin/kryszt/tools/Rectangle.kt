package kryszt.tools

data class Rectangle(val x: Int, val y: Int, val width: Int, val height: Int) {
    val right = x + width - 1
    val bottom = y + height - 1

    val left = x
    val top = y

    fun contains(point: Point): Boolean =
        point.x in left..right && point.y in top..bottom
}