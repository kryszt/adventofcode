package kryszt.tools

object BinarySearch {

    fun binarySearchSolution(condition: (Int) -> Boolean): Int {
        var valueToCheck = 1

        var largestFalse = 0
        var smallestTrue = -1

        while (largestFalse + 1 != smallestTrue) {
            val current = valueToCheck
            val isTrueAtCurrent = condition(current)
            if (isTrueAtCurrent) {
                smallestTrue = current
                valueToCheck = (largestFalse + smallestTrue) / 2
            } else {
                largestFalse = current
                valueToCheck = if (smallestTrue < 0) {
                    current * 2
                } else {
                    (largestFalse + smallestTrue) / 2
                }
            }
        }
        return smallestTrue
    }

}