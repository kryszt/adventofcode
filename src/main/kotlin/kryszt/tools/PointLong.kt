package kryszt.tools

data class PointLong(val x: Long = 0, val y: Long = 0) {
    constructor(x: Int, y: Int) : this(x.toLong(), y.toLong())

    fun move(dx: Long = 0, dy: Long = 0): PointLong = PointLong(x + dx, y + dy)
}