package kryszt.tools

interface MazeNeighbours<T> {
    fun neighbours(place: T): List<T>
}