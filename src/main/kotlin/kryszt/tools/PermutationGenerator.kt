package kryszt.tools

class PermutationGenerator<K>(val values: List<K>) {

    val count = values.size
    val indexes: IntArray = IntArray(count, { it })
    var hasNext = true

    fun getNext(): List<K> {
        val next = indexes.map { values[it] }
        hasNext = nextPass()
        return next
    }

    private fun nextPass(): Boolean {
        return nextPassAt(count - 1)
    }

    private fun nextPassAt(at: Int): Boolean {
        if (at < 0) {
            return false
        }
        val start = indexes[at]
        for (i in (start + 1) until count) {
            if (canUse(i, at)) {
                indexes[at] = i
                return true
            }
        }
        //if cannot increase, update at left
        if (nextPassAt(at - 1)) {
            for (i in 0 until count) {
                if (canUse(i, at)) {
                    indexes[at] = i
                    return true
                }
            }
        }

        return false
    }

    private fun canUse(index: Int, at: Int): Boolean {
        for (i in 0 until at) {
            if (index == indexes[i])
                return false
        }
        return true
    }

}