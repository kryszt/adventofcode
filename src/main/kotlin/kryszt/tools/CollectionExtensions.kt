package kryszt.tools

import java.util.concurrent.atomic.AtomicInteger

fun <T> MutableMap<T, AtomicInteger>.addCount(key: T) =
    this.getOrPut(key) { AtomicInteger(0) }
        .incrementAndGet()

fun <T> MutableMap<T, AtomicInteger>.addCount(key: T, value: Int) =
    this.getOrPut(key) { AtomicInteger(0) }
        .addAndGet(value)

fun Collection<Point>.bounds(): Rectangle {
    val left: Int = this.minOfOrNull { it.x }?: 0
    val right: Int = this.maxOfOrNull { it.x } ?: 0
    val top: Int = this.minOfOrNull { it.y } ?: 0
    val bottom: Int = this.maxOfOrNull { it.y } ?: 0

    return Rectangle(
        x = left,
        y = top,
        width = right - left + 1,
        height = bottom - top + 1
    )
}

fun Set<Point>.printBoard(onPresent: Char = '#', onEmpty: Char = '.') {
    val left: Int = this.minByOrNull { it.x }?.x ?: 0
    val right: Int = this.maxByOrNull { it.x }?.x ?: 0
    val top: Int = this.minByOrNull { it.y }?.y ?: 0
    val bottom: Int = this.maxByOrNull { it.y }?.y ?: 0

    (top..bottom).forEach { y ->
        (left..right).forEach { x ->
            val c = if (contains(Point(x, y))) onPresent else onEmpty
            print(c)
        }
        println()
    }
}

fun <T> Map<Point, T>.printBoard(printItem: (T?) -> String) {
    val left: Int = keys.minByOrNull { it.x }?.x ?: 0
    val right: Int = keys.maxByOrNull { it.x }?.x ?: 0
    val top: Int = keys.minByOrNull { it.y }?.y ?: 0
    val bottom: Int = keys.maxByOrNull { it.y }?.y ?: 0

    (top..bottom).forEach { y ->
        (left..right).forEach { x ->
            print(printItem(this[Point(x, y)]))
        }
        println()
    }
}

fun <T> List<T>.asRepeatingSequence(): Sequence<T> {
    var position = 0
    return generateSequence { this[position++ % this.size] }
}

fun String.asRepeatingSequence(): Sequence<Char> {
    var position = 0
    return generateSequence { this[position++ % this.length] }
}

fun List<Long>.multiply(): Long = this.reduce { acc, l -> acc * l }