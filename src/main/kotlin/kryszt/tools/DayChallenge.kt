package kryszt.tools

import kotlin.time.measureTime

interface DayChallenge {
    fun taskOne()

    fun taskTwo()

    fun printTasks() {
        println("Task 1")
        val time1 = measureTime {
            taskOne()
        }
        println("Took ${time1.inWholeMilliseconds}ms\n")
        println("\nTask 2")
        val time2 = measureTime {
            taskTwo()
        }
        println("Took ${time2.inWholeMilliseconds}ms\n")
    }
}
