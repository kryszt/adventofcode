package kryszt.tools

data class MazePath<T>(val path: List<T>) {
    constructor(firstItem: T) : this(listOf(firstItem))

    fun withStep(step: T) = copy(path = path.plus(step))

    val length: Int = path.size

    val current: T
        get() = path.last()
}