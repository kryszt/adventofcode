package kryszt.tools

import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter

object MD5Util {

    val digest = MessageDigest.getInstance("MD5")

    fun hashedHexString(input: String): String {
        return digest.digest(input.toByteArray()).let { DatatypeConverter.printHexBinary(it).toLowerCase() }
    }

}