package kryszt.tools

import kotlin.math.abs

data class Point4D(val x: Int = 0, val y: Int = 0, val z: Int = 0, val t: Int = 0) {
    fun distance(other: Point4D) = abs(x - other.x) + abs(y - other.y) + abs(z - other.z) + abs(t - other.t)

    operator fun plus(other: Point4D): Point4D = Point4D(x + other.x, y + other.y, z + other.z, t + other.t)
}
