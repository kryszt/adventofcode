package kryszt.tools

import java.util.concurrent.atomic.AtomicLong

fun AtomicLong.printMillis() = get().div(1_000_000).also { println("$it ms") }

fun <T> measured(timer: AtomicLong, action: () -> T): T {
    val start = System.nanoTime()
    try {
        return action()
    } finally {
        val end = System.nanoTime()
        timer.addAndGet(end - start)
    }
}
