package kryszt.tools

object Primes {


    @JvmStatic
    fun main(args: Array<String>) {
        val primes = mutableListOf(2)

        val start = System.currentTimeMillis()
//        for (i in 3..1_000_000) {
        for (i in 3..360) {
            val isPrime = isPrime(i, primes)
//            println("$i prime: $isPrime")
            if (isPrime) {
                primes.add(i)
            }
        }
        val end = System.currentTimeMillis()
        println("Primes: ${primes.size} in ${end - start}ms")

        val primeCount = (108100..125100 step 17).count { !isPrime(it, primes) }
        println("Found: $primeCount")
    }

    fun isPrime(value: Int, primes: List<Int>): Boolean {
        primes.forEach { prime ->
            if (prime * prime > value) {
                return true
            } else if (value % prime == 0) {
                return false
            }
        }
        return true
    }
}
