package kryszt.tools

abstract class Solver<State> {
//
//    fun checkAllMovesDeep(currentState: State): State? {
//        visiting(currentState)
//        if (isEnd(currentState)) {
//            return currentState
//        }
//        return selectMoves(currentState).map { checkAllMovesDeep(it) }.minByOrNull { it?.path?.length ?: 0 }
//    }

    var stepsCount = 0

    fun solve(states: Collection<State>): State? {
        if (states.isEmpty()) {
            return null
        }
        states.forEach { visiting(it) }
        states.find { isEnd(it) }?.let {
            return it
        }
        stepsCount++
        val nextLevel = mutableListOf<State>()

        states.forEach {
            val possibleMoves = selectMoves(it)
            possibleMoves.forEach { visiting(it) }
            nextLevel.addAll(possibleMoves)
        }
        return solve(nextLevel)
    }

    abstract fun visiting(state: State)

    abstract fun wasVisited(state: State): Boolean

    abstract fun isEnd(state: State): Boolean

    abstract fun selectMoves(state: State): Collection<State>

    abstract fun allowed(state: State): Boolean
}
