package kryszt.tools

class BreadthFirstMazeSolver<T>(
    val neighbours: (T) -> List<T>,
    val isEnd: (T) -> Boolean
) {

    constructor(neighbours: MazeNeighbours<T>, target: MazeEnd<T>)
            : this(neighbours::neighbours, target::isEnd)

    val visited = mutableSetOf<T>()

    fun checkAllMovesWide(start: T): MazePath<T>? = checkAllMovesWideIterative(listOf(MazePath(start)))

    private fun checkAllMovesWideIterative(current: List<MazePath<T>>): MazePath<T>? {
        var nextToCheck = current

        while (true) {
            nextToCheck.find { isEnd(it.current) }?.run { return this }
            if (nextToCheck.isEmpty()) return null

            visited.addAll(nextToCheck.map { it.current })

            nextToCheck = nextToCheck
                .map { path ->
                    neighbours(path.current)
                        .filterNot { n -> visited.contains(n) }
                        .map {
                            path.withStep(it)
                        }
                }
                .flatten()
                .distinct()
        }
    }

}