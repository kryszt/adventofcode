package kryszt.tools

import java.math.BigInteger
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.math.absoluteValue

fun Int.gcd(other: Int): Int {
    var n1 = this.absoluteValue
    var n2 = other.absoluteValue

    if (n1 == 0 || n2 == 0) return 0

    while (n1 != n2) {
        if (n1 > n2) {
            n1 -= n2
        } else {
            n2 -= n1
        }
    }
    return n1
}

fun Long.gcd(other: Long): Long {
    var n1 = this.absoluteValue
    var n2 = other.absoluteValue

    if (n1 == 0L || n2 == 0L) return 0

    while (n1 != n2) {
        if (n1 > n2) {
            n1 = multiMinus(n1, n2)
        } else {
            n2 = multiMinus(n2, n1)
        }
    }
    return n1
}

fun List<BigInteger>.gcdBI(): BigInteger =
    this
        .filterNot { it == BigInteger.ZERO }
        .takeUnless { it.isEmpty() }
        ?.reduce { acc: BigInteger, l: BigInteger -> acc.gcd(l) }
        ?: BigInteger.ZERO

private fun multiMinus(larger: Long, smaller: Long): Long {
    require(larger >= smaller) { " $larger is smaller than $smaller " }

    val m = larger / smaller
    return when (larger % smaller) {
        0L -> smaller
        else -> larger - (m * smaller)
    }
}

fun Long.lcm(other: Long): Long {
    val c = this.gcd(other)
    val a = this / c
    return a * other
}

fun BigInteger.lcm(other: BigInteger): BigInteger {
    val c = this.gcd(other)
    val a = this / c
    return a * other
}

fun List<Long>.lcm(): Long {
    if (isEmpty()) return 0
    if (size == 1) return get(0)

    var lcm = get(0)
    drop(1).forEach { other ->
        lcm = lcm.lcm(other)
    }
    return lcm
}

fun LongRange.translate(diff: Long): LongRange = (this.first + diff)..(this.last + diff)

fun Int.coPrime(other: Int): Boolean = gcd(other) == 1

fun <K> Map<Point, K>.printMap(asString: (Point, K?) -> String) {
    val left = keys.minByOrNull { it.x }?.x ?: 0
    val right = keys.maxByOrNull { it.x }?.x ?: 0
    val top = keys.minByOrNull { it.y }?.y ?: 0
    val bottom = keys.maxByOrNull { it.y }?.y ?: 0

    for (y in (top - 1)..(bottom + 1)) {
        for (x in (left - 1)..(right + 1)) {
            val point = Point(x, y)
            print(asString(point, get(point)))
        }
        println()
    }
}

fun <T> T.alsoPrint(prefix: String = ""): T = also { println(prefix + it) }
fun <C : Iterable<T>, T> C.alsoPrintOnEach(): C = onEach(::println)

fun <T> List<String>.parseWith(pattern: Pattern, parser: (Matcher) -> T): List<T> =
    map { line -> line.parseWith(pattern, parser) }

fun Matcher.intAt(index: Int): Int = group(index).toInt()

fun <T> String.parseWith(pattern: Pattern, parser: (Matcher) -> T): T =
    pattern
        .matcher(this)
        .takeIf { it.find() }
        ?.let { parser(it) }
        ?: throw IllegalArgumentException("$this doesn't match $pattern")

fun Long.remAbs(divider: Long) = rem(divider).plus(divider).rem(divider)
fun Int.remAbs(divider: Long) = rem(divider).plus(divider).rem(divider)

fun Int.isEven(): Boolean = this.mod(2) == 0
fun Int.isOdd(): Boolean = this.mod(2) != 0
fun Long.isEven(): Boolean = this.mod(2) == 0
fun Long.isOdd(): Boolean = this.mod(2) != 0
fun Long.sqr(): Long = this * this
fun Long.sqrt(): Long = kotlin.math.sqrt(this.toDouble()).toLong()

fun String.numberAtEnd(): Int = this.takeLastWhile { it.isDigit() }.toInt()
fun String.numberAtEndOption(): Int? = this.takeLastWhile { it.isDigit() }.toIntOrNull()
fun String.lastWord(): String = this.takeLastWhile { it != ' ' }
fun Int.dividesBy(other: Int): Boolean = (this % other) == 0
fun Long.dividesBy(other: Long): Boolean = (this % other) == 0L
fun Long.dividesBy(other: Int): Boolean = (this % other) == 0L

fun Int.roofDiv(other: Int): Int = if (this.dividesBy(other)) this / other else 1 + this / other
