package kryszt.tools

interface MazeEnd<T> {
    fun isEnd(place: T): Boolean
}