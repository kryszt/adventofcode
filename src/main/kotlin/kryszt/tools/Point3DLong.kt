package kryszt.tools

data class Point3DLong(val x: Long = 0, val y: Long = 0, val z: Long = 0) {
    constructor(x: Int, y: Int, z: Int) : this(x.toLong(), y.toLong(), z.toLong())

    fun move(dx: Long = 0, dy: Long = 0, dz: Long = 0): Point3DLong = Point3DLong(x + dx, y + dy, z + dz)
}