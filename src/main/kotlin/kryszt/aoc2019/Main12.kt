package kryszt.aoc2019

import kryszt.tools.Point3D
import kryszt.tools.lcm
import kotlin.math.absoluteValue

object Main12 {

    private val taskPositions = listOf(
            Point3D(x = 1, y = 2, z = -9),
            Point3D(x = -1, y = -9, z = -4),
            Point3D(x = 17, y = 6, z = 8),
            Point3D(x = 12, y = 4, z = 2)
    )
    private val testPositions1 = listOf(
            Point3D(x = -1, y = 0, z = 2),
            Point3D(x = 2, y = -10, z = -7),
            Point3D(x = 4, y = -8, z = 8),
            Point3D(x = 3, y = 5, z = -1)
    )

    private val testPositions2 = listOf(
            Point3D(x = -8, y = -10, z = 0),
            Point3D(x = 5, y = 5, z = 10),
            Point3D(x = 2, y = -7, z = 3),
            Point3D(x = 9, y = -8, z = -3)
    )

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun makeMoons(): List<Moon> =
            taskPositions.map { Moon(it, Point3D()) }

    private fun task1(): Int {
        val moons = makeMoons()
        repeat(1000) {
            step(moons)
        }
        moons.print()
        return moons.totalEnergy()
    }

    private fun task2(): Long {

        val cycles = mutableListOf<Long>()

        val keySelectors = listOf(Point3D::x, Point3D::y, Point3D::z)
        for (k in keySelectors) {
            val moons = makeMoons()
            val repeats = findCycleLength(moons, k)
            cycles.add(repeats.toLong())
        }

        return cycles.lcm()
    }

    private fun findCycleLength(moons: List<Moon>, coordinateSelector: (Point3D) -> Int): Int {
        fun selectValues() = moons.map { coordinateSelector(it.position) }
        fun allZeros(): Boolean = moons.all { coordinateSelector(it.velocity) == 0 }

        var steps = 0
        val startPosition = selectValues()

        while (true) {
            steps++
            step(moons)
            if (!allZeros()) {
                continue
            }
            val position = selectValues()
            if (position == startPosition) {
                return steps
            }
        }
    }

    private fun step(moons: List<Moon>) {
        applyGravity(moons)
        applyVelocity(moons)
    }

    private fun applyGravity(moons: List<Moon>) {
        for (i in 0..moons.lastIndex) {
            for (j in (i + 1)..moons.lastIndex) {
                applyGravity(moons[i], moons[j])
            }
        }
    }

    private fun applyGravity(moon1: Moon, moon2: Moon) {
        val dx = velocityDiff(moon1.position.x, moon2.position.x)
        val dy = velocityDiff(moon1.position.y, moon2.position.y)
        val dz = velocityDiff(moon1.position.z, moon2.position.z)

        moon1.velocity = moon1.velocity.moveBy(dx, dy, dz)
        moon2.velocity = moon2.velocity.moveBy(-dx, -dy, -dz)
    }

    private fun velocityDiff(v1: Int, v2: Int): Int = when {
        v1 < v2 -> 1
        v1 > v2 -> -1
        else -> 0
    }

    private fun applyVelocity(moons: List<Moon>) {
        moons.forEach {
            it.position = it.position + it.velocity
        }
    }
}

private fun List<Moon>.print() =
        forEach(::println)

private data class Moon(var position: Point3D, var velocity: Point3D)

private fun Point3D.energy() = x.absoluteValue + y.absoluteValue + z.absoluteValue
private fun Moon.energy() = position.energy() * velocity.energy()
private fun List<Moon>.totalEnergy() = sumBy(Moon::energy)

