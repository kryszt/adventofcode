package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.tools.IoUtil

object Main5 {

    @JvmStatic
    fun main(args: Array<String>) {

        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): Long {
        val orders = IoUtil.readTaskFile(this).split(",").map(String::toLong).toMutableList()
//        val orders = "3,0,4,0,99".split(",").map(String::toInt).toMutableList()
        val codeMachine = IntCodeMachine(orders, listOf(1L))
        return codeMachine.execute().also { println(it) }.last().toLong()
    }

    private fun task2(): Long {
        val orders = IoUtil.readTaskFile(this).split(",").map(String::toLong).toMutableList()
//        val orders = "3,9,8,9,10,9,4,9,99,-1,8".split(",").map(String::toInt).toMutableList()
//        val orders = "3,9,7,9,10,9,4,9,99,-1,8".split(",").map(String::toInt).toMutableList()
//        val orders = "3,3,1108,-1,8,3,4,3,99".split(",").map(String::toInt).toMutableList()
//        val orders = "3,3,1107,-1,8,3,4,3,99".split(",").map(String::toInt).toMutableList()
//        val orders = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9".split(",").map(String::toInt).toMutableList()
//        val orders = "3,3,1105,-1,9,1101,0,0,12,4,12,99,1".split(",").map(String::toInt).toMutableList()
//        val orders = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99".split(",").map(String::toInt).toMutableList()

        return IntCodeMachine(orders, listOf(5L)).execute().also { println(it) }.last().toLong()
    }
}