package kryszt.aoc2019

import kryszt.tools.IoUtil
import kotlin.math.absoluteValue

object Main16 {

    private val pattern = intArrayOf(0, 1, 0, -1)

    private val input = IoUtil.readTaskFile(this).map { it.toInt() - '0'.toInt() }.toIntArray()
//    private val input = "80871224585914546619083218645595".map { it.toInt() - '0'.toInt() }.toIntArray()
//    private val input = "19617804207202209144916044189917".map { it.toInt() - '0'.toInt() }.toIntArray()
//    private val input = "69317163492948606335995924319873".map { it.toInt() - '0'.toInt() }.toIntArray()

    @JvmStatic
    fun main(args: Array<String>) {
        println(input.size)
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): String {
        var newOne = input
        println(newOne.joinToString(""))
        repeat(100) {
            newOne = nextSignal(newOne)
        }
        return newOne.take(8).joinToString(separator = "")
    }

    private val inputTask2 = IoUtil.readTaskFile(this)
//    private val inputTask2 = "03036732577212944063491565474664"
//    private val inputTask2 = "02935109699940807407585447034323"
//    private val inputTask2 = "03081770884921959731165446850517"

    private const val REPEATS = 10_000

    private fun task2(): String {
        val input = inputTask2
        val skipValue = input.take(7).toInt()
        if (skipValue < (input.length * REPEATS / 2)) {
            throw IllegalArgumentException("We are skipping too little to make it countable")
        }

        val bytes = input.map { (it.toInt() - '0'.toInt()) }.toIntArray()

        var nextArray = buildInitialPart(skipValue, bytes)

        repeat(100) {
            nextArray = makeNextRow(nextArray)
        }

        //last half of values is just a sum%10 of last values from previous list
        return nextArray.take(8).joinToString(separator = "")
    }

    private fun buildInitialPart(skipValue: Int, originalValues: IntArray): IntArray {
        val inputSize = originalValues.size
        val originalSize = inputSize * REPEATS - skipValue
        val baseArray = IntArray(originalSize)
        baseArray.indices.forEach { index ->
            val originalIndex = (index + skipValue) % inputSize
            baseArray[index] = originalValues[originalIndex]
        }
        return baseArray
    }

    private fun makeNextRow(source: IntArray): IntArray {
        val nextRow = IntArray(source.size)
        var sum = 0

        nextRow.indices.reversed().forEach { index ->
            sum += source[index]
            nextRow[index] = (sum % 10)
        }
        return nextRow
    }

    private fun nextSignal(signal: IntArray): IntArray {
        val next = IntArray(signal.size)
        signal.forEachIndexed { index, _ ->
            next[index] = nextValue(signal, index + 1)
        }
        return next
    }

    private fun nextValue(signal: IntArray, repeat: Int): Int {
        var sum = 0
        signal.forEachIndexed { index, i ->
            val patternIndex = ((index + 1) / repeat) % pattern.size
            val patternValue = pattern[patternIndex]
            sum += (i * patternValue)
        }
        return sum.absoluteValue.rem(10)
    }

}