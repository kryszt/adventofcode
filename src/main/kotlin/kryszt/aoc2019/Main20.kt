package kryszt.aoc2019

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines
import java.lang.IllegalStateException
import kotlin.math.abs
import kotlin.math.max

object Main20 : DayChallenge {

    private fun readData(): List<String> = readTaskLines()

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        val tiles = allTiles()
        val portals = findPortals()
        val startPoint =
            portals.find { it.name == "AA" }?.up ?: throw IllegalStateException("No 'AA' tile")
        val endPoint =
            portals.find { it.name == "ZZ" }?.up ?: throw IllegalStateException("No 'ZZ' tile")

        val solver = BreadthFirstMazeSolver(Neighbours(tiles.toSet(), portals)::neighbours, EndRule(endPoint)::isEnd)
        solver.checkAllMovesWide(startPoint)
            ?.path
            ?.size?.minus(1).alsoPrint()
    }

    override fun taskTwo() {
        val tiles = allTiles()
        val portals = findPortals()

        val startPoint =
            portals.find { it.name == "AA" }?.up?.to3D() ?: throw IllegalStateException("No 'AA' tile")
        val endPoint =
            portals.find { it.name == "ZZ" }?.up?.to3D() ?: throw IllegalStateException("No 'ZZ' tile")

        val solver =
            BreadthFirstMazeSolver(
                NeighboursPartTwo(tiles.toSet(), portals)::neighbours,
                EndRulePartTwo(endPoint)::isEnd
            )

        val path = solver.checkAllMovesWide(startPoint)
        path?.path?.size?.minus(1).alsoPrint()
    }

    private class Neighbours(private val tilePoints: Set<Point>, private val portals: List<Portal>) {

        fun neighbours(spot: Point): List<Point> {
            val nextTiles = Point.crossNeighbours.map { it.plus(spot) }.filter { tilePoints.contains(it) }
            val behindPortal: List<Point> = portals
                .filter { it.connectedTo(spot) }
                .mapNotNull { it.other(spot) }
            return nextTiles.plus(behindPortal)
        }
    }

    private class NeighboursPartTwo(private val tilePoints: Set<Point>, portals: List<Portal>) {

        private val upPortals = portals.filter { it.up != null }
            .associateBy { it.up }

        private val downPortals = portals.filter { it.down != null }
            .associateBy { it.down }

        fun neighbours(spot: Point3D): List<Point3D> {
            val spot2D = spot.to2D()
            val nextTiles = Point
                .crossNeighbours
                .map { it.plus(spot2D) }
                .filter { tilePoints.contains(it) }
                .map { it.to3D(z = spot.z) }

            upPortals[spot2D]
                ?.takeUnless { spot.z >= 0 }
                ?.down
                ?.to3D(spot.z + 1)
                ?.run { return nextTiles.plus(this) }

            downPortals[spot2D]
                ?.up
                ?.to3D(spot.z - 1)
                ?.run { return nextTiles.plus(this) }

            return nextTiles
        }
    }

    private class EndRule(private val endPoint: Point) {
        fun isEnd(spot: Point): Boolean = spot == endPoint
    }

    private class EndRulePartTwo(private val endPoint: Point3D) {
        fun isEnd(spot: Point3D): Boolean = spot.z == 0 && spot == endPoint
    }

    private fun allTiles(): List<Point> {
        return readData()
            .mapIndexed { y, line ->
                line.mapIndexedNotNull { x, c ->
                    if (c == '.') Point(x, y) else null
                }
            }.flatten()
    }

    private fun findPortals(): List<Portal> {
        val data = readData()
        val midPoint = Point(data.map(String::length).maxOrNull()?.div(2) ?: 0, data.size / 2)
        return data
            .asSequence()
            .mapIndexed { y, line ->
                line.indices.mapNotNull { x ->
                    takePortalData(Point(x, y), data)
                }
            }.flatten()
            .groupBy { it.name }
            .map { PortalData(it.key, it.value.map(PortalData::ends).flatten()) }
            .map { it.asPortal(midPoint) }
            .toList()
    }

    private fun PortalData.asPortal(midPoint: Point): Portal {
        if (ends.size == 1) {
            return Portal(name, up = ends.first(), down = null)
        }
        val inner = ends.minByOrNull { it.maxDistance(midPoint) }!!
        val outer = ends.maxByOrNull { it.maxDistance(midPoint) }!!
        return Portal(name, up = outer, down = inner)
    }

    private fun Point.maxDistance(other: Point): Int {
        return max(abs(other.x - x), abs(other.y - y))
    }

    private fun takePortalData(point: Point, data: List<String>): PortalData? =
        takePortalData(point, Point.Right, data) ?: takePortalData(point, Point.Down, data)

    private fun takePortalData(point: Point, direction: Point, data: List<String>): PortalData? {
        val first = data.at(point)?.takeIf { it.isUpperCase() } ?: return null
        val second = data.at(point.plus(direction))?.takeIf { it.isUpperCase() } ?: return null
        val entry = point.plus(direction.times(2)).takeIf { data.at(it) == '.' }
            ?: point.minus(direction).takeIf { data.at(it) == '.' } ?: return null
        return PortalData("$first$second", listOf(entry))
    }

    private fun List<String>.at(point: Point): Char? = this.getOrNull(point.y)?.getOrNull(point.x)
    data class Portal(val name: String, val up: Point?, val down: Point?) {
        fun connectedTo(point: Point): Boolean = up == point || down == point
        fun other(point: Point): Point? = if (point == up) down else up
    }

    private data class PortalData(val name: String, val ends: List<Point>)

    private fun Point.to3D(z: Int = 0) = Point3D(x, y, z)
    private fun Point3D.to2D() = Point(x, y)
}
