package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.tools.IoUtil

object Main9 {

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): Long {
        return IntCodeMachine(getOrders(), mutableListOf(1L)).execute().first().longValueExact()
    }

    private fun task2(): Long {
        return IntCodeMachine(getOrders(), mutableListOf(2L)).execute().first().longValueExact()
    }

    private fun getOrders(): List<Long> {
        val input = IoUtil.readTaskFile(this)
//        val input = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"
//        val input = "1102,34915192,34915192,7,4,7,99,0"
//        val input = "104,1125899906842624,99"
//        val input = "109,2000,109,19,203,-34,99"
        return input.split(",").map(String::toLong)
    }

}