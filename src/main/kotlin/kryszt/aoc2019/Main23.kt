package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.aoc2019.machine.PushableInput
import kryszt.aoc2019.machine.getMachineOrders
import java.math.BigInteger
import java.util.*

object Main23 {

    private val machineOrders: List<Long>
        get() = getMachineOrders(this)


    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    fun task1(): BigInteger {
        val hub = CentalHub(50) { machineOrders }
        while (hub.natRegister == null) {
            hub.step()
        }
        return hub.natRegister!!.y
    }

    fun task2(): BigInteger {
        var lastDeliveredY: BigInteger? = null

        val hub = CentalHub(50) { machineOrders }
        while (true) {
            hub.step()
            if (hub.allIdle()) {
                val newY = hub.natRegister!!.y
                if (newY == lastDeliveredY) {
                    return newY
                }
                lastDeliveredY = newY
                hub.deliverNAT()
            }
        }
    }

    class CentalHub(size: Int, ordersSource: () -> List<Long>) {

        var natRegister: NatRegister? = null
            private set
        var result: BigInteger? = null
            private set

        private val inputs = List(size) { index ->
            MonitoredInput(index)
        }

        private val outputs = List(size) {
            PacketReceiver()
        }

        private val machines = List(size) { index ->
            IntCodeMachine(ordersSource(), inputs[index], outputs[index])
        }

        fun step() {
            machines.forEach { it.step() }
        }

        fun allIdle(): Boolean = inputs.all { it.isIdle() }

        fun deliverNAT() {
            natRegister!!.let { register ->
                inputs[0].apply {
                    pushNextValue(register.x)
                    pushNextValue(register.y)
                }
            }
        }

        internal fun deliver(address: Int, x: BigInteger, y: BigInteger) {
            if (address == 255) {
                natRegister = NatRegister(x, y)
            } else {
                inputs[address].apply {
                    pushNextValue(x)
                    pushNextValue(y)
                }
            }
        }

        inner class PacketReceiver
            : IntCodeMachine.MachineOutput<BigInteger> {

            private var target: BigInteger? = null
            private var valueX: BigInteger? = null
            private var valueY: BigInteger? = null

            override fun newValue(output: BigInteger) {
                when {
                    target == null -> target = output
                    valueX == null -> valueX = output
                    valueY == null -> {
                        valueY = output
                        deliver()
                        clear()
                    }
                }
            }

            private fun deliver() {
                deliver(target!!.intValueExact(), valueX!!, valueY!!)
            }

            private fun clear() {
                target = null
                valueY = null
                valueX = null
            }
        }

        class MonitoredInput(address: Int) : PushableInput<BigInteger> {

            private val defaultValue = BigInteger.valueOf(-1L)
            private val idleThreshold = 3

            private var requestedOnEmpty: Int = 0

            private val buffer: LinkedList<BigInteger> = LinkedList(listOf(address.toBigInteger()))

            fun isIdle(): Boolean {
                return buffer.isEmpty() && requestedOnEmpty > idleThreshold
            }

            override fun pushNextValue(nextValue: BigInteger) {
                buffer.add(nextValue)
            }

            override fun nextValue(): BigInteger {
                val next = buffer.poll()
                if (next == null) {
                    requestedOnEmpty++
                } else {
                    requestedOnEmpty = 0
                }
                return next ?: defaultValue
            }
        }
    }

    data class NatRegister(val x: BigInteger, val y: BigInteger)
}