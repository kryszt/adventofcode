package kryszt.aoc2019

object Main4 {

    @JvmStatic
    fun main(args: Array<String>) {
        //402328-864247
        task1()
        task2()
    }

    private fun task1() {
        (444444..799999)
                .asSequence()
                .filter { number -> number.split().let { it.notDecreasing() && it.adjacentPair() } }
                .count().also { println("Task 1: $it") }
//                .forEach { println("Found $it") }
    }

    private fun task2() {
        (444444..799999)
                .asSequence()
                .filter { number -> number.split().let { it.notDecreasing() && it.adjacentJustPair() } }
                .count().also { println("Task 2: $it") }
//                .forEach { println("Found $it") }
    }

    private fun Int.split(): IntArray {
        val split = IntArray(6)
        var value = this
        for (i in 5 downTo 0) {
            split[i] = value.rem(10)
            value /= 10
        }
        return split
    }

    private fun IntArray.adjacentPair(): Boolean =
            (1..this.lastIndex).any { i -> this[i] == this[i - 1] }

    private fun IntArray.adjacentJustPair(): Boolean =
            (1..this.lastIndex).any { i ->
                val digit = this[i]
                digit == this[i - 1]
                        && digit != this.getOrNull(i - 2)
                        && digit != this.getOrNull(i + 1)
            }

    private fun IntArray.notDecreasing(): Boolean =
            (1..this.lastIndex).all { i -> this[i] >= this[i - 1] }
}