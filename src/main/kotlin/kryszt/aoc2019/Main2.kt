package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.tools.IoUtil

object Main2 {

    @JvmStatic
    fun main(args: Array<String>) {
//        check()

        println("task1: ${task1()}")
        println("task2: ${task2()}")
    }

    private fun check() {
        "1,1,1,4,99,5,6,0,99".split(",").map { it.toLong() }.toMutableList().also { println(it) }.also { execute(it) }.also { println(it) }
    }

    private fun task1(): Long {
        return executeProcedure(12, 2)
    }

    private fun task2(): Long {
        val expected = 19690720L
        for (n in 0..99) {
//            println("checking n=$n")
            for (v in 0..99) {
                if (executeProcedure(n.toLong(), v.toLong()) == expected) {
                    return 100L * n + v
                }
            }
        }
        return 0
    }

    private fun executeProcedure(noun: Long, verb: Long): Long {
        val orders = IoUtil.readTaskFile(this).split(",").map { it.toLong() }
                .toMutableList()
                .update(1, noun)
                .update(2, verb)
        return execute(orders)
    }

    private fun <T> MutableList<T>.update(at: Int, value: T) = apply {
        set(at, value)
    }

    private fun execute(orders: MutableList<Long>): Long {
        val machine = IntCodeMachine(orders)
        machine.execute()
        return machine.readAddress(0).toLong()
    }
}