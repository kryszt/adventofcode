package kryszt.aoc2019.machine

import java.lang.IllegalStateException
import java.util.*

open class DefaultMachineInput<T>(
        private val onEmpty: () -> T = { throw IllegalStateException("No values in input") },
        initialValues: List<T> = emptyList()
) : PushableInput<T> {

    private val buffer: LinkedList<T> = LinkedList(initialValues)

    override fun pushNextValue(nextValue: T) {
        buffer.add(nextValue)
    }

    override fun nextValue(): T = buffer.poll() ?: onEmpty()
}
