package kryszt.aoc2019.machine

interface ObservableOutput<T> : IntCodeMachine.MachineOutput<T> {

    var onOutput: ((T) -> Unit)?
}