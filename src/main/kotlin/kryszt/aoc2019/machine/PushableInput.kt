package kryszt.aoc2019.machine

interface PushableInput<T> : IntCodeMachine.MachineInput<T> {
    fun pushNextValue(nextValue: T)

}