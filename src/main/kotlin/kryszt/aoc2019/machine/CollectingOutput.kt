package kryszt.aoc2019.machine

interface CollectingOutput<T> : IntCodeMachine.MachineOutput<T> {

    val outputs: List<T>
}