package kryszt.aoc2019.machine

import kryszt.tools.IoUtil
import java.lang.IllegalArgumentException
import java.math.BigInteger

class IntCodeMachine(
    orders: List<Long>,
    private val input: MachineInput<BigInteger> = DefaultMachineInput(),
    private val output: MachineOutput<BigInteger> = DefaultMachineOutput()
) {

    constructor(orders: List<Long>, initialInputs: List<Long> = emptyList()) :
            this(
                orders,
                DefaultMachineInput(initialValues = initialInputs.map(Long::toBigInteger)),
                DefaultMachineOutput()
            )

    var onOutput: ((BigInteger) -> Unit)?
        get() = output.asObservable().onOutput
        set(value) {
            output.asObservable().onOutput = value
        }

    private var orderIndex: BigInteger = BigInteger.ZERO

    private val memoryHolder: Memory =
        Memory(orders.withIndex().associate { it.index.toBigInteger() to it.value.toBigInteger() })
    private var running = true

    fun pushInstruction(line: String) {
        input.asPushable().also { pushable ->
            line.trim().forEach { pushable.pushNextValue(it.toInt().toBigInteger()) }
            pushable.pushNextValue(BigInteger.TEN)
        }
    }

    fun pushInput(value: Int) {
        input.asPushable().pushNextValue(value.toBigInteger())
    }

    fun pushInput(value: Long) {
        input.asPushable().pushNextValue(value.toBigInteger())
    }

    fun pushInput(value: BigInteger) {
        input.asPushable().pushNextValue(value)
    }

    fun readAddress(index: Long): BigInteger = memoryHolder[index.toBigInteger()]

    fun halt() {
        running = false
    }

    fun step(): Boolean = makeStep()

    fun executeTillOutput(): BigInteger {
        var output = BigInteger.ZERO
        onOutput = {
            output = it
            halt()
        }
        execute()
        return output
    }

    fun execute(): List<BigInteger> {
        running = true

        while (running && makeStep()) {
        }
        return output.asCollecting()?.outputs ?: emptyList()
    }

    private fun makeStep(): Boolean {
        val order = parseOrder(memoryHolder, orderIndex)
        val stepResult = order.execute()

        orderIndex = when (stepResult) {
            is OrderResult.Jump -> stepResult.target
            is OrderResult.Halt -> orderIndex
            is OrderResult.NotImplemented -> orderIndex
            else -> orderIndex.plus((order.order.argCount + 1).toBigInteger())
        }

        when (stepResult) {
            is OrderResult.Output -> {
                output.newValue(stepResult.value)
            }
            is OrderResult.Write -> memoryHolder[stepResult.atIndex] = stepResult.value
            is OrderResult.Input -> {
                memoryHolder[stepResult.atIndex] = input.nextValue()
            }
            is OrderResult.Jump -> Unit
            is OrderResult.NOP -> Unit
            is OrderResult.AdjustRelativeBase -> memoryHolder.updateRelativeBase(stepResult.value)
            OrderResult.Halt -> return false
            is OrderResult.NotImplemented -> {
                println("Not supported: ${stepResult.order}")
                return false
            }
        }
        return true
    }

    private fun parseOrder(orders: Memory, orderIndex: BigInteger): OrderData {
        val orderInt = orders[orderIndex].toInt()
        val id = orderInt % 100
        val order = ALL_ORDERS[id] ?: return OrderData(Order.NotSupported, intArrayOf(), orderIndex, orders)
        val modes = IntArray(order.argCount)
        var modesInt = orderInt / 100
        for (i in 0 until order.argCount) {
            modes[i] = modesInt % 10
            modesInt /= 10
        }
        return OrderData(order, modes, orderIndex, orders)
    }

    sealed class Order(val id: Int, val argCount: Int, val job: OrderData.() -> OrderResult) {
        object Add : Order(1, 3, { OrderResult.Write(readArgument(0) + readArgument(1), readAddress(2), mode(2)) })
        object Multiply : Order(2, 3, { OrderResult.Write(readArgument(0) * readArgument(1), readAddress(2), mode(2)) })
        object Input : Order(3, 1, { OrderResult.Input(readAddress(0)) })
        object Output : Order(4, 1, { OrderResult.Output(readArgument(0)) })

        object JumpIfTrue : Order(
            5,
            2,
            { if (readArgument(0) != BigInteger.ZERO) OrderResult.Jump(readArgument(1)) else OrderResult.NOP })

        object JumpIfFalse : Order(
            6,
            2,
            { if (readArgument(0) == BigInteger.ZERO) OrderResult.Jump(readArgument(1)) else OrderResult.NOP })

        object LessThan : Order(7, 3, {
            val isLess = readArgument(0) < readArgument(1)
            OrderResult.Write(isLess.toInt(), readAddress(2), mode(2))
        })

        object Equals : Order(8, 3, {
            val isEqual = readArgument(0) == readArgument(1)
            OrderResult.Write(isEqual.toInt(), readAddress(2), mode(2))
        })

        object AdjustRelativeBase : Order(9, 1, { OrderResult.AdjustRelativeBase(readArgument(0)) })

        object Halt : Order(99, 0, { OrderResult.Halt })
        object NotSupported : Order(0, 0, { OrderResult.NotImplemented(readCode()) })
    }

    @Suppress("ArrayInDataClass")
    data class OrderData(
        val order: Order,
        private val inputModes: IntArray,
        private val orderIndex: BigInteger,
        private val orders: Memory
    ) {

        fun readCode(): Int {
            return orders[orderIndex].intValueExact()
        }

        fun mode(argumentIndex: Int) = inputModes[argumentIndex]

        fun execute(): OrderResult = order.job.invoke(this)

        fun readAddress(argumentIndex: Int): BigInteger {
            return when (val mode = inputModes[argumentIndex]) {
                MODE_POSITION -> orders[orderIndex.plus(BigInteger.ONE).plus(argumentIndex.toBigInteger())]
                MODE_RELATIVE -> orders[orderIndex.plus(BigInteger.ONE)
                    .plus(argumentIndex.toBigInteger())] + orders.relativeBase
                else -> throw IllegalStateException("Unsupported mode $mode at $argumentIndex for ${readCode()} with $orders")
            }
        }

        fun readArgument(argumentIndex: Int): BigInteger {
            val mode = inputModes[argumentIndex]
            val indexInInput = orderIndex.plus(BigInteger.ONE).plus(argumentIndex.toBigInteger())
            return when (mode) {
                MODE_POSITION -> orders[orders[indexInInput]]
                MODE_IMMEDIATE -> orders[indexInInput]
                MODE_RELATIVE -> orders[orders[indexInInput] + orders.relativeBase]
                else -> throw IllegalStateException("Unsupported mode $mode")
            }
        }

    }

    sealed class OrderResult {
        data class Write(val value: BigInteger, val atIndex: BigInteger, val mode: Int) : OrderResult()
        data class NotImplemented(val order: Int) : OrderResult()
        data class Output(val value: BigInteger) : OrderResult()
        data class Input(val atIndex: BigInteger) : OrderResult()
        data class Jump(val target: BigInteger) : OrderResult()
        data class AdjustRelativeBase(val value: BigInteger) : OrderResult()
        object NOP : OrderResult()
        object Halt : OrderResult()
    }

    companion object {

        private val ALL_ORDERS: Map<Int, Order> = listOf(
            Order.Add,
            Order.Multiply,
            Order.Input,
            Order.Output,
            Order.JumpIfTrue,
            Order.JumpIfFalse,
            Order.Equals,
            Order.LessThan,
            Order.AdjustRelativeBase,
            Order.Halt
        )
            .associateBy { it.id }

        const val MODE_POSITION = 0
        const val MODE_IMMEDIATE = 1
        const val MODE_RELATIVE = 2
    }

    interface MachineInput<T> {
        fun nextValue(): T
    }

    interface MachineOutput<T> {
        fun newValue(output: T)
    }
}

private fun Boolean.toInt(): BigInteger = if (this) 1.toBigInteger() else 0.toBigInteger()

class Memory(initialState: Map<BigInteger, BigInteger>) {

    private var _relativeBase: BigInteger = BigInteger.ZERO
    val relativeBase: BigInteger
        get() = _relativeBase

    private val memoryHolder: MutableMap<BigInteger, BigInteger> = initialState.toMutableMap()

    operator fun get(i: BigInteger): BigInteger = memoryHolder[i] ?: BigInteger.ZERO
    operator fun set(i: BigInteger, value: BigInteger) {
        memoryHolder[i] = value
    }

    fun updateRelativeBase(value: BigInteger) {
        _relativeBase += value
    }

    override fun toString(): String {
        return "Memory(_relativeBase=$_relativeBase, memoryHolder=$memoryHolder)"
    }
}

fun getMachineOrders(source: Any): List<Long> =
    IoUtil.readTaskFile(source).split(",").map(String::toLong)

private fun IntCodeMachine.MachineInput<BigInteger>.asPushable(): PushableInput<BigInteger> =
    (this as? PushableInput<BigInteger>) ?: throw IllegalArgumentException("Not a PushableInput ")

private fun IntCodeMachine.MachineOutput<BigInteger>.asCollecting(): CollectingOutput<BigInteger>? =
    (this as? CollectingOutput<BigInteger>)

private fun IntCodeMachine.MachineOutput<BigInteger>.asObservable(): ObservableOutput<BigInteger> =
    (this as? ObservableOutput<BigInteger>) ?: throw IllegalArgumentException("Not a ObservableOutput")

