package kryszt.aoc2019.machine

class DefaultMachineOutput<T> : CollectingOutput<T>, ObservableOutput<T> {

    private val collector = ArrayList<T>()

    override var onOutput: ((T) -> Unit)? = null

    override val outputs: List<T>
        get() = collector

    override fun newValue(output: T) {
        collector.add(output)
        onOutput?.invoke(output)
    }
}