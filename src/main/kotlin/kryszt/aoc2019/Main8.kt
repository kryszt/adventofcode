package kryszt.aoc2019

import kryszt.tools.IoUtil
import java.lang.IllegalStateException

object Main8 {

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): Int {
        return IoUtil.readTaskFile(this)
                .chunked(SIZE)
                .minByOrNull { layer -> layer.zeroCount() }
                ?.let { layer -> layer.oneCount() * layer.twoCount() }!!
    }


    private fun task2(): Int {
        val layers = IoUtil.readTaskFile(this)
                .chunked(SIZE)
        val pixels = List(SIZE) { index -> selectPixel(index, layers) }
        pixels
                .chunked(WIDTH)
                .forEach { println(it.toPixelString()) }
//        println(pixels)
        return 0
    }

    private fun selectPixel(pixel: Int, layers: List<String>): Char {
        layers.forEach { layer ->
            if (layer[pixel] != '2') {
                return layer[pixel]
            }
        }
        throw IllegalStateException("No pixel at $pixel")
    }


    private const val WIDTH = 25
    private const val HEIGHT = 6
    private const val SIZE = WIDTH * HEIGHT
}

private fun String.count(c: Char): Int = count { it == c }
private fun String.zeroCount(): Int = count('0')
private fun String.oneCount(): Int = count('1')
private fun String.twoCount(): Int = count('2')
private fun List<Char>.toPixelString() = joinToString(separator = "") { if (it == '1') "#" else " " }
