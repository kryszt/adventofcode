package kryszt.aoc2019

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines

object Main24 : DayChallenge {

//    private val BOARD = listOf(
//        "....#",
//        "#..#.",
//        "#..##",
//        "..#..",
//        "#...."
//    )

    private val BOARD = readTaskLines()
    private const val BOARD_SIZE = 5

    @JvmStatic
    fun main(args: Array<String>) {
//        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        val initial = buildFrom()
        val visited = mutableSetOf<Int>()
        var next = initial
        while (visited.add(next)) {
            next = nextState(next)
//            next.printBoard()
        }
        next.alsoPrint()
    }

    override fun taskTwo() {
        val solver = InfiniteBoardSolver(Rule()::isAlive, Neighbours()::neighbours)
        val initialState = readFarmPoints()

        val endState = solver.iterateCells(initialState, 200)
        printFarm(endState)
        println(endState.size)
    }

    private fun printFarm(points: Set<Point3D>) {
        val minX = points.minByOrNull(Point3D::x)?.x ?: 0
        val maxX = points.maxByOrNull(Point3D::x)?.x ?: 0

        val minY = points.minByOrNull(Point3D::y)?.y ?: 0
        val maxY = points.maxByOrNull(Point3D::y)?.y ?: 0

        val minZ = points.minByOrNull(Point3D::z)?.z ?: 0
        val maxZ = points.maxByOrNull(Point3D::z)?.z ?: 0

        for (z in minZ..maxZ) {
            println("z = $z")
            for (y in minY..maxY) {
                for (x in minX..maxX) {
                    val c = if (points.contains(Point3D(x, y, z))) '#' else if (x == 2 && y == 2) '?' else '.'
                    print(c)
                }
                println()
            }
            println()
        }
    }

    private class Rule {
        fun isAlive(alive: Boolean, aliveNeighbours: Int): Boolean {
            return when (alive) {
                true -> aliveNeighbours == 1
                false -> aliveNeighbours == 1 || aliveNeighbours == 2
            }
        }
    }

    private class Neighbours {
        fun neighbours(point: Point3D): List<Point3D> {
            return baseNeighbours(point) + upperLevel(point) + lowerLevel(point)
        }

        private fun baseNeighbours(point: Point3D): List<Point3D> {
            return Point
                .crossNeighbours
                .map { move -> point.moveBy(move.x, move.y) }
                .filter { it.x in 0..4 && it.y in 0..4 && (it.x != 2 || it.y != 2) }
        }

        private fun upperLevel(point: Point3D): List<Point3D> {
            val upperNeighbours = mutableListOf<Point3D>()
            if (point.isLeftSide()) {
                upperNeighbours.add(middleLeft.to3D(point.z + 1))
            }
            if (point.isRightSide()) {
                upperNeighbours.add(middleRight.to3D(point.z + 1))
            }
            if (point.isTopSide()) {
                upperNeighbours.add(middleUp.to3D(point.z + 1))
            }
            if (point.isBottomSide()) {
                upperNeighbours.add(middleDown.to3D(point.z + 1))
            }
            return upperNeighbours
        }

        private fun lowerLevel(point: Point3D): List<Point3D> {
            return lowerMappings[point.to2D()]
                .orEmpty()
                .map { it.to3D(point.z - 1) }
        }

        fun Point3D.isLeftSide() = x == 0
        fun Point3D.isRightSide() = x == 4
        fun Point3D.isTopSide() = y == 0
        fun Point3D.isBottomSide() = y == 4

        val topLine: List<Point> = (0..4).map { x -> Point(x = x) }
        val bottomLine: List<Point> = (0..4).map { x -> Point(x = x, y = 4) }
        val leftLine: List<Point> = (0..4).map { y -> Point(y = y) }
        val rightLine: List<Point> = (0..4).map { y -> Point(y = y, x = 4) }

        val middle = Point(2, 2)
        val middleUp = middle.plus(Point.Up)
        val middleDown = middle.plus(Point.Down)
        val middleLeft = middle.plus(Point.Left)
        val middleRight = middle.plus(Point.Right)

        private val lowerMappings: Map<Point, List<Point>> = mapOf(
            middleUp to topLine,
            middleDown to bottomLine,
            middleLeft to leftLine,
            middleRight to rightLine
        )

    }

    private fun readFarmPoints(): Set<Point3D> {
        return BOARD
            .mapIndexed { y, line ->
                line.mapIndexedNotNull { x, c ->
                    if (c == '#') Point3D(x, y, 0) else null
                }
            }
            .flatten()
            .toSet()
    }

    private fun buildFrom(): Int {
        var value = 0
        BOARD.forEachIndexed { y, s ->
            s.forEachIndexed { x, c ->
                value = value.setValue(x, y, c == '#')
            }
        }
        return value
    }

    private fun nextState(current: Int): Int {
        var next = 0
        for (y in 0 until BOARD_SIZE) {
            for (x in 0 until BOARD_SIZE) {
                next = next.setValue(x, y, shouldBeAlive(current.isAlive(x, y), current.neighboursCount(x, y)))
            }
        }
        return next
    }

    private fun shouldBeAlive(isAlive: Boolean, aliveNeighbours: Int): Boolean =
        when (isAlive) {
            true -> aliveNeighbours == 1
            false -> aliveNeighbours in 1..2
        }

    private fun Int.neighboursCount(x: Int, y: Int): Int =
        isAlive(x - 1, y).toInt() +
                isAlive(x + 1, y).toInt() +
                isAlive(x, y - 1).toInt() +
                isAlive(x, y + 1).toInt()

    private fun Int.setValue(x: Int, y: Int, alive: Boolean): Int {
        if (x !in 0 until BOARD_SIZE || y !in 0 until BOARD_SIZE)
            throw IllegalArgumentException("$x, $y")
        val flag = flag(x, y)
        return when (alive) {
            true -> this.or(flag)
            else -> this.and(flag.inv())
        }
    }

    private fun Int.isAlive(x: Int, y: Int): Boolean {
        return if (x in 0 until BOARD_SIZE && y in 0 until BOARD_SIZE) {
            this.and(flag(x, y)) > 0
        } else {
            false
        }
    }

    private fun flag(x: Int, y: Int): Int = 1.shl(flagIndex(x, y))
    private fun flagIndex(x: Int, y: Int) = y * BOARD_SIZE + x

    private fun Boolean.toInt() = when (this) {
        true -> 1
        else -> 0
    }

    private fun Int.printBoard() {
        for (y in 0 until BOARD_SIZE) {
            for (x in 0 until BOARD_SIZE) {
                print(if (isAlive(x, y)) "#" else ".")
            }
            println()
        }
        println()
    }

    private fun Point.to3D(z: Int = 0) = Point3D(x, y, z)
    private fun Point3D.to2D() = Point(x, y)

}
