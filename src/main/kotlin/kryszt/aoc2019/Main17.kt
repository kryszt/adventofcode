package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.aoc2019.machine.getMachineOrders
import kryszt.tools.Point

object Main17 {
    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun loadMap(): List<String> =
            IntCodeMachine(getMachineOrders(this))
                    .execute()
                    .map { it.intValueExact().toChar() }
                    .toCharArray()
                    .let { String(it) }
                    .split("\n")
                    .filterNot { it.isEmpty() }

    private fun loadCrawler(): IntCodeMachine =
            IntCodeMachine(getMachineOrders(this).let { listOf(2L) + it.drop(1) })

    private fun task1(): Int {
//        loadMap().forEach(::println)
        return countCrossing(loadMap()).sumBy { it.x * it.y }
    }

    private fun countCrossing(map: List<String>): List<Point> {
        val width = map[0].length
        val height = map.size

        val crossings = mutableListOf<Point>()

        (1..(width - 2)).forEach { x ->
            (1..(height - 2)).forEach { y ->
                if (isCrossing(x, y, map)) {
                    crossings.add(Point(x, y))
                }
            }
        }
        return crossings
    }

    private fun isCrossing(x: Int, y: Int, map: List<String>): Boolean {
        return map[y][x - 1] == '#'
                && map[y][x + 1] == '#'
                && map[y - 1][x] == '#'
                && map[y + 1][x] == '#'
                && map[y][x] == '#'
    }

    private fun task2(): Long {
        val moves = Traverser(loadMap()).traverse()
        val splitMoves = splitAllMoves(moves)
        val crawler = loadCrawler()
        crawler.inputCommands(splitMoves)
        return crawler.execute().last().longValueExact()
    }

    private fun splitAllMoves(moves: List<String>): List<String> {
        return listOf("B,C,B,C,B,A,C,A,B,A",
                "L,10,R,10,R,6,L,4",
                "L,10,L,12,R,6",
                "R,10,L,4,L,4,L,12",
                "n")
        //TODO - implement splitting, this one is done manually
    }

    private class Traverser(lines: List<String>) {

        private val maze: Map<Point, Char> = makeMaze(lines)
        private var position: Point = findStart()
        private var direction: Point = initialDirections()

        fun traverse(): MutableList<String> {
            val orders = mutableListOf<String>()

            while (true) {
                val moves = moveForward()
                if (moves > 0) {
                    orders.add(moves.toString())
                }
                val turn = tryTurn() ?: return orders
                orders.add(turn)
            }
        }

        private fun moveForward(): Int {
            var moves = 0
            while (true) {
                val next = position + direction
                if (isWay(next)) {
                    position = next
                    moves++
                } else {
                    return moves
                }
            }
        }

        private fun tryTurn(): String? {
            if (isWay(position + direction.left())) {
                direction = direction.left()
                return "L"
            }
            if (isWay(position + direction.right())) {
                direction = direction.right()
                return "R"
            }
            return null
        }

        private fun isWay(point: Point): Boolean = maze[point] == '#'

        private fun makeMaze(lines: List<String>): Map<Point, Char> {
            val m = mutableMapOf<Point, Char>()
            lines.forEachIndexed { y, s ->
                s.forEachIndexed { x, c ->
                    m[Point(x, y)] = c
                }
            }
            return m
        }

        private fun findStart(): Point {
            val starts = setOf('^', 'v', '<', '>')
            return maze.entries.find { starts.contains(it.value) }?.key ?: throw IllegalStateException("No start point")
        }

        private fun initialDirections(): Point {
            return when (val value = maze[position]) {
                '^' -> Point.Up
                'v' -> Point.Down
                '<' -> Point.Left
                '>' -> Point.Right
                else -> throw IllegalArgumentException("Not a start $value")
            }
        }
    }
}

private fun IntCodeMachine.inputCommands(splitMoves: List<String>) {
    splitMoves.forEach { line ->
        line.forEach { pushInput(it.toInt()) }
        pushInput(10)
    }
}
