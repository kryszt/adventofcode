package kryszt.aoc2019

import kryszt.tools.IoUtil

object Main6 {

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): Int {
        val input = IoUtil.readTaskFileLines(this)
        return sumDepth(buildGraph(input))
    }

    private fun task2(): Int {
        val input = IoUtil.readTaskFileLines(this)
        val parents = buildParents(input)
        val pathYou = buildPath(parents, YOU)
        val pathSan = buildPath(parents, SAN)
        val intersect = pathYou.intersect(pathSan)
        return pathYou.minus(intersect).size + pathSan.minus(intersect).size - 2
    }

    private fun buildGraph(nodes: List<String>): Map<String, List<String>> {
        val graph = mutableMapOf<String, MutableList<String>>()
        nodes.forEach { node ->
            val (left, right) = node.split(")")
            graph.getOrPut(left) { mutableListOf() }.apply { add(right) }
        }
        return graph
    }

    private fun buildParents(nodes: List<String>): Map<String, String> {
        val graph = mutableMapOf<String, String>()
        nodes.forEach { node ->
            val (left, right) = node.split(")")
            graph[right] = left
        }
        return graph
    }

    private fun buildPath(parents: Map<String, String>, start: String): List<String> {
        val path = mutableListOf<String>()
        var next: String? = start
        while (next != null) {
            path.add(next)
            next = parents[next]
        }
        return path
    }

    private fun sumDepth(graph: Map<String, List<String>>): Int {
        return sumDepth(0, listOf(COM), graph)
    }

    private fun sumDepth(level: Int, atLevel: List<String>, graph: Map<String, List<String>>): Int {
        if (atLevel.isEmpty()) return 0
        val atNextLevel = atLevel.mapNotNull { graph[it] }.flatten()
        return level * atLevel.size + sumDepth(level + 1, atNextLevel, graph)
    }

    private const val COM = "COM"
    private const val YOU = "YOU"
    private const val SAN = "SAN"
}