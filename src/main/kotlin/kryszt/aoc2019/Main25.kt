package kryszt.aoc2019

import kryszt.aoc2019.machine.DefaultMachineInput
import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.aoc2019.machine.getMachineOrders
import kryszt.tools.alsoPrintOnEach
import java.math.BigInteger
import java.util.*

object Main25 {

    @JvmStatic
    fun main(args: Array<String>) {
        task1()
    }

    fun task1() {
        val skipPick = mutableSetOf(
            "infinite loop",
            "giant electromagnet",
            "escape pod",
            "photons",
            "molten lava"
        )

        val rooms = findAllRooms()
            .alsoPrintOnEach()
        val items = allItems(rooms)

        val canPick = items.filterNot { skipPick.contains(it.name) }

        canPick.alsoPrintOnEach()

        val security = rooms.find { it.name.contains("Security") }!!
        val allPickups = canPick.map { it.toPickupOrders() }.flatten().plus("inv")

        val withSecurity = allPickups.plus(security.path)

        val tries = buildAllTries(canPick)

        val withTries = withSecurity.plus(tries)
        runCommands(withTries)
    }

    private fun buildAllTries(canPick: List<Item>): List<String> {
        val allOrders = mutableListOf<String>()
        //drop all
        allOrders.addAll(canPick.map { "drop ${it.name}" })

        allOrders.add("north")

        val allSelected = BooleanArray(canPick.size)
        while (allSelected.any { !it }) {
            for (index in 0..canPick.size) {
                if (allSelected[index]) {
                    allSelected[index] = false
                    allOrders.add("drop ${canPick[index].name}")
                } else {
                    allSelected[index] = true
                    allOrders.add("take ${canPick[index].name}")
                    allOrders.add("north")
                    break
                }
            }
        }
        return allOrders
    }

    private fun Item.toPickupOrders(): List<String> {
        return path + "take $name" + path.reversed().map { it.back() }
    }

    private fun findAllRooms(): List<Room> {
        val input = TextCommandInput()
        val walker = BaseWalker(input)
        val output = TextBufferOutput { line ->
//            println(line)
            walker.nextLine(line)
//            if (line == "Command?") {
//                val inText = System.`in`.bufferedReader().readLine()
//                input.pushCommand(inText)
//            }
        }

        val machine = IntCodeMachine(getMachineOrders(this), input, output)
        walker.machine = machine
        machine.execute()

        println("Finished")
        return walker.rooms.values.simplifyAllPaths()
    }

    private fun runCommands(commands: List<String>) {
        val commandQueue = LinkedList(commands)

        val input = TextCommandInput()
//        commands.forEach { command ->
//            input.pushCommand(command)
//        }
        val output = TextBufferOutput { line ->
            println(line)
            if (line == "Command?") {
                val nextCommand = if (commandQueue.isEmpty()) {
                    System.`in`.bufferedReader().readLine()
                } else {
                    commandQueue.removeFirst()
                }
                println("Command: $nextCommand")
                input.pushCommand(nextCommand)
            }
        }

        val machine = IntCodeMachine(getMachineOrders(this), input, output)
        machine.execute()
    }

    private fun allItems(rooms: List<Room>): List<Item> {
        return rooms.mapNotNull { room -> room.items.firstOrNull()?.let { Item(it, room.path) } }
    }

    private fun Collection<Room>.simplifyAllPaths(): List<Room> =
        map { it.copy(path = it.path.simplifyPath()) }


    private fun List<String>.simplifyPath(): List<String> {
        val simplified = LinkedList<String>()
        forEach { step ->
            if (simplified.peekLast() == step.back()) {
                simplified.removeLast()
            } else {
                simplified.add(step)
            }
        }
        return simplified
    }

    class TextBufferOutput(private val onFullLine: (String) -> Unit) : IntCodeMachine.MachineOutput<BigInteger> {

        private val lineEnd = 10.toChar()

        private val buffer = mutableListOf<Char>()

        override fun newValue(output: BigInteger) {
            handleNewChar(output.intValueExact().toChar())
        }

        private fun handleNewChar(c: Char) {
            when (c) {
                lineEnd -> sendBuffer()
                else -> buffer.add(c)
            }
        }

        private fun sendBuffer() {
            val line = buffer.joinToString(separator = "")
            buffer.clear()
            onFullLine(line)
        }
    }

    class TextCommandInput : DefaultMachineInput<BigInteger>() {

        fun pushCommand(line: String) {
            line.trim().forEach {
                pushNextValue(it.toLong().toBigInteger())
            }
            pushNextValue(10L.toBigInteger())
        }
    }

    private class BaseWalker(private val textCommandInput: TextCommandInput) {

        private val endTag = "END"

        lateinit var machine: IntCodeMachine

        val rooms = mutableMapOf<String, Room>()
        var roomBuilder = RoomBuilder()
        var path = listOf<String>()

        val moves = Stack<String>()

        fun nextLine(line: String) {
            roomBuilder.nextLine(line)
            if (line == "Command?") {
                val room = roomBuilder.build(path)
                roomBuilder = RoomBuilder()
                roomEntered(room)
            } else if (line.startsWith("A loud, robotic voice says")) {
                val room = roomBuilder.build(path)
                roomBuilder = RoomBuilder()
                rooms[room.name] = room
                path = path.dropLast(1)
            }
        }

        fun roomEntered(room: Room) {
            if (rooms.containsKey(room.name)) {
                val nextMove = moves.pop()
                if (nextMove == endTag) {
                    machine.halt()
                } else {
                    visitDirection(nextMove)
                }
            } else {
                rooms[room.name] = room
                val enteredFrom = path.lastOrNull()
                val back = enteredFrom?.back() ?: endTag
                val roomMoves = room
                    .doors
                    //move back to last place
                    .minus(back)
                    .plus(back)

                val nextMove = roomMoves.first()
                val rest = roomMoves.drop(1).reversed()
                moves.addAll(rest)
                visitDirection(nextMove)
            }
        }

        fun visitDirection(direction: String) {
            path = path.plus(direction)
            textCommandInput.pushCommand(direction)
        }
    }

    private class RoomBuilder {
        val lines = mutableListOf<String>()

        fun nextLine(line: String) {
            lines.add(line)
        }

        fun build(path: List<String>): Room {
            val nameIndex = lines.indexOfFirst { it.startsWith("==") }
            val name = lines[nameIndex]
            val description = lines[nameIndex + 1]

            return Room(
                name,
                description,
                catchList("Doors here lead"),
                catchList("Items here"),
                path = path
            )
        }

        private fun catchList(startsWith: String): List<String> {
            val listIndex = lines.indexOfFirst { it.startsWith(startsWith) }.takeUnless { it < 0 } ?: return emptyList()
            return lines.subList(listIndex + 1, lines.size).takeWhile { it.startsWith("- ") }
                .map { it.substringAfter(' ') }
        }
    }

    private data class Room(
        val name: String,
        val description: String,
        val doors: List<String>,
        val items: List<String>,
        val path: List<String>
    )

    private data class Item(val name: String, val path: List<String>)

    fun String.back(): String =
        when (this) {
            "north" -> "south"
            "south" -> "north"
            "east" -> "west"
            "west" -> "east"
            else -> throw IllegalArgumentException("Not a direction $this")
        }

}