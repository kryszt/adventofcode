package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.aoc2019.machine.getMachineOrders
import kryszt.tools.Point
import kryszt.tools.printMap
import kotlin.math.sign

object Main13 {
    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): Int =
            IntCodeMachine(getOrders())
                    .execute()
                    .chunked(3)
                    .count { it[2].intValueExact() == 2 }

    private fun task2(): Int {
        val boardHandler = BoardHandler()
        val machine = IntCodeMachine(getFreeGameOrders(), listOf(0L))

        val updateJoystick = { direction: Int ->
            machine.pushInput(direction.toLong())
        }
        machine.onOutput = { boardHandler.nextUpdate(it.intValueExact(), updateJoystick) }
        machine.execute()
        return boardHandler.score
    }

    private fun getOrders(): List<Long> =
            getMachineOrders(this)

    private fun getFreeGameOrders(): List<Long> =
            getMachineOrders(this)
                    .let { listOf(2L) + it.drop(1) }
}

class BoardHandler {

    private val scorePoint = Point(-1, 0)

    private val board = mutableMapOf<Point, Int>()
    var score: Int = 0
    private var ball = Point(0, 0)
    private var paddle = Point(0, 0)

    private val collector = mutableListOf<Int>()

    fun nextUpdate(value: Int, updateJoystick: (Int) -> Unit) {
        collector.add(value)
        if (collector.size == 3) {
            val (x, y, v) = collector
            nextUpdate(Point(x, y), v, updateJoystick)
            collector.clear()
        }
    }

    private fun nextUpdate(p: Point, v: Int, updateJoystick: (Int) -> Unit) {
        when {
            p == scorePoint -> {
                score = v
//                print()
            }
            v == 0 -> {
                board.remove(p)
            }
            v in 1..2 -> {
                board[p] = v
            }
            v == 3 -> {
                board[p] = v
                paddle = p
            }
            v == 4 -> {
                board[p] = v
                ball = p
                if (paddle.y > 0) {
                    updateJoystick((ball.x - paddle.x).sign)
                }
            }
        }
    }

    fun print() {
        board.printMap { _, value ->
            when (value) {
                null, 0 -> " "
                else -> value.toString()
            }
        }
        println()
        println("Score $score")
        println("Ball $ball")
        println("Paddle $paddle")
    }
}
