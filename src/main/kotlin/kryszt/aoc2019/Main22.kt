package kryszt.aoc2019

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import java.math.BigInteger

object Main22 : DayChallenge {

    private val shuffle: List<ShuffleMethod> = readTaskLines().map { parseAction(it) }

    private fun parseAction(task: String): ShuffleMethod {
        return when {
            task.startsWith("deal into new stack") -> DealIntoNewStack
            task.startsWith("deal with increment") -> DealWithIncrement(task.endValue().toLong())
            task.startsWith("cut") -> CutCards(task.endValue().toLong())
            else -> throw IllegalArgumentException(task)
        }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1")
        taskOne()
        println("Task 2")
        taskTwo()
//        check()
    }

    override fun taskOne() {
        val deckSize = 10007
        shuffle
            .combineInverse(deckSize.toLong())
            .apply(2019, deckSize)
            .alsoPrint()
    }

    override fun taskTwo() {
        val deckSize: Long = 119315717514047
        val repeats = 101741582076661

        val shuffler = shuffle
            .combine(deckSize)

        val multiplied = multiplyShuffler(shuffler, repeats, deckSize)

        multiplied.apply(2020, deckSize).alsoPrint()
    }

    @Suppress("SameParameterValue")
    private fun multiplyShuffler(function: LinearFunction, repeats: Long, size: Long): LinearFunction {
        if (repeats == 1L) return function

        var repeatedFunction = function
        var repeatsCount: Long = 1

        while (repeatsCount <= repeats - repeatsCount) {
            repeatsCount *= 2
            repeatedFunction = repeatedFunction.combineWith(repeatedFunction).normalize(size)
        }
        val repeatsLeft = repeats - repeatsCount
        return if (repeatsLeft == 0L) {
            repeatedFunction
        } else {
            repeatedFunction.combineWith(multiplyShuffler(function, repeatsLeft, size))
        }
    }

    private fun List<ShuffleMethod>.combine(size: Long): LinearFunction {
        var result = LinearFunction(1, 0)
        forEach {
            result = result.combineWith(it.asFunction(size)).normalize(size)
        }
        return result
    }

    private fun List<ShuffleMethod>.combineInverse(size: Long): LinearFunction {
        var result = LinearFunction(1, 0)
        this.reversed().forEach {
            result = result.combineWith(it.inverse(size).asFunction(size)).normalize(size)
        }
        return result
    }

    private interface ShuffleMethod {
        fun inverse(size: Long): ShuffleMethod
        fun asFunction(size: Long): LinearFunction
    }

    private object DealIntoNewStack : ShuffleMethod {
        override fun inverse(size: Long): ShuffleMethod = this

        override fun asFunction(size: Long) = LinearFunction(a = -1, b = -1).normalize(size)
    }

    private data class CutCards(private val cards: Long) : ShuffleMethod {
        override fun inverse(size: Long) = CutCards(-cards)

        override fun asFunction(size: Long): LinearFunction = LinearFunction(b = cards).normalize(size)
    }

    private data class DealWithIncrement(private val stepSize: Long) : ShuffleMethod {
        override fun inverse(size: Long): ShuffleMethod {
            val a = stepSize.toBigInteger().modInverse(size.toBigInteger())
            return DealWithIncrement(a.toLong())
        }

        override fun asFunction(size: Long): LinearFunction {
            val a = stepSize.toBigInteger().modInverse(size.toBigInteger())
            return LinearFunction(a = a)
        }
    }

    private fun String.endValue(): Int =
        split(" ").last().toInt()

    private class LinearFunction(val a: BigInteger = BigInteger.ONE, val b: BigInteger = BigInteger.ZERO) {

        constructor(a: Long = 1L, b: Long = 0L) : this(a.toBigInteger(), b.toBigInteger())
        constructor(a: Int = 1, b: Int = 0) : this(a.toBigInteger(), b.toBigInteger())

        fun apply(value: Int, size: Int): Int =
            apply(value.toBigInteger(), size.toBigInteger()).toInt()

        fun apply(value: Long, size: Long): Long =
            apply(value.toBigInteger(), size.toBigInteger()).toLong()

        fun apply(value: BigInteger, size: BigInteger): BigInteger =
            a.times(value).plus(b).normalize(size)

        fun combineWith(other: LinearFunction): LinearFunction =
            LinearFunction(this.a.times(other.a), this.a.times(other.b).plus(this.b))

        fun normalize(size: Long): LinearFunction =
            normalize(size.toBigInteger())

        fun normalize(size: BigInteger): LinearFunction =
            LinearFunction(a.normalize(size), b.normalize(size))

    }

    private fun BigInteger.normalize(size: BigInteger): BigInteger {
        var rest = this % size
        while (rest < BigInteger.ZERO) {
            rest += size
        }
        return rest
    }
}
