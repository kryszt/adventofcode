package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.aoc2019.machine.getMachineOrders
import kryszt.tools.Point
import java.math.BigInteger

object Main19 {

    private const val SHIP_SIZE = 100

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): Int {
        var count = 0
        (0 until 50).forEach { y ->
            (0 until 50).forEach { x ->
                if (beamAt(x, y)) {
                    count++
//                    print("#")
//                } else {
//                    print(".")
                }
            }
//            println()
        }
        return count
    }

    private fun task2(): Int {
        val findBeams = findBeams(SHIP_SIZE * 2)
        val row = findShipRow(findBeams, SHIP_SIZE)
        val point = Point(row.xr - SHIP_SIZE + 1, row.y)
        return point.x * 10000 + row.y
    }

    private fun findBeams(width: Int): List<Beam> {
        val beam = mutableListOf(Beam(0, 0, 0))

        var l = 0
        var r = 1
        var y = 1
        while (true) {
            if (!beamAt(l, y)) {
                l++
            }
            if (beamAt(r, y)) {
                r++
            }
            val beamPart = Beam(y, l, r - 1)
            beam.add(beamPart)
            if (beamPart.width >= width) {
                break
            }
            y++
        }

        return beam
    }

    private fun findShipRow(beams: List<Beam>, width: Int): Beam {
        beams.forEachIndexed { index, beam ->
            val bottom = beams[index + width - 1]
            val left = beam.xr - width + 1
            if (left == bottom.xl) {
                println("From $beam to $bottom")
                return beam
            }
        }
        throw IllegalStateException("Cannot find this place")
    }

    private fun beamAt(x: Int, y: Int): Boolean {
        val machine = IntCodeMachine(getMachineOrders(this))
        machine.pushInput(x)
        machine.pushInput(y)
        return machine.executeTillOutput() == BigInteger.ONE
    }

    data class Beam(val y: Int, val xl: Int, val xr: Int) {
        val width = (xr - xl) + 1
    }
}
