package kryszt.aoc2019

import kryszt.tools.IoUtil
import kryszt.tools.Point
import kryszt.tools.coPrime
import kryszt.tools.gcd
import kotlin.math.atan2
import kotlin.math.sign

object Main10 {

    private fun loadMap(): List<String> {
        return IoUtil.readTaskFileLines(this, "")
    }

    @JvmStatic
    fun main(args: Array<String>) {
        val (center, score) = task1()
        println("Task 1: $score")
        println("Center: $center")
        println("Task 2: ${task2(center)}")
    }

    private fun task1(): Pair<Point, Int> {
        return findBest(loadMap())
    }

    private fun task2(center: Point): Int {
        val map = loadMap()

        val visible = selectVisible(center, map)
                .map { it to atan2(it.x.toDouble().minus(center.x), it.y.toDouble().minus(center.y)) }
                .sortedByDescending { it.second }

        return visible[199].first.also { println(it) }.let { it.x * 100 + it.y }
    }

    private fun findBest(points: List<String>): Pair<Point, Int> {
        var best = 0
        var bestPlace = Point()
        for (y in points.indices) {
            val line = points[y]
            for (x in line.indices) {
                val inPlace = line[x]
                if (inPlace == '#') {
                    val possibility = countVisible(Point(x, y), points)
                    if (possibility > best) {
                        best = possibility
                        bestPlace = Point(x, y)
                    }
                }
            }
        }
        return bestPlace to best
    }


    private fun countVisible(center: Point, points: List<String>): Int {
        return selectVisible(center, points).size
    }

    private fun selectVisible(center: Point, points: List<String>): List<Point> {
        val visible = mutableListOf<Point>()
        for (y in points.indices) {
            val line = points[y]
            for (x in line.indices) {
                val inPlace = line[x]
                val point = Point(x, y)
                if (inPlace == '#' && isPointVisible(center, points, point)) {
                    visible.add(point)
                }
            }
        }
        return visible
    }

    private fun isPointVisible(center: Point, points: List<String>, point: Point): Boolean {
        if (center == point) return false
        val direction = point - center
        if (direction.x.coPrime(direction.y)) return true

        return !isPointObstructed(center, points, point)
    }

    private fun shortDirection(direction: Point): Point {
        val multiplier = direction.x.gcd(direction.y)
        if (multiplier == 0) return Point(direction.x.sign, direction.y.sign)
        return Point(direction.x / multiplier, direction.y / multiplier)
    }

    private fun isPointObstructed(center: Point, points: List<String>, point: Point): Boolean {
        val shortDirection = shortDirection(point - center)
        var checking = center + shortDirection
        while (checking != point) {
            if (points.occupiedAt(checking)) {
                return true
            }
            checking += shortDirection
        }
        return false
    }
}

private fun List<String>.occupiedAt(checking: Point): Boolean {
    return this[checking.y][checking.x] == '#'
}
