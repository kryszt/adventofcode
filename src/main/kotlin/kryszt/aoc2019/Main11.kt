package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.tools.IoUtil
import kryszt.tools.Point
import java.lang.IllegalArgumentException

object Main11 {

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): Int {
        val collector = PositionCollector(Point(), Point.Up)
        val machine = createMachine(0)
        machine.onOutput = { output ->
            collector.nextOutput(output.intValueExact())?.let { machine.pushInput(it.toLong()) }
        }
        machine.execute()

        collector.print()

        return collector.colorMap.size
    }

    private fun task2(): Int {
        val collector = PositionCollector(Point(), Point.Up)
        collector.paint(Point(), 1)

        val machine = createMachine(1)
        machine.onOutput = { output ->
            collector.nextOutput(output.intValueExact())?.let { machine.pushInput(it.toLong()) }
        }
        machine.execute()

        collector.print()

        return collector.colorMap.size
    }

    private fun createMachine(initColor: Int): IntCodeMachine {
        return IntCodeMachine(getOrders(), listOf(initColor.toLong()))
    }

    private fun getOrders(): List<Long> =
            IoUtil.readTaskFile(this).split(",").map(String::toLong)
}

private class PositionCollector(initialPosition: Point, initialDirection: Point) {

    private var position = initialPosition
    private var direction = initialDirection
    private var nextIsPaint = true

    val colorMap = mutableMapOf<Point, Int>()

    fun colorAt(point: Point): Int = colorMap[point] ?: 0

    fun nextOutput(value: Int): Int? {
        val returnValue: Int? = when (nextIsPaint) {
            true -> {
                paint(position, value)
                null
            }
            false -> {
                moveAndGetColor(value)
            }
        }
        nextIsPaint = nextIsPaint.not()
        return returnValue
    }

    internal fun paint(place: Point, value: Int) {
//        println("paint $place with ${if (value == 0) "." else "#"}")
        colorMap[place] = value
    }

    private fun moveAndGetColor(value: Int): Int {
        direction = nextDirection(value)
        position += direction
        return colorMap.getOrDefault(position, 0)
//                .also { println("Moved to $position with color $it after turn ${if (value == 0) "left" else "right"}") }
//                .also { print() }
    }

    private fun nextDirection(value: Int): Point {
        return when (value) {
            0 -> direction.left()
            1 -> direction.right()
            else -> throw IllegalArgumentException("Unsupported direction change: $value")
        }
    }

    fun print() {
        val left = colorMap.keys.minByOrNull { it.x }?.x ?: 0
        val right = colorMap.keys.maxByOrNull { it.x }?.x ?: 0
        val top = colorMap.keys.minByOrNull { it.y }?.y ?: 0
        val bottom = colorMap.keys.maxByOrNull { it.y }?.y ?: 0

        for (y in (top - 2)..(bottom + 2)) {
            for (x in (left - 2)..(right + 2)) {
                val point = Point(x, y)
                if (point == position) {
                    printDirection()
                } else {
                    printPlace(point)
                }
            }
            println()
        }
    }

    private fun printPlace(point: Point) {
        when (colorAt(point)) {
            0 -> if (colorMap.contains(point)) print(' ') else print('.')
            1 -> print('#')
            else -> print('&')
        }
    }

    private fun printDirection() {
        val char = when (direction) {
            Point.Down -> 'v'
            Point.Up -> '^'
            Point.Left -> '<'
            Point.Right -> '>'
            else -> '?'
        }
        print(char)
    }

}
