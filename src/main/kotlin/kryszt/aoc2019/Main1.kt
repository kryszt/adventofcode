package kryszt.aoc2019

import kryszt.tools.IoUtil

object Main1 {

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1 ${task1()}")
        println("Task 2 ${task2()}")
    }

    private fun task1(): Int =
            IoUtil.readTaskFileLinesAsSequence(this)
                    .map { it.toInt().fuelMass() }
                    .sum()

    private fun task2(): Int =
            IoUtil.readTaskFileLinesAsSequence(this)
                    .map { it.toInt().totalFuelMass() }
                    .sum()

    private fun Int.fuelMass() = this.div(3).minus(2).coerceAtLeast(0)
    private fun Int.totalFuelMass(): Int =
            fuelSequence().sum()

    private fun Int.fuelSequence(): Sequence<Int> =
            generateSequence(this.fuelMass(), { mass -> mass.fuelMass().takeIf { it > 0 } })
}