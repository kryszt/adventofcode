package kryszt.aoc2019

import kryszt.tools.IoUtil
import kryszt.tools.Point
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import kotlin.math.absoluteValue

object Main3 {

    @JvmStatic
    fun main(args: Array<String>) {
        println("task1 ${task1()}")
        println("task2 ${task2()}")
    }

    private fun task1(): Int {
        val lines = IoUtil.readTaskFileLines(this)
//        val lines = listOf("R8,U5,L5,D3", "U7,R6,D4,L4")
//        val lines = listOf("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83")
//        val lines = listOf("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")
//        val line1 = lines[0].split(",").iterator()

        return solve1(lines)
    }

    private fun task2(): Int {
        val lines = IoUtil.readTaskFileLines(this)
//        val lines = listOf("R8,U5,L5,D3", "U7,R6,D4,L4")
//        val lines = listOf("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83")
//        val lines = listOf("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")
//        val line1 = lines[0].split(",").iterator()

        val intersections = intersections(lines)
        val visited0 = traverse(Point(), lines[0].split(",").iterator())
        val visited1 = traverse(Point(), lines[1].split(",").iterator())

        return intersections.map { crossing -> visited0.indexOf(crossing) + visited1.indexOf(crossing) }.minOrNull()?.plus(2)
                ?: throw IllegalStateException("No intersection")
    }

    private fun solve1(lines: List<String>): Int =
            intersections(lines).minByOrNull { it.distance() }?.distance()
                    ?: throw IllegalStateException("No intersection")

    private fun intersections(lines: List<String>): Set<Point> {
        val visited0 = traverse(Point(), lines[0].split(",").iterator()).toSet()
        val visited1 = traverse(Point(), lines[1].split(",").iterator()).toSet()
        return visited0.intersect(visited1)
    }

    private fun traverse(startPoint: Point, iterator: Iterator<String>): List<Point> {
        val visited = mutableListOf<Point>()

        var currentPoint = startPoint
        var direction: Point
        iterator.forEach { order ->
            direction = direction(order[0])
            val repeats = order.drop(1).toInt()
            repeat(repeats) {
                currentPoint += direction
                visited.add(currentPoint)
            }
        }
        return visited
    }

    private fun direction(directionString: Char): Point {
        return when (directionString) {
            'R' -> Point.Right
            'L' -> Point.Left
            'U' -> Point.Up
            'D' -> Point.Down
            else -> throw IllegalArgumentException("Unsupported direction: $directionString")
        }
    }

    private fun Point.distance(): Int = x.absoluteValue.plus(y.absoluteValue)
}
