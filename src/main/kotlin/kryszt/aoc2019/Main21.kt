package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.aoc2019.machine.getMachineOrders

object Main21 {

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): String {
        return executeCommands(instructionsWalk)
    }

    private fun task2(): String {
        return executeCommands(instructionsRun)
    }

    private fun executeCommands(commands: List<String>): String {
        val machine = IntCodeMachine(getMachineOrders(this))
        pushInstructions(machine, commands)
        machine.onOutput = { print(it.intValueExact().toChar()) }

        return machine.execute().last().toString()
    }

    private fun pushInstructions(machine: IntCodeMachine, instructions: List<String>) {
        instructions.forEach { line ->
            machine.pushInstruction(line)
        }
    }

    private val instructionsWalk = listOf(
            "NOT C J",
            "NOT B T",
            "OR T J",
            "NOT A T",
            "OR T J",
            "AND D J",
            "WALK"
    )

    private val instructionsRun = listOf(
            "NOT C J",
            "NOT B T",
            "OR T J",
            "NOT A T",
            "OR T J",
            "AND D J",
            "NOT E T",
            "NOT T T",
            "OR H T",
            "AND T J",
            "RUN"
    )
}