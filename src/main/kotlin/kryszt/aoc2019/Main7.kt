package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.tools.IoUtil
import kryszt.tools.PermutationGenerator

object Main7 {
    @JvmStatic
    fun main(args: Array<String>) {

        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): Long {
        return task(listOf(0L, 1L, 2L, 3L, 4L), ::executeOrder)
    }

    private fun task2(): Long {
        return task(listOf(5L, 6L, 7L, 8L, 9L), ::executeCircularOrder)
    }

    private fun task(phases: List<Long>, execution: (List<Long>) -> Long): Long {
        val generator = PermutationGenerator(phases)

        var bestValue = 0L
        var bestOrder: List<Long> = emptyList()

        while (generator.hasNext) {
            val next = generator.getNext()
            val total = execution(next)
            if (total > bestValue) {
                bestValue = total
                bestOrder = next
            }
        }

        println("$bestValue for $bestOrder")
        return bestValue
    }

    private fun executeOrder(items: List<Long>): Long {
        var input = 0L
        items.forEach { phase ->
            input = executePhase(phase, input)
        }
        return input
    }

    private fun executePhase(phase: Long, input: Long): Long {
        return IntCodeMachine(getOrders(), listOf(phase, input))
                .execute()
//                .also { println("for p=$phase and i=$input o=$it") }
                .last().toLong()
    }

    private fun executeCircularOrder(items: List<Long>): Long {
        val machines = items.map { IntCodeMachine(getOrders(), listOf(it)) }
        var largestOutput = 0L

        for (i in machines.indices) {
            val machine = machines[i]
            val next = machines[(i + 1) % machines.size]
            machine.onOutput = { output ->
                largestOutput = largestOutput.coerceAtLeast(output.longValueExact())
                next.pushInput(output)
                next.execute()
            }
        }

        machines[0].pushInput(0L)
        machines[0].execute()

        return largestOutput
    }

    private fun getOrders(): MutableList<Long> {
        val input = IoUtil.readTaskFile(this)
//        val input = "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0"
//        val input = "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5"
//        val input = "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10"
        return input.split(",").map(String::toLong).toMutableList()
    }
}
