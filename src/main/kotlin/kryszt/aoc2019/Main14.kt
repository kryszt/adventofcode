package kryszt.aoc2019

import kryszt.tools.BinarySearch
import kryszt.tools.IoUtil

object Main14 {

    private const val ORE = "ORE"
    private const val FUEL = "FUEL"

    private fun loadRules(): Map<String, Rule> {
        return IoUtil.readTaskFileLinesAsSequence(this, "")
                .map { line ->
                    val (partsPart, result) = line.split("=>").map(String::trim)
                    val parts = partsPart.split(",").map(String::trim).map { it.toIngredient() }
                    Rule(result.toIngredient(), parts)
                }
                .associateBy { it.result.name }
    }

    private fun String.toIngredient(): Ingredient {
        val (number, name) = split(" ")
        return Ingredient(number.toLong(), name)
    }

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")
    }

    private fun task1(): Long {
        return fuelOreCost(1, loadRules())
    }

    private fun task2(): Int {
        val rules = loadRules()
        return BinarySearch.binarySearchSolution { value -> fuelOreCost(value.toLong(), rules) > 1_000_000_000_000 } - 1
    }

    private fun fuelOreCost(number: Long, rules: Map<String, Rule>): Long {
        val factory = ElementFactory(rules)
        factory.take(FUEL, number)
        return factory.totalOreCount
    }

    private class ElementFactory(val rules: Map<String, Rule>) {
        private val elementsHolder = mutableMapOf<String, Long>()
        var totalOreCount = 0L
            private set

        fun take(element: String, number: Long) {
            if (element == ORE) {
                takeOre(number)
            } else {
                takeNotOre(element, number)
            }
        }

        private fun takeOre(number: Long) {
            totalOreCount += number
        }

        private fun takeNotOre(name: String, number: Long) {
            val existing = elementsHolder.getOrDefault(name, 0)
            val missing = number.minus(existing).coerceAtLeast(0)
            if (missing > 0) {
                val rule = rules[name] ?: throw  IllegalArgumentException("No rule for $name")
                takeForRule(rule, missing)
            }
            elementsHolder[name] = elementsHolder.getOrDefault(name, 0) - number
        }

        private fun takeForRule(rule: Rule, number: Long) {
            val ruleTimes = number.divRoundUp(rule.result.amount)
            rule.parts.forEach { ingredient ->
                take(ingredient.name, ingredient.amount * ruleTimes)
            }
            elementsHolder[rule.result.name] = elementsHolder.getOrDefault(rule.result.name, 0) + (ruleTimes * rule.result.amount)
        }
    }
}

private fun Long.divRoundUp(divisor: Long): Long = (this + divisor - 1) / divisor
private data class Ingredient(val amount: Long, val name: String)
private data class Rule(val result: Ingredient, val parts: List<Ingredient>)
