package kryszt.aoc2019

import kryszt.aoc2019.machine.IntCodeMachine
import kryszt.aoc2019.machine.getMachineOrders
import kryszt.tools.Point
import kryszt.tools.printMap

object Main15 {

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1: ${task1()}")
        println("Task 2: ${task2()}")

    }

    private fun buildMaze(): Map<Point, Place> {
        val caveBuilder = CaveBuilder(IntCodeMachine(getMachineOrders(this)))
        return caveBuilder.build()
    }

    private fun task1(): Int {
        return travelToExit(buildMaze())
    }

    private fun task2(): Int {
        val maze = buildMaze()
        val startPoint = maze
                .entries
                .find { (_, p) -> p == Place.TARGET }
                ?.key!!

        maze.print()
        return fillAll(startPoint, maze)
    }

    private fun fillAll(startPoint: Point, map: Map<Point, Place>): Int {
        return fillAll(0, setOf(startPoint), mutableSetOf(), map)
    }

    private fun fillAll(steps: Int, placesToVisit: Set<Point>, visited: MutableSet<Point>, map: Map<Point, Place>): Int {
        if (placesToVisit.isEmpty()) return steps
        visited.addAll(placesToVisit)
        val next = nextSteps(placesToVisit, visited, map)
        return when {
            next.isEmpty() -> steps
            else -> fillAll(steps + 1, next, visited, map)
        }
    }

    private fun travelToExit(map: Map<Point, Place>): Int {
        return travelToExit(0, setOf(Point()), mutableSetOf(), map)
    }

    private fun travelToExit(steps: Int, placesToVisit: Set<Point>, visited: MutableSet<Point>, map: Map<Point, Place>): Int {
        if (placesToVisit.any { map[it] == Place.TARGET }) return steps
        visited.addAll(placesToVisit)
        val next = nextSteps(placesToVisit, visited, map)
        return travelToExit(steps + 1, next, visited, map)
    }

    private fun nextSteps(placesToVisit: Set<Point>, visited: MutableSet<Point>, map: Map<Point, Place>): Set<Point> {
        val nextPlaces = mutableSetOf<Point>()
        placesToVisit.forEach { current ->
            ALL_DIRECTIONS.forEach { direction ->
                nextPlaces.add(current.plus(direction.change))
            }
        }
        return nextPlaces
                .filter { !visited.contains(it) }
                .filter { place -> map[place]?.let { it == Place.TARGET || it == Place.EMPTY } ?: false }
                .toSet()
    }

    class CaveBuilder(private val machine: IntCodeMachine) {

        private val cave = mutableMapOf<Point, Place>()

        fun build(): Map<Point, Place> {
            explore(Point())
            return cave
        }

        private fun explore(point: Point) {
            ALL_DIRECTIONS.forEach { move ->
                move(point, move)
            }
        }

        fun move(startPoint: Point, direction: Direction) {
            val target = startPoint + direction.change
            if (visited(target)) return

            val movedTo = move(direction)
            cave[target] = movedTo

            if (movedTo == Place.EMPTY || movedTo == Place.TARGET) {
                explore(target)
                move(direction.back())
            }
        }

        private fun move(direction: Direction): Place {
            machine.pushInput(direction.id)
            return toPlace(machine.executeTillOutput().intValueExact())
        }

        fun visited(point: Point): Boolean = cave.containsKey(point)
    }

    enum class Place(val key: Int) {
        WALL(0), EMPTY(1), TARGET(2)
    }

    private val ALL_PLACES = Place.values()

    private fun toPlace(key: Int): Place = ALL_PLACES.find { it.key == key }
            ?: throw IllegalArgumentException("No type key $key")

    enum class Direction(val change: Point, val id: Int) {
        NORTH(Point(0, -1), 1),
        SOUTH(Point(0, 1), 2),
        WEST(Point(-1, 0), 3),
        EAST(Point(1, 0), 4)
    }

    private fun Direction.back(): Direction = when (this) {
        Direction.NORTH -> Direction.SOUTH
        Direction.SOUTH -> Direction.NORTH
        Direction.WEST -> Direction.EAST
        Direction.EAST -> Direction.WEST
    }

    private val ALL_DIRECTIONS = Direction.values()

    private fun Map<Point, Place>.print() = printMap { point, place ->
        when {
            point.x == 0 && point.y == 0 -> "*"
            place == Place.WALL -> "#"
            place == Place.EMPTY -> "."
            place == Place.TARGET -> "@"
            else -> " "
        }
    }
}

