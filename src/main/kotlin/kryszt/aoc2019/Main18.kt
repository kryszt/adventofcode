package kryszt.aoc2019

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines
import java.util.*

object Main18 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
        checkSolver()
    }

    @Suppress("unused")
    private fun checkSolver() {
        checkSolver("-test1", 8)
        checkSolver("-test2", 86)
        checkSolver("-test3", 136)
        checkSolver("-2-test1", 8)
        checkSolver("-2-test2", 24)
        checkSolver("-2-test3", 32)
        checkSolver("-2-test4", 72)
    }

    private fun checkSolver(version: String, value: Int) {
        val solved = solveTask(readTask(version))
        if (solved == value) {
            println("$version correct ($value)")
        } else {
            System.err.println("$version incorrect, expected $value, got $solved")
        }
    }


    override fun taskOne() {
        solveTask(readTask()).alsoPrint()
    }

    override fun taskTwo() {
        solveTask(readTask().asTaskTwo()).alsoPrint()
    }

    private fun solveTask(task: Task): Int {
        return GraphSolver(task).solve()?.also { displayPath(it) }?.distance ?: -1
    }

    private fun readTask(version: String = ""): Task {
        val input = readTaskLines(version)
        val startPoints = mutableListOf<Point>()
        val passages = mutableSetOf<Point>()
        val doors = mutableMapOf<Point, Char>()
        val keys = mutableMapOf<Point, Char>()

        input.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                when {
                    c.isUpperCase() -> doors[Point(x, y)] = c
                    c.isLowerCase() -> keys[Point(x, y)] = c
                    c == '.' -> passages.add(Point(x, y))
                    c == '@' -> startPoints.add(Point(x, y))
                }
            }
        }
        //add keys to passages
        if (startPoints.isEmpty()) throw IllegalArgumentException("Start point not found")
        passages.addAll(keys.keys)
        passages.addAll(startPoints)
        passages.addAll(doors.keys)
        return Task(
            starts = startPoints,
            passages = passages,
            keys = keys,
            doors = doors
        )
    }

    private data class PlacesWithKeys(val places: SortedSet<Point>, val keys: Int) {
        companion object {
            fun from(places: Collection<Point>, keys: Int) =
                PlacesWithKeys(places.toSortedSet(Point.comparatorXY), keys)
        }

        fun replacePlace(from: Point, to: Point): PlacesWithKeys {
            val newPlaces = places.minus(from).plus(to)
            return from(newPlaces, keys)
        }
    }

    private data class PlacesWithDistance(val places: PlacesWithKeys, val distance: Int) {
        companion object {
            fun from(places: Collection<Point>, keys: Int, distance: Int) =
                PlacesWithDistance(PlacesWithKeys.from(places, keys), distance)
        }

        var parent: PlacesWithDistance? = null
    }

    private data class TargetInfo(val place: Point, val key: Char, val requires: Int, val distance: Int)
    private data class TargetInfoKey(val place: Point, val key: Char, val requires: Int)

    private class TargetsFinderDepth(val task: Task) {
        private val neighbours: MazeNeighbours<Point> = MazeNeighboursPoint2D(task.passages)
        private var visited = mutableSetOf<Point>()
        private val foundMap = mutableMapOf<TargetInfoKey, Int>()
        private val found: List<TargetInfo>
            get() = foundMap.map {
                TargetInfo(
                    place = it.key.place,
                    key = it.key.key,
                    requires = it.key.requires,
                    distance = it.value
                )
            }

        fun findAllDistances(starting: Point): List<TargetInfo> {
            visit(starting, 0, 0)
            return found
        }

        private fun visit(place: Point, distance: Int, requiredKeys: Int) {
            visited.add(place)

            task.keys[place]?.also { key -> keyReached(place, key, distance, requiredKeys) }
            val nextRequires =
                task.doors[place]?.let { door -> requiredKeys.addKey(door.toLowerCase()) } ?: requiredKeys

            neighbours
                .neighbours(place)
                .filterNot { visited.contains(it) }
                .forEach { nextPlace ->
                    visit(nextPlace, distance + 1, nextRequires)
                }

            visited.remove(place)
        }

        private fun keyReached(place: Point, key: Char, distance: Int, required: Int) {
            val infoKey = TargetInfoKey(
                place = place,
                key = key,
                requires = required
            )

            if (foundMap[infoKey] ?: Int.MAX_VALUE > distance) {
                foundMap[infoKey] = distance
            }
        }
    }

    private class GraphSolver(val task: Task) {

        private val finalKeys = task.keys.values.keyValue()

        fun solve(): PlacesWithDistance? {
            val nextPlaces: TreeSet<PlacesWithDistance> =
                sortedSetOf(
                    comparator = compareBy(PlacesWithDistance::distance)
                        .thenBy { it.places.keys }
                        .thenBy { it.places.places.size }
                        .thenComparing { o1: PlacesWithDistance, o2: PlacesWithDistance ->
                            o1.places.places.zip(o2.places.places).forEach { (p1, p2) ->
                                val compare = Point.comparatorXY.compare(p1, p2)
                                if (compare != 0) return@thenComparing compare
                            }
                            return@thenComparing 0
                        }
                )

            nextPlaces.add(PlacesWithDistance.from(task.starts, 0, 0))

            val knownDistances = mutableMapOf<PlacesWithKeys, PlacesWithDistance>()

            while (nextPlaces.isNotEmpty()) {
                val nextToCheck = nextPlaces.pollFirst()!!

                if (nextToCheck.places.keys == finalKeys) {
                    return nextToCheck
                }

                val directions = nextPlacesToCheck(nextToCheck)
                directions.forEach { direction: PlacesWithDistance ->
                    if (direction.distance < knownDistances[direction.places]?.distance ?: Int.MAX_VALUE) {
                        knownDistances[direction.places] = direction
                        nextPlaces.add(direction)
                    }
                }
            }

            return null
        }

        fun nextPlacesToCheck(from: PlacesWithDistance): List<PlacesWithDistance> {
            return from.places.places.map {
                nextPlacesToCheck(from, it)
            }.flatten()
        }

        private val knownTargets: MutableMap<Point, List<TargetInfo>> = mutableMapOf()

        private fun knownTargets(fromPlace: Point) = knownTargets.getOrPut(fromPlace) {
            TargetsFinderDepth(task).findAllDistances(fromPlace)
        }

        private fun PlacesWithDistance.moved(start: Point, to: Point, withKey: Char, moveBy: Int): PlacesWithDistance {
            val newPlaces = places
                .replacePlace(start, to)
                .copy(keys = places.keys.addKey(withKey))

            return copy(
                places = newPlaces,
                distance = distance + moveBy
            ).apply {
                parent = this@moved
            }
        }

        fun nextPlacesToCheck(from: PlacesWithDistance, start: Point): List<PlacesWithDistance> =
            knownTargets(start)
                .filter { from.places.keys.hasKeys(it.requires) } //only reachable
                .filterNot { from.places.keys.hasKey(it.key) } //skip already got
                .map { target ->
                    from.moved(start, target.place, target.key, target.distance)
                }
    }

    private fun displayPath(endPlace: PlacesWithDistance) {
        val keys = mutableListOf<Pair<Char, Int>>()
        var current: PlacesWithDistance? = endPlace
        while (current?.parent != null) {
            val parent = current.parent
            val newKey =
                current.places.keys.toKeys().minus(current.parent?.places?.keys?.toKeys().orEmpty()).first()
            val distance = current.distance.minus(parent?.distance ?: 0)
            keys.add(newKey to distance)
            current = current.parent
        }
        println("${endPlace.distance} -> ${keys.reversed()}")
    }

    private data class Task(
        val starts: List<Point>,
        val passages: Set<Point>,
        val keys: Map<Point, Char>,
        val doors: Map<Point, Char>
    ) {
        fun asTaskTwo(): Task {
            if (starts.size == 4) return this
            if (starts.size != 1) throw IllegalStateException("One start point is required")
            val originalStart = starts.first()
            val newStarts = listOf(
                originalStart.move(1, 1),
                originalStart.move(1, -1),
                originalStart.move(-1, 1),
                originalStart.move(-1, -1)
            )
            val newWalls = listOf(
                originalStart,
                originalStart.move(1, 0),
                originalStart.move(-1, 0),
                originalStart.move(0, 1),
                originalStart.move(0, -1)
            )
            val newPassages = passages.minus(newWalls)
            return copy(starts = newStarts, passages = newPassages)
        }
    }

    private fun Int.addKey(key: Char): Int = this or (key.keyValue())
    private fun Int.hasKey(key: Char): Boolean = this and key.keyValue() != 0
    private fun Int.hasKeys(keys: Int): Boolean = (this and keys) == keys

    private fun Int.toKeys(): List<Char> {
        val keys = mutableListOf<Char>()
        for (k in 'a'..'z') {
            if (hasKey(k)) keys.add(k)
        }
        return keys
    }

    private fun Char.keyShift(): Int = this - 'a'
    private fun Char.keyValue(): Int = (1 shl this.keyShift())
    private fun Collection<Char>.keyValue(): Int {
        var allKeys = 0
        this.forEach {
            allKeys = allKeys.addKey(it)
        }
        return allKeys
    }

}