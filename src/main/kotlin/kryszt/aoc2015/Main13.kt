package kryszt.aoc2015

import kryszt.tools.ActionProcessor
import kryszt.tools.IoUtil
import kryszt.tools.PermutationGenerator

object Main13 {

    private val PatternRule = "${ActionProcessor.GroupText} would ${ActionProcessor.GroupText} ${ActionProcessor.GroupNumber} happiness units by sitting next to ${ActionProcessor.GroupText}.".toPattern()


    @JvmStatic
    fun main(args: Array<String>) {
        val people = mutableSetOf<String>()
        val rules = mutableMapOf<String, Int>()

//        IoUtil.forEachLine(this, "-test") {
        IoUtil.forEachLine(this) {
            parseRule(it).run {
                people.add(this.person)
                rules[this.key] = this.score
            }
        }

//        people.add("me")

        println(people)
        println(rules)

        var max = Int.MIN_VALUE
        var maxOrder: List<String>? = null
        val generator = PermutationGenerator(people.toList())

        var steps = 0

        while (generator.hasNext) {
            steps++
            val candidateOrder = generator.getNext()
            val candidate = valueOfPermutation(candidateOrder, rules)
            if (candidate > max) {
                max = candidate
                maxOrder = candidateOrder
            }
        }

        println("Steps: $steps")
        println("Max: $max (${valueOfPermutation(maxOrder!!, rules)}) for $maxOrder ")
//        printValueOfPermutation(maxOrder!!, rules)

    }

    fun valueOfPermutation(order: List<String>, rules: Map<String, Int>): Int {
        var score = 0
        val size = order.size

        order.indices.forEach {
            val person = order[it]
            val left = order[(it + 1) % size]
            val right = order[(it - 1 + size) % size]
            score += getScore(person, left, rules)
            score += getScore(person, right, rules)
        }

        return score
    }

    fun printValueOfPermutation(order: List<String>, rules: Map<String, Int>): Int {
        var score = 0
        val size = order.size

        order.indices.forEach {
            val person = order[it]
            val left = order[(it + 1) % size]
            val right = order[(it - 1 + size) % size]
            score += getScore(person, left, rules)
            score += getScore(person, right, rules)
            println("$person -> (left) $left ${getScore(person, left, rules)}")
            println("$person -> (right) $right ${getScore(person, right, rules)}")
            println("$person -> $score")
        }

        println("Score : $score")
        return score
    }

    fun getScore(p1: String, p2: String, rules: Map<String, Int>): Int {
        return rules["$p1-$p2"] ?: 0
    }

    fun parseRule(line: String): Rule {
        return PatternRule.matcher(line).takeIf { it.find() }?.let {
            val sign: Int = if (it.group(2) == "gain") 1 else -1
            Rule(it.group(1), it.group(4), sign * it.group(3).toInt())
        } ?: throw IllegalArgumentException(line)
    }

    data class Rule(val person: String, val person2: String, val score: Int) {
        val key = "$person-$person2"
    }

}
