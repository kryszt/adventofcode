package kryszt.aoc2015

import kryszt.tools.IoUtil
import kryszt.tools.Solver

object Main19 {

    const val molecule = "ORnPBPMgArCaCaCaSiThCaCaSiThCaCaPBSiRnFArRnFArCaCaSiThCaCaSiThCaCaCaCaCaCaSiRnFYFArSiRnMgArCaSiRnPTiTiBFYPBFArSiRnCaSiRnTiRnFArSiAlArPTiBPTiRnCaSiAlArCaPTiTiBPMgYFArPTiRnFArSiRnCaCaFArRnCaFArCaSiRnSiRnMgArFYCaSiRnMgArCaCaSiThPRnFArPBCaSiRnMgArCaCaSiThCaSiRnTiMgArFArSiThSiThCaCaSiRnMgArCaCaSiRnFArTiBPTiRnCaSiAlArCaPTiRnFArPBPBCaCaSiThCaPBSiThPRnFArSiThCaSiThCaSiThCaPTiBSiRnFYFArCaCaPRnFArPBCaCaPBSiRnTiRnFArCaPRnFArSiRnCaCaCaSiThCaRnCaFArYCaSiRnFArBCaCaCaSiThFArPBFArCaSiRnFArRnCaCaCaFArSiRnFArTiRnPMgArF"
    const val molecule2 = "ORnPBPMgArCaCaCaSiThCaCaSiThCaCaPBCaCaCaSiThCaCaSiThCaCaCaCaCaCaCaCaCaSiRnPTiTiBFYPBFArSiRnCaSiRnBSiAlArPTiBPTiRnCaSiAlArCaPTiTiBPMgYFArPBSiRnCaCaFArRnCaFArCaSiRnCaFYCaCaCaCaSiThCaPBCaCaCaCaSiThCaSiRnTiMgArFArSiThSiThCaCaCaCaCaPTiBPTiRnCaSiAlArCaPBPBPBCaCaSiThCaPBSiThCaSiThCaSiThCaSiThCaPTiBCaCaCaCaPBCaCaPBSiRnBCaCaSiRnCaCaCaSiThCaRnCaFArYCaPBCaCaCaSiThFArPBFArCaPRnCaCaCaFArPTiRnPMgArF"
    const val molecule3 = "ORnPBPMgArCaSiThCaSiThCaPBCaSiThCaSiThCaSiRnPTiBFYPBFArSiRnCaSiRnBSiAlArPTiBPTiRnCaSiAlArCaPTiBPMgYFArPBSiRnCaFArRnCaFArCaSiRnCaFYCaSiThCaPBCaSiThCaSiRnTiMgArFArSiThSiThCaPTiBPTiRnCaSiAlArCaPBPBPBCaSiThCaPBSiThCaSiThCaSiThCaSiThCaPTiBCaPBCaPBSiRnBCaSiRnCaSiThCaRnCaFArYCaPBCaSiThFArPBFArCaPRnCaFArPTiRnPMgArF"

    const val testMolecule1 = "HOH"
    const val testMolecule2 = "HOHOHO"

    const val testReplacements = "H => HO\n" +
            "H => OH\n" +
            "O => HH\n" +
            "e => H\n" +
            "e => O"

    @JvmStatic
    fun main(args: Array<String>) {
        val replacements = parseInvertedReplacements(IoUtil.readTaskFileLines(this))
        println(replacements)

        println(tryReplacements(molecule, replacements))
    }

    fun tryReplacements(molecule: String, replacements: Map<String, String>): Int {
        var count = 0
        var current = molecule
        while (current != "e") {
            println(current)
            current = replaceLastMatch(current, replacements)
            count++
        }
        return count
    }

    fun replaceLastMatch(molecule: String, replacements: Map<String, String>): String {
        val from = findLastMatching(molecule, replacements)
        val to = replacements[from]!!
        println("Replace $from -> $to")
        return replaceLast(molecule, from, to)
    }

    fun replaceLast(molecule: String, from: String, to: String): String {
        val startIndex = molecule.lastIndexOf(from)
        return molecule.substring(0, startIndex) + to + molecule.substring(startIndex + from.length)
    }

    fun findLastMatching(molecule: String, replacements: Map<String, String>): String {
        var lastValue = ""
        var lastEnd = 0
        replacements.keys.forEach { key ->
            molecule.lastIndexOf(key).takeIf { it > -1 }?.let { it + key.length }?.takeIf { it > lastEnd }?.let {
                lastEnd = it
                lastValue = key
            }
        }
        return lastValue
    }

    fun replaceAll(molecule: String, replacements: Map<String, String>): Pair<String, Int> {
        var totalReplacements = 0
        var newestMolecule = molecule
        do {
            var replacedInRound = 0
            replacements.forEach { k, v ->

                countAndReplace(newestMolecule, k, v).let {
                    replacedInRound += it.second
                    newestMolecule = it.first
                }
            }
            if (replacedInRound == 0) {
                return newestMolecule to totalReplacements
            } else {
                totalReplacements += replacedInRound
            }

        } while (replacedInRound != 0)
        return newestMolecule to totalReplacements
    }

    fun countAndReplace(molecule: String, from: String, to: String): Pair<String, Int> {
        val newMolecule = molecule.replace(from, to)

        val countFrom = countTotalMolecules(from)
        val countTo = countTotalMolecules(to)
        val countBefore = countTotalMolecules(molecule)
        val countAfter = countTotalMolecules(newMolecule)

        val count = (countBefore - countAfter) / (countFrom - countTo)

        return newMolecule to count
    }

    @JvmStatic
    fun main1(args: Array<String>) {
//        val replacements = parseReplacements(testReplacements.split("\n"))
        val replacements = parseReplacements(IoUtil.readTaskFileLines(this))

        println(replacements)
        println(allProducts(molecule, replacements).size)
    }

    fun parseReplacements(lines: List<String>): Map<String, MutableList<String>> {
        val map = mutableMapOf<String, MutableList<String>>()

        lines.map { it.split("=>").let { Pair(it[0].trim(), it[1].trim()) } }.forEach {
            map.getOrPut(it.first, { mutableListOf() }).add(it.second)
        }

        return map
    }

    fun parseInvertedReplacements(lines: List<String>): Map<String, String> {
        return lines.map { it.split("=>").let { Pair(it[1].trim(), it[0].trim()) } }.toMap()
    }

    fun allProducts(molecule: String, replacements: Map<String, List<String>>): Set<String> {
        val products = mutableSetOf<String>()

        replacements.forEach { from, toList ->
            toList.forEach { to ->
                makeProducts(molecule, from, to, products)
            }
        }
        return products
    }

    fun makeProducts(molecule: String, from: String, to: String, output: MutableSet<String> = mutableSetOf()): Set<String> {
        var foundAt = molecule.indexOf(from, 0)
        while (foundAt >= 0 && foundAt < molecule.length) {
            val end = foundAt + from.length
            val newMolecule = molecule.substring(0, foundAt) + to + molecule.substring(end)
//            println("replace from $from with $to, $foundAt-$end (${molecule.substring(foundAt, end)}) => $newMolecule")
            output.add(newMolecule)
            foundAt = molecule.indexOf(from, end)
        }
        return output
    }

    fun countMolecules(molecule: String): Map<String, Int> {
        val counters = sortedMapOf<String, Int>()
        splitMolecules(molecule).forEach {
            counters[it] = 1 + (counters[it] ?: 0)
        }
        return counters
    }

    fun splitMolecules(molecule: String): List<String> {
        val split = mutableListOf<String>()

        var lastStart = 0
        for (i in 1 until molecule.length) {
            val charAt = molecule[i]
            if (charAt.isUpperCase()) {
                val atom = molecule.substring(lastStart, i)
                split.add(atom)
                lastStart = i
            }
        }
        split.add(molecule.substring(lastStart, molecule.length))
        return split
    }

    fun countTotalMolecules(molecule: String): Int {
        return molecule.count { it.isUpperCase() }
    }

    class MoleculeSolver(val replacements: Map<String, List<String>>) : Solver<String>() {
        val visited = mutableSetOf<String>()

        override fun visiting(state: String) {
//            println("Visiting: $state")
            visited.add(state)
        }

        override fun wasVisited(state: String): Boolean = visited.contains(state)

        override fun isEnd(state: String): Boolean =
                state == "HF" || state == "NAl" || state == "OMg"

        override fun selectMoves(state: String): Collection<String> {
            val allPossible = allProducts(state, replacements)
//            val minLen = allPossible.minByOrNull { it.length }!!.length
//            return allPossible.filter { it.length == minLen }
//            return allPossible.sortedBy { it.length }.subList(0, 1)
            return allPossible
        }

        override fun allowed(state: String): Boolean {
            return true
        }

    }
}
