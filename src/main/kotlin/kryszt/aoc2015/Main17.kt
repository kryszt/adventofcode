package kryszt.aoc2015

object Main17 {

    val input = "11\n" +
            "30\n" +
            "47\n" +
            "31\n" +
            "32\n" +
            "36\n" +
            "3\n" +
            "1\n" +
            "5\n" +
            "3\n" +
            "32\n" +
            "36\n" +
            "15\n" +
            "11\n" +
            "46\n" +
            "26\n" +
            "28\n" +
            "1\n" +
            "19\n" +
            "3"

    val testInput = "20\n" +
            "15\n" +
            "10\n" +
            "5\n" +
            "5"

    @JvmStatic
    fun main(args: Array<String>) {
        val sizes = input.split("\n").map { it.toInt() }.sorted()
//        val sizes = testInput.split("\n").map { it.toInt() }.sorted()

//        val possibilities = countMatches(25, sizes, 0, emptyList())
//        val possibilities = countMatches(150, sizes, 0, emptyList())

//        println(countMatches(25, sizes, 0, emptyList(), 2))
        println(countMatches(150, sizes, 0, emptyList(), 4))
    }

    fun countMatches(required: Int, sizes: List<Int>, startWith: Int, selected: List<Int>, maxLength: Int = Int.MAX_VALUE): Int {
        val count: Int = sizes.size
        if (startWith >= count) {
            return 0
        }

        var optionsCount = 0
        for (i in startWith until count) {
            val left = required - sizes[i]
            val newFound = selected + sizes[i]
            when {
                left == 0 -> {
                    if (newFound.size <= maxLength) {
                        optionsCount++
                        println("Found $newFound ")
                    }
                }
                left > 0 -> optionsCount += countMatches(left, sizes, i + 1, newFound, maxLength)
                left < 0 -> return optionsCount
            }
        }

        return optionsCount
    }

    fun minLength(required: Int, sizes: List<Int>, startWith: Int, found: List<Int>): Int {
        val count: Int = sizes.size
        if (startWith >= count) {
            return Int.MAX_VALUE
        }

        var maxLen = Int.MAX_VALUE
        for (i in startWith until count) {
            val left = required - sizes[i]
            val newFound = found + sizes[i]
            when {
                left == 0 -> {
                    println("Found $newFound ")
                    return newFound.size
                }
                left > 0 -> {
                    val foundMax = minLength(left, sizes, i + 1, newFound)
                    if (foundMax < maxLen) {
                        maxLen = foundMax
                    }
                }
                left < 0 -> return maxLen
            }
        }

        return maxLen
    }

}
