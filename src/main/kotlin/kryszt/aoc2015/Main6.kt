package kryszt.aoc2015

import kryszt.tools.ActionProcessor
import kryszt.tools.ActionProcessor.ActionPattern
import kryszt.tools.ActionProcessor.Companion.GroupNumber
import kryszt.tools.IoUtil
import kryszt.tools.Point

object Main6 {
    val PTurnOn = ActionPattern("turnOn", "turn on $GroupNumber,$GroupNumber through $GroupNumber,$GroupNumber".toPattern())
    val PTurnOff = ActionPattern("turnOff", "turn off $GroupNumber,$GroupNumber through $GroupNumber,$GroupNumber".toPattern())
    val PToggle = ActionPattern("toggle", "toggle $GroupNumber,$GroupNumber through $GroupNumber,$GroupNumber".toPattern())

    @JvmStatic
    fun main(args: Array<String>) {

        val processor = ActionProcessor6(listOf(PToggle, PTurnOff, PTurnOn))

        IoUtil.forEachLine(this) {
            processor.executeLine(it)
        }

        println("Lit ${processor.countPower()}")

    }

    class ActionProcessor6(patterns: List<ActionProcessor.ActionPattern>) : ActionProcessor(patterns) {

        val lights: Array<Array<Int>> = Array(1000, { Array(1000, { 0 }) })

        //version 2
        override fun execute(action: Action) {
            when (action.action) {
                PTurnOn.action -> execute(parseFrom(action.args), parseTo(action.args), { it + 1 })
                PTurnOff.action -> execute(parseFrom(action.args), parseTo(action.args), { Math.max(0, it - 1) })
                PToggle.action -> execute(parseFrom(action.args), parseTo(action.args), { it + 2 })
            }
        }

//        override fun execute(action: Action) {
//            when (action.action) {
//                PTurnOn.action -> execute(parseFrom(action.args), parseTo(action.args), { 1 })
//                PTurnOff.action -> execute(parseFrom(action.args), parseTo(action.args), { 0 })
//                PToggle.action -> execute(parseFrom(action.args), parseTo(action.args), { 1 - it })
//            }
//        }

        fun execute(from: Point, to: Point, newValue: (oldValue: Int) -> Int) {
            for (x in from.x..to.x) {
                for (y in from.y..to.y) {
                    lights[y][x] = newValue(lights[y][x])
                }
            }
        }

        fun countLit(): Int =
                lights.sumBy { it.count { it > 0 } }

        fun countPower(): Int =
                lights.sumBy { it.sum() }

        fun parseFrom(args: List<String>) = Point(args[0].toInt(), args[1].toInt())
        fun parseTo(args: List<String>) = Point(args[2].toInt(), args[3].toInt())
    }
}
