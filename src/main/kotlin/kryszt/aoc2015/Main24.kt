package kryszt.aoc2015

object Main24 {

    val testItems = listOf(1, 2, 3, 4, 5, 7, 8, 9, 10, 11)
    val items = listOf(1, 3, 5, 11, 13, 17, 19, 23, 29, 31, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113)

//            val sizes = testItems
    val sizes = items

    val size = sizes.sum() / 4


    @JvmStatic
    fun main(args: Array<String>) {

        println(sizes.sum())

        var best = emptyList<Int>()

        var minLength = Int.MAX_VALUE
        var minValue = Long.MAX_VALUE


        countMatches(size, sizes, 0, emptyList(), 6, { selected ->
            if (selected.size < minLength) {
                minLength = selected.size; println("New shortest = $selected")
                minValue = product(selected)
                best = selected
            } else if (selected.size == minLength) {
                val product = product(selected)
                if (product < minValue) {
                    minValue = product
                    best = selected
                }
            }
        })
        println("Best $best $minValue")
    }

    fun product(list: List<Int>): Long {
        var product = 1L
        list.forEach { product *= it }
        return product
    }

    fun countMatches(required: Int, sizes: List<Int>, startWith: Int, selected: List<Int>, maxLength: Int = Int.MAX_VALUE, onFound: (List<Int>) -> Unit): Int {
        val count: Int = sizes.size
        if (startWith >= count) {
            return 0
        }

        var optionsCount = 0
        for (i in startWith until count) {
            val left = required - sizes[i]
            val newFound = selected + sizes[i]
            when {
                left == 0 -> {
                    if (newFound.size <= maxLength) {
                        optionsCount++
                        onFound(newFound)
                    }
                }
                left > 0 -> optionsCount += countMatches(left, sizes, i + 1, newFound, maxLength, onFound)
                left < 0 -> return optionsCount
            }
        }

        return optionsCount
    }


//    data class
}
