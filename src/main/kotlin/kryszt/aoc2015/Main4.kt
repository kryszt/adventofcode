package kryszt.aoc2015

import kryszt.tools.MD5Util

object Main4 {

    @JvmStatic
    fun main(args: Array<String>) {

//        val base = "abcdef"
//        val base = "pqrstuv"
        val base = "yzbqklnj"

        var index = 1

        while (true) {
            if (index % 100000 == 0)
                println("Check: $index")

            val hash = MD5Util.hashedHexString("$base$index")
//            println(hash)
            if (hash.startsWith("000000")) {
                println("Found: $index")
                return
            }
            index++


        }
    }

}
