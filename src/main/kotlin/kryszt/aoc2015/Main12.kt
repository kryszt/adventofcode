package kryszt.aoc2015


import kryszt.tools.IoUtil


object Main12 {

    val PNumbers = "([-]{0,1}[0-9]+)".toPattern()
    val Red = ":\"red\""

    @JvmStatic
    fun main(args: Array<String>) {
//        val txt="[[\"green\",[{\"e\":\"green\",\"a\":77,\"d\":{\"c\":\"violet\",\"a\":\"yellow\",\"b\":\"violet\"},\"c\":\"yellow\",\"h\":\"red\",\"b\":144,\"g\":{\"a\":[\"yellow\",-48,72,87,{\"e\":\"violet\",\"c\":123,\"a\":101,\"b\":87,\"d\":\"red\",\"f\":88},{\"e\":\"red\",\"c\":2,\"a\":1,\"g\":\"blue\",\"b\":\"green\",\"d\":\"violet\",\"f\":170},\"orange\",171,162]},\"f\":\"orange\",\"i\":\"orange\"},49,[{\"c\":{\"e\":\"violet\",\"a\":-44,\"d\":115,\"c\":117,\"h\":194,\"b\":{\"e\":-17,\"a\":172,\"d\":\"green\",\"c\":197,\"h\":53,\"b\":106,\"g\":\"violet\",\"f\":-10},\"g\":\"red\",\"f\":\"orange\"},\"a\":-49,\"b\":[\"violet\",\"orange\",\"blue\"]}],\"green\"]],[\"orange\"],{\"e\":\"blue\",\"a\":[\"red\",\"yellow\"],\"d\":{\"a\":[{\"c\":{\"a\":181,\"b\":[\"orange\",-40,\"red\",\"orange\",\"yellow\",31,60,71,\"yellow\"]},\"a\":[114,-40],\"b\":\"orange\"},[\"green\",93,10,{\"c\":11,\"a\":170,\"b\":[161,-3],\"d\":-16},58,{\"e\":{\"c\":-2,\"a\":117,\"b\":\"violet\"},\"c\":"
//        val txt = IoUtil.readTaskFile(this, "-2")
//        println(findNumbers(txt).sum())

        println(findNumbers(cleanOfRed(IoUtil.readTaskFile(this))).sum())

        println(findNumbers(cleanOfRed("[1,2,3]")).sum())
        println(findNumbers(cleanOfRed("[1,{\"c\":\"red\",\"b\":2},3]")).sum())
        println(findNumbers(cleanOfRed("{\"d\":\"red\",\"e\":[1,2,3,4],\"f\":5}")).sum())
        println(findNumbers(cleanOfRed("[1,\"red\",5]")).sum())
    }

//    [1,2,3] still has a sum of 6.
//    [1,{"c":"red","b":2},3] now has a sum of 4, because the middle object is ignored.
//    {"d":"red","e":[1,2,3,4],"f":5} now has a sum of 0, because the entire structure is ignored.
//    [1,"red",5] has a sum of 6, because "red" in an array has no effect.

    fun findNumbers(line: CharSequence): List<Int> {
        val matcher = PNumbers.matcher(line)
        val pairs = mutableListOf<Int>()
        while (matcher.find()) {
            pairs.add(matcher.group().toInt())
        }
        return pairs
    }

    fun cleanOfRed(text: String): String {
        var work = text
        var foundAt: Int
        do {
            foundAt = indexOfRedValue(work)
            if (foundAt != -1) {
                work = cleanAround(work, foundAt)
            }
        } while (foundAt != -1)
        return work
    }

    fun cleanAround(text: String, around: Int): String {
        val start = findObjectStart(text, around)
        val end = findObjectEnd(text, around)
        return text.substring(0, start) + text.substring(end + 1)
    }

    fun indexOfRedValue(text: String): Int {
        return text.indexOf(Red)
    }

    fun findObjectStart(text: String, from: Int): Int {
        var openings = 0
        for (i in from downTo 0) {
            if (text[i] == '}') {
                openings--
            } else if (text[i] == '{') {
                openings++
                if (openings == 1) {
                    return i
                }
            }
        }

        return -1
    }

    fun findObjectEnd(text: String, from: Int): Int {
        var closings = 0
        for (i in from until text.length) {
            if (text[i] == '{') {
                closings--
            } else if (text[i] == '}') {
                closings++
                if (closings == 1) {
                    return i
                }
            }
        }

        return -1
    }
}
