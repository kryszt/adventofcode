package kryszt.aoc2015

import kryszt.tools.IoUtil

object Main1 {

    @JvmStatic
    fun main(args: Array<String>) {

        println(getFloor("(())"))
        println(getFloor("()()"))
        println(getFloor("((("))
        println(getFloor("(()(()("))
        println(getFloor("))((((("))
        println(getFloor("())"))
        println(getFloor("))("))
        println(getFloor(")))"))
        println(getFloor(")())())"))
        println(getFloor(IoUtil.readTaskFile(this)))

        println(firstBasement(")"))
        println(firstBasement("()())"))
        println(firstBasement(IoUtil.readTaskFile(this)))
    }

    fun getFloor(instruction: String): Int {
        return instruction.count { it == '(' } - instruction.count { it == ')' }
    }

    fun firstBasement(instruction: String): Int {
        var level = 0
        instruction.forEachIndexed { index, c ->

            level += (if (c == '(') 1 else -1)

            if (level < 0) {
                return index + 1
            }
        }
        return -1
    }
}
