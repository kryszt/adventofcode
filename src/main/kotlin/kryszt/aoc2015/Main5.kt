package kryszt.aoc2015

import kryszt.tools.IoUtil

object Main5 {
    val RulePair = "([a-z|0-9])\\1".toRegex()
    val ThreeVowels = "[aeiou].*[aeiou].*[aeiou]".toRegex()
    val Forbidden = "ab|cd|pq|xy".toRegex()

    val TwoPairs = "([a-z][a-z]).*\\1".toRegex()
    val SplitPair = "([a-z|0-9]).\\1".toRegex()

    @JvmStatic
    fun main(args: Array<String>) {

        println(isNice(""))
        println(isNice("ugknbfddgicrmopn"))
        println(isNice("aaa"))
        println(isNice("jchzalrnumimnmhp"))
        println(isNice("haegwjzuvuyypxyu"))
        println(isNice("dvszwmarrgswjxmb"))
        println()
        println(isNice2("qjhvhtzxzqqjkmpb"))
        println(isNice2("xxyxx"))
        println(isNice2("uurcxstgmygtbstg"))
        println(isNice2("ieodomkazucvgmuy"))



        var count = 0

        IoUtil.forEachLine(this) { line ->
            val nice = isNice2(line)
            println("$line -> $nice")
            if (nice) {
                count++
            }
        }

        println("Nice count: $count")
    }

    fun isNice(line: String): Boolean {
        return !Forbidden.containsMatchIn(line) && RulePair.containsMatchIn(line) && ThreeVowels.containsMatchIn(line)
    }

    fun isNice2(line: String): Boolean {
        return TwoPairs.containsMatchIn(line) && SplitPair.containsMatchIn(line)
    }
}
