package kryszt.aoc2015

import kryszt.tools.ActionProcessor

object Main14 {


    val parser = "${ActionProcessor.GroupText} can fly ${ActionProcessor.GroupNumber} km/s for ${ActionProcessor.GroupNumber} seconds, but then must rest for ${ActionProcessor.GroupNumber} seconds.".toPattern()

    val testInput = "Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.\n" +
            "Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds."

    val input = "Dancer can fly 27 km/s for 5 seconds, but then must rest for 132 seconds.\n" +
            "Cupid can fly 22 km/s for 2 seconds, but then must rest for 41 seconds.\n" +
            "Rudolph can fly 11 km/s for 5 seconds, but then must rest for 48 seconds.\n" +
            "Donner can fly 28 km/s for 5 seconds, but then must rest for 134 seconds.\n" +
            "Dasher can fly 4 km/s for 16 seconds, but then must rest for 55 seconds.\n" +
            "Blitzen can fly 14 km/s for 3 seconds, but then must rest for 38 seconds.\n" +
            "Prancer can fly 3 km/s for 21 seconds, but then must rest for 40 seconds.\n" +
            "Comet can fly 18 km/s for 6 seconds, but then must rest for 103 seconds.\n" +
            "Vixen can fly 18 km/s for 5 seconds, but then must rest for 84 seconds."
    const val time = 2503
    const val testTime = 1000

    @JvmStatic
    fun main(args: Array<String>) {

        val animals = input.split("\n").map { parse(it) }
//        val animals = testInput.split("\n").map { parse(it) }

        val counters = mutableMapOf<String, Int>()

        for (i in 1..time) {
            val best = animals.map { countDistance(it, i) }.maxOrNull()!!
            animals.forEach {
                if (best == countDistance(it, i)) {
                    counters[it.name] = 1 + (counters[it.name] ?: 0)
                }
            }
        }

        println(counters)

//        animals.forEach { println("$it ${countDistance(it,2503)}") }
//        println(animals.map { countDistance(it, 2503) }.maxOrNull())
//        println(animals.map { countDistance(it, 2503) }.maxOrNull())


    }

    fun parse(line: String): Reindeer {
        return parser.matcher(line).takeIf { it.find() }?.let {
            Reindeer(it.group(1), it.group(2).toInt(), it.group(3).toInt(), it.group(4).toInt())
        } ?: run { throw IllegalArgumentException(line) }
    }

    fun countDistance(r: Reindeer, time: Int): Int {
        val times = time / r.total
        val left = time % r.total

        val totalFlight = times * r.flightTime + Math.min(left, r.flightTime)
        return totalFlight * r.speed
    }

    data class Reindeer(val name: String, val speed: Int, val flightTime: Int, val restTime: Int, val total: Int = flightTime + restTime)

}
