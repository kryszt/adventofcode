package kryszt.aoc2015

object Main20 {

    //    const val limit = 600
    const val limit = 36000000

    @JvmStatic
    fun main(args: Array<String>) {
//        faster()
        faster2()
//        slower()
    }

    fun slower() {
        var house = 830000
        do {
            var presents = 0
            if (house % 10000 == 0) {
                println("Checking house $house")
            }
            for (elf in 1..house) {
                if (house % elf == 0) {
                    presents += (10 * elf)
                    if (presents >= limit) {
                        println("Matched for house $house")
                        return
                    }
                }
            }
            house++

        } while (true)
    }

    fun faster() {
        var house = 1
        do {
            var presents = 0
            if (house % 10000 == 0) {
                println("Checking house $house")
            }
            val lastToCheck = Math.sqrt(house.toDouble()).toInt()
            for (elf in 1..lastToCheck) {
                if (house % elf == 0) {
                    presents += (10 * elf)
                    val otherElf = house / elf
                    if (otherElf > elf) {
//                        println("House $house, $elf, $otherElf")
                        presents += (10 * otherElf)
                    }
                    if (presents >= limit) {
                        println("Matched for house $house")
                        return
                    }
                }
            }
//            println("House $house got $presents presents.")
            house++

        } while (true)

    }

    fun faster2() {
        val maxDelivered = 50

        var house = 1
        do {
            var presents = 0
            if (house % 10000 == 0) {
                println("Checking house $house")
            }
            val lastToCheck = Math.sqrt(house.toDouble()).toInt()
            for (elf in 1..lastToCheck) {
                if (house % elf == 0) {
                    if (house / elf <= maxDelivered) {
                        presents += (11 * elf)
                    }

                    val otherElf = house / elf
                    if ((otherElf > elf) && (house / otherElf <= maxDelivered)) {
//                        println("House $house, $elf, $otherElf")
                        presents += (11 * otherElf)
                    }
                    if (presents >= limit) {
                        println("Matched for house $house")
                        return
                    }
                }
            }
//            println("House $house got $presents presents.")
            house++

        } while (true)

    }
}
