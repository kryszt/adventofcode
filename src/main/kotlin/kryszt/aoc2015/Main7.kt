package kryszt.aoc2015

import kryszt.tools.ActionProcessor
import kryszt.tools.ActionProcessor.ActionPattern
import kryszt.tools.ActionProcessor.Companion.GroupAny
import kryszt.tools.ActionProcessor.Companion.GroupNumber
import kryszt.tools.ActionProcessor.Companion.GroupText
import kryszt.tools.IoUtil

object Main7 {
    val PInput = ActionPattern("input", "^$GroupAny -> $GroupText".toPattern())
    val POr = ActionPattern("or", "^$GroupAny OR $GroupAny -> $GroupText".toPattern())
    val PAnd = ActionPattern("and", "^$GroupAny AND $GroupAny -> $GroupText".toPattern())
    val PLShift = ActionPattern("lshift", "^$GroupAny LSHIFT $GroupNumber -> $GroupText".toPattern())
    val PRShift = ActionPattern("rshift", "^$GroupAny RSHIFT $GroupNumber -> $GroupText".toPattern())
    val PNot = ActionPattern("not", "^NOT $GroupAny -> $GroupText".toPattern())

    val input = "123 -> x\n" +
            "456 -> y\n" +
            "x AND y -> d\n" +
            "x OR y -> e\n" +
            "x LSHIFT 2 -> f\n" +
            "y RSHIFT 2 -> g\n" +
            "NOT x -> h\n" +
            "NOT y -> i"

    @JvmStatic
    fun main(args: Array<String>) {

        val processor = ActionProcessor7(listOf(PInput, PAnd, POr, PLShift, PRShift, PNot))

//        input.split("\n").forEach {
        IoUtil.forEachLine(this,"-2") {
            processor.executeLine(it)
        }

        processor.wires.forEach { t, u -> println("$t -> ${u.value}") }
    }

    class ActionProcessor7(patterns: List<ActionProcessor.ActionPattern>) : ActionProcessor(patterns) {

        val wires = sortedMapOf<String, Wire>()

        override fun executeLine(line: String) {
            println("Executing $line")
            super.executeLine(line)
        }

        //version 2
        override fun execute(action: Action) {
            val args = action.args
            when (action.action) {
                PInput.action -> Gate(PInput.action, listOf(getWire(args[0])), getWire(args[1]), action = { g -> g.first() })
                POr.action -> Gate(POr.action, listOf(getWire(args[0]), getWire(args[1])), getWire(args[2]), action = { g -> g.first() or g.second() })
                PAnd.action -> Gate(PAnd.action, listOf(getWire(args[0]), getWire(args[1])), getWire(args[2]), action = { g -> g.first() and g.second() })
                PLShift.action -> {
                    val moveBy = args[1].toInt()
                    Gate(PLShift.action, listOf(getWire(args[0])), getWire(args[2]), action = { g -> (g.first() shl moveBy) and 0xFFFF })
                }
                PRShift.action -> {
                    val moveBy = args[1].toInt()
                    Gate(PRShift.action, listOf(getWire(args[0])), getWire(args[2]), action = { g -> (g.first() shr moveBy) and 0xFFFF })
                }

                PNot.action -> Gate(PNot.action, listOf(getWire(args[0])), getWire(args[1]), action = { g -> g.first().inv() and 0xFFFF })
            }
        }

        fun getWire(id: String): Wire {
            return (id.toIntOrNull()?.let { Wire(null, it) } ?: wires.getOrPut(id, { Wire(id) })).also {
                println("$id -> $it")
            }
        }

    }

    interface Receiver {
        fun receive(value: Int)
    }

    data class Wire(val id: String? = null, var value: Int? = null) : Receiver {
        private val connections = mutableListOf<Receiver>()

        fun hasValue(): Boolean = value != null

        fun connect(receiver: Receiver) {
            value?.let { receiver.receive(it) }
            connections.add(receiver)
        }

        override fun receive(value: Int) {
            if (this.value == null) {
                this.value = value
                connections.forEach { it.receive(value) }
            } else if (this.value != value) {
                throw IllegalStateException("Already initialized in $this")
            }
        }
    }

    data class Gate(val type: String, val inWires: List<Wire>, val outWire: Wire, val action: (Gate) -> Int) : Receiver {

        init {
            sendIfConnected()
            inWires.forEach {
                it.connect(this)
            }
        }

        override fun receive(value: Int) {
            sendIfConnected()
        }

        fun sendIfConnected() {
            if (hasAll()) {
                val value = action(this)
                println("Sending $value from $this to $outWire")
                outWire.receive(value)
            }
        }

        fun hasAll() =
                inWires.all { it.hasValue() }

        fun first(): Int = inWires[0].value!!
        fun second(): Int = inWires[1].value!!
    }

}
