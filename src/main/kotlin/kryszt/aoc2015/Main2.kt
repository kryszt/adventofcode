package kryszt.aoc2015

import kryszt.tools.IoUtil

object Main2 {

    @JvmStatic
    fun main(args: Array<String>) {
        println(getArea("2x3x4"))
        println(getArea("1x1x10"))

        println()
        println(getRibbon("2x3x4"))
        println(getRibbon("1x1x10"))

        var totalArea = 0
        var totalRibbon = 0
        IoUtil.forEachLine(this) {
            totalArea += getArea(it)
            totalRibbon += getRibbon(it)
        }

        println("Area $totalArea")
        println("Ribbon $totalRibbon")
    }

    fun getArea(line: String): Int {
        val dims = line.split("x").map { it.toInt() }
        val lw = dims[0] * dims[1]
        val wh = dims[1] * dims[2]
        val hl = dims[2] * dims[0]

        return 2 * (lw + wh + hl) + Math.min(Math.min(lw, wh), hl)
    }

    fun getRibbon(line: String): Int {
        val dims = line.split("x").map { it.toInt() }
        val l = dims[0]
        val w = dims[1]
        val h = dims[2]

        val max = Math.max(l, Math.max(w, h))

        return (2 * (l + w + h - max)) + (l * w * h)

    }
}
