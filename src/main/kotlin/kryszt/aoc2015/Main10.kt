package kryszt.aoc2015

object Main10 {

    @JvmStatic
    fun main(args: Array<String>) {

        var line = "1113222113"

        for (i in 1..50) {
            line = expand(line)
//            println(line)
        }

        println(line.length)
    }

    fun expand(str: String): String {
        val builder = StringBuilder()
        var lastChar = str[0]
        var count = 1
        for (i in 1 until str.length) {
            val char = str[i]
            if (char == lastChar) {
                count++
            } else {
                builder.append(count)
                builder.append(lastChar)
                lastChar = char
                count = 1
            }
        }
        builder.append(count)
        builder.append(lastChar)
        return builder.toString()
    }
}
