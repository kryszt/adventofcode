package kryszt.aoc2015

import kryszt.tools.BasicProcessor
import kryszt.tools.IoUtil

object Main23 {

    val rules = "inc a\n" +
            "jio a +2\n" +
            "tpl a\n" +
            "inc a"

    @JvmStatic
    fun main(args: Array<String>) {
//        val instructions = rules.split("\n").map { it.split(" ").toMutableList() }
        val instructions = IoUtil.readTaskFileLines(this).map { it.split(" ").toMutableList() }

        val processor = Processor23(instructions)
        processor.write("a", 1)
        while (processor.canStep()) {
            processor.step()
        }
        println(processor.registers)
    }

    class Processor23(instructions: List<MutableList<String>>) : BasicProcessor(instructions) {

        override fun executeInstruction(order: String, args: List<String>): Boolean {
            println("Executing $order $args")
            when (order) {
                "hlf" -> {
                    write(args[0], valueOrRegister(args[0]) / 2)
                    return true
                }
                "tpl" -> {
                    write(args[0], 3 * valueOrRegister(args[0]))
                    return true
                }
                "inc" -> {
                    write(args[0], valueOrRegister(args[0]) + 1)
                    return true
                }
                "jmp" -> {
                    movePointer(args[0].toInt())
                    return false
                }
                "jie" -> {
                    if (valueOrRegister(args[0]) % 2 == 0) {
                        movePointer(args[1].toInt())
                        return false
                    }
                    return true
                }
                "jio" -> {
                    if (valueOrRegister(args[0]) == 1) {
                        movePointer(args[1].toInt())
                        return false
                    }
                    return true
                }

                else -> throw IllegalArgumentException("$order $args")
            }
            return super.executeInstruction(order, args)
        }
    }
}
