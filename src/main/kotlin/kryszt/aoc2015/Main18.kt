package kryszt.aoc2015

import kryszt.tools.IoUtil

object Main18 {

    const val test = ".#.#.#\n" +
            "...##.\n" +
            "#....#\n" +
            "..#...\n" +
            "#.#..#\n" +
            "####.."

    @JvmStatic
    fun main(args: Array<String>) {
//        var world = readWorld(test)
        var world = readWorld(IoUtil.readTaskFile(this))

        lightUpCorners(world)
        println(printWorld(world))

        for (i in 1..100) {
            println()
            world = nextWorld(world)
            lightUpCorners(world)
//            println(printWorld(world))
            println(counOn(world))
        }
        println(printWorld(world))
    }

    fun readWorld(worldString: String): Array<IntArray> {
        return worldString.split("\n").map { line ->
            line.trim().map {
                if (it != '#' && it != '.') {
                    throw IllegalArgumentException("Unsupported = ${it.toInt()}")
                }
                if (it == '#') 1 else 0
            }.toIntArray()
        }.toTypedArray()
    }

    fun lightUpCorners(realm: Array<IntArray>) {
        realm[0][0] = 1
        realm[0][realm.lastIndex] = 1
        realm[realm[0].lastIndex][0] = 1
        realm[realm[realm.lastIndex].lastIndex][realm.lastIndex] = 1
    }

    fun printWorld(realm: Array<IntArray>): String =
            realm.joinToString("\n", transform = { it.joinToString("", transform = { if (it == 0) "." else "#" }) })

    fun nextWorld(realm: Array<IntArray>): Array<IntArray> {
        val nextWorld = Array(realm.size, { IntArray(realm[0].size) })
        realm.forEachIndexed { indexY, ints ->
            ints.indices.forEach { indexX ->
                nextWorld[indexY][indexX] = nextState(indexX, indexY, realm)
            }
        }
        return nextWorld
    }

    fun nextState(px: Int, py: Int, realm: Array<IntArray>): Int {
        val value = realm[py][px]
        val around = coundAround(px, py, realm)

        return if (value == 1) {
            if ((around == 2) || (around == 3)) 1 else 0
        } else {
            if (around == 3) 1 else 0
        }
    }

    fun coundAround(px: Int, py: Int, realm: Array<IntArray>): Int {
        var count = 0
        for (x in (px - 1)..(px + 1)) {
            for (y in (py - 1)..(py + 1)) {
                val atPoint: Int = realm.getOrNull(y)?.let { row -> row.getOrNull(x) } ?: 0
                count += atPoint
            }
        }
        count -= realm[py][px]
        return count
    }

    fun counOn(realm: Array<IntArray>): Int {
        return realm.sumBy { it.sum() }
    }

}
