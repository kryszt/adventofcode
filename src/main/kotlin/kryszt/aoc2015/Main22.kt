package kryszt.aoc2015

import kryszt.tools.Solver

object Main22 {

    @JvmStatic
    fun main(args: Array<String>) {
        val startState = GameState(playersTurn = true, playerHp = 50, bossHp = 55, mana = 500)

        val solver = Solver22()

        solver.solve(listOf(startState))

        println("Best ${solver.best}")
    }

    class Solver22 : Solver<GameState>() {

        var best: GameState = GameState(true, 0, 0, 0, totalSpent = Int.MAX_VALUE)

        override fun visiting(state: GameState) {
            state.takeIf { it.bossHp <= 0 && it.playerHp > 0 }?.also { println("Win in $it") }
                    ?.takeIf { it.totalSpent < best.totalSpent }?.let { best = it }
        }

        override fun wasVisited(state: GameState): Boolean = false

        override fun isEnd(state: GameState): Boolean = false

        override fun selectMoves(state: GameState): Collection<GameState> {
            if (state.totalSpent >= best.totalSpent) {
                return emptyList()
            }
            if (isWin(state) || isLoss(state)) {
                return emptyList()
            }
            val withEffects = applyEffects(state)
            if (!allowed(withEffects)) {
                return emptyList()
            } else if (isWin(withEffects)) {
                return listOf(withEffects)
            }
            val nextTurn = withEffects.copy(playersTurn = !withEffects.playersTurn)
            if (state.playersTurn) {
                val nextStates = mutableListOf<GameState>()

                //MM
                if (nextTurn.mana >= 53) {
                    nextStates.add(nextTurn.copy(mana = nextTurn.mana - 53, totalSpent = nextTurn.totalSpent + 53, bossHp = nextTurn.bossHp - 4, history = nextTurn.history + "M"))
                }

                //Drain
                if (nextTurn.mana >= 73) {
                    nextStates.add(nextTurn.copy(mana = nextTurn.mana - 73, totalSpent = nextTurn.totalSpent + 73, bossHp = nextTurn.bossHp - 2, playerHp = nextTurn.playerHp + 2, history = nextTurn.history + "D"))
                }

                //Shield
                if (nextTurn.mana >= 113 && nextTurn.shieldTurns == 0) {
                    nextStates.add(nextTurn.copy(mana = nextTurn.mana - 113, totalSpent = nextTurn.totalSpent + 113, shieldTurns = 6, history = nextTurn.history + "S"))
                }

                //Poison
                if (nextTurn.mana >= 173 && nextTurn.poisonTurns == 0) {
                    nextStates.add(nextTurn.copy(mana = nextTurn.mana - 173, totalSpent = nextTurn.totalSpent + 173, poisonTurns = 6, history = nextTurn.history + "P"))
                }

                //Recharge
                if (nextTurn.mana >= 229 && nextTurn.rechargeTurns == 0) {
                    nextStates.add(nextTurn.copy(mana = nextTurn.mana - 229, totalSpent = nextTurn.totalSpent + 229, rechargeTurns = 5, history = nextTurn.history + "R"))
                }

                return nextStates
            } else {
                val damage = if (nextTurn.shieldTurns > 0) 1 else 8
                return listOf(nextTurn.copy(playerHp = nextTurn.playerHp - damage))
            }

        }

        private fun isWin(state: GameState): Boolean = state.bossHp <= 0 && state.playerHp > 0
        private fun isLoss(state: GameState): Boolean = state.playerHp <= 0

        private fun applyEffects(state: GameState): GameState {
            var nextState = state
            if (nextState.playersTurn) {
                nextState = nextState.copy(playerHp = nextState.playerHp - 1)
            }
            if (nextState.shieldTurns > 0) {
                nextState = nextState.copy(shieldTurns = nextState.shieldTurns - 1)
            }

            if (nextState.poisonTurns > 0) {
                nextState = nextState.copy(poisonTurns = nextState.poisonTurns - 1, bossHp = nextState.bossHp - 3)
            }

            if (state.rechargeTurns > 0) {
                nextState = nextState.copy(rechargeTurns = nextState.rechargeTurns - 1, mana = nextState.mana + 101)
            }


            return nextState
        }

        override fun allowed(state: GameState): Boolean = state.playerHp > 0

    }

    data class GameState(val playersTurn: Boolean, val bossHp: Int,
                         val playerHp: Int, val mana: Int, val history: String = "",
                         val totalSpent: Int = 0,
                         val shieldTurns: Int = 0, val poisonTurns: Int = 0, val rechargeTurns: Int = 0)

}
