package kryszt.aoc2015

import kryszt.tools.IoUtil

object Main8 {

    val EscapeQuote = "\\\\\"".toRegex()
    val EscapeDash = "\\\\\\\\".toRegex()
    val UnicodeBlock = "\\\\x[a-fA-F0-9][a-fA-F0-9]".toRegex()
    val AllInOne = "(\\\\x[a-fA-F0-9][a-fA-F0-9])|(\\\\\")|(\\\\\\\\)".toRegex()
//    val EscapeQuote = "\\\\\"".toPattern()
//    val EscapeDash = "\\\\\\\\".toPattern()
//    val UnicodeBlock = "\\\\x[a-zA-Z0-9][a-zA-Z0-9]".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {


        var diffTotal = 0
//        IoUtil.forEachLine(this, "-test") {
        IoUtil.forEachLine(this) {
            println("$it -> ${clean(it)}")
            val diff = diffEncode(it)
            diffTotal += diff
            println(diff)
            println("${writeLen(it)} de=${storeLen(it)} en=${encodeLen(it)} $diff")
        }
        println("Total diff $diffTotal")
    }


    fun writeLen(str: String): Int = str.length

    fun storeLen(str: String): Int = writeLen(clean(str)) - 2

    fun encodeLen(str: String): Int = 2 + str.length + str.count { it == '\\' } + str.count { it == '\"' }

    //    fun clean(str: String): String = str.replace(UnicodeBlock, "u").replace(EscapeQuote, "q").replace(EscapeDash, "d")
    fun clean(str: String): String = str.replace(AllInOne, "r")
//    fun clean(str: String): String =
//            UnicodeBlock.matcher(str).replaceAll("u").let {
//                EscapeQuote.matcher(it).replaceAll("q").let {
//                    EscapeDash.matcher(it).replaceAll("d")
//                }
//            }

    fun diffDecode(str: String): Int = writeLen(str) - storeLen(str)

    fun diffEncode(str: String): Int = encodeLen(str) - writeLen(str)
}
