package kryszt.aoc2015

import kryszt.tools.IoUtil
import kryszt.tools.Point

object Main3 {

    @JvmStatic
    fun main(args: Array<String>) {

        println(countHouses(">"))
        println(countHouses("^>v<"))
        println(countHouses("^v^v^v^v^v"))
        println(countHouses(IoUtil.readTaskFile(this)))

        println()
        println(countHouses2("^v"))
        println(countHouses2("^>v<"))
        println(countHouses2("^v^v^v^v^v"))
        println(countHouses2(IoUtil.readTaskFile(this)))
    }

    fun countHouses(directions: String): Int {
        val visited = mutableSetOf<Point>()

        var current = Point()
        visited.add(current)

        directions.forEach { dir ->
            current = nextLocation(current, dir)
            visited.add(current)
        }
        return visited.size
    }

    fun countHouses2(directions: String): Int {
        val visited = mutableSetOf<Point>()

        var first = Point()
        var second = Point()
        visited.add(first)

        directions.forEachIndexed { index, dir ->
            if (index % 2 == 0) {
                first = nextLocation(first, dir)
                visited.add(first)
            } else {
                second = nextLocation(second, dir)
                visited.add(second)
            }
        }
        return visited.size
    }

    fun nextLocation(current: Point, dir: Char): Point = when (dir) {
        '<' -> current.oneLeft()
        '>' -> current.oneRight()
        '^' -> current.oneUp()
        'v' -> current.oneDown()
        else -> throw IllegalArgumentException(": $dir")
    }

}
