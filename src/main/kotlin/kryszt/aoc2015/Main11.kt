package kryszt.aoc2015

object Main11 {

    val PForbidden = "([iol])".toRegex()
    val PPairs = "([a-z])\\1".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {

//        var pass = "ghijklmn"
//        var pass = "cqjxjnds"
        var pass = nextPass("cqjxxyzz")
        while (!isValid(pass)) {
            pass = nextPass(pass)
        }
        println(pass)
    }

    fun isValid(line: CharSequence): Boolean = noForbidden(line) && hasIncreasingTripple(line) && twoPairs(line)

    fun twoPairs(line: CharSequence): Boolean {
        val matcher = PPairs.matcher(line)
        val pairs = mutableSetOf<String>()
        while (matcher.find()) {
            pairs.add(matcher.group())
        }
        return pairs.size > 1
    }

    fun noForbidden(line: CharSequence): Boolean {
        return !PForbidden.containsMatchIn(line)
    }

    fun hasIncreasingTripple(line: CharSequence): Boolean {
        var last = line[0]
        var count = 1
        for (i in 1 until line.length) {
            if (line[i] == last + 1) {
                count++
                if (count == 3) {
                    return true
                }
            } else {
                count = 1
            }
            last = line[i]
        }
        return false
    }

    fun nextPass(current: String): String {
        val chars: CharArray = current.toCharArray()

        for (i in current.length - 1 downTo 0) {
            if (chars[i] == 'z') {
                chars[i] = 'a'
                continue
            } else {
                chars[i] = chars[i] + 1
                return String(chars)
            }
        }
        return String(chars)
    }

}
