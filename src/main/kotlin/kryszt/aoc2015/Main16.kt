package kryszt.aoc2015

import kryszt.tools.IoUtil

object Main16 {

    val known = mapOf(
            "children" to 3,
            "cats" to 7,
            "samoyeds" to 2,
            "pomeranians" to 3,
            "akitas" to 0,
            "vizslas" to 0,
            "goldfish" to 5,
            "trees" to 3,
            "cars" to 2,
            "perfumes" to 1
    )

    @JvmStatic
    fun main(args: Array<String>) {
        IoUtil.forEachLine(this) {
            val aunt = parseLine(it)
//            if(canBe(aunt, known)){
            if (canBe2(aunt, known)) {
                println(aunt)
            }
        }

    }

    fun parseLine(line: String): Aunt {
        val number = line.substring(line.indexOf(' ') + 1, line.indexOf(':')).toInt()
        val has: Map<String, Int> = line.substring(line.indexOf(':') + 1).trim().split(',').map { s ->
            s.split(':').let { Pair(it[0].trim(), it[1].trim().toInt()) }
        }.toMap()
        return Aunt(number, has)
    }

    fun canBe2(aunt: Aunt, known: Map<String, Int>): Boolean {
        return aunt.content.all { e ->
            val has = e.value
            val reading = known[e.key]!!
            when (e.key) {
                "cats", "trees" -> has > reading
                "pomeranians", "goldfish" -> has < reading
                else -> has == reading
            }


        }
    }

    fun canBe(aunt: Aunt, known: Map<String, Int>): Boolean {
        return aunt.content.all { e -> known[e.key] == e.value }
    }

    data class Aunt(val number: Int, val content: Map<String, Int>)
}
