package kryszt.aoc2015

import kryszt.tools.ActionProcessor.Companion.GroupText
import kryszt.tools.ActionProcessor.Companion.GroupNumber

object Main15 {

    val Pattern = "$GroupText capacity $GroupNumber, durability $GroupNumber, flavor $GroupNumber, texture $GroupNumber, calories $GroupNumber".toPattern()

    val input = "Sprinkles capacity 5, durability -1, flavor 0, texture 0, calories 5\n" +
            "PeanutButter capacity -1, durability 3, flavor 0, texture 0, calories 1\n" +
            "Frosting capacity 0, durability -1, flavor 4, texture 0, calories 6\n" +
            "Sugar capacity -1, durability 0, flavor 0, texture 2, calories 8"

    //    val testInput = "Butterscotch capacity 1, durability 2, flavor 6, texture 3, calories 8\n" +
//            "Cinnamon capacity 2, durability 3, flavor -2, texture -1, calories 3"
    val testInput = "Butterscotch capacity -1, durability -2, flavor 6, texture 3, calories 8\n" +
            "Cinnamon capacity 2, durability 3, flavor -2, texture -1, calories 3"

    @JvmStatic
    fun main(args: Array<String>) {
        val ingredients = input.split("\n").map { parse(it) }
//        val ingredients = testInput.split("\n").map { parse(it.trim()) }

        ingredients.forEach { println(it) }

        println(findBest(ingredients, 0, 100, Properties()))
    }

    fun parse(line: String): Ingredient {
        return Pattern.matcher(line).takeIf { it.find() }?.let {
            Ingredient(
                    it.group(1),
                    Properties(it.group(2).toInt(), it.group(3).toInt(), it.group(4).toInt(), it.group(5).toInt(), it.group(6).toInt()))
        } ?: run { throw IllegalArgumentException(line) }
    }

    fun findBest(ingredients: List<Ingredient>, ingIndex: Int, left: Int, properties: Properties): Int {
        val thisProperties = ingredients[ingIndex].properties
        if (ingIndex >= (ingredients.size - 1)) {
            val finalProperties = (properties + (thisProperties * left))
            return finalProperties.takeIf { it.calories == 500 }?.totalValue ?: Int.MIN_VALUE
        }

        var max = Int.MIN_VALUE
        for (ingAmount in 0..left) {
            val withCurrent = properties + (thisProperties * ingAmount)
            val foundNext = findBest(ingredients, ingIndex + 1, left - ingAmount, withCurrent)
            if (foundNext > max) {
                max = foundNext
            }
        }
        return max
    }

    data class Ingredient(val name: String, val properties: Properties)

    data class Properties(val capacity: Int = 0, val durability: Int = 0, val flavor: Int = 0, val texture: Int = 0, val calories: Int = 0) {

        fun nonZeroSquare(value: Int): Int = value.let { Math.max(it, 0) }
        val totalValue = nonZeroSquare(capacity) * nonZeroSquare(durability) * nonZeroSquare(flavor) * nonZeroSquare(texture)

        operator fun plus(other: Properties): Properties {
            return Properties(
                    capacity = capacity + other.capacity,
                    durability = durability + other.durability,
                    flavor = flavor + other.flavor,
                    texture = texture + other.texture,
                    calories = calories + other.calories)
        }

        operator fun times(times: Int): Properties {
            return Properties(
                    capacity = capacity * times,
                    durability = durability * times,
                    flavor = flavor * times,
                    texture = texture * times,
                    calories = calories * times)
        }
    }

}
