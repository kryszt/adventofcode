package kryszt.aoc2015

import kryszt.tools.ActionProcessor.Companion.GroupNumber
import kryszt.tools.ActionProcessor.Companion.GroupText
import kryszt.tools.IoUtil
import java.util.*

object Main9 {

    val Group = "$GroupText to $GroupText = $GroupNumber".toPattern()

    const val input = "London to Dublin = 464\n" +
            "London to Belfast = 518\n" +
            "Dublin to Belfast = 141"

    @JvmStatic
    fun main(args: Array<String>) {

        val distances = DistanceHolder()

//        input.split("\n").forEach { parseLine(it).let { distances.add(it) } }
        IoUtil.forEachLine(this) { parseLine(it).let { distances.add(it) } }

        println(distances.cities)
        println(distances.dists)

        println(" ${startFindShortest(distances, distances.cities.toList())}")
        println(" ${startFindLongest(distances, distances.cities.toList())}")
        println(3 + 22 + 52 + 8 + 4 + 14 + 14)
    }

    class DistanceHolder {

        val dists = mutableMapOf<String, Int>()
        val cities = mutableSetOf<String>()

        fun add(distance: Distance) {
            dists["${distance.from}-${distance.to}"] = distance.distance
            dists["${distance.to}-${distance.from}"] = distance.distance
            cities.add(distance.from)
            cities.add(distance.to)
        }

        fun distance(from: String, to: String): Int = dists["$from-$to"]!!
    }

    fun parseLine(line: String): Distance {
        return Group.matcher(line).takeIf { it.find() }?.let {
            Distance(it.group(1), it.group(2), it.group(3).toInt())
        } ?: kotlin.run { throw  IllegalArgumentException(line) }
    }

    data class Distance(val from: String, val to: String, val distance: Int = 0)

    data class Path(val path: String, val lenght: Int)

    fun startFindShortest(distances: DistanceHolder, options: List<String>): Path {
        var shortestRoad = Path("", Int.MAX_VALUE)
        val visits = LinkedHashSet<String>()

        for (i in options) {
            visits.add(i)
            val found = findShortest(distances, options, visits, 0)
            if (found.lenght < shortestRoad.lenght) {
                shortestRoad = found
            }
            visits.remove(i)
        }
        return shortestRoad
    }

    fun findShortest(distances: DistanceHolder, options: List<String>, visits: LinkedHashSet<String>, currentDistance: Int): Path {
        if (visits.size == options.size) {
            return Path(visits.joinToString("-"), currentDistance).also {
                println("Found road $it")
            }
        } else {
            val lastPoint = visits.last()
            var shortestRoad = Path("", Int.MAX_VALUE)

            for (i in options) {
                if (visits.add(i)) {
                    val newDistance = currentDistance + distances.distance(lastPoint, i)
                    val found = findShortest(distances, options, visits, newDistance)
                    if (found.lenght < shortestRoad.lenght) {
                        shortestRoad = found
                    }
                    visits.remove(i)
                }
            }
            return shortestRoad
        }
    }

    fun startFindLongest(distances: DistanceHolder, options: List<String>): Path {
        var longestPath = Path("", 0)
        val visits = LinkedHashSet<String>()

        for (i in options) {
            visits.add(i)
            val found = findLongest(distances, options, visits, 0)
            if (found.lenght > longestPath.lenght) {
                longestPath = found
            }
            visits.remove(i)
        }
        return longestPath
    }


    fun findLongest(distances: DistanceHolder, options: List<String>, visits: LinkedHashSet<String>, currentDistance: Int): Path {
        if (visits.size == options.size) {
            return Path(visits.joinToString("-"), currentDistance).also {
                println("Found road $it")
            }
        } else {
            val lastPoint = visits.last()
            var longestPath = Path("", 0)

            for (i in options) {
                if (visits.add(i)) {
                    val newDistance = currentDistance + distances.distance(lastPoint, i)
                    val found = findLongest(distances, options, visits, newDistance)
                    if (found.lenght > longestPath.lenght) {
                        longestPath = found
                    }
                    visits.remove(i)
                }
            }
            return longestPath
        }
    }

}
