package kryszt.aoc2017

import java.util.*

object Main18 {

    val realInstructions = "set i 31\n" +
            "set a 1\n" +
            "mul p 17\n" +
            "jgz p p\n" +
            "mul a 2\n" +
            "add i -1\n" +
            "jgz i -2\n" +
            "add a -1\n" +
            "set i 127\n" +
            "set p 735\n" +
            "mul p 8505\n" +
            "mod p a\n" +
            "mul p 129749\n" +
            "add p 12345\n" +
            "mod p a\n" +
            "set b p\n" +
            "mod b 10000\n" +
            "snd b\n" +
            "add i -1\n" +
            "jgz i -9\n" +
            "jgz a 3\n" +
            "rcv b\n" +
            "jgz b -1\n" +
            "set f 0\n" +
            "set i 126\n" +
            "rcv a\n" +
            "rcv b\n" +
            "set p a\n" +
            "mul p -1\n" +
            "add p b\n" +
            "jgz p 4\n" +
            "snd a\n" +
            "set a b\n" +
            "jgz 1 3\n" +
            "snd b\n" +
            "set f 1\n" +
            "add i -1\n" +
            "jgz i -11\n" +
            "snd a\n" +
            "jgz f -16\n" +
            "jgz a -19"

    val testInstructions = "set a 1\n" +
            "add a 2\n" +
            "mul a a\n" +
            "mod a 5\n" +
            "snd a\n" +
            "set a 0\n" +
            "rcv a\n" +
            "jgz a -1\n" +
            "set a 1\n" +
            "jgz a -2"

    val paralelInstructions = "snd 1\n" +
            "snd 2\n" +
            "snd p\n" +
            "rcv a\n" +
            "rcv b\n" +
            "rcv c\n" +
            "rcv d"

    @JvmStatic
    fun main(args: Array<String>) {
        val instructions = realInstructions.split("\n").map { parseLine(it) }
//        val instructions = paralelInstructions.split("\n").map { parseLine(it) }

        val processor0 = Processor2(instructions, 0)
        val processor1 = Processor2(instructions, 1)

        processor0.otherProcessor = processor1
        processor1.otherProcessor = processor0

        while (!processor0.endOrBlocked() || !processor1.endOrBlocked()) {

            if (!processor0.endOrBlocked()) {
                processor0.tick()
            }
            if (!processor1.endOrBlocked()) {
                processor1.tick()
            }

        }

        println("End, 0 sent ${processor0.sendCount} 1 sent ${processor1.sendCount}")
    }

    @JvmStatic
    fun main2(args: Array<String>) {
        val instructions = realInstructions.split("\n").map { parseLine(it) }
//        val instructions = testInstructions.split("\n").map { parseLine(it) }

        var finished = false

        val listener = object : ProcessorListener {
            override fun onRecover(processor: Processor, value: Long?) {
                println("onRecover: $value on ${processor.steps}")
                finished = true
            }

            override fun onSound(processor: Processor, value: Long) {
                println("onSound: $value on ${processor.steps}")
            }
        }

        val processor = Processor(instructions, listener)

        while (!processor.finished && !finished) {
            processor.tick()
//            printRegisters(processor.registers)
        }

        println("Done in steps: ${processor.steps}")

    }

    private fun printRegisters(register: Map<String, Long>) {
        println(register.entries.joinToString(", ") { "${it.key}=${it.value}" })
    }

    private fun parseLine(line: String): Any {
        val parts = line.split(" ")
        val instruction = parts[0]
        return when (instruction) {
            "snd" -> Snd(parts[1])
            "set" -> Set(parts[1], parts[2])
            "add" -> Add(parts[1], parts[2])
            "mul" -> Mul(parts[1], parts[2])
            "mod" -> Mod(parts[1], parts[2])
            "rcv" -> Rcv(parts[1])
            "jgz" -> Jump(parts[1], parts[2])
            else -> throw IllegalArgumentException(line)
        }
    }

    class Processor(val instructions: List<Any>, val listener: ProcessorListener) {

        val registers = mutableMapOf<String, Long>()

        var nextInstruction: Long = 0

        var lastPlayed: Long? = null

        var finished = false

        var steps = 0

        fun tick() {
            val ins = instructions.getOrNull(nextInstruction.toInt())
            steps++
            nextInstruction++
            println("instruction $ins")
            printRegisters(registers)
            when (ins) {
                is Snd -> {
                    lastPlayed = getValue(ins.x)
                    listener.onSound(this, lastPlayed!!)
                }
                is Set -> registers[ins.x] = getValue(ins.y)
                is Add -> registers[ins.x] = ((registers[ins.x] ?: 0) + getValue(ins.y))
                is Mul -> registers[ins.x] = ((registers[ins.x] ?: 0) * getValue(ins.y))
                is Mod -> registers[ins.x] = ((registers[ins.x] ?: 0) % getValue(ins.y))
                is Rcv -> {
                    if (getValue(ins.x) != 0L) listener.onRecover(this, lastPlayed)
                }
                is Jump -> {
                    if (getValue(ins.x) > 0) {
                        nextInstruction--
                        nextInstruction += getValue(ins.y)
                    }

                }

                null -> finished = true
                else -> throw IllegalArgumentException(ins.toString())
            }
        }

        fun getValue(value: String): Long {
            return value.toLongOrNull() ?: registers[value] ?: 0L
        }
    }

    class Processor2(val instructions: List<Any>, processId: Long) {

        val registers = mutableMapOf("p" to processId)

        var nextInstruction: Long = 0

        var finished = false

        var steps = 0

        val valueQueue: Queue<Long> = LinkedList<Long>()

        var otherProcessor: Processor2? = null

        var blocked = false

        var sendCount = 0

        fun endOrBlocked(): Boolean {
            return blocked || finished
        }

        fun post(value: Long) {
            valueQueue.add(value)
            blocked = false
        }

        fun tick() {
            val ins = instructions.getOrNull(nextInstruction.toInt())
            steps++
            nextInstruction++
//            println("instruction $ins")
//            printRegisters(registers)
            when (ins) {
                is Snd -> {
                    sendCount ++
                    otherProcessor!!.post(getValue(ins.x))
                }
                is Set -> registers[ins.x] = getValue(ins.y)
                is Add -> registers[ins.x] = ((registers[ins.x] ?: 0) + getValue(ins.y))
                is Mul -> registers[ins.x] = ((registers[ins.x] ?: 0) * getValue(ins.y))
                is Mod -> registers[ins.x] = ((registers[ins.x] ?: 0) % getValue(ins.y))
                is Rcv -> {
                    val posted = valueQueue.poll()
                    if (posted == null) {
                        nextInstruction--
                        blocked = true
                    } else {
                        registers[ins.x] = posted
                    }
                }
                is Jump -> {
                    if (getValue(ins.x) > 0) {
                        nextInstruction--
                        nextInstruction += getValue(ins.y)
                    }

                }

                null -> finished = true
                else -> throw IllegalArgumentException(ins.toString())
            }
        }

        fun getValue(value: String): Long {
            return value.toLongOrNull() ?: registers[value] ?: 0L
        }
    }

    interface ProcessorListener {
        fun onRecover(processor: Processor, value: Long?)
        fun onSound(processor: Processor, value: Long)
    }

    data class Snd(val x: String)
    data class Set(val x: String, val y: String)
    data class Add(val x: String, val y: String)
    data class Mul(val x: String, val y: String)
    data class Mod(val x: String, val y: String)
    data class Rcv(val x: String)
    data class Jump(val x: String, val y: String)

}
