package kryszt.aoc2017

import kryszt.tools.IoUtil
import java.util.*

object Main12 {
    val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\12.txt"
//    val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\12-test.txt"

    @JvmStatic
    fun main(args: Array<String>) {
        val pipes = buildPipes(IoUtil.readTaskFileLines(this))


        var groups = 0

        do {
            val collector = selectGroup(pipes, pipes.keys.first())
            println("collected ${collector.size} -> ${collector.joinToString("-")}")
            collector.forEach { pipes.remove(it) }
            groups++
        } while (pipes.isNotEmpty())

        println("Groups: $groups")

//        collector.forEach {
//            pipes.remove(it)
//        }
    }

    private fun selectGroup(pipes: MutableMap<Int, Pipes>, startId: Int): Set<Int> {
        val collector = HashSet<Int>()
        collect(collector, pipes, startId)
        return collector
    }

    private fun collect(collector: MutableSet<Int>, pipes: MutableMap<Int, Pipes>, key: Int) {
        if (collector.add(key)) {
            pipes[key]?.connections?.forEach {
                collect(collector, pipes, it)
            }
        }
    }

    private fun buildPipes(lines: List<String>): MutableMap<Int, Pipes> {
        val map = TreeMap<Int, Pipes>()
        lines.forEach { line ->
            val pipes = parseLine(line)
            map[pipes.id] = pipes
        }
        return map
    }

    private fun parseLine(line: String): Pipes {
        val split = line.split("<->").map { it.trim() }
        val id = split[0].toInt()
        val pipes = split[1].split(",").map { it.trim().toInt() }
        return Pipes(id, pipes)
    }

    data class Pipes(val id: Int, val connections: List<Int>)
}