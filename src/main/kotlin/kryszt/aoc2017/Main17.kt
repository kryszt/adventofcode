package kryszt.aoc2017

object Main17 {

    //    private const val steps = 370
    private const val steps = 3


    @JvmStatic
    fun main(args: Array<String>) {
        val buffer = spinlock(2017, 370)
        val index = buffer.indexOf(2017)
        val nextVal = buffer[index + 1]
        println("i: $index next $nextVal")

        val index0 = buffer.indexOf(0)
        val after0 = buffer[index0 +1]
        println("zero: i: $index0 next $after0")

        var zeroSpin = spinlock2(2017, 370)
        println("zero: i: ${zeroSpin.first} next ${zeroSpin.second}")

        zeroSpin = spinlock3(50000000, 370)
        println("zero: i: ${zeroSpin.first} next ${zeroSpin.second}")

    }

    private fun spinlock(steps: Int, stepSize: Int): List<Int> {
        val buffer = arrayListOf(0)
        var position = 0

        for (i in 1..steps) {
            //move stepSize
            val insertPosition = findInsertPosition(position, stepSize, size = i)

            //insert i
            buffer.add(index = insertPosition, element = i)

            position = insertPosition
        }

        return buffer
    }

    private fun spinlock2(steps: Int, stepSize: Int): Pair<Int,Int> {
        var afterZero = 0
        var zeroPosition = 0
        var position = 0

        for (i in 1..steps) {
            //move stepSize
            if (i % 1000000 == 0) println("step : $i")
            val spinPosition = findSpinPosition(position, stepSize, size = i)
            if (spinPosition == zeroPosition) {
                afterZero = i
            } else if (spinPosition < zeroPosition) {
                zeroPosition++
            }

            //insert i

            position = spinPosition + 1
        }

        return Pair(zeroPosition, afterZero)
    }

    private fun spinlock3(steps: Int, stepSize: Int): Pair<Int,Int> {
        var afterZero = 0
        var position = 0

        for (i in 1..steps) {
            //move stepSize
            if (i % 10000 == 0) println("step : $i")
            position = findInsertPosition(position, stepSize, size = i)
            if(position == 1){
                afterZero = i
            }
        }

        return Pair(0, afterZero)
    }

    private fun findSpinPosition(position: Int, steps: Int, size: Int): Int {
        val reducedSteps = steps % size
        return (position + reducedSteps) % size
    }

    private fun findInsertPosition(position: Int, steps: Int, size: Int): Int {
        return findSpinPosition(position, steps, size) + 1
    }

}
