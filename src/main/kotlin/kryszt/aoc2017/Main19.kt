package kryszt.aoc2017

import kryszt.tools.Point
import kryszt.tools.IoUtil

object Main19 {

    val testLab =
            "     |          \n" +
                    "     |  +--+    \n" +
                    "     A  |  C    \n" +
                    " F---|----E|--+ \n" +
                    "     |  |  |  D \n" +
                    "     +B-+  +--+ \n" +
                    "                "

    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\19.txt"

    @JvmStatic
    fun main(args: Array<String>) {
//        val grid = testLab.split("\n").toList()
        val grid = IoUtil.readTaskFileLines(this)
        grid.forEach {
            println("$it<")
        }

        val start = findEntry(grid)

        println(start)

        val crawler = Crawler(grid, start)
        println()
        for (i in 1..1000000) {
            val nextPoint = crawler.move()
            if (Character.isLetter(nextPoint)) {
                print(nextPoint)
            }
            if (nextPoint == ' ') {
                println()
                println()
                println("End at $i")
                break
            }
        }
        println()
    }

    fun findEntry(grid: List<String>): Point {
        return grid[0].indexOf('|').let { Point(it, 0) }
    }

    class Crawler(val grid: List<String>, startPoint: Point) {
        var steps = 0
        var direction = Point.Down
        var point = startPoint

        fun move(): Char {
            if (canMoveTo(grid, point.move(direction))) {
                point = point.move(direction)
            } else if (canMoveTo(grid, point.move(direction.left()))) {
                direction = direction.left()
                point = point.move(direction)
            } else if (canMoveTo(grid, point.move(direction.right()))) {
                direction = direction.right()
                point = point.move(direction)
            } else {
                return ' '
            }
            steps++
            return grid[point.y][point.x]
        }

    }

    fun canMoveTo(grid: List<String>, point: Point): Boolean {
        return grid.getOrNull(point.y)?.getOrNull(point.x) ?: ' ' != ' '
    }

}
