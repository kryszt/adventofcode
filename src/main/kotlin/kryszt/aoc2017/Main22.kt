package kryszt.aoc2017

import kryszt.tools.Point
import kotlin.math.absoluteValue

object Main22 {

    val testGridInput = "..#\n#..\n..."

    val gridInput = "...###.#.#.##...##.#..##.\n" +
            ".#...#..##.#.#..##.#.####\n" +
            "#..#.#...######.....#####\n" +
            ".###.#####.#...#.##.##...\n" +
            ".#.#.##......#....#.#.#..\n" +
            "....##.##.#..##.#...#....\n" +
            "#...###...#.###.#.#......\n" +
            "..#..#.....##..####..##.#\n" +
            "#...#..####.#####...#.##.\n" +
            "###.#.#..#..#...##.#..#..\n" +
            ".....##..###.##.#.....#..\n" +
            "#.....#...#.###.##.##...#\n" +
            ".#.##.##.##.#.#####.##...\n" +
            "##.#.###..#.####....#.#..\n" +
            "#.##.#...#.###.#.####..##\n" +
            "#.##..#..##..#.##.####.##\n" +
            "#.##.#....###.#.#......#.\n" +
            ".##..#.##..###.#..#...###\n" +
            "#..#.#.#####.....#.#.#...\n" +
            ".#####..###.#.#.##..#....\n" +
            "###..#..#..##...#.#.##...\n" +
            "..##....##.####.....#.#.#\n" +
            "..###.##...#..#.#####.###\n" +
            "####.########.#.#..##.#.#\n" +
            "#####.#..##...####.#..#.."

    @JvmStatic
    fun main(args: Array<String>) {
        val grid = buildGrid(gridInput)
//        val grid = buildGrid(testGridInput)

        printGrid(grid, findSize(grid))
        val worm = Worm2(grid)
        println()
        println()

        for (i in 1..10000000) {
            worm.step()

//            printGrid(grid, findSize(grid), worm.point)
//            println()
//            println()
        }
        println("infected = ${worm.infectCount}")
        val gridRadius = findSize(grid)
        println("size = $gridRadius")

//        printGrid(grid, gridRadius, worm.point)
    }

    fun findSize(grid: MutableMap<Point, State>): Int {

        val max = grid.keys.maxByOrNull { Math.max(it.x.absoluteValue, it.y.absoluteValue) }
        return Math.max(max!!.x.absoluteValue, max.y.absoluteValue)
    }

    fun buildGrid(input: String): MutableMap<Point, State> {
        val rows = input.split("\n")
        val size = rows.size
        val start = -(size / 2)

        val grid = mutableMapOf<Point, State>()
        for (y in 0..(size - 1)) {
            (0..(size - 1))
                    .filter { rows[y][it] == '#' }
                    .map { Point(start + it, start + y) }
                    .forEach { grid[it] = State.Infected }
        }
        return grid
    }

    fun printGrid(grid: MutableMap<Point, State>, radius: Int, point: Point = Point(0, 0)) {
        for (y in -radius..radius) {
            for (x in -radius..radius) {
                val mark = when (grid[Point(x, y)]) {
                    State.Infected -> "#"
                    State.Flagged -> "F"
                    State.Weakened -> "W"
                    State.Clean -> "."
                    else -> "."
                }
                if ((point.x == x) and (point.y == y)) {
                    print("[$mark]")
                } else {
                    print(" $mark ")
                }
            }
            println()
        }
    }

    class Worm(val grid: MutableMap<Point, State>) {

        var direction = Point.Up
        var point: Point = Point(0, 0)
        var infectCount = 0

        fun step() {
            val state = grid[point] ?: State.Clean
            if (state == State.Infected) {
                direction = direction.right()
                grid[point] = State.Clean

            } else {
                //clean
                infectCount++
                direction = direction.left()
                grid[point] = State.Infected

            }
            point = Point(point.x + direction.x, point.y + direction.y)
        }
    }

    class Worm2(val grid: MutableMap<Point, State>) {

        var direction = Point.Up
        var point: Point = Point(0, 0)
        var infectCount = 0

        fun step() {
            val state = grid[point] ?: State.Clean
            when (state) {
                State.Clean -> {
                    direction = direction.left()
                    grid[point] = State.Weakened
                }
                State.Weakened -> {
                    infectCount++
                    //no turn
                    grid[point] = State.Infected
                }
                State.Infected -> {
                    direction = direction.right()
                    grid[point] = State.Flagged
                }
                State.Flagged -> {
                    direction = direction.revert()
                    grid[point] = State.Clean
                }
            }
            point = Point(point.x + direction.x, point.y + direction.y)
        }
    }


    enum class State {
        Clean, Infected, Weakened, Flagged
    }

}
