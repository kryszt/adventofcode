package kryszt.aoc2017

import kryszt.tools.Point
import kotlin.experimental.xor

object Main14 {

    const val baseString = "hwlqcszp"

    @JvmStatic
    fun main(args: Array<String>) {

        println(1 shl 3)
        var sum = 0

        val hashes = mutableListOf<String>()

        for (i in 0..127) {
            val hash = makeHash("${baseString}-$i")
            hashes += hash
            sum += countOnes(hash)
        }
        println(sum)

        val grid = makeGrid(hashes)

        val groups = groupGrid(grid)
        printGrid(grid)
        println("groups: " + groups)
    }

    private fun printGrid(grid: Array<Array<Int>>) {
        grid.forEach {
            it.forEach {
                print("\t${-it}")
//                print(String.format("%04d", it))
            }
            println()
        }
    }

    private fun groupGrid(grid: Array<Array<Int>>): Int {
        var count = 0

        for (y in grid.indices) {
            for (x in grid[y].indices)
                if (grid[y][x] == 1) {
                    count--
                    markGroup(grid, Point(x, y), count)
                }
        }

        return -count
    }

    private fun markGroup(grid: Array<Array<Int>>, p: Point, value: Int) {
        if ((p.y >= 0) and (p.y < grid.size)) {
            val row = grid[p.y]
            if ((p.x >= 0) && (p.x < row.size) && (row[p.x] == 1)) {
                row[p.x] = value
                markGroup(grid, p.oneUp(), value)
                markGroup(grid, p.oneDown(), value)
                markGroup(grid, p.oneLeft(), value)
                markGroup(grid, p.oneRight(), value)
            }
        }
    }

    private fun makeGrid(hashes: List<String>): Array<Array<Int>> {
        return hashes.map { hashToNumbers(it) }.toTypedArray()
    }

    private fun hashToNumbers(hash: String): Array<Int> {
        return Array(hash.length * 4, { if (isBitSet(hash, it)) 1 else 0 })

    }

    private fun isBitSet(s: String, index: Int): Boolean {
        val charIndex = index / 4
        val bitIndex = 3 - (index % 4)
        val bitMap = 1 shl bitIndex
        val value = Integer.parseInt(s[charIndex].toString(), 16)
        return (value and bitMap) != 0
    }

    private fun countOnes(s: String): Int {
        var sum = 0
        s.forEach { c ->
            val b = Integer.parseInt(c.toString(), 16)
            sum += Integer.bitCount(b)
        }
        return sum
    }

    private fun makeHash(input: String): String {
        val size = 256
        val base = createArray(size)
        val lengths = createLengths2(input)

        var extraSkip = 0
        var position = 0

        for (i in 1..64) {
            lengths.forEach { length ->
                revertPart(base, position, length)
                position = (position + length + extraSkip) % size
                extraSkip++
            }
//            println(base.joinToString(":"))
        }
        return hashArray(base)

    }

    private fun createLengths(input: String): List<Int> {
        return input.split(",").map { it.toInt() }
    }

    private fun createLengths2(input: String): List<Int> {
//        return input.toCharArray().map { it.toInt() }
        val fromInput = input.toCharArray().map { it.toInt() }
        val extra = listOf(17, 31, 73, 47, 23)
        return fromInput + extra
    }

    fun hashArray(values: Array<Byte>): String {
        val size = values.size
        val boxSize = 16
        val hashes = (0..(size - 1) step boxSize).map { xorHash(values, it, boxSize) }

        return hashes.joinToString(separator = "", transform = { String.format("%02X", it) }).toLowerCase()
    }

    fun xorHash(values: Array<Byte>, start: Int, length: Int): Byte {
        var hash: Byte = 0
        for (i in start..(start + length - 1)) {
            hash = hash xor values[i]
        }
        return hash
    }

    private fun createArray(size: Int): Array<Byte> =
            Array(size, { it.toByte() })


    private fun <T> revertPart(array: Array<T>, startIndex: Int, length: Int) {
        val size = array.size
        for (i in 0..((length / 2) - 1)) {
            val firstIndex = (startIndex + i) % size
            val lastIndex = (startIndex + length - 1 - i) % size
            swap(array, firstIndex, lastIndex)
        }
    }

    private fun <T> swap(array: Array<T>, firstIndex: Int, secondIndex: Int) {
        val v = array[firstIndex]
        array[firstIndex] = array[secondIndex]
        array[secondIndex] = v
    }
}