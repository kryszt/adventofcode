package kryszt.aoc2017

import kryszt.tools.IoUtil
import kotlin.math.absoluteValue

object Main11 {

    val test1 = listOf("ne", "ne", "ne")
    val test2 = listOf("ne", "ne", "sw", "sw")
    val test3 = listOf("ne", "ne", "s", "s")
    val test4 = listOf("se", "sw", "se", "sw", "sw")

    val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\11.txt"
    @JvmStatic
    fun main(args: Array<String>) {
        doMoves(test1)
        doMoves(test2)
        doMoves(test3)
        doMoves(test4)
        doMoves(IoUtil.readTaskFileLines(this))
    }

    fun doMoves(moves: List<String>): Hex {
        val hex = Hex()
        var maxDistance = 0
        moves.forEach {
            move(hex, it)
            maxDistance = Math.max(maxDistance, distance(hex))
        }
        println(hex)
        println("distance: ${distance(hex)}")
        println("maxDistance: $maxDistance")
        println(distance(hex))
        return hex
    }

    private fun move(hex: Hex, direction: String) {
        when (direction) {
            "n" -> hex.y -= 2
            "s" -> hex.y += 2
            "nw" -> {
                hex.y--
                hex.x--
            }
            "sw" -> {
                hex.y++
                hex.x--
            }
            "ne" -> {
                hex.y--
                hex.x++
            }
            "se" -> {
                hex.y++
                hex.x++
            }

        }
    }

//    private fun move(hex: Hex, direction: String) {
//        when (direction) {
//            "n" -> hex.y--
//            "s" -> hex.y++
//            "nw" -> {
//                if (hex.x % 2 == 0) hex.y--
//                hex.x--
//                //TODO
//            }
//            "sw" -> {
//                if (hex.x % 2 == 1) hex.y++
//                hex.x--
//                //TODO
//            }
//            "ne" -> {
//                if (hex.x % 2 == 0) hex.y--
//                hex.x++
//                //TODO
//            }
//            "se" -> {
//                if (hex.x % 2 == 1) hex.y++
//                hex.x++
//                //TODO
//            }
//
//        }
//    }

    fun distance(hex: Hex): Int {
        val dx = hex.x.absoluteValue
        val dy = hex.y.absoluteValue

        val min = Math.min(dx, dy)

        val dist = min + (dx - min) + (dy - min) / 2
        return dist
    }

    data class Hex(var x: Int = 0, var y: Int = 0)
}
