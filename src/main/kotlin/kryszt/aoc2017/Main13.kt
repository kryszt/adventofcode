package kryszt.aoc2017

import kryszt.tools.IoUtil

object Main13 {
    val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\13.txt"
//    val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\13-test.txt"

    @JvmStatic
    fun main(args: Array<String>) {
        val firewall = buildFirewall(IoUtil.readTaskFileLines(this))
        val maxLayer = firewall.keys.maxOrNull() ?: 0
//        val severity = traverse(firewall, maxLayer)
//        println("severity $severity")
        val delay = findDelay(firewall, maxLayer)
        println("delay: $delay")
    }

    private fun findDelay(firewall: Map<Int, Layer>, lastLayer: Int): Int {
        val packets = mutableListOf<Packet>()
        var delay = 0

        var successfulPacket: Packet? = null
        while (successfulPacket == null) {

            packets.forEach { it.move() }
            packets.add(Packet(delay))

            successfulPacket = packets.find { it.position > lastLayer }

            //remove caught
            val caught = packets.filter { firewall[it.position]?.place == 0 }
            println("Caught in $delay: $caught")
            packets.removeAll(caught)

            firewall.values.forEach { it.step() }

            delay++
        }
        return successfulPacket.delay
    }

    private fun traverse(firewall: Map<Int, Layer>, lastLayer: Int): Int {
        var atLevel = 0
        var severity = 0

        while (atLevel <= lastLayer) {
            firewall[atLevel]?.let {
                if (it.place == 0) {
                    println("caught at $atLevel for ${it.severity}")
                    severity += it.severity
                }
            }

            firewall.values.forEach { it.step() }

            atLevel++
        }
        return severity
    }

    private fun buildFirewall(lines: List<String>): Map<Int, Layer> {
        return lines.map { parseLine(it) }.associateBy { it.index }
    }

    private fun parseLine(line: String): Layer {
        val split = line.split(":").map { it.trim() }
        return Layer(
                index = split[0].toInt(),
                depth = split[1].toInt()
        )
    }

    class Layer(val index: Int, val depth: Int) {

        val severity = index * depth

        var step = 1
        var place = 0

        fun step() {
            place += step

            if (place == 0) {
                step = 1
            } else if (place == (depth - 1)) {
                step = -1
            }
        }

    }

    data class Packet(val delay: Int, var position: Int = 0) {
        fun move() {
            position++
        }
    }
}
