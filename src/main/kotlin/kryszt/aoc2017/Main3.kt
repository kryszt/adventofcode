package kryszt.aoc2017

import kryszt.tools.Point

object Main3 {

    @JvmStatic
    fun main(args: Array<String>) {

        val values = HashMap<Point, Int>()

        val generator = PointsGenerator()
        for (i in 0..10) {
            println(generator.nextPoint())
        }


    }

    class PointsGenerator() {

        val Left = Point(-1, 0)
        val Right = Point(1, 0)
        val Up = Point(0, -1)
        val Down = Point(0, 1)

        val order = arrayOf(Right, Up, Left, Down)

        var steps = 1
        var stepsLeft = steps
        var stepIncreaseLeft = 2
        var nextPointIndex = 0

        var currentPoint = Point(0, 0)

        fun nextChange(): Point {
            val change = order[nextPointIndex]

            stepsLeft--
            if (stepsLeft == 0) {
                nextPointIndex = (nextPointIndex + 1) % 4
                stepIncreaseLeft--
                stepsLeft = steps
                if (stepIncreaseLeft == 0) {
                    steps++
                    stepIncreaseLeft = 2

                }
            }
            return change
        }

        fun nextPoint(): Point {
            val change = nextChange()
            currentPoint = currentPoint.copy(x = currentPoint.x + change.x, y = currentPoint.y + change.y)
            return currentPoint
        }
    }
}
