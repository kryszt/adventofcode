package kryszt.aoc2017

import kryszt.tools.IoUtil

object Main16 {

    @JvmStatic
    fun main(args: Array<String>) {

//        var base = mutableListOf("a", "b", "c", "d", "e")

        var base = Array(16, { i -> (i + 97).toChar().toString() }).toMutableList()
        println(base.joinToString(""))
//        val moves = readActionsTest()
        val moves = IoUtil.readTaskFile(this).split(",").map { parseAction(it) }
        println("moves: ${moves.size}")

        println("reminder = ${1000000000 % 30}")

        var steps = 0
        for (i in 1..10) {
            if (i % 1000 == 0) {
                println("step $i")
                println(base.joinToString(""))
            }

            moves.forEach { move ->
                val state = base.joinToString("")
                if (state == "abcdefghijklmnop") {
                    println("back to start at $steps")
                }
                base = makeMove(base, move)
                steps++
            }
        }
        println(base.joinToString(""))

    }

    fun readActionsTest(): List<Move> {
        val input = "s1,x3/4,pe/b"
        return input.split(",").map { parseAction(it) }
    }

    fun parseAction(action: String): Move {
        val type = action[0]
        val values = action.substring(1).split("/")

        return when (type) {
            's' -> Spin(values[0].toInt())
            'x' -> Exchange(values[0].toInt(), values[1].toInt())
            'p' -> Partner(values[0], values[1])
            else -> throw IllegalArgumentException(action)
        }

    }

    fun makeMove(values: MutableList<String>, move: Move): MutableList<String> {
        return when (move) {
            is Spin -> (values.takeLast(move.count) + values.subList(0, values.size - move.count)).toMutableList()
            is Exchange -> {
                exchange(values, move.indexA, move.indexB)
                values
            }
            is Partner -> {
                exchange(values, values.indexOf(move.nameA), values.indexOf(move.nameB))
                values
            }
            else -> throw IllegalArgumentException(move.toString())
        }
    }

    fun exchange(values: MutableList<String>, indexA: Int, indexB: Int) {
        val item = values[indexA]
        values[indexA] = values[indexB]
        values[indexB] = item
    }

    interface Move

    data class Spin(val count: Int) : Move

    data class Exchange(val indexA: Int, val indexB: Int) : Move

    data class Partner(val nameA: String, val nameB: String) : Move
}
