package kryszt.aoc2017

import kryszt.tools.IoUtil

object Main8 {

    //    private const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\8-test.txt"
    private const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\8.txt"

    @JvmStatic
    fun main(args: Array<String>) {

        val registers: MutableMap<String, Register> = HashMap()

        var maxValue = 0

        IoUtil.forEachLine(this) { line ->
            val instruction = parseLine(line)
            println(instruction)

            if (instruction.condition.passes(getRegister(registers, instruction.conditionRegistry), instruction.conditionValue)) {
                println("applied")
                instruction.action.apply(getRegister(registers, instruction.register), instruction.changeValue)
                val maxInRegisters: Int = registers.values.maxByOrNull { it.value }?.value ?: 0
                maxValue = Math.max(maxValue, maxInRegisters)
            } else {
                println("NOT applied")
            }

        }

        println()
        registers.values.forEach { println(it) }
        println()

        val max = registers.values.maxByOrNull { it.value }
        println("max: $max")
        println("max anytime: $maxValue")
    }

    fun getRegister(registers: MutableMap<String, Register>, key: String): Register =
            registers.getOrPut(key, { Register(key, 0) })

    fun parseLine(line: String): InstructionLine {
        //b inc 5 if a > 1
        val split = line.split(" ").map { it.trim() }

        return InstructionLine(
                register = split[0],
                action = Action.valueOf(split[1]),
                changeValue = split[2].toInt(),
                conditionRegistry = split[4],
                condition = parseCondition(split[5]),
                conditionValue = split[6].toInt()

        )
    }

    fun parseCondition(conditionString: String): Condition = when (conditionString) {
        ">" -> Condition.gt
        "<" -> Condition.lt
        ">=" -> Condition.ge
        "<=" -> Condition.le
        "==" -> Condition.eq
        "!=" -> Condition.ne
        else -> throw IllegalArgumentException(conditionString)
    }

    data class Register(val name: String, var value: Int)

    enum class Action(val apply: (Register, Int) -> Unit) {
        inc({ r, v -> r.value += v }),
        dec({ r, v -> r.value -= v })
    }

    enum class Condition(val passes: (Register, Int) -> Boolean) {
        gt({ r, v -> r.value > v }),
        lt({ r, v -> r.value < v }),
        ge({ r, v -> r.value >= v }),
        le({ r, v -> r.value <= v }),
        eq({ r, v -> r.value == v }),
        ne({ r, v -> r.value != v })
    }

    data class InstructionLine(val register: String, val action: Action, val changeValue: Int, val conditionRegistry: String, val condition: Condition, val conditionValue: Int)

}
