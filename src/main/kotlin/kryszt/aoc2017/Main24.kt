package kryszt.aoc2017

import java.util.*

object Main24 {

    val parts = "14/42\n" +
            "2/3\n" +
            "6/44\n" +
            "4/10\n" +
            "23/49\n" +
            "35/39\n" +
            "46/46\n" +
            "5/29\n" +
            "13/20\n" +
            "33/9\n" +
            "24/50\n" +
            "0/30\n" +
            "9/10\n" +
            "41/44\n" +
            "35/50\n" +
            "44/50\n" +
            "5/11\n" +
            "21/24\n" +
            "7/39\n" +
            "46/31\n" +
            "38/38\n" +
            "22/26\n" +
            "8/9\n" +
            "16/4\n" +
            "23/39\n" +
            "26/5\n" +
            "40/40\n" +
            "29/29\n" +
            "5/20\n" +
            "3/32\n" +
            "42/11\n" +
            "16/14\n" +
            "27/49\n" +
            "36/20\n" +
            "18/39\n" +
            "49/41\n" +
            "16/6\n" +
            "24/46\n" +
            "44/48\n" +
            "36/4\n" +
            "6/6\n" +
            "13/6\n" +
            "42/12\n" +
            "29/41\n" +
            "39/39\n" +
            "9/3\n" +
            "30/2\n" +
            "25/20\n" +
            "15/6\n" +
            "15/23\n" +
            "28/40\n" +
            "8/7\n" +
            "26/23\n" +
            "48/10\n" +
            "28/28\n" +
            "2/13\n" +
            "48/14"

    val example = "0/2\n" +
            "2/2\n" +
            "2/3\n" +
            "3/4\n" +
            "3/5\n" +
            "0/1\n" +
            "10/1\n" +
            "9/10"

    @JvmStatic
    fun main(args: Array<String>) {
//        val parts = example.split("\n").map { parsePart(it) }
        val parts = parts.split("\n").map { parsePart(it) }

        parts.forEach {
            println("$it")
        }

        val stack = Stack<Part>()

        val firstParts = parts.filter { it.hasPins(0) }
//        firstParts.forEach {
//            it.connect(0)
//            val total = buildBridge(parts, it.streangth, it, stack)
//            it.disconnect()
//            println("total $total starting with $it")
//        }
        val max = firstParts.map {
            it.connect(0)
            val total = buildBridge2(parts, LenStr(1, it.streangth), it, stack)
            it.disconnect()
            println("total $total starting with $it")
            total
        }.maxOrNull()

        println("max $max ")
    }

    fun buildBridge(parts: List<Part>, strength: Int, part: Part, stack: Stack<Part>): Int {
        if (part.outside == 0) {
            return strength
        }
        stack.push(part)
        val canConnect = parts.filter {
            //find parts that we can connect
            !it.used && it.hasPins(part.outside)
        }

        val bridge = stack.joinToString("--", transform = { p -> "${p.left}/${p.right}" })
        println("$strength\t$bridge")

        val maxStrength = canConnect.map {
            it.connect(part.outside)
            val totalStrength = buildBridge(parts, strength + it.streangth, it, stack)
            it.disconnect()
            totalStrength
        }.maxOrNull() ?: strength

        stack.pop()
        return maxStrength
    }

    fun buildBridge2(parts: List<Part>, strength: LenStr, part: Part, stack: Stack<Part>): LenStr {
        if (part.outside == 0) {
            val bridge = stack.joinToString("--", transform = { p -> "${p.left}/${p.right}" })
            println("$strength\t$bridge")
            return strength
        }
        stack.push(part)
        val canConnect = parts.filter {
            //find parts that we can connect
            !it.used && it.hasPins(part.outside)
        }

        if(canConnect.isEmpty()){
            val bridge = stack.joinToString("--", transform = { p -> "${p.left}/${p.right}" })
            println("$strength\t$bridge")
        }

        val maxStrength: LenStr = canConnect.map {
            it.connect(part.outside)
            val nextLenStr = LenStr(length = strength.length + 1, strength = strength.strength + it.streangth)
            val totalStrength = buildBridge2(parts, nextLenStr, it, stack)
            it.disconnect()
            totalStrength
        }.maxOrNull() ?: strength

        stack.pop()
        return maxStrength
    }

    fun parsePart(line: String): Part {
        val split = line.split("/").map { it.trim() }
        return Part(split[0].toInt(), split[1].toInt())
    }

    data class LenStr(val length: Int, val strength: Int) : Comparable<LenStr> {

        override fun compareTo(other: LenStr): Int = if (this.length == other.length) {
            this.strength - other.strength
        } else {
            this.length - other.length
        }

    }

    data class Part(val left: Int, val right: Int, var outside: Int = 0, var used: Boolean = false) {
        val streangth = left + right

        fun hasPins(pins: Int): Boolean = (left == pins || right == pins)

        fun connect(pins: Int) {
            outside = when (pins) {
                left -> right
                right -> left
                else -> throw IllegalArgumentException("Cannot connect $this to $pins pins")
            }
            used = true
        }

        fun disconnect() {
            used = false
        }
    }
}
