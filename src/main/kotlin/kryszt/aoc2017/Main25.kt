package kryszt.aoc2017

object Main25 {

    val testStates = mapOf(
            "A" to State(name = "A",
                    onZero = Action(1, 1, "B"),
                    onOne = Action(0, -1, "B")
            ),
            "B" to State(name = "B",
                    onZero = Action(1, -1, "A"),
                    onOne = Action(1, 1, "A")
            )

    )

    val mainStates = mapOf(
            "A" to State(name = "A",
                    onZero = Action(1, 1, "B"),
                    onOne = Action(0, -1, "C")
            ),
            "B" to State(name = "B",
                    onZero = Action(1, -1, "A"),
                    onOne = Action(1, -1, "D")
            ),
            "C" to State(name = "C",
                    onZero = Action(1, 1, "D"),
                    onOne = Action(0, 1, "C")
            ),
            "D" to State(name = "D",
                    onZero = Action(0, -1, "B"),
                    onOne = Action(0, 1, "E")
            ),
            "E" to State(name = "E",
                    onZero = Action(1, 1, "C"),
                    onOne = Action(1, -1, "F")
            ),
            "F" to State(name = "F",
                    onZero = Action(1, -1, "E"),
                    onOne = Action(1, 1, "A")
            )

    )


    val testTask = Task(6, testStates, "A")
    val mainTask = Task(12172063, mainStates, "A")

    data class Task(val steps: Int, val states: Map<String, State>, val startState: String)

    @JvmStatic
    fun main(args: Array<String>) {
//        val task = testTask
        val task = mainTask

        val machine = TuringMachine(task.states, task.states[task.startState]!!)


        for (i in 1..task.steps) {
            if (i % 1000 == 0) println(machine.printState())
            machine.doStep()
        }
        println(machine.printState())
        println("checksum: ${machine.checksum()}")
    }

    class TuringMachine(val states: Map<String, State>, startState: State) {

        val tape: MutableMap<Int, Byte> = mutableMapOf()

        var position: Int = 0
        var step: Int = 0
        var currentState: State = startState

        fun doStep() {
            val read = tape[position]?.toInt() ?: 0
            val action = if (read == 0) currentState.onZero else currentState.onOne

            //execute action
            tape[position] = action.write
            position += action.move
            currentState = states[action.nextState]!!

            step++
        }

        fun checksum() = tape.values.sum()

        fun printState(): String {
            val tape = (-3..3).map { tape[it] ?: 0 }.joinToString(separator = " ")
            return "$tape => step=$step p=$position state=${currentState.name} "
        }
    }

    data class State(val name: String, val onZero: Action, val onOne: Action)

    data class Action(val write: Byte, val move: Int, val nextState: String)
}
