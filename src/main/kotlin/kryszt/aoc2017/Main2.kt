package kryszt.aoc2017

import kryszt.tools.IoUtil

object Main2 {

    @JvmStatic
    fun main(args: Array<String>) {
        var sum = 0
        IoUtil.forEachLine(this) { line ->
            val values = line.split("\t").map { it.toInt() }
            val checksum = lineChecksum2(values)
            sum += checksum
        }

        println("sum: $sum")
    }

    fun lineChecksum1(values: List<Int>): Int {
        return (values.maxOrNull() ?: 0) - (values.minOrNull() ?: 0)
    }

    fun lineChecksum2(values: List<Int>): Int {
        values.forEach { v ->
            val divider = values.find { (v > it) and ((v % it) == 0) }
            divider?.let {
                return v / it
            }
        }
        throw IllegalStateException()
    }
}
