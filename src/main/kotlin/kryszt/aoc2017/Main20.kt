package kryszt.aoc2017

import kryszt.tools.IoUtil
import kotlin.math.absoluteValue

object Main20 {

    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\20.txt"

    @JvmStatic
    fun main(args: Array<String>) {
        val particles = IoUtil.readTaskFileLines(this).mapIndexed { index, line ->
            parseLine(index, line)
        }

        val sorted = particles.sortedBy { it.totalP() }.sortedBy { it.totalV() }.sortedBy { it.totalA() }

        println("first: ${sorted.first()}  =>  ${sorted.first().totalA()}")
        println("first: ${sorted.last()}  =>  ${sorted.last().totalA()}")

        println()
        println()

        val p1 = Coordinates(1, 2, 3)
        val p2 = Coordinates(3, 2, 1)

        println("Equals = ${p1 == p2}")

        var forClearing = sorted
        for (i in 1..10000) {
            forClearing = clearCollisions(forClearing)
            forClearing.forEach { it.tick() }
        }
        println("Left: ${forClearing.size}")
    }

    private fun clearCollisions(input: List<Particle>): List<Particle> {
        val collided = HashSet<Particle>()

        val size = input.size

        for (i in 0..(size - 1)) {
            val current = input[i]
            for (j in (i + 1)..(size - 1)) {
                val checked = input[j]
                if (current.p == checked.p) {
                    collided.add(current)
                    collided.add(checked)
                }
            }
        }
        return if (collided.isEmpty()) {
            input
        } else {
            println("collisions: ${collided.size}")
            input - collided
        }
    }

    fun parseLine(index: Int, line: String): Particle {
        val coordinates = line.split(", ").map { parseCoordinates(it) }
        return Particle(index, coordinates[0], coordinates[1], coordinates[2])
    }

    fun parseCoordinates(s: String): Coordinates {
        val cleaned = s.substringAfter("<").substringBefore(">").split(",").map { it.trim().toInt() }
        return Coordinates(cleaned[0], cleaned[1], cleaned[2])
    }

    data class Coordinates(var x: Int, var y: Int, var z: Int)

    data class Particle(val id: Int, val p: Coordinates, val v: Coordinates, val a: Coordinates) {
        fun totalA() = a.x.absoluteValue + a.y.absoluteValue + a.z.absoluteValue
        fun totalV() = v.x.absoluteValue + v.y.absoluteValue + v.z.absoluteValue
        fun totalP() = p.x.absoluteValue + p.y.absoluteValue + p.z.absoluteValue

        override fun toString(): String {
            return "id=$id   => a=${totalA()} v=${totalV()} p=${totalP()}"
        }

        fun tick() {
            v.x += a.x
            v.y += a.y
            v.z += a.z

            p.x += v.x
            p.y += v.y
            p.z += v.z
        }
    }


}
