package kryszt.aoc2017

import kotlin.experimental.xor

object Main10 {

    const val inputString = "14,58,0,116,179,16,1,104,2,254,167,86,255,55,122,244"
    //    const val inputString = "14,58,0,116,179,16,1,104,2,254,167,86,255,55,122,244"
//    val input = arrayOf(3, 4, 1, 5)
//    val input = arrayOf(14,58,0,116,179,16,1,104,2,254,167,86,255,55,122,244)
    //    const val repeats = 2
    const val repeats = 64

    @JvmStatic
    fun main(args: Array<String>) {
        var extraSkip = 0
        var position = 0
        val size = 256
        val base = createArray(size)

        val lengths = createLengths2(inputString)
//        println(hashArray(arrayOf(65, 27, 9, 1, 4, 3, 40, 50, 91, 7, 6, 0, 2, 5, 68, 22)))

        for (i in 0..(repeats - 1)) {


            lengths.forEach { length ->
                revertPart(base, position, length)
                position = (position + length + extraSkip) % size
                extraSkip++
            }
            println(hashArray(base))
//            println(base.joinToString(":"))
        }
        println("sum: ${base[0] * base[1]}")

    }

    private fun createLengths(input: String): List<Int> {
        return input.split(",").map { it.toInt() }
    }

    private fun createLengths2(input: String): List<Int> {
        val fromInput = input.toCharArray().map { it.toInt() }
        val extra = listOf(17, 31, 73, 47, 23)
        return fromInput + extra
    }

    fun hashArray(values: Array<Byte>): String {
        val size = values.size
        val boxSize = 16
        val hashes = (0..(size - 1) step boxSize).map { xorHash(values, it, boxSize) }

        return hashes.joinToString(separator = "", transform = { String.format("%02X", it) }).toLowerCase()
    }

    fun xorHash(values: Array<Byte>, start: Int, length: Int): Byte {
        var hash: Byte = 0
        for (i in start..(start + length - 1)) {
            hash = hash xor values[i]
        }
        return hash
    }

    private fun createArray(size: Int): Array<Byte> =
            Array(size, { it.toByte() })


    private fun <T> revertPart(array: Array<T>, startIndex: Int, length: Int) {
        val size = array.size
        for (i in 0..((length / 2) - 1)) {
            val firstIndex = (startIndex + i) % size
            val lastIndex = (startIndex + length - 1 - i) % size
            swap(array, firstIndex, lastIndex)
        }
    }

    private fun <T> swap(array: Array<T>, firstIndex: Int, secondIndex: Int) {
        val v = array[firstIndex]
        array[firstIndex] = array[secondIndex]
        array[secondIndex] = v
    }
}
