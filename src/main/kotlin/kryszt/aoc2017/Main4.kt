package kryszt.aoc2017

import kryszt.tools.IoUtil
import java.util.*

object Main4 {

    const val test1 = "aa bb cc dd ee"
    const val test2 = "aa bb cc dd aa"
    const val test3 = "aa bb cc dd aaa"

    @JvmStatic
    fun main(args: Array<String>) {
        val lines = IoUtil.readTaskFileLines(this)
        var count = 0
        lines.forEach {
            println("$it -> ${valid(it.split(" "))}")
            if (valid(it.split(" "))) {
                count++
            }
        }
        println("count = $count")
    }

    fun valid(words: List<String>): Boolean {
        val set = HashSet<String>()
        return words.all {
            set.add(asUniqueString2(it))
        }
    }

    fun asUniqueString(line: String): String = line

    fun asUniqueString2(line: String): String {
        return asString(histogram(line))
    }

    fun histogram(word: String): SortedMap<Char, Int> {
        val hist = TreeMap<Char, Int>()
        word.forEach { c ->
            hist[c] = 1 + (hist[c] ?: 0)
        }
        return hist
    }

    fun asString(hist: SortedMap<Char, Int>): String {
        return hist.entries.joinToString(separator = "-", transform = { e -> "${e.key}:${e.value}" })
    }
}
