package kryszt.aoc2017

import java.util.*

object Main23 {

    val realInstructions = "set b 81\n" +
            "set c b\n" +
            "jnz a 2\n" +
            "jnz 1 5\n" +
            "mul b 100\n" +
            "sub b -100000\n" +
            "set c b\n" +
            "sub c -17000\n" +
            "set f 1\n" +
            "set d 2\n" +
            "set e 2\n" +
            "set g d\n" +
            "mul g e\n" +
            "sub g b\n" +
            "jnz g 2\n" +
            "set f 0\n" +
            "sub e -1\n" +
            "set g e\n" +
            "sub g b\n" +
            "jnz g -8\n" +
            "sub d -1\n" +
            "set g d\n" +
            "sub g b\n" +
            "jnz g -13\n" +
            "jnz f 2\n" +
            "sub h -1\n" +
            "set g b\n" +
            "sub g c\n" +
            "jnz g 2\n" +
            "jnz 1 3\n" +
            "sub b -17\n" +
            "jnz 1 -23"


    @JvmStatic
    fun main(args: Array<String>) {
        val instructions = realInstructions.split("\n").map { parseLine(it) }
//        val instructions = testInstructions.split("\n").map { parseLine(it) }

        realInstructions.split("\n").forEachIndexed { index, any ->
            println("$index : $any")
        }

        val processor = Processor(instructions, false)

        for (i in 1..1) {
            processor.tick()
            if (processor.finished) {
                break
            }
//            printRegisters(processor.registers)
        }

        println("Done in steps: ${processor.steps} mul: ${processor.mulCount}")
        println(emulator(1))
    }

    private fun printRegisters(register: Map<String, Long>) {
        println(register.entries.joinToString(", ") { "${it.key}=${it.value}" })
    }

    private fun parseLine(line: String): Any {
        val parts = line.split(" ")
        val instruction = parts[0]
        return when (instruction) {
            "snd" -> Snd(parts[1])
            "set" -> Set(parts[1], parts[2])
            "add" -> Add(parts[1], parts[2])
            "sub" -> Sub(parts[1], parts[2])
            "mul" -> Mul(parts[1], parts[2])
            "mod" -> Mod(parts[1], parts[2])
            "rcv" -> Rcv(parts[1])
            "jnz" -> Jnz(parts[1], parts[2])
            else -> throw IllegalArgumentException(line)
        }
    }

    class Processor(val instructions: List<Any>, debug: Boolean = true) {

        val registers = TreeMap<String, Long>()

        var nextInstruction: Long = 0

        var finished = false

        var steps = 0

        var mulCount = 0

        init {
            if (!debug) {
                registers["a"] = 1
            }
        }

        fun tick() {
            val ins = instructions.getOrNull(nextInstruction.toInt())
            steps++
            nextInstruction++
//            println("instruction $ins")
            printRegisters(registers)

            when (ins) {
//                is Snd -> {
//                }
                is Set -> registers[ins.x] = getValue(ins.y)
//                is Add -> registers[ins.x] = ((registers[ins.x] ?: 0) + getValue(ins.y))
                is Mul -> {
                    mulCount++
                    registers[ins.x] = ((registers[ins.x] ?: 0) * getValue(ins.y))
                }
//                is Mod -> registers[ins.x] = ((registers[ins.x] ?: 0) % getValue(ins.y))
                is Sub -> registers[ins.x] = ((registers[ins.x] ?: 0) - getValue(ins.y))
//                is Rcv -> {
//                }
                is Jnz -> {
                    if (getValue(ins.x) != 0L) {
                        nextInstruction--
                        nextInstruction += getValue(ins.y)
                    }

                }

                null -> finished = true
                else -> throw IllegalArgumentException(ins.toString())
            }
        }

        fun getValue(value: String): Long {
            return value.toLongOrNull() ?: registers[value] ?: 0L
        }
    }

    fun emulator(startA: Int): Int {
        var a = startA
        var b = 0
        var c = 0
        var d = 0
        var e = 0
        var f = 0
        var g = 0
        var h = 0

        var mulCount = 0

        b = 81 //0
        c = b //1
        if (a != 0) { // 2+3

            b *= 100 //4
            mulCount++
            b += 100000 //5
            c = b //6
            c += 17000 //7
        }
        println("pre 8")
        println("a=$a b=$b c=$c d=$d e=$e f=$f g=$g h=$h")

        do {// pre 8

            f = 1 //8
            d = 2 //9

            do { // pre 10
                e = 2 //10
                do { // pre 11
                    g = d //11
                    g *= e //12
                    mulCount++
                    g -= b //13
                    if (g == 0) {//14
                        f = 0//15
                    }
                    e += 1//16
                    g = e//17
                    g -= b//18

//                    println("pre 19")
//                    println("a=$a b=$b c=$c d=$d e=$e f=$f g=$g h=$h")
                } while (g != 0) // 19

                d += 1//20
                g = d//21
                g -= b//22
                println("pre 23")
                println("a=$a b=$b c=$c d=$d e=$e f=$f g=$g h=$h")
            } while (g != 0) //23

            if (f == 0) {//24
                h += 1//25
            }
            g = b//26
            g -= c//27

            if (g == 0) {//28
                println("a=$a b=$b c=$c d=$d e=$e f=$f g=$g h=$h")
                return mulCount//29
            } else {
                b += 17 // 30
            }

            println("pre 31")
            println("a=$a b=$b c=$c d=$d e=$e f=$f g=$g h=$h")
        } while (true) //31

    }

    data class Snd(val x: String)
    data class Set(val x: String, val y: String)
    data class Add(val x: String, val y: String)
    data class Sub(val x: String, val y: String)
    data class Mul(val x: String, val y: String)
    data class Mod(val x: String, val y: String)
    data class Rcv(val x: String)
    data class Jnz(val x: String, val y: String)

}
