package kryszt.aoc2017

object Main15 {

    const val iterations: Int = 5000000
//    const val iterations: Int = 40000000

    @JvmStatic
    fun main(args: Array<String>) {
//        val generatorA = Generator(65, 16807, 2147483647, 4)
//        val generatorB = Generator(8921, 48271, 2147483647, 8)
        val generatorA = Generator(722, 16807, 2147483647, 4)
        val generatorB = Generator(354, 48271, 2147483647, 8)

        var count = 0

        for (i in 1..iterations) {
            val vA = generatorA.makeNext()
            val vB = generatorB.makeNext()
            if (i % 100000 == 0) {
                println("step $i")
            }
            if ((vA and 0xFFFF) == (vB and 0xFFFF)) {
                count++
                println("in step $i")
                println(String.format("0x%08X", vA))
                println(String.format("0x%08X", vB))
                println()
            }
        }

        println("count = $count")
    }

    class Generator(startValue: Long, val multiplier: Int, val divider: Long, val acceptMultiplier: Long) {
        var currentValue: Long = startValue

        fun makeNext(): Long {
            do {
                val newValue = (currentValue * multiplier) % divider
                currentValue = newValue
            } while ((newValue % acceptMultiplier) != 0L)
            return currentValue
        }
    }
}
