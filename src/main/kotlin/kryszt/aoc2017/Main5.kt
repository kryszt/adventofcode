package kryszt.aoc2017

import kryszt.tools.IoUtil

object Main5 {

    @JvmStatic
    fun main(args: Array<String>) {
//        val values = asMaze(listOf("0","3","0","1","-3"))
        val values = asMaze(IoUtil.readTaskFileLines(this))
        println("count = ${values.size}")
        values.forEach { print("$it ") }

        val size = values.size
        var index = 0
        var stepCount = 0

        while (index in 0..(size - 1)) {
            stepCount++
            index = makeStep2(values, index)
        }
        println("")
        values.forEach { print("$it ") }
        println("")
        println("steps: $stepCount")

    }

    private fun makeStep(values: Array<Int>, index: Int): Int {
        val change = values[index]
        values[index]++
        return index + change
    }

    private fun makeStep2(values: Array<Int>, index: Int): Int {
        val change = values[index]
        if (change >= 3) {
            values[index]--
        } else {
            values[index]++
        }
        return index + change
    }

    private fun asMaze(words: List<String>): Array<Int> {
        val values = Array(words.size, { 0 })

        words.forEachIndexed { index, s ->
            values[index] = s.toInt()
        }

        return values
    }

}
