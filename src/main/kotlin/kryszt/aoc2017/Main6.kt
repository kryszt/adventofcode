package kryszt.aoc2017

object Main6 {

        private val initialBanks: Array<Int> = arrayOf(5, 1, 10, 0, 1, 7, 13, 14, 3, 12, 8, 10, 7, 12, 0, 6)
//    private val initialBanks: Array<Int> = arrayOf(0, 2, 7, 0)

    @JvmStatic
    fun main(args: Array<String>) {
        val knownStates = HashMap<String, Int>()
        val banks = initialBanks.copyOf()

        var stepsCount = 0
        var prevStep: Int? = null

        do {
            prevStep = knownStates.put(asString(banks), stepsCount)
            println(asString(banks))
            stepsCount++
            makeStep(banks)

        } while (prevStep == null)
        println(asString(banks))
        println("steps: $stepsCount")
        println("prevStep: $prevStep")
        println("cycle: ${stepsCount - prevStep - 1}")
    }

    private fun makeStep(values: Array<Int>) {
        val max = values.maxOrNull() ?: 0
        val index = values.indexOf(max)
        values[index] = 0
        split(values, index, max)
    }

    private fun split(values: Array<Int>, cleanedIndex: Int, blocks: Int) {
        val size = values.size
        for (i in 1..blocks) {
            val index = (i + cleanedIndex) % size
            values[index]++
        }
    }

    private fun asString(values: Array<Int>): String = values.joinToString("-")

}
