package kryszt.aoc2017

import kryszt.tools.IoUtil

object Main7 {

    //    private const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\7-test.txt"
    private const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\7.txt"

    @JvmStatic
    fun main(args: Array<String>) {

        val programs = IoUtil.readTaskFileLines(this).map { parseLine(it) }
        val system: Map<String, Program> = programs.associateBy { it.name }

        println("Starting")

        programs.forEach { p ->
            p.connected = p.pointing.mapNotNull { system[it] }
        }
        programs.forEach { p ->
            p.fullWeight = fullWeight(p)

            if (p.fullWeight ?: 0 != (p.weight + p.connected.sumBy { it.fullWeight ?: 0 })) {
                println(p)
                throw IllegalStateException()
            }
        }

        programs.forEach {
            if (!isBalanced(it)) {
                println(it)
            }
        }

        val head = findHead(programs)

        println("head: $head")


    }

    fun isBalanced(program: Program): Boolean {
        return (program.connected.minByOrNull { it.fullWeight ?: 0 }?.fullWeight) == (program.connected.maxByOrNull {
            it.fullWeight ?: 0
        }?.fullWeight)
    }

    fun fullWeight(program: Program): Int {
        var fullWeight: Int? = program.fullWeight
        if (fullWeight == null) {
            fullWeight = (program.weight + program.connected.sumBy { fullWeight(it) })
            println("setting full $fullWeight for ${program.name}")
        }
        program.fullWeight = fullWeight
        if(fullWeight == null){
            throw IllegalStateException("Whaaa???")
        }
        return fullWeight
    }

    fun findHead(programs: List<Program>): String {
        val pointing = HashSet<String>()
        val pointed = HashSet<String>()
        programs.forEach {
            collectLine(pointing, pointed, it)
        }

        println("pointing: $pointing")
        println("pointed: $pointed")
        pointing.removeAll(pointed)
        println("pointing: $pointing")
        return pointing.first()
    }

    fun collectLine(pointing: MutableSet<String>, pointed: MutableSet<String>, program: Program) {
        if (program.pointing.isNotEmpty()) {
            pointing.add(program.name)
            pointed.addAll(program.pointing)
        }
    }

    fun parseLine(line: String): Program {
        val split = line.split("->").map { it.trim() }
        val name: String = split[0].substringBeforeLast(" ")

        val weight = split[0].substring(split[0].indexOf("(") + 1, split[0].indexOf(")")).toInt()
        val pointings: List<String> = split.getOrNull(1)?.split(",")?.map { it.trim() } ?: emptyList()
        return Program(name, weight, pointings)
    }

    data class Program(val name: String, val weight: Int, val pointing: List<String>, var connected: List<Program> = emptyList(), var fullWeight: Int? = null) {
        override fun toString(): String {
            val connectedString = connected.joinToString(separator = ", ", transform = { "${it.name} (${it.fullWeight})" })
            return "$name, $weight (full: $fullWeight) [$connectedString] "
        }
    }
}
