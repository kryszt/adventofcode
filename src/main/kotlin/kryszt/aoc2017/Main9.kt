package kryszt.aoc2017

import kryszt.tools.IoUtil

object Main9 {

    @JvmStatic
    fun main(args: Array<String>) {
        IoUtil.forEachLine(this) {
            consume(it)
        }
    }

    fun consume(line: String) {
        val collector = StreamCollector()
        line.forEach { collector.consume(it) }

        println("Result: ${collector.score} ${collector.groupLevel}, dropped: ${collector.dropped}")
    }

    class StreamCollector {

        var garbage: Boolean = false
        var groupLevel: Int = 0
        var cancel: Boolean = false

        var dropped = 0

        var score: Int = 0

        fun consume(char: Char) {
            if (cancel) {
                cancel = false
                return
            }
            when (char) {
                '!' -> cancel = true
                '<' -> {
                    if (garbage) dropped++
                    garbage = true
                }
                '>' -> garbage = false
                '{' -> {
                    if (!garbage) {
                        groupLevel++
                        score += groupLevel
                    } else {
                        dropped++
                    }
                }
                '}' -> if (!garbage) groupLevel-- else dropped++
                else -> if (garbage) dropped++
            }


        }
    }

}
