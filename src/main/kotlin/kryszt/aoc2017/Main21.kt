package kryszt.aoc2017

import java.util.*

object Main21 {

    val primaryMapping = "../.. => .../#../#..\n" +
            "#./.. => ###/#.#/.#.\n" +
            "##/.. => ###/.##/##.\n" +
            ".#/#. => .#./..#/...\n" +
            "##/#. => ##./.##/#..\n" +
            "##/## => #.#/###/.##\n" +
            ".../.../... => #.#./.#.#/#.#./###.\n" +
            "#../.../... => #..#/.###/##../##..\n" +
            ".#./.../... => #.##/####/.###/....\n" +
            "##./.../... => ####/.#../#.##/#.##\n" +
            "#.#/.../... => ..../#.../.##./#.##\n" +
            "###/.../... => .###/.#../...#/.#..\n" +
            ".#./#../... => .###/#..#/#.../#...\n" +
            "##./#../... => ..##/...#/#.##/..##\n" +
            "..#/#../... => #.##/.#../...#/..##\n" +
            "#.#/#../... => #.##/..##/..../##.#\n" +
            ".##/#../... => .###/.###/#.../....\n" +
            "###/#../... => #.../####/.#.#/....\n" +
            ".../.#./... => ...#/##.#/...#/###.\n" +
            "#../.#./... => #.#./####/.#../##.#\n" +
            ".#./.#./... => #..#/.##./..##/...#\n" +
            "##./.#./... => ###./#.#./#.../###.\n" +
            "#.#/.#./... => ..#./###./####/.#.#\n" +
            "###/.#./... => .#.#/#..#/..#./#..#\n" +
            ".#./##./... => ####/##../##../..##\n" +
            "##./##./... => #.../..##/#.#./....\n" +
            "..#/##./... => ..../#..#/.#../#..#\n" +
            "#.#/##./... => ###./..##/#.#./#...\n" +
            ".##/##./... => ...#/#..#/####/...#\n" +
            "###/##./... => ..../#.##/###./...#\n" +
            ".../#.#/... => #.../#.../...#/#...\n" +
            "#../#.#/... => ##../#..#/.##./.##.\n" +
            ".#./#.#/... => ##../.###/#.##/#.#.\n" +
            "##./#.#/... => ##.#/.#.#/#.#./..#.\n" +
            "#.#/#.#/... => .##./...#/...#/.#..\n" +
            "###/#.#/... => ####/..#./###./#.##\n" +
            ".../###/... => #..#/.#.#/#.##/..#.\n" +
            "#../###/... => .#../##../##../#.##\n" +
            ".#./###/... => #.#./...#/#.#./#.##\n" +
            "##./###/... => #.#./#..#/.###/.###\n" +
            "#.#/###/... => ..#./...#/..#./#..#\n" +
            "###/###/... => ##../###./####/....\n" +
            "..#/.../#.. => ##../.#../#.#./.##.\n" +
            "#.#/.../#.. => .##./##.#/.#../#...\n" +
            ".##/.../#.. => ####/..#./#..#/##..\n" +
            "###/.../#.. => #.#./..../..#./####\n" +
            ".##/#../#.. => ..##/..##/.##./##..\n" +
            "###/#../#.. => #..#/#..#/.##./.#..\n" +
            "..#/.#./#.. => #..#/#.##/##../#..#\n" +
            "#.#/.#./#.. => .#.#/.#.#/.##./.#.#\n" +
            ".##/.#./#.. => ####/#.##/..../.###\n" +
            "###/.#./#.. => #..#/.#.#/.##./....\n" +
            ".##/##./#.. => ###./##../#..#/....\n" +
            "###/##./#.. => ...#/.#../.#../....\n" +
            "#../..#/#.. => ###./#.../..##/#...\n" +
            ".#./..#/#.. => .#../#.##/.##./..#.\n" +
            "##./..#/#.. => ..#./.##./..../..##\n" +
            "#.#/..#/#.. => #.#./###./.#.#/#..#\n" +
            ".##/..#/#.. => ####/..##/###./.#.#\n" +
            "###/..#/#.. => ##.#/.##./.###/###.\n" +
            "#../#.#/#.. => ..../#.##/.#.#/#..#\n" +
            ".#./#.#/#.. => .###/..../.###/#.##\n" +
            "##./#.#/#.. => ####/..##/#.##/#.##\n" +
            "..#/#.#/#.. => ..#./..##/####/#...\n" +
            "#.#/#.#/#.. => .##./.#.#/.#.#/##..\n" +
            ".##/#.#/#.. => ##.#/##.#/#.##/.###\n" +
            "###/#.#/#.. => #..#/.##./#.##/.###\n" +
            "#../.##/#.. => ####/...#/..##/##..\n" +
            ".#./.##/#.. => .##./#.##/...#/#...\n" +
            "##./.##/#.. => .##./..#./###./....\n" +
            "#.#/.##/#.. => .#.#/##.#/..#./##.#\n" +
            ".##/.##/#.. => ###./####/.##./####\n" +
            "###/.##/#.. => ..#./##.#/.#../..#.\n" +
            "#../###/#.. => ##../#.##/#.../.#.#\n" +
            ".#./###/#.. => ..#./#.##/...#/...#\n" +
            "##./###/#.. => .###/###./.##./###.\n" +
            "..#/###/#.. => #.../..../#.../#...\n" +
            "#.#/###/#.. => .###/...#/...#/..#.\n" +
            ".##/###/#.. => #.#./..../###./.#.#\n" +
            "###/###/#.. => #..#/#.../#.##/##.#\n" +
            ".#./#.#/.#. => .#../##../..##/#.##\n" +
            "##./#.#/.#. => #.##/#.#./#..#/##.#\n" +
            "#.#/#.#/.#. => #..#/.###/..../###.\n" +
            "###/#.#/.#. => #.#./.#.#/####/#.#.\n" +
            ".#./###/.#. => ..##/..#./..##/###.\n" +
            "##./###/.#. => ##../#.#./#.#./.#..\n" +
            "#.#/###/.#. => ####/.##./####/#.#.\n" +
            "###/###/.#. => ####/..#./####/....\n" +
            "#.#/..#/##. => ###./..#./.#../...#\n" +
            "###/..#/##. => #.#./#.##/#..#/##..\n" +
            ".##/#.#/##. => ..../.#../..../....\n" +
            "###/#.#/##. => .###/..#./#.#./####\n" +
            "#.#/.##/##. => ..../.#.#/#.#./...#\n" +
            "###/.##/##. => ##../.#../.#.#/..##\n" +
            ".##/###/##. => ..#./#.#./##../..##\n" +
            "###/###/##. => ..#./###./#.#./..##\n" +
            "#.#/.../#.# => #.#./..../#.##/.#.#\n" +
            "###/.../#.# => #.##/#.../..##/...#\n" +
            "###/#../#.# => ####/.###/..#./.#.#\n" +
            "#.#/.#./#.# => ..#./#..#/#..#/##..\n" +
            "###/.#./#.# => ..../##../.#.#/##.#\n" +
            "###/##./#.# => ..##/..##/.#../####\n" +
            "#.#/#.#/#.# => ####/...#/#.#./#.#.\n" +
            "###/#.#/#.# => #.##/...#/..#./...#\n" +
            "#.#/###/#.# => #.##/####/#..#/..##\n" +
            "###/###/#.# => .##./.##./.##./.#..\n" +
            "###/#.#/### => .#../..../..../.###\n" +
            "###/###/### => #.#./#.#./###./###."

    val startGrid = ".#./..#/###"

    val testMapping = "../.# => ##./#../...\n" +
            ".#./..#/### => #..#/..../..../#..#"

    @JvmStatic
    fun main(args: Array<String>) {
        val startCell = parseSquare(startGrid)
        val mapper = Mapper()

        primaryMapping.split("\n").map { parseMapping(it) }.forEach {
//        testMapping.split("\n").map { parseMapping(it) }.forEach {
            mapper.addMappint(it)
        }

//        generateValiants(startCell).forEach {
//            printSquare(it)
//        }

        var workCell = startCell
        printSquare(workCell)
        println()
        for (i in 1..18) {
            workCell = scaleSquare(workCell, mapper)
            val count = workCell.content.count { it=='#' }
            println("iteration $i => $count")
        }

        val count = workCell.content.count { it=='#' }

        println("on: $count")
    }

    fun scaleSquare(square: Square, mapper: Mapper): Square {
        val currentSize = square.size
        val (stepBefore, stepAfter) = getScaling(currentSize)
        val newSize = stepAfter * currentSize / stepBefore
        val newSquare = Square(newSize)

        val steps = currentSize / stepBefore

        for (y in 0..(steps - 1)) {
            for (x in 0..(steps - 1)) {

                val part = square.part(x * stepBefore, y * stepBefore, stepBefore)
                val newPart = mapper.map(part)
                newSquare.insert(x * stepAfter, y * stepAfter, newPart)
            }
        }
        return newSquare
    }

    fun getScaling(size: Int): Pair<Int, Int> =
            if (size % 2 == 0) {
                Pair(2, 3)
            } else if (size % 3 == 0) {
                Pair(3, 4)
            } else {
                throw IllegalArgumentException("incorrect size: $size")
            }


    private fun parseMapping(line: String): Pair<Square, Square> {
        val split = line.split(" => ").map { it.trim() }
        return Pair(parseSquare(split[0]), parseSquare(split[1]))
    }

    private fun parseSquare(line: String): Square {
        val split = line.split("/")
        val size = split.size
        return Square(size, split.joinToString("").toCharArray())
    }

    private fun printSquare(square: Square) {
        for (y in 0..(square.size - 1)) {
            for (x in 0..(square.size - 1)) {
                print(square[x, y])

            }
            println()
        }
        println()
    }

    class Mapper {

        val mappings = mutableMapOf<Square, Square>()

        fun addMappint(mapping: Pair<Square, Square>) {
            generateValiants(mapping.first).forEach {
                mappings[it] = mapping.second
            }
        }

        fun map(square: Square): Square {
//            return mappings.find { isEquivalent(it.first, square) }?.second ?: kotlin.run { throw IllegalArgumentException("No mapping for $square") }
            return mappings[square] ?: kotlin.run { throw IllegalArgumentException("No mapping for $square") }
        }

        fun isEquivalent(square1: Square, square2: Square): Boolean {
            if (square1.size != square2.size) {
                return false
            }
            if (square1 == square2) {
                return true
            }
            return false
        }

    }

    fun generateValiants(square: Square): Set<Square> {
        val variants = mutableSetOf<Square>()

        var workSquare = square
        for (i in 1..4) {
            variants.add(workSquare)
            variants.add(workSquare.flipVertical())
            variants.add(workSquare.flipHorizontal())
            variants.add(workSquare.flipHorizontal().flipVertical())
            workSquare = workSquare.rotateRight()

        }
        return variants
    }

    data class Square(val size: Int, val content: CharArray) {
        constructor(size: Int) : this(size, CharArray(size * size))

        operator fun get(x: Int, y: Int): Char = content[y * size + x]
        operator fun set(x: Int, y: Int, c: Char) {
            content[y * size + x] = c
        }

        fun flipVertical(): Square {
            val newOne = Square(size)
            val s = size - 1
            for (y in (0..s)) {
                for (x in (0..s)) {
                    newOne[s - x, y] = this[x, y]
                }
            }
            return newOne
        }

        fun flipHorizontal(): Square {
            val newOne = Square(size)
            val s = size - 1
            for (y in (0..s)) {
                for (x in (0..s)) {
                    newOne[x, s - y] = this[x, y]
                }
            }
            return newOne
        }

        fun rotateRight(): Square {
            val newOne = Square(size)
            val s = size - 1
            for (y in (0..s)) {
                for (x in (0..s)) {
                    newOne[x, y] = this[y, s - x]
                }
            }
            return newOne
        }


        fun part(x: Int, y: Int, size: Int): Square {
            val square = Square(size)
            for (i in 0..(size - 1)) {
                for (j in 0..(size - 1)) {
                    square[i, j] = this[x + i, y + j]
                }
            }
            return square
        }

        override fun toString(): String {
            return String(content)
        }

        fun insert(x: Int, y: Int, square: Square): Square {
            val size = square.size
            for (i in 0..(size - 1)) {
                for (j in 0..(size - 1)) {
                    this[x + i, y + j] = square[i, j]
                }
            }
            return this
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Square

            if (size != other.size) return false
            if (!Arrays.equals(content, other.content)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = size
            result = 31 * result + Arrays.hashCode(content)
            return result
        }


    }

}
