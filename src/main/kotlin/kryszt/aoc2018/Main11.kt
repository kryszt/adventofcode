package kryszt.aoc2018

import kryszt.tools.Point

object Main11 {


    @JvmStatic
    fun main(args: Array<String>) {
        println(findMaxWithSize(9798))
    }

    private fun findMax(gridId: Int): Pair<Point, Int> {
        val grid = generateGrid(gridId)

        var bestPoint = Point()
        var bestValue = 0

        for (x in 1..298) {
            for (y in 1..298) {
                val power = gridPower(x, y, 3, grid)
                if (power > bestValue) {
                    bestValue = power
                    bestPoint = Point(x, y)
                }
            }
        }
        return Pair(bestPoint, bestValue)
    }

    private fun findMaxWithSize(gridId: Int): Pair<Point, Int> {
        val grid = generateGrid(gridId)

        var bestPoint = Point()
        var bestValue = 0
        var bestSize = 0

        for (s in 1..300) {
            println("Checking size $s")
            for (x in 1..(300 - s + 1)) {
                for (y in 1..(300 - s + 1)) {
                    val power = gridPower(x, y, s, grid)
                    if (power > bestValue) {
                        bestValue = power
                        bestPoint = Point(x, y)
                        bestSize = s
                    }
                }
            }
        }

        return Pair(bestPoint, bestSize)
    }

    private fun gridPower(startX: Int, startY: Int, size: Int, grid: Grid): Int {
        var total = 0
        for (x in startX..(startX + size - 1)) {
            for (y in startY..(startY + +size - 1)) {
                total += powerLevel(x, y, grid)
            }
        }
        return total
    }

    private fun powerLevel(x: Int, y: Int, gridId: Int): Int {
        val rackId = x + 10
        return ((((rackId * y + gridId) * rackId) / 100) % 10) - 5
    }

    private fun powerLevel(x: Int, y: Int, grid: Grid): Int {
        return grid[x][y]
    }

    private fun generateGrid(gridId: Int): Grid {
        val grid = Array(301) { IntArray(301) }
        for (x in 1..300) {
            for (y in 1..300) {
                grid[x][y] = powerLevel(x, y, gridId)
            }
        }
        return grid
    }

}

typealias Grid = Array<IntArray>
