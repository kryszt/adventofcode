package kryszt.aoc2018

import kryszt.tools.IoUtil

object Main12 {

    val seenStates = mutableSetOf<String>()

    @JvmStatic
    fun main(args: Array<String>) {
        val board = loadBoard(IoUtil.readTaskFileLines(this, "-test"))
        println(board)
//        val result = solve(board, 20)
        val result = BoardHelper("##################################################################################################################################################################################################", 50000000000 - 100)
        println(result)
        println(result.plantValue())
    }

    private fun solve(board: Board, steps: Int): BoardHelper {
        var boardHelper = BoardHelper(board.initialState, 0)
        seenStates.add(boardHelper.initialState)
        for (i in 1..steps) {
            boardHelper = makeNext(boardHelper, board.mappings)
            if (!seenStates.add(boardHelper.initialState)) {
                println("Seen state $boardHelper at $i")
//                return boardHelper
            }
        }
        return boardHelper
    }

    private fun makeNext(boardHelper: BoardHelper, mappings: Map<String, Char>): BoardHelper {
        val newPlants = mutableListOf<Char>()
        for (position in -3..boardHelper.size + 2) {
            val atPosition = boardHelper.stateAt(position)
            val nextState = mappings.getOrDefault(atPosition, '.')
            newPlants.add(nextState)
        }
        val plantsString = newPlants.joinToString(separator = "")
        val plants = nextState(currentState = plantsString, firstPosition = boardHelper.startIndex - 3)
        return BoardHelper(plants.state, plants.startIndex)
    }

    private fun nextState(currentState: String, firstPosition: Long): Plants {
        val first = currentState.indexOf('#')
        val last = currentState.lastIndexOf('#') + 1
        return Plants(currentState.substring(first, last), firstPosition + first)
    }

    private fun loadBoard(lines: List<String>): Board {
        val initial = lines.first()
        val map = lines.subList(1, lines.size).map { parseMap(it) }.toMap()
        return Board(initial, map)
    }

    private fun parseMap(line: String): Pair<String, Char> =
            line.split("=>").let {
                Pair(it.first().trim(), it.last().trim().first())
            }

    data class BoardHelper(val initialState: String, val startIndex: Long) {

        val helperState = ".....$initialState....."

        val size = initialState.length

        fun stateAt(position: Int): String {
            val internalPosition = position + 5
            return helperState.substring(internalPosition - 2, internalPosition + 3)
        }

        fun plantValue(): Long = initialState.mapIndexed { index, c -> if (c == '#') (index + startIndex) else 0 }.sum()

    }

    class Plants(val state: String, val startIndex: Long)

    data class Board(val initialState: String, val mappings: Map<String, Char>)
}
