package kryszt.aoc2018

import kryszt.tools.IoUtil
import kryszt.tools.Point
import java.lang.IllegalArgumentException
import java.util.*

object Main20 {

    val testString = "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))\$"

    @JvmStatic
    fun main(args: Array<String>) {
        val paths = IoUtil.readTaskFile(this)
        val collector = MapCollector()
        paths.forEach {
            collector.nextChar(it)
        }
        println(countDepth(Point(0, 0), collector.graph))
    }

    class MapCollector {
        private var currentPoint = Point()
        private var pointsStack = LinkedList<Point>()
        val graph = mutableMapOf<Point, Main20.GraphNode>()

        fun nextChar(c: Char) {
            when (c) {
                '^' -> println("^") //currentNode = Node(null)
                '$' -> println("done, on stack: $pointsStack")
                'N', 'S', 'E', 'W' -> {
                    val nextPoint = currentPoint + step(c)
                    addMove(currentPoint, nextPoint)
                    currentPoint = nextPoint
                }
                ')' -> {
                    currentPoint = pointsStack.pop()
                }
                '(' -> {
                    pointsStack.push(currentPoint)
                }
                '|' -> {
                    currentPoint = pointsStack.peek()
                }
            }
        }

        private fun addMove(from: Point, to: Point) {
            val fromNode = graph.getOrPut(from) { GraphNode(from) }
            val toNode = graph.getOrPut(to) { GraphNode(to) }
            fromNode.connections.add(to)
            toNode.connections.add(from)
        }
    }

    private fun countDepth(startPoint: Point, graph: Map<Point, GraphNode>): Int {
        val visited = mutableSetOf<Point>().apply { add(startPoint) }
        var nextToCheck: Set<Point> = setOf(startPoint)
        var steps = 0
        var atLeast1000Doors = 0

        while (nextToCheck.isNotEmpty()) {
            val potential = nextToCheck.mapNotNull { graph[it]?.connections }.flatten()
            val next = potential.filter { visited.add(it) }
            nextToCheck = next.toSet()
            if (nextToCheck.isNotEmpty()) {
                steps++
                if (steps >= 1000) {
                    atLeast1000Doors += nextToCheck.size
                }
            }
            println("step $steps -> next ${nextToCheck.size} -> $next")
        }
        println("End: seen ${visited.size} out of ${graph.size}")
        println("At least 1000 doors: $atLeast1000Doors")
        return steps
    }

    fun step(char: Char): Point = when (char) {
        'N' -> Point.Up
        'S' -> Point.Down
        'W' -> Point.Left
        'E' -> Point.Right
        else -> throw IllegalArgumentException("$char")
    }

    class GraphNode(val point: Point, val connections: MutableSet<Point> = mutableSetOf())

}
