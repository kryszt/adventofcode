package kryszt.aoc2018

class ProcessorAoc2018(val instructions: List<Instruction>, private val boundPointer: Int) {

    private var verbose = false
    private var pointer = 0L
    val intPointer: Int
        get() = pointer.toInt()

    var registers = mutableListOf(0L, 0L, 0L, 0L, 0L, 0L)
    var steps = 0

    fun canMove(): Boolean = pointer >= 0 && pointer < instructions.size

    fun executeNext() {
        val step = instructions[pointer.toInt()]
        registers[boundPointer] = pointer
        execute(step)
        pointer = registers[boundPointer]
        pointer++
        steps++
        if (verbose) {
            println("$steps -> $registers after $step")
        }
    }

    private fun execute(instruction: Instruction) {
        val (action, arg1, arg2, target) = instruction
        val result: Long = when (action) {
            "addr" -> registers[arg1] + registers[arg2]
            "addi" -> registers[arg1] + arg2
            //
            "mulr" -> registers[arg1] * registers[arg2]
            "muli" -> registers[arg1] * arg2
            //
            "banr" -> registers[arg1] and registers[arg2]
            "bani" -> registers[arg1] and arg2.toLong()
            //
            "borr" -> registers[arg1] or registers[arg2]
            "bori" -> registers[arg1] or arg2.toLong()
            //
            "setr" -> registers[arg1]
            "seti" -> arg1.toLong()
            //
            "gtir" -> if (arg1 > registers[arg2]) 1 else 0
            "gtri" -> if (registers[arg1] > arg2) 1 else 0
            "gtrr" -> if (registers[arg1] > registers[arg2]) 1 else 0
            //
            "eqir" -> if (arg1.toLong() == registers[arg2]) 1 else 0
            "eqri" -> if (registers[arg1] == arg2.toLong()) 1 else 0
            "eqrr" -> if (registers[arg1] == registers[arg2]) 1 else 0
            //
            else -> throw IllegalArgumentException("Unknown action: $action")
        }
        registers[target] = result
    }

    data class Instruction(val order: String, val arg1: Int, val arg2: Int, val target: Int)

    companion object {

        fun parseLine(line: String): Instruction = line.split(" ").let {
            Instruction(it.first(), it[1].toInt(), it[2].toInt(), it[3].toInt())
        }


        fun printUserReadableForm(lines: List<Instruction>, boundPointer: Int) {

            fun name(register: Int): String {
                return if (register == boundPointer) {
                    "R"
                } else {
                    ('A' + register).toString()
                }
            }

            lines.forEachIndexed { index, instrution ->
                val (action, arg1, arg2, target) = instrution
                val result: String = when (action) {
                    "addr" -> "${name(target)} = ${name(arg1)} + ${name(arg2)}"
                    "addi" -> "${name(target)} = ${name(arg1)} + $arg2"
                    //
                    "mulr" -> "${name(target)} = ${name(arg1)} * ${name(arg2)}"
                    "muli" -> "${name(target)} = ${name(arg1)} * $arg2"
                    //
                    "banr" -> "${name(target)} = ${name(arg1)} & ${name(arg2)}"
                    "bani" -> "${name(target)} = ${name(arg1)} & $arg2"
                    //
                    "borr" -> "${name(target)} = ${name(arg1)} | ${name(arg2)}"
                    "bori" -> "${name(target)} = ${name(arg1)} | $arg2"
                    //
                    "setr" -> "${name(target)} = ${name(arg1)}"
                    "seti" -> "${name(target)} = $arg1"
                    //
                    "gtir" -> "${name(target)} = ($arg1 > ${name(arg2)})? 1 : 0"
                    "gtri" -> "${name(target)} = (${name(arg1)} > $arg2)? 1 : 0"
                    "gtrr" -> "${name(target)} = (${name(arg1)} > ${name(arg2)})? 1 : 0"
                    //
                    "eqir" -> "${name(target)} = ($arg1 == ${name(arg2)})? 1 : 0"
                    "eqri" -> "${name(target)} = (${name(arg1)} == $arg2)? 1 : 0"
                    "eqrr" -> "${name(target)} = (${name(arg1)} == ${name(arg2)})? 1 : 0"
                    //
                    else -> throw IllegalArgumentException("Unknown action: $action")
                }
                println("$index: $result")
            }

        }

    }
}