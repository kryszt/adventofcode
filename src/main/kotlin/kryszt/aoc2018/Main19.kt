package kryszt.aoc2018

import kryszt.tools.IoUtil

object Main19 {

    @JvmStatic
    fun main(args: Array<String>) {
        solution1(IoUtil.readTaskFileLines(this).map { ProcessorAoc2018.parseLine(it) })
        solution2()
    }

    fun solution1(instructions: List<ProcessorAoc2018.Instruction>) {
        val processor = ProcessorAoc2018(instructions, 4)
        while (processor.canMove()) {
            processor.executeNext()
        }
        println("${processor.steps} ${processor.registers}")
    }

    fun solution2() {
        //provided algorithm calculates sum of dividers of a number in register[2]
        //when register[0] == 0, register[2] is initialized to 887 and its dividers are 887 and 1
        //when register[0] == 0, register[2] is initialized to 10.551.287
        ProcessorAoc2018.printUserReadableForm(IoUtil.readTaskFileLines(this).map { ProcessorAoc2018.parseLine(it) }, 4)
        println("Sum for 887 is ${sumUpDividers(887)}")
        println("Sum for 10.551.287 is ${sumUpDividers(10_551_287)}")
    }

    private fun sumUpDividers(number: Int): Int {
        val maxDivider = Math.sqrt(number.toDouble()).toInt() + 1
        val allDividers = mutableSetOf<Int>()

        for (i in 1..maxDivider) {
            if (number % i == 0) {
                allDividers.add(i)
                allDividers.add(number / i)
            }
        }
        return allDividers.sum()
    }

}
