package kryszt.aoc2018

import kryszt.tools.IoUtil
import java.util.*

object Main7 {

    val testValues = listOf(
            "Step C must be finished before step A can begin.",
            "Step C must be finished before step F can begin.",
            "Step A must be finished before step B can begin.",
            "Step A must be finished before step D can begin.",
            "Step B must be finished before step E can begin.",
            "Step D must be finished before step E can begin.",
            "Step F must be finished before step E can begin.")

    @JvmStatic
    fun main(args: Array<String>) {
        solution2()
    }

    private fun solution2() {
        val builder = GraphBuilder()
//        val values = testValues
        val values = IoUtil.readTaskFileLines(this)
        values.map(::parseLine).forEach { edge ->
            builder.addConnection(edge.required, edge.task)
        }
//        val solver = ParallelSolver(2, builder.nodes) { node -> 1 + (node.name - 'A') }
        val solver = ParallelSolver(5, builder.nodes) { node -> 61 + (node.name - 'A') }
        val solution = solver.solve()
        println(solution)
    }

    private fun solution1() {
        val builder = GraphBuilder()
//        val values = testValues
        val values = IoUtil.readTaskFileLines(this)
        values.map(::parseLine).forEach { edge ->
            builder.addConnection(edge.required, edge.task)
        }
        val solution = solveGraph(builder.nodes)
        println(solution.joinToString(separator = ""))
    }

    private fun show(graph: TreeMap<Char, Node>) {
        graph.forEach { t, u ->
            println("$t -> $u")
        }
    }

    private fun parseLine(line: String): Edge {
        return Edge(line[5], line[36])
    }

    private fun solveGraph(graph: TreeMap<Char, Node>): List<Char> {
        val resolution = mutableListOf<Char>()

        while (graph.isNotEmpty()) {
            println()
//            show(graph)
            val node = graph.entries.find { it.value.isAvailable }!!.value
            println("Selected: $node")
            println("enables: ${node.enables}")
            graph.remove(node.name)
//            firstFree.value.enables.forEach { println(" removing ${it.name} -> ${it.requires.remove(firstFree.value)}") }
            node.enables.forEach { dependent -> dependent.requires.remove(node) }
            resolution.add(node.name)
        }
        return resolution
    }

    class GraphBuilder {

        val nodes = TreeMap<Char, Node>()

        fun addConnection(required: Char, dependent: Char) {
            val first = findNode(required)
            val second = findNode(dependent)
            first.enables.add(second)
            second.requires.add(first)
        }

        private fun findNode(name: Char): Node =
                nodes.getOrPut(name) { Node(name) }

    }

    data class Edge(val required: Char, val task: Char)
    class Node(val name: Char, val requires: MutableSet<Node> = TreeSet(), val enables: MutableSet<Node> = TreeSet()) : Comparable<Node> {
        val isAvailable: Boolean
            get() = requires.isEmpty()

        override fun equals(other: Any?): Boolean {
            return (other as? Node)?.name == name
        }

        override fun hashCode(): Int {
            return name.hashCode()
        }

        override fun compareTo(other: Node): Int {
            return name.compareTo(other.name)
        }

        override fun toString(): String {
            return "Node $name," +
                    " $isAvailable" +
                    " requires: ${requires.joinToString(separator = ",", transform = { it.name.toString() })}" +
                    " enables: ${enables.joinToString(separator = ",", transform = { it.name.toString() })}"
        }

        fun taskDone(node: Node) {
            requires.remove(node)
        }

        fun informDone() {
            enables.forEach { it.taskDone(this) }
        }
    }

    class ParallelSolver(workersCount: Int, val nodes: TreeMap<Char, Node>, val timeForTask: (Node) -> Int) {

        val workers = (0 until workersCount).map { Worker(it) }
        var time = 0

        fun solve(): Int {
            val solution = mutableListOf<Char>()
            while (nodes.isNotEmpty() || workers.any { it.isWorking }) {
                applyTasks()
                val timeToSkip = workers.filter { it.isWorking }.minByOrNull { it.timeLeft }?.timeLeft!!
                time += timeToSkip
                workers.forEach { it.moveBy(timeToSkip) }
                val finishedTasks = workers
                        .filter { it.hasTask && it.isFinished }
                        .map {
                            println("Worker ${it.id} finished task ${it.task?.node?.name} at $time")
                            it
                        }
                        .mapNotNull {
                            val doneTask = it.task
                            it.clear()
                            doneTask
                        }
                finishedTasks.forEach { it.node.informDone() }
                finishedTasks.map { it.node }.sorted().forEach { solution.add(it.name) }
            }
            return time
        }

        private fun applyTasks() {
            println("applyTasks at $time")
            workers
                    .filter { it.isFinished }
                    .forEach {
                        val nextNode = takeNextReadyNode()
                        if (nextNode != null) {
                            val task = Task(timeForTask(nextNode), nextNode)
                            it.startTask(task)
                        }
                    }

        }

        private fun takeNextReadyNode(): Node? {
            return nodes.entries.find { it.value.isAvailable }?.value?.also {
                nodes.remove(it.name)
            }
        }

    }

    class Worker(val id: Int) {
        var task: Task? = null

        fun moveBy(time: Int) {
            task?.moveBy(time)
        }

        val isFinished: Boolean
            get() = with(task) { return this == null || this.isFinished }

        val isWorking: Boolean
            get() = with(task) { return this != null && !this.isFinished }

        val hasTask: Boolean
            get() = task != null

        val timeLeft: Int
            get() = task?.timeLeft ?: 0

        fun startTask(newTask: Task) {
            println("Worker $id starts task ${newTask.node.name} for ${newTask.initialTime}")
            task = newTask
        }

        fun clear() {
            task = null
        }

    }

    class Task(val initialTime: Int, val node: Node) {
        var timeLeft = initialTime

        val isFinished
            get() = timeLeft <= 0

        fun moveBy(time: Int) {
            timeLeft -= time
        }
    }
}
