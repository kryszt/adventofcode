package kryszt.aoc2018

import java.util.concurrent.atomic.AtomicLong

object Main9 {

    val result0: Triple<Int, Int, Long> = Triple(9, 25, 32)
    val result1: Triple<Int, Int, Long> = Triple(10, 1618, 8317)
    val result2: Triple<Int, Int, Long> = Triple(13, 7999, 146373)
    val result3: Triple<Int, Int, Long> = Triple(17, 1104, 2764)
    val result4: Triple<Int, Int, Long> = Triple(21, 6111, 54718)
    val result5: Triple<Int, Int, Long> = Triple(30, 5807, 37305)
    val resultReal: Triple<Int, Int, Long> = Triple(465, 71940, 384475)
    val resultReal2: Triple<Int, Int, Long> = Triple(465, 7194000, 37305)

    @JvmStatic
    fun main(args: Array<String>) {
        showResultsFor(result0)
        showResultsFor(result1)
        showResultsFor(result2)
        showResultsFor(result3)
        showResultsFor(result4)
        showResultsFor(result5)
        showResultsFor(resultReal)
        showResultsFor(resultReal2)
    }

    private fun showResultsFor(setup: Triple<Int, Int, Long>) {
        showResultsFor(setup.first, setup.second, setup.third)
    }

    private fun showResultsFor(players: Int, steps: Int, expected: Long) {
        val start = System.currentTimeMillis()
//        val game = Game(players, steps)
        val game = Game2(players, steps)
        val scores = game.solve()
        val maxScore = scores.values.maxByOrNull { it.get() }!!.get()
        val end = System.currentTimeMillis()
//        showScores(scores, 23)
        println("Result for $players @ $steps is $maxScore, expected? ${maxScore == expected}, took ${end - start}ms")
    }

    private fun showScores(scores: MutableMap<Int, AtomicLong>, players: Int) {
        println(scores)
        scores.toSortedMap().forEach { (k, v) ->
            println("$k -> $v; ${v.get() / players} + ${v.get() % players}")
        }
    }

    class Game(val players: Int, val lastNumber: Int) {

        val scores: MutableMap<Int, AtomicLong> = mutableMapOf()

        val circle = mutableListOf<Int>().apply { add(0) }
        var currentIndex = 0
        val size: Int
            get() = circle.size

        fun solve(): MutableMap<Int, AtomicLong> {
            for (move in 0 until lastNumber) {
                if (move % 100000 == 0) {
                    println("move $move")
                }
                val player = (move % players) + 1
                val marble = move + 1
                move(player, marble)
            }
            return scores
        }

        fun move(player: Int, marble: Int) {
            if (marble % 23 == 0) {
                addScore(player, marble)
                val indexToTake = (size + currentIndex - 7) % size
                val removedValue = circle.removeAt(indexToTake)
                addScore(player, removedValue)
                currentIndex = indexToTake
            } else {
                val nextPlace = (currentIndex + 2) % size
                circle.add(nextPlace, marble)
                currentIndex = nextPlace
            }
        }

        private fun addScore(player: Int, points: Int) {
//            println("adding player $player $points points")
            scores.getOrPut(player) { AtomicLong() }.addAndGet(points.toLong())
        }

    }

    class Game2(val players: Int, val lastNumber: Int) {

        val scores: MutableMap<Int, AtomicLong> = mutableMapOf()

        var currentNode: Node = Node(0).apply {
            before = this
            after = this
        }

        fun solve(): MutableMap<Int, AtomicLong> {
            for (move in 0 until lastNumber) {
                if (move % 100000 == 0) {
                    println("move $move")
                }
                val player = (move % players) + 1
                val marble = move + 1
                move(player, marble)
            }
            return scores
        }

        fun move(player: Int, marble: Int) {
            if (marble % 23 == 0) {
                addScore(player, marble)
                val toRemove = findForRemoval(currentNode)
                addScore(player, toRemove.number)
                val newCurrent = toRemove.after!!
                toRemove.before!!.after = toRemove.after
                toRemove.after!!.before = toRemove.before
                currentNode = newCurrent
            } else {
                val next = currentNode.after!!
                val afterThat = next.after!!
                val newNode = Node(marble, before = next, after = afterThat)
                next.after = newNode
                afterThat.before = newNode
                currentNode = newNode
            }
        }

        fun findForRemoval(current: Node): Node {
            var toRemove = current
            for (i in 0 until 7) {
                toRemove = toRemove.before!!
            }
            return toRemove
        }

        private fun addScore(player: Int, points: Int) {
//            println("adding player $player $points points")
            scores.getOrPut(player) { AtomicLong() }.addAndGet(points.toLong())
        }
    }

    class Node(val number: Int, var before: Node? = null, var after: Node? = null)
}
