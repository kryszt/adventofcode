package kryszt.aoc2018

import kryszt.tools.BinarySearch
import kryszt.tools.IoUtil
import kryszt.tools.Point

object Main15 {

    @JvmStatic
    fun main(args: Array<String>) {
        solution2()
    }

    fun solution1() {
        val arena = readMap(IoUtil.readTaskFileLines(this))
        arena.printMap()
        while (!arena.isFinished()) {
            arena.executeRound()
            println("Steps ${arena.steps}")
        }
        arena.printMap()
        arena.printAlive()
        println("outcome ${arena.outcome()} after ${arena.steps}")
    }

    fun solution2() {
        BinarySearch.binarySearchSolution { power ->
            findOutcomeFor(readMap(IoUtil.readTaskFileLines(this), power)) > 0
        }.also { lowest ->
            println("Found: $lowest")
            findOutcomeFor(readMap(IoUtil.readTaskFileLines(this), lowest)).also {
                println("Outcome $it")
            }
        }

    }

    private fun findOutcomeFor(arena: FightMap): Int {
        while (!arena.isFinished()) {
            arena.executeRound()
        }
        return if (arena.allElvesAlive()) {
            arena.outcome()
        } else {
            -1
        }
    }

    private fun readMap(lines: List<String>, elfPower: Int = 3): FightMap {
        val units = mutableListOf<Fighter>()
        lines.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                when (c) {
                    'G' -> units.add(Fighter(c, Point(x, y)))
                    'E' -> units.add(Fighter(c, Point(x, y), attack = elfPower))
                }
            }
        }

        val cave = CaveMap(lines.map { it.replace('E', '.').replace('G', '.') })
        return FightMap(cave, units)
    }

    class CaveMap(private val base: List<String>) {
        val width = base.maxByOrNull { it.length }?.length ?: 0
        val height = base.size

        fun isOpen(place: Point): Boolean {
            return base.getOrNull(place.y)?.getOrNull(place.x) == '.'
        }
    }

    class FightMap(private val cave: CaveMap, private val fighters: List<Fighter>) {
        var steps = 0

        private val alive: List<Fighter>
            get() = fighters.filter { it.alive }

        fun executeRound() {
            steps++
            val moveOrder = alive.sortedBy { it.position.x }.sortedBy { it.position.y }
            moveOrder.forEach {
                if (it.alive) {
                    if (!unitTurn(it, alive)) {
                        steps--
                        return
                    }
                }
            }
        }

        private fun unitTurn(unit: Fighter, alive: List<Fighter>): Boolean {
            val enemies = alive.filter { it.alive && it.type != unit.type }
            if (enemies.isEmpty()) {
                return false
            }
            //attack if next to enemy
            if (attackNextTo(unit, enemies)) {
                return true
            }
            //find ranges
            unit.position = findNextPosition(unit, enemies)
            attackNextTo(unit, enemies)
            return true
        }

        private fun attackNextTo(unit: Fighter, enemies: List<Fighter>): Boolean {
            val firstTarget = enemies
                    .filter { it.position.manhattanDistance(unit.position) == 1 }
                    .sortedBy { it.position.x }
                    .sortedBy { it.position.y }
                    .sortedBy { it.hp }
                    .firstOrNull()
            firstTarget?.run {
                firstTarget.hp -= unit.attack
                return true
            }
            return false
        }

        private fun findNextPosition(fighter: Fighter, enemies: List<Fighter>): Point {
            val allRanges = enemies
                    .map { it.unitRanges() }
                    .flatten()
                    .filter {
                        isEmpty(it)
                    }.takeIf { it.isNotEmpty() }
                    ?: return fighter.position

            val targetRange = findClosest(fighter.position, allRanges.toSet())
                    .firstOrNull()
                    ?: return fighter.position

            val unitMoves = fighter
                .position
                .crossNeighbours()
                .filter { isEmpty(it) }
                .takeIf { it.isNotEmpty() }
                ?: return fighter.position

            return findClosest(targetRange, unitMoves.toSet())
                    .firstOrNull()
                    ?: fighter.position
        }

        private fun findClosest(startPoint: Point, targets: Set<Point>): List<Point> {
            if (targets.contains(startPoint)) {
                return listOf(startPoint)
            }

            val visited = mutableSetOf<Point>()
            var nextToCheck: Set<Point> = startPoint
                .crossNeighbours()
                .filter { isEmpty(it) && visited.add(it) }
                .toSet()

            while (nextToCheck.isNotEmpty()) {
                val nearest = nextToCheck.intersect(targets)
                if (nearest.isNotEmpty()) {
                    return nearest.sortedBy { it.x }.sortedBy { it.y }
                }
                nextToCheck = nextToCheck
                        .map { it.crossNeighbours() }
                        .flatten()
                        .filter { isEmpty(it) && visited.add(it) }
                        .toSet()
            }

            return emptyList()
        }

        private fun isEmpty(place: Point): Boolean {
            return cave.isOpen(place) && fighters.none { it.alive && it.position == place }
        }

        fun isFinished(): Boolean {
            return with(alive) {
                val aliveType = alive.first().type
                alive.all { it.type == aliveType }
            }
        }

        fun printMap() {
            for (y in 0 until cave.height) {
                for (x in 0 until cave.width) {
                    val point = Point(x, y)
                    val charAt: Char = fighters.firstOrNull { it.alive && it.position == point }?.type
                            ?: if (cave.isOpen(point)) '.' else '#'
                    print(charAt)
                }
                println()
            }
        }

        fun printAlive() {
            alive.sortedBy { it.position.x }.sortedBy { it.position.y }.forEach {
                println("${it.type}(${it.hp}) at ${it.position}")
            }
        }

        fun outcome(): Int {
            return (steps) * (alive.map { it.hp }.sum())
        }

        fun allElvesAlive(): Boolean {
            return fighters.filter { it.type == 'E' }.all { it.alive }
        }
    }

    class Fighter(val type: Char, var position: Point, var hp: Int = 200, val attack: Int = 3) {
        val alive
            get() = hp > 0

        fun unitRanges(): List<Point> {
            return position.crossNeighbours()
        }
    }

}
