package kryszt.aoc2018

import kryszt.tools.ActionProcessor
import kryszt.tools.IoUtil
import kryszt.tools.Point3D
import java.util.*
import kotlin.math.max

object Main23 {

    private val Pattern = "pos=<${ActionProcessor.GroupNumber},${ActionProcessor.GroupNumber},${ActionProcessor.GroupNumber}>, r=${ActionProcessor.GroupNumber}".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {
        val drones = IoUtil.readTaskFileLines(this).map { parseDrone(it) }
//        val drones = IoUtil.readTaskFileLines(this, "-test").map { parseDrone(it) }
//        val drones = IoUtil.readTaskFileLines(this, "-test2").map { parseDrone(it) }
        solution2(drones)
    }

    private fun solution1(drones: List<Drone>) {
        val strongest = drones.maxByOrNull { it.range }!!
        val inRange = drones.count { strongest.position.distance(it.position) <= strongest.range }
        println("$inRange for $strongest")
    }

    private fun solution2(drones: List<Drone>) {

        val coverage = drones.map { drone ->
            val min = max(0, drone.position.distance(Center) - drone.range)
            val max = drone.position.distance(Center) + drone.range
            Distance(min, max)
        }
        val sortedAdds: LinkedList<Distance> = LinkedList(coverage.sortedBy { it.from })
        val sortedEnds: LinkedList<Distance> = LinkedList(coverage.sortedBy { it.to })

        var nextAdd: Distance? = sortedAdds.pollFirst()
        var nextRemove: Distance? = sortedEnds.pollFirst()

        var bestInRange = 0
        var firstBest = 0
        var currentInRange = 0
        var currentPosition = 0

        while (nextAdd != null || nextRemove != null) {
            val nextAddAt = nextAdd?.from ?: Int.MAX_VALUE
            val nextRemoveAt = nextRemove?.to ?: Int.MAX_VALUE

            if (nextAddAt < nextRemoveAt) {
                currentPosition = nextAddAt
                currentInRange++
                nextAdd = sortedAdds.pollFirst()
            } else {
                currentPosition = nextRemoveAt
                currentInRange--
                nextRemove = sortedEnds.pollFirst()
            }

            if (currentInRange > bestInRange) {
                bestInRange = currentInRange
                firstBest = currentPosition
                println("New best $bestInRange at $firstBest")
            }
        }

        println("Best $bestInRange at $firstBest")
    }

    data class Drone(val position: Point3D, val range: Int)
    data class Distance(val from: Int, val to: Int)

    private fun parseDrone(line: String): Drone {
        return Pattern.matcher(line).takeIf { it.find() }?.let {
            Drone(Point3D(it.group(1).toInt(), it.group(2).toInt(), it.group(3).toInt()), it.group(4).toInt())
        } ?: run { throw IllegalArgumentException(line) }
    }

    private val Center = Point3D()
}
