package kryszt.aoc2018

import kryszt.tools.IoUtil
import kryszt.tools.Point
import kryszt.tools.Rectangle

object Main17 {

    @JvmStatic
    fun main(args: Array<String>) {
//        val mapLines = IoUtil.readTaskFileLines(this, "-test").map { parseLine(it) }
        val mapLines = IoUtil.readTaskFileLines(this).map { parseLine(it) }
        val waterMap = WaterMap(mapLines)
//        waterMap.print()
        println(waterMap.coordinates)
        val solver = WaterSolver(waterMap)
        solver.openWater(Point(500, 0))
        println()
//        solver.print()
        println("total water: ${solver.countWater()}")
        println("total kept: ${solver.countNotFlowingWater()}")
    }

    class WaterMap(lines: List<MapLine>) {
        private val closedPlaces: Set<Point>
        val coordinates: Rectangle

        init {
            val places = mutableSetOf<Point>()
            lines.forEach { line ->
                for (x in line.x) {
                    for (y in line.y) {
                        places.add(Point(x, y))
                    }
                }
            }
            closedPlaces = places
            coordinates = findArea(places)
        }

        private fun findArea(places: MutableSet<Point>): Rectangle {
            val x0 = places.minByOrNull { it.x }?.x ?: 0
            val y0 = places.minByOrNull { it.y }?.y ?: 0
            val x1 = places.maxByOrNull { it.x }?.x ?: 0
            val y1 = places.maxByOrNull { it.y }?.y ?: 0
            return Rectangle(
                    x = x0, y = y0,
                    width = 1 + x1 - x0,
                    height = 1 + y1 - y0
            )
        }

        private fun isClosed(x: Int, y: Int): Boolean = closedPlaces.contains(Point(x, y))

        fun isClosed(point: Point): Boolean = closedPlaces.contains(point)

        fun print() {
            for (y in coordinates.top..coordinates.bottom) {
                for (x in coordinates.left..coordinates.right) {
                    if (isClosed(x, y)) {
                        print("#")
                    } else {
                        print(".")
                    }

                }
                println()
            }
        }

        fun isBelowMap(point: Point): Boolean {
            return point.y > coordinates.bottom
        }

    }

    class WaterSolver(private val caveMap: WaterMap) {

        private val waterPoints = mutableSetOf<Point>()
        private val waterDrainPoints = mutableSetOf<Point>()

        fun openWater(waterSource: Point) {
            visitPlace(waterSource.move(Point.Down))
        }

        private fun visitPlace(place: Point) {
//            println("Visiting $place")
            val bottom = place.move(Point.Down)
            if (isClosed(place)) {
                return
            }
            if (isBelowMap(bottom)) {
                waterPoints.add(place)
                waterDrainPoints.add(place)
                return
            }
            waterPoints.add(place)
            visitPlace(bottom)
            if (isSolid(bottom)) {

                val right = place.move(Point.Right)
                if (canVisit(right)) {
                    visitPlace(right)
                }
                val left = place.move(Point.Left)
                if (canVisit(left)) {
                    visitPlace(left)
                }

                if (isDrain(bottom) || isDrain(left) || isDrain(right)) {
                    waterDrainPoints.add(place)
                    markDrainOnLevel(place)
                }
            } else {
                waterDrainPoints.add(place)
            }
        }

        private fun markDrainOnLevel(place: Point) {
            markDrainOnLevel(place.move(Point.Left), Point.Left)
            markDrainOnLevel(place.move(Point.Right), Point.Right)
        }

        private fun markDrainOnLevel(place: Point, direction: Point) {
            var position = place
            while (isWater(position) && !isDrain(position)) {
                waterDrainPoints.add(position)
                position = position.move(direction)
            }
        }

        private fun isSolid(place: Point): Boolean {
            return caveMap.isClosed(place) || (waterPoints.contains(place) && !waterDrainPoints.contains(place))
        }

        private fun isDrain(place: Point): Boolean {
            return waterDrainPoints.contains(place)
        }

        private fun isWater(place: Point): Boolean {
            return waterPoints.contains(place)
        }

        private fun isClosed(place: Point): Boolean {
            return caveMap.isClosed(place)
        }

        private fun isBelowMap(place: Point): Boolean {
            return caveMap.isBelowMap(place)
        }

        private fun canVisit(place: Point): Boolean {
            return !isSolid(place) && !isBelowMap(place) && !waterPoints.contains(place)
        }

        fun print() {
            val coordinates = caveMap.coordinates
            for (y in coordinates.top..coordinates.bottom) {
                for (x in (coordinates.left - 1)..(coordinates.right + 1)) {
                    val place = Point(x, y)
                    when {
                        caveMap.isClosed(place) -> print("#")
                        isDrain(place) -> print("|")
                        isWater(place) -> print("*")
                        else -> print(".")
                    }

                }
                println()
            }
        }

        fun countWater(): Int {
            return waterPoints.count { it.y >= caveMap.coordinates.top && it.y <= caveMap.coordinates.bottom }
        }

        fun countNotFlowingWater(): Int {
            return waterPoints.count { it.y >= caveMap.coordinates.top && it.y <= caveMap.coordinates.bottom && !isDrain(it) }
        }
    }

    fun parseLine(line: String): MapLine {
        val firstValue = line.substring(2, line.indexOf(",")).toInt()
        val secondValue = line.substring(line.lastIndexOf("=") + 1, line.indexOf(".")).toInt()
        val thirdValue = line.substring(line.lastIndexOf(".") + 1, line.length).toInt()
        return if (line.startsWith("x")) {
            MapLine(x = firstValue..firstValue, y = secondValue..thirdValue)
        } else {
            MapLine(x = secondValue..thirdValue, y = firstValue..firstValue)
        }
    }

    data class MapLine(val x: IntRange, val y: IntRange)


}
