package kryszt.aoc2018

import kryszt.tools.IoUtil
import kryszt.tools.Point
import java.lang.IllegalArgumentException

object Main13 {


    @JvmStatic
    fun main(args: Array<String>) {
        val map = loadMap(IoUtil.readTaskFileLines(this, ""))
        solve2(map)
    }

    private fun solve1(map: RoadMap) {
        while (true) {
            map.printMap()
            moveAllCarts(map)
        }
    }

    private fun solve2(map: RoadMap) {
        while (map.carts.size > 1) {
//            map.printMap()
            moveAllCartsSafe(map)
        }
        map.printMap()
        println(map.carts)
    }

    private fun loadMap(lines: List<String>): RoadMap {
        val carts = loadCarts(lines)
        val clearedLines = lines.map { it.cleanCarts() }
        return RoadMap(clearedLines, carts.toMutableList())
    }

    private fun String.cleanCarts(): String {
        return replace('^', '|').replace('v', '|').replace('<', '-').replace('>', '-')
    }

    private fun loadCarts(lines: List<String>): List<Cart> {
        val carts = mutableListOf<Cart>()
        lines.forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                when (c) {
                    '^' -> carts.add(Cart(Point(x, y), Point.Up))
                    '<' -> carts.add(Cart(Point(x, y), Point.Left))
                    '>' -> carts.add(Cart(Point(x, y), Point.Right))
                    'v' -> carts.add(Cart(Point(x, y), Point.Down))
                }
            }
        }
        return carts
    }

    private fun moveAllCarts(map: RoadMap) {
        val sortedCarts = map.carts.sortedBy { it.position.x }.sortedBy { it.position.y }
        sortedCarts.forEach {
            moveCart(it, map)
        }
    }

    private fun moveAllCartsSafe(map: RoadMap) {
        val sortedCarts = map.carts.sortedBy { it.position.x }.sortedBy { it.position.y }
        val crashed = mutableSetOf<Cart>()
        sortedCarts.forEach {
            try {
                if (!crashed.contains(it)) {
                    moveCart(it, map)
                }
            } catch (e: CrashException) {
                crashed.add(it)
                map.removeCart(it)
                map.cartAt(e.at)?.let { otherCrashed ->
                    crashed.add(otherCrashed)
                    map.removeCart(otherCrashed)
                }
            }
        }
    }

    @Throws(CrashException::class)
    private fun moveCart(cart: Cart, map: RoadMap) {
        val newPosition = cart.position + cart.direction
        if (map.isCart(newPosition)) {
            throw CrashException(newPosition)
        }
        cart.position = newPosition
        when {
            map.isCrossing(newPosition) -> {
                cart.direction = cart.direction.nextPosition(cart.nextTurn)
                cart.nextTurn = cart.nextTurn.next()
            }
            map.isFallingTurn(newPosition) -> {
                val nextDirection = when (cart.direction) {
                    Point.Up -> Point.Left
                    Point.Down -> Point.Right
                    Point.Left -> Point.Up
                    Point.Right -> Point.Down
                    else -> throw IllegalArgumentException("unsupported direction ${cart.direction}")
                }
                cart.direction = nextDirection
            }
            map.isRisingTurn(newPosition) -> {
                val nextDirection = when (cart.direction) {
                    Point.Up -> Point.Right
                    Point.Down -> Point.Left
                    Point.Left -> Point.Down
                    Point.Right -> Point.Up
                    else -> throw IllegalArgumentException("unsupported direction ${cart.direction}")
                }
                cart.direction = nextDirection
            }
        }
    }

    private fun Point.nextPosition(turn: NextTurn): Point = when (turn) {
        NextTurn.Left -> left()
        NextTurn.Straight -> this
        NextTurn.Right -> right()
    }

    private fun NextTurn.next(): NextTurn = when (this) {
        NextTurn.Left -> NextTurn.Straight
        NextTurn.Straight -> NextTurn.Right
        NextTurn.Right -> NextTurn.Left
    }

    class RoadMap(private val lines: List<String>, val carts: MutableList<Cart>) {

        fun removeCart(cart: Cart) {
            carts.remove(cart)
        }

        fun isCart(point: Point): Boolean {
            return cartAt(point) != null
        }

        fun cartAt(point: Point): Cart? = carts.find { it.position == point }

        fun isCrossing(point: Point): Boolean {
            return placeAt(point) == '+'
        }

        fun isFallingTurn(point: Point) = placeAt(point) == '\\'

        fun isRisingTurn(point: Point) = placeAt(point) == '/'

        private fun placeAt(point: Point): Char = lines[point.y][point.x]

        fun printMap() {
            val chars = lines.map { it.toList().toMutableList() }
            carts.forEach {
                val directionChar: Char = when (it.direction) {
                    Point.Up -> '^'
                    Point.Down -> 'v'
                    Point.Left -> '<'
                    Point.Right -> '>'
                    else -> throw IllegalArgumentException("Unsupported direction ${it.direction}")
                }
                chars[it.position.y][it.position.x] = directionChar
            }
            chars.forEach {
                println(it.joinToString(separator = ""))
            }
            println()
        }


        override fun toString(): String {
            return "RoadMap(lines=$lines, carts=$carts)"
        }


    }

    class CrashException(val at: Point) : Exception("Crash at $at")
    data class Cart(var position: Point, var direction: Point, var nextTurn: NextTurn = NextTurn.Left)
    enum class NextTurn {
        Left, Straight, Right
    }
}
