package kryszt.aoc2018

import kryszt.tools.IoUtil

object Main21 {

    @JvmStatic
    fun main(args: Array<String>) {
        solution2()
    }

    fun solution1() {
        val instructions = IoUtil.readTaskFileLines(this).map { ProcessorAoc2018.parseLine(it) }
        val processor = ProcessorAoc2018(instructions, 5)
        while (processor.canMove()) {
            if (processor.intPointer == 28) {
                val nextCheck = processor.registers[1]
                println("Found number $nextCheck")
                return
            }
            processor.executeNext()
        }
        println("${processor.steps} ${processor.registers} ")
    }

    fun solution2() {
        val instructions = IoUtil.readTaskFileLines(this).map { ProcessorAoc2018.parseLine(it) }
        val processor = ProcessorAoc2018(instructions, 5)

        val seenNumbers = mutableSetOf<Long>()

        var previousSeen = 0L

        while (processor.canMove()) {
            if (processor.intPointer == 28) {
                val nextCheck = processor.registers[1]
                if (!seenNumbers.add(nextCheck)) {
                    println("Already seen $nextCheck")
                    println("Previous $previousSeen")
                    return
                }
                previousSeen = nextCheck
                if (seenNumbers.size % 100 == 0) {
                    println("seen numbers = ${seenNumbers.size}")
                }
            }
            processor.executeNext()
        }
        println("${processor.steps} ${processor.registers} ")
    }

}
