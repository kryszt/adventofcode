package kryszt.aoc2018

import kryszt.tools.IoUtil

object Main18 {

    @JvmStatic
    fun main(args: Array<String>) {
        solution2()
    }

    private fun solution1() {
        val map = LumberMap(IoUtil.readTaskFileLines(this))
        map.print()
        var current = map
        for (i in 1..10) {
            current = nextRound(current)
        }
        println("${current.countOf('|')} x ${current.countOf('#')} = ${current.countOf('|') * current.countOf('#')}")
    }

    private fun solution2() {
        val map = LumberMap(IoUtil.readTaskFileLines(this))
        map.print()
        val seenStates = mutableSetOf<String>()

        var current = map
        for (i in 1..(525 + 27)) {
            current = nextRound(current)
            if (!seenStates.add(current.combined())) {
                seenStates.clear()
                seenStates.add(current.combined())
                println("repeated at $i")
            }
        }
        println("${current.countOf('|')} x ${current.countOf('#')} = ${current.countOf('|') * current.countOf('#')}")
    }

    private fun nextRound(map: LumberMap): LumberMap {
        val next = map.base.mapIndexed { y, line ->
            line.mapIndexed { x, sign ->
                nextAt(map, x, y)
            }.joinToString(separator = "")
        }
        return LumberMap(next)
    }

    private fun nextAt(map: LumberMap, x: Int, y: Int): Char {
        val sign = map.signAt(x, y)
        return when (sign) {
            '.' -> if (map.countAround(x, y) { it == '|' } >= 3) '|' else '.'
            '|' -> if (map.countAround(x, y) { it == '#' } >= 3) '#' else '|'
            '#' -> if ((map.countAround(x, y) { it == '#' } >= 1) && (map.countAround(x, y) { it == '|' } >= 1)) '#' else '.'
            else -> sign
        }
    }

    class LumberMap(val base: List<String>) {
        private val width = base.maxByOrNull { it.length }!!.length
        private val height = base.size

        fun signAt(x: Int, y: Int): Char = base[y][x]

        fun countAround(midX: Int, midY: Int, predicate: (Char) -> Boolean): Int {
            val minX = Math.max(midX - 1, 0)
            val maxX = Math.min(midX + 1, width - 1)

            val minY = Math.max(midY - 1, 0)
            val maxY = Math.min(midY + 1, height - 1)

            var sum = 0
            for (x in minX..maxX) {
                for (y in minY..maxY) {
                    if ((x != midX || y != midY) && predicate(base[y][x])) {
                        sum++
                    }
                }
            }
            return sum
        }

        fun countOf(char: Char): Int {
            return base.sumBy { line -> line.count { it == char } }
        }

        fun print() {
            base.forEach { println(it) }
            println()
        }

        fun combined(): String = base.joinToString(separator = "")
    }

}
