package kryszt.aoc2018

import kryszt.tools.IoUtil

object Main5 {

    @JvmStatic
    fun main(args: Array<String>) {
        val values = IoUtil.readTaskFile(this)
        println(values.length)

//    val reduced = reduce("dabAcCaCBAcCcaDA")
//    val reduced = reduce(values)
//    println("${reduced.length} -> $reduced")
//    val allReduced = reduceAll("dabAcCaCBAcCcaDA")
        val allReduced = reduceAll(values)
        val min = allReduced.values.minByOrNull { v -> v.length }?.length
        println(min)
        println(allReduced)
    }

    private fun reduceAll(line: String): Map<Char, String> {
        val results = mutableMapOf<Char, String>()
        for (c in 'a'..'z') {
            val cleared = line.replace(c.toString(), "", ignoreCase = true)
            val reduced = reduce(cleared)
            results[c] = reduced
        }
        return results
    }

    private fun reduce(line: String): String {
        val replacements = buildReplacements()
        var changedInLoop: Boolean
        var latestVersion = line
        do {
            changedInLoop = false
            replacements.forEach { unit ->
                val changed = latestVersion.replace(unit, "")
                if (changed.length != latestVersion.length) {
                    latestVersion = changed
                    changedInLoop = true
                }
            }

        } while (changedInLoop)
        return latestVersion
    }

    private fun buildReplacements(): List<String> {
        val removing = mutableListOf<String>()
        for (c in 'a'..'z') {
            removing.add("$c${c.toUpperCase()}")
            removing.add("${c.toUpperCase()}$c")
        }
        return removing
    }
}
