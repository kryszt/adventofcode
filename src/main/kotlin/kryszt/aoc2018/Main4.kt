package kryszt.aoc2018

import kryszt.tools.IoUtil

object Main4 {

    val testLines = listOf(
            "[1518-11-01 00:00] Guard #10 begins shift",
            "[1518-11-01 00:05] falls asleep",
            "[1518-11-01 00:25] wakes up",
            "[1518-11-01 00:30] falls asleep",
            "[1518-11-01 00:55] wakes up",
            "[1518-11-01 23:58] Guard #99 begins shift",
            "[1518-11-02 00:40] falls asleep",
            "[1518-11-02 00:50] wakes up",
            "[1518-11-03 00:05] Guard #10 begins shift",
            "[1518-11-03 00:24] falls asleep",
            "[1518-11-03 00:29] wakes up",
            "[1518-11-04 00:02] Guard #99 begins shift",
            "[1518-11-04 00:36] falls asleep",
            "[1518-11-04 00:46] wakes up",
            "[1518-11-05 00:03] Guard #99 begins shift",
            "[1518-11-05 00:45] falls asleep",
            "[1518-11-05 00:55] wakes up")

    @JvmStatic
    fun main(args: Array<String>) {
        val lines = IoUtil.readTaskFileLines(this).sorted()

        println(solve2(collect(lines)))
    }

    fun collect(lines: List<String>): Map<Int, IntArray> {
        val collector = TimeCollector()

        lines.forEach { collectLine(it, collector) }

        collector.times.forEach { (key, value) ->
            println("$key : sleeps = ${value.totalSleep()}, most = ${value.sleepMax()} -> ${value.joinToString(separator = ",")}")
        }
        return collector.times
    }

    fun solve1(times: Map<Int, IntArray>): Int {
        val sleepsMost = times.maxByOrNull { (_, value) -> value.totalSleep() }
        println(sleepsMost)
        val idOfMax = sleepsMost?.key ?: 0
        val maxSleep = sleepsMost?.value?.maxOrNull() ?: 0
        val minuteOfMax = sleepsMost?.value?.indexOf(maxSleep) ?: 0

        println("max sleep: $maxSleep")
        println("minute of max: $minuteOfMax")

        return idOfMax * minuteOfMax
    }

    fun solve2(times: Map<Int, IntArray>): Int {
        val sleepsMax = times.maxByOrNull { (_, value) -> value.sleepMax() }
        println(sleepsMax)
        val idOfMax = sleepsMax?.key ?: 0
        val maxSleep = sleepsMax?.value?.maxOrNull() ?: 0
        val minuteOfMax = sleepsMax?.value?.indexOf(maxSleep) ?: 0

        println("max sleep: $maxSleep")
        println("minute of max: $minuteOfMax")

        return idOfMax * minuteOfMax
    }

    fun collectLine(line: String, collector: TimeCollector) {
        println(line)
        when {
            line.contains("falls asleep") -> collector.fallsAsleep(getMinutes(line))
            line.contains("wakes up") -> collector.awakes(getMinutes(line), line)
            line.contains("begins shift") -> collector.newGuard(getGuardId(line))
        }
    }

    fun getMinutes(line: String): Int {
        val start = line.indexOf(':') + 1
        val end = line.indexOf(']', start)
        return line.substring(start, end).toInt()
    }

    fun getGuardId(line: String): Int {
        val start = line.indexOf('#') + 1
        val end = line.indexOf(' ', start)
        return line.substring(start, end).toInt()
    }

    class TimeCollector {
        val times = mutableMapOf<Int, IntArray>()

        var lastGuard = 0
        var lastFallAsleep = 0

        fun newGuard(id: Int) {
            lastGuard = id
        }

        fun fallsAsleep(minute: Int) {
            lastFallAsleep = minute
        }

        fun awakes(minute: Int, line: String) {
            if (lastGuard < 0 || lastFallAsleep < 0) {
                throw IllegalStateException(line)
            }
            addSleep(lastGuard, lastFallAsleep, minute, times)
            lastFallAsleep = -1
        }

        private fun addSleep(guardId: Int, from: Int, to: Int, guardMinutes: MutableMap<Int, IntArray>) {
            val minutes = guardMinutes.getOrPut(guardId) { IntArray(60) }
            (from until to).forEach { minute -> minutes[minute]++ }
        }

    }

    fun IntArray.totalSleep() = sum()
    fun IntArray.sleepMax() = maxOrNull() ?: 0

}
