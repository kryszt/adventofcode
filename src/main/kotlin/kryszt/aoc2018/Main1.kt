package kryszt.aoc2018

import kryszt.tools.IoUtil

object Main1 {

    @JvmStatic
    fun main(args: Array<String>) {
        val values = IoUtil.readTaskFileLines(this).map { it.toInt() }.toIntArray()
        println("sum ${values.sum()}")
        println(repeatUntilSame(values))
    }

    private fun repeatUntilSame(values: IntArray): Int {
        val seenValues = mutableSetOf<Int>().apply {
            add(0)
        }
        var lastValue = 0
        while (true) {
            values.forEach {
                lastValue += it
                if (!seenValues.add(lastValue)) {
                    return lastValue
                }
            }
        }
    }
}
