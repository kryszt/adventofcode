package kryszt.aoc2018

import kryszt.tools.IoUtil
import java.lang.IllegalArgumentException

object Main16 {

    @JvmStatic
    fun main(args: Array<String>) {
//        val possibilities = countPossibilities(Step(listOf(3, 2, 1, 1), listOf(9, 2, 1, 2), listOf(3, 2, 2, 1)))
//        println("${possibilities.size} -> $possibilities")
        solution2()
    }

    fun solution1() {
        val collector = RulesBuilder()
        IoUtil.forEachLine(this, "-1") {
            collector.nextLine(it)
        }
        val result = collector.collector.count {
            countPossibilities(it).size >= 3
        }
        println(collector.collector.size)
        println(result)
    }

    fun solution2() {
        val codes = findCodes()
        val requests = loadRequests()
        println(requests)
        executeAll(requests, codes)
    }

    private fun executeAll(requests: List<List<Int>>, codes: Map<Int, String>) {
        var registers = listOf(0, 0, 0, 0)
        requests.forEach { request ->
            val nextRegisters = execute(registers, codes[request[0]]!!, request.subList(1, 4))
            registers = nextRegisters
        }
        println(registers)
    }

    private fun findCodes(): Map<Int, String> {
        val collector = RulesBuilder()
        IoUtil.forEachLine(this, "-1") {
            collector.nextLine(it)
        }
        val codeCollection = RuleCollector()

        collector.collector.forEach {
            codeCollection.options(it.orderCode, countPossibilities(it))
        }
        val actionCodes = codeCollection.reduceOptions()
        println(actionCodes)
        return actionCodes
    }

    private fun loadRequests(): List<List<Int>> {
        return IoUtil.readTaskFileLines(this, "-2").map { line ->
            line.split(" ").map { it.toInt() }
        }
    }

    class RuleCollector {
        private val codeOptions = mutableMapOf<Int, Set<String>>()

        fun options(code: Int, options: List<String>) {
            val newSet = options.toSet()
            val lastValues = codeOptions.getOrDefault(code, options.toSet())
            val common = lastValues.filter { newSet.contains(it) }.toSet()
            codeOptions[code] = common
        }

        fun reduceOptions(): Map<Int, String> {
            val singleOptions = mutableMapOf<Int, String>()

            val copy: MutableMap<Int, MutableSet<String>> = codeOptions.mapValues { it.value.toMutableSet() }.toMutableMap()

            while (copy.isNotEmpty()) {
                val sureOption = copy.entries.find { it.value.size == 1 }
                sureOption?.let { single ->
                    copy.remove(single.key)
                    singleOptions[single.key] = single.value.first()
                }
                sureOption?.let { single ->
                    copy.entries.forEach {
                        if (it.value.isNotEmpty()) {
                            it.value.remove(single.value.first())
                        }
                    }
                }
            }
            return singleOptions.toSortedMap()
        }
    }

    class RulesBuilder {

        val collector = mutableListOf<Step>()

        private var before: List<Int>? = null
        private var orders: List<Int>? = null
        private var after: List<Int>? = null

        fun nextLine(line: String) {
            if (line.startsWith("Before")) {
                before = line.substring(line.indexOf("[") + 1, line.indexOf("]")).split(", ").map { it.toInt() }
            } else if (line.startsWith("After")) {
                after = line.substring(line.indexOf("[") + 1, line.indexOf("]")).split(", ").map { it.toInt() }
                collector.add(Step(before!!, orders!!, after!!))
            } else if (!line.isBlank()) {
                orders = line.split(" ").map { it.toInt() }
            }
        }
    }

    private fun execute(initial: List<Int>, action: String, values: List<Int>): List<Int> {
        val target = values[2]
        val result: Int = when (action) {
            "addr" -> initial[values[0]] + initial[values[1]]
            "addi" -> initial[values[0]] + values[1]
            //
            "mulr" -> initial[values[0]] * initial[values[1]]
            "muli" -> initial[values[0]] * values[1]
            //
            "banr" -> initial[values[0]] and initial[values[1]]
            "bani" -> initial[values[0]] and values[1]
            //
            "borr" -> initial[values[0]] or initial[values[1]]
            "bori" -> initial[values[0]] or values[1]
            //
            "setr" -> initial[values[0]]
            "seti" -> values[0]
            //
            "gtir" -> if (values[0] > initial[values[1]]) 1 else 0
            "gtri" -> if (initial[values[0]] > values[1]) 1 else 0
            "gtrr" -> if (initial[values[0]] > initial[values[1]]) 1 else 0
            //
            "eqir" -> if (values[0] == initial[values[1]]) 1 else 0
            "eqri" -> if (initial[values[0]] == values[1]) 1 else 0
            "eqrr" -> if (initial[values[0]] == initial[values[1]]) 1 else 0
            //
            else -> throw IllegalArgumentException("Unknown action: $action")
        }
        return initial.toMutableList().apply { set(target, result) }
    }

    private fun countPossibilities(step: Step): List<String> {
        return possibilities.filter { action ->
            val result = execute(step.before, action, step.orders.subList(1, 4))
            result == step.after
        }
    }

    private val possibilities = listOf("addr",
            "addi",
            "mulr",
            "muli",
            "banr",
            "bani",
            "borr",
            "bori",
            "setr",
            "seti",
            "gtir",
            "gtri",
            "gtrr",
            "eqir",
            "eqri",
            "eqrr")

    data class Step(val before: List<Int>, val orders: List<Int>, val after: List<Int>) {
        val orderCode: Int = orders.first()
    }

}
