package kryszt.aoc2018

import kryszt.tools.IoUtil
import kryszt.tools.Point4D

object Main25 {

    private var Verbose: Boolean = false

    @JvmStatic
    fun main(args: Array<String>) {
//        println(countConstellationsInLines(example1))
//        println(countConstellationsInLines(example2))
//        println(countConstellationsInLines(example3))
//        println(countConstellationsInLines(example4))
        println(countConstellationsInLines(IoUtil.readTaskFileLines(this)))

    }

    private fun parseStar(line: String): Point4D {
        return line.split(",").map { it.trim() }.let {
            Point4D(it[0].toInt(), it[1].toInt(), it[2].toInt(), it[3].toInt())
        }
    }

    private fun countConstellationsInLines(stars: List<String>): Int {
        return countConstellations(stars.map { parseStar(it) })
    }

    private fun countConstellations(stars: List<Point4D>): Int {
        val constellations = mutableListOf<Constellation>()
        val mutableStart = stars.toMutableList()

        while (mutableStart.isNotEmpty()) {
            if (Verbose) {
                println("creating constellation having ${mutableStart.size} left")
            }
            val newConstellation = Constellation(mutableStart.removeAt(0))
            constellations.add(newConstellation)

            val addedToConstellation = mutableListOf<Point4D>()
            do {
                addedToConstellation.clear()

                mutableStart.forEach {
                    if (newConstellation.offerStar(it)) {
                        addedToConstellation.add(it)
                    }
                }
                if (Verbose) {
                    println("added ${addedToConstellation.size} in iteration")
                }
                mutableStart.removeAll(addedToConstellation)
            } while (addedToConstellation.isNotEmpty())
        }
        return constellations.size
    }

    class Constellation(firstStar: Point4D) {
        private val stars = mutableListOf<Point4D>().apply {
            add(firstStar)
        }

        fun offerStar(star: Point4D): Boolean {
            if (stars.any { it.distance(star) <= Distance }) {
                stars.add(star)
                return true
            }
            return false
        }
    }

    const val Distance = 3


    val example1 = listOf(
            "0,0,0,0",
            "3,0,0,0",
            "0,3,0,0",
            "0,0,3,0",
            "0,0,0,3",
            "0,0,0,6",
            "9,0,0,0",
            "12,0,0,0"
    )
    val example2 = listOf(
            "-1,2,2,0",
            "0,0,2,-2",
            "0,0,0,-2",
            "-1,2,0,0",
            "-2,-2,-2,2",
            "3,0,2,-1",
            "-1,3,2,2",
            "-1,0,-1,0",
            "0,2,1,-2",
            "3,0,0,0"
    )
    val example3 = listOf(
            "1,-1,0,1",
            "2,0,-1,0",
            "3,2,-1,0",
            "0,0,3,1",
            "0,0,-1,-1",
            "2,3,-2,0",
            "-2,2,0,0",
            "2,-2,0,-1",
            "1,-1,0,-1",
            "3,2,0,2"
    )
    val example4 = listOf(
            "1,-1,-1,-2",
            "-2,-2,0,1",
            "0,2,1,3",
            "-2,3,-2,1",
            "0,2,3,-2",
            "-1,-1,1,-2",
            "0,-2,-1,0",
            "-2,2,3,-1",
            "1,2,2,0",
            "-1,-2,0,-2"
    )

}
