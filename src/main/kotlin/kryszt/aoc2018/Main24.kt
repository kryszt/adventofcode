package kryszt.aoc2018

import java.lang.IllegalArgumentException

object Main24 {


    @JvmStatic
    fun main(args: Array<String>) {
        solution2()
    }

    fun solution1() {
        val battlefield = BattleField(Defenders, Invaders)
        while (battlefield.selectSkirmishes()) {
            battlefield.executeSkirmishes()
        }

        battlefield.printActiveGroups()
        println("Result: ${battlefield.resultScore()}")
    }

    private fun solution2() {
        val requiredBoost = binarySearchSolution()
//        println()
        println("${systemResult(requiredBoost)} for boost=$requiredBoost")
    }

    private fun binarySearchSolution(): Int {
        var valueToCheck = 1

        var largestFalse = 0
        var smallestTrue = -1

        while (largestFalse + 1 != smallestTrue) {
            val current = valueToCheck
            val valueAtCurrent = systemResult(current)
            val isTrueAtCurrent = valueAtCurrent > 0
            println("Value at $current is $valueAtCurrent")

            if (isTrueAtCurrent) {
                smallestTrue = current
                valueToCheck = (largestFalse + smallestTrue) / 2
            } else {
                largestFalse = current
                valueToCheck = if (smallestTrue < 0) {
                    current * 2
                } else {
                    (largestFalse + smallestTrue) / 2
                }
            }
        }
        return smallestTrue
    }

    private fun systemResult(boost: Int): Int {
        val battlefield = BattleField(Defenders, Invaders, boost)
        while (battlefield.selectSkirmishes()) {
            if (!battlefield.executeSkirmishes()) {
                return -666
            }
        }
        return battlefield.resultScore()
    }

    class BattleField(initialImmune: List<BattleGroup>, initialInfections: List<BattleGroup>, boost: Int = 0, private val verbose: Boolean = false) {
        private var skirmishes: List<Skirmish> = emptyList()
        private val immuneSystem: List<BattleGroup> = initialImmune.map { it.copy(damage = it.damage + boost) }
        private val infections: List<BattleGroup> = initialInfections.map { it.copy() }

        init {
            immuneSystem.forEach { validateGroup(it) }
            infections.forEach { validateGroup(it) }
        }

        private fun canFight() = skirmishes.isNotEmpty()

        fun selectSkirmishes(): Boolean {
            val activeImmune = immuneSystem.filter { it.units > 0 }
            val activeInfection = infections.filter { it.units > 0 }
            if (verbose) {
                printGroups(activeImmune, activeInfection)
            }

            val skirmishHolder = mutableListOf<Skirmish>()
            selectSkirmishes(activeInfection, activeImmune, skirmishHolder)
            selectSkirmishes(activeImmune, activeInfection, skirmishHolder)

            skirmishes = skirmishHolder.sortedByDescending { it.attacker.initiative }
            return canFight()
        }

        fun executeSkirmishes(): Boolean {
            return skirmishes.map { execute(it) }.any { it }
        }

        private fun execute(skirmish: Skirmish): Boolean {
            val damage = calculateDamage(skirmish.attacker, skirmish.target)
            val kills = Math.min(skirmish.target.units, damage / skirmish.target.unitHp)
            skirmish.target.units -= kills
            if (verbose) {
                println("${skirmish.attacker.side} Group ${skirmish.attacker.id} attacks group ${skirmish.target.id} killing $kills")
            }
            return kills > 0
        }

        private fun selectSkirmishes(attackers: List<BattleGroup>, allTargets: List<BattleGroup>, holder: MutableList<Skirmish>) {
            val selectedTargets = mutableSetOf<BattleGroup>()
            attackers
                    .sortedByDescending { it.initiative }
                    .sortedByDescending { it.effectivePower }
                    .forEach { selector ->
                        val targets = allTargets.filter { !selectedTargets.contains(it) }.map { target -> Pair(target, calculateDamage(selector, target)) }
                        if (verbose) {
                            targets.forEach {
                                println("${selector.side} ${selector.id} would deal ${it.second} to group ${it.first.id}")
                            }
                        }
                        targets
                                .sortedByDescending { it.first.initiative }
                                .sortedByDescending { it.first.effectivePower }
                                .sortedByDescending { it.second }
                                .firstOrNull { it.second > 0 }?.also { selected ->
                                    selectedTargets.add(selected.first)
                                    holder.add(Skirmish(selector, selected.second, selected.first))
                                }
                    }
        }

        private fun calculateDamage(attacker: BattleGroup, target: BattleGroup): Int {
            return when {
                target.immuneTo.contains(attacker.attackType) -> 0
                target.weakTo.contains(attacker.attackType) -> Math.max(0, attacker.damage * attacker.units * 2)
                else -> Math.max(0, attacker.damage * attacker.units)
            }
        }

        fun printActiveGroups() {
            val activeImmune = immuneSystem.filter { it.units > 0 }
            val activeInfection = infections.filter { it.units > 0 }
            printGroups(activeImmune, activeInfection)
            println("Total immune: ${activeImmune.sumBy { it.units }}")
            println("Total infection: ${activeInfection.sumBy { it.units }}")
        }

        private fun printGroups(immuneSystem: List<BattleGroup>, infections: List<BattleGroup>) {
            println("Immune:")
            immuneSystem.forEach { println("Group ${it.id} contains ${it.units}") }
            println("Infection:")
            infections.forEach { println("Group ${it.id} contains ${it.units}") }
            println()
        }

        private fun systemWins(): Boolean {
            return immuneSystem.any { it.units > 0 } && infections.all { it.units <= 0 }
        }

        fun resultScore(): Int {
            return if (systemWins()) {
                immuneSystem.filter { it.units > 0 }.sumBy { it.units }
            } else {
                -infections.filter { it.units > 0 }.sumBy { it.units }
            }
        }
    }

    val TestDefenders = listOf(
            BattleGroup("Immune", 1, 17, 5390, 4507, "fire", 2, weakTo = setOf("radiation", "bludgeoning")),
            BattleGroup("Immune", 2, 989, 1274, 25, "slashing", 3, weakTo = setOf("slashing", "bludgeoning"), immuneTo = setOf("fire"))
    )
    val TestInvaders = listOf(
            BattleGroup("Infection", 1, 801, 4706, 116, "bludgeoning", 1, weakTo = setOf("radiation")),
            BattleGroup("Infection", 2, 4485, 2961, 12, "slashing", 4, weakTo = setOf("fire", "cold"), immuneTo = setOf("radiation"))
    )

    private val Defenders = listOf(
            BattleGroup("Immune", 1, 933, 3691, 37, "cold", 15),
            BattleGroup("Immune", 2, 262, 2029, 77, "cold", 4),
            BattleGroup("Immune", 3, 3108, 2902, 7, "cold", 13, weakTo = setOf("bludgeoning"), immuneTo = setOf("slashing", "fire")),
            BattleGroup("Immune", 4, 5158, 9372, 17, "radiation", 16, weakTo = setOf("bludgeoning"), immuneTo = setOf("cold")),
            BattleGroup("Immune", 5, 2856, 4797, 16, "cold", 20),
            BattleGroup("Immune", 6, 86, 8311, 724, "slashing", 14),
            BattleGroup("Immune", 7, 7800, 3616, 4, "bludgeoning", 7, immuneTo = setOf("radiation", "cold", "bludgeoning")),
            BattleGroup("Immune", 8, 1374, 8628, 61, "radiation", 1, weakTo = setOf("fire", "slashing")),
            BattleGroup("Immune", 9, 1661, 4723, 25, "slashing", 8),
            BattleGroup("Immune", 10, 1285, 4156, 32, "fire", 18, weakTo = setOf("bludgeoning"))
    )

    private val Invaders = listOf(
            BattleGroup("Infection", 1, 2618, 29001, 17, "radiation", 3, weakTo = setOf(), immuneTo = setOf("bludgeoning", "radiation", "cold")),
            BattleGroup("Infection", 2, 31, 20064, 1082, "bludgeoning", 10, weakTo = setOf("radiation"), immuneTo = setOf("slashing", "bludgeoning")),
            BattleGroup("Infection", 3, 281, 15311, 90, "slashing", 9, weakTo = setOf("fire", "cold"), immuneTo = setOf()),
            BattleGroup("Infection", 4, 1087, 14744, 22, "fire", 12, weakTo = setOf("fire", "cold"), immuneTo = setOf("radiation")),
            BattleGroup("Infection", 5, 7810, 48137, 10, "slashing", 5, weakTo = setOf("fire", "radiation"), immuneTo = setOf()),
            BattleGroup("Infection", 6, 232, 18762, 153, "bludgeoning", 2, weakTo = setOf("radiation", "cold"), immuneTo = setOf()),
            BattleGroup("Infection", 7, 69, 11032, 296, "slashing", 6, weakTo = setOf(), immuneTo = setOf("radiation", "slashing", "cold", "fire")),
            BattleGroup("Infection", 8, 2993, 10747, 6, "radiation", 19, weakTo = setOf("cold"), immuneTo = setOf("slashing")),
            BattleGroup("Infection", 9, 273, 7590, 49, "fire", 17, weakTo = setOf("radiation"), immuneTo = setOf("slashing", "fire")),
            BattleGroup("Infection", 10, 2041, 38432, 29, "cold", 11, weakTo = setOf("bludgeoning"), immuneTo = setOf())
    )

    fun validateGroup(battleGroup: BattleGroup) {
        validateType(battleGroup.attackType)
        battleGroup.weakTo.forEach { validateType(it) }
        battleGroup.immuneTo.forEach { validateType(it) }
    }

    private fun validateType(damageType: String) {
        if (!validTypes.contains(damageType)) {
            throw IllegalArgumentException("Type: $damageType")
        }
    }

    private val validTypes = setOf("fire", "cold", "radiation", "slashing", "bludgeoning")

    data class BattleGroup(val side: String, val id: Int,
                           var units: Int, val unitHp: Int, val damage: Int, val attackType: String,
                           val initiative: Int, val weakTo: Set<String> = emptySet(), val immuneTo: Set<String> = emptySet()) {
        val effectivePower: Int
            get() = units * damage
    }

    data class Skirmish(val attacker: BattleGroup, val damage: Int, val target: BattleGroup)
}
