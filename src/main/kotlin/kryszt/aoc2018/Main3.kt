package kryszt.aoc2018

import kryszt.tools.IoUtil
import kryszt.tools.Rectangle

object Main3 {

    val ClaimPattern = "#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {
        part2()
    }

    private fun part1() {
//        val collector = Collector(8, 8)
        val collector = Collector(1000, 1000)
//        IoUtil.forEachLine(this, "-test") {
        IoUtil.forEachLine(this) {
            val claim = splitLine(it)
            println(claim)
            collector.claimArea(claim)
        }

        println(collector.count { it > 1 })
//        collector.print()
    }

    private fun part2() {
//        val collector = OverlappingCollector(8, 8)
        val collector = OverlappingCollector(1000, 1000)
//        IoUtil.forEachLine(this, "-test") {
        IoUtil.forEachLine(this) {
            val claim = splitLine(it)
            println(claim)
            collector.claimArea(claim)
        }

        collector.print()
        println(collector.notOverlappingClaims)
    }

    private fun splitLine(line: String): Claim {
        val m = ClaimPattern.matcher(line)
        if (m.find()) {
            return Claim(elf = m.group(1).toInt(),
                    area = Rectangle(
                            x = m.group(2).toInt(), y = m.group(3).toInt(),
                            width = m.group(4).toInt(), height = m.group(5).toInt()))
        } else {
            throw IllegalArgumentException(line)
        }
    }

    data class Claim(val elf: Int, val area: Rectangle)

    class Collector(private val width: Int, private val height: Int) {
        private val area = Array(width) { IntArray(height) }

        fun print() {
            (0 until height).forEach { y ->
                (0 until width).forEach { x ->
                    print(area[x][y])
                }
                println()
            }
        }

        fun claimArea(claim: Claim) {
            (0 until claim.area.width).forEach { x ->
                (0 until claim.area.height).forEach { y ->
                    area[x + claim.area.x][y + claim.area.y]++
                }
            }
        }

        fun count(predicate: (Int) -> Boolean): Int {
            return area.sumBy { it.count(predicate) }
        }
    }

    class OverlappingCollector(private val width: Int, private val height: Int) {
        private val area = Array(width) { IntArray(height) }
        val notOverlappingClaims = mutableSetOf<Int>()

        fun print() {
            (0 until height).forEach { y ->
                (0 until width).forEach { x ->
                    print(area[x][y])
                }
                println()
            }
        }

        fun claimArea(claim: Claim) {
            var overlapped = false
            (0 until claim.area.width).forEach { x ->
                (0 until claim.area.height).forEach { y ->
                    val oldClaim = area[x + claim.area.x][y + claim.area.y]
                    area[x + claim.area.x][y + claim.area.y] = claim.elf
                    if (oldClaim > 0) {
                        notOverlappingClaims.remove(oldClaim)
                        overlapped = true
                    }
                }
            }
            if (!overlapped) {
                notOverlappingClaims.add(claim.elf)
            }
        }

    }
}
