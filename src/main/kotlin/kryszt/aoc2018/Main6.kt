package kryszt.aoc2018

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.Rectangle
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.abs

object Main6 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solution1()
    }

    override fun taskTwo() {
        solution2()
    }

    private fun solution2() {
        val values = readTaskLines()
        val positions = values.mapIndexed { index, s ->
            Position(index, lineToPoint(s))
        }
        val area = buildArea(positions)
        println("area $area")
        val resultArea = markTotalDistance(area, positions)
        resultArea.print()

        println(resultArea.countFields { it < 10000 })
    }

    private fun solution1() {
        val values = IoUtil.readTaskFileLines(this)
        val positions = values.mapIndexed { index, s ->
            Position(index, lineToPoint(s))
        }
        val area = buildArea(positions)
        println("area $area")
        val resultArea = markClosest(area, positions)
        resultArea.print()
        val counts = countValues(resultArea)
        val infinite = findInfinite(resultArea)
        println(counts)
        println(infinite)
        println(biggestFinite(counts, infinite))
    }

    private fun biggestFinite(valuesCount: Map<Int, Int>, infinite: Set<Int>): Pair<Int, Int> {
        val filtered = valuesCount.filter { it.key >= 0 && !infinite.contains(it.key) }
        println(filtered)
        return filtered.maxByOrNull { it.value }!!.toPair()
    }

    private fun buildArea(points: List<Position>): Rectangle {
        val left = points.minByOrNull { it.point.x }!!.point.x
        val right = points.maxByOrNull { it.point.x }!!.point.x
        val top = points.minByOrNull { it.point.y }!!.point.y
        val bottom = points.maxByOrNull { it.point.y }!!.point.y
        return Rectangle(left, top, 1 + right - left, 1 + bottom - top)
    }

    private fun lineToPoint(line: String): Point {
        val split = line.split(",")
        return Point(
            split.first().trim().toInt(),
            split.last().trim().toInt()
        )
    }

    private fun markClosest(map: Rectangle, positions: List<Position>): Area {
        val area = Area(map.width, map.height, Point(map.x, map.y))

        for (x in map.left..map.right) {
            for (y in map.top..map.bottom) {
                val currentPoint = Point(x, y)
                val closest = findClosest(currentPoint, positions)
                closest?.let { markClosest(area, currentPoint, it) } ?: run { markConflict(area, currentPoint) }
            }
        }
        return area
    }

    private fun markTotalDistance(map: Rectangle, positions: List<Position>): Area {
        val area = Area(map.width, map.height, Point(map.x, map.y))

        for (x in map.left..map.right) {
            for (y in map.top..map.bottom) {
                val currentPoint = Point(x, y)
                area.setAt(x, y, findTotalDistance(currentPoint, positions))
            }
        }
        return area
    }

    private fun markClosest(area: Area, point: Point, position: Position) {
        area.setAt(point.x, point.y, position.id)
    }

    private fun markConflict(area: Area, point: Point) {
        area.setAt(point.x, point.y, -1)
    }

    private fun findClosest(point: Point, positions: List<Position>): Position? {
        val distances = positions
            .map { p -> p to p.point.distance(point) }
        val minimumItem = distances.minByOrNull { it.second }!!
        val minimum = minimumItem.second
        val numberOf = distances.count { it.second == minimum }
        return minimumItem.first.takeIf { numberOf == 1 }
    }

    private fun findTotalDistance(point: Point, positions: List<Position>): Int {
        return positions.sumOf { p -> p.point.distance(point) }
    }

    private fun findInfinite(area: Area): Set<Int> {
        val infinite = mutableSetOf<Int>()
        val bottom = area.height - 1
        val right = area.width - 1
        for (x in 0 until area.width) {
            infinite.add(area.valueNoOffset(x, 0))
            infinite.add(area.valueNoOffset(x, bottom))
        }
        for (y in 0 until area.height) {
            infinite.add(area.valueNoOffset(0, y))
            infinite.add(area.valueNoOffset(right, y))
        }
        return infinite
    }

    private fun countValues(area: Area): Map<Int, Int> {
        val values = mutableMapOf<Int, AtomicInteger>()
        for (x in 0 until area.width) {
            for (y in 0 until area.height) {
                val value = area.valueNoOffset(x, y)
                values.getOrPut(value) { AtomicInteger() }.incrementAndGet()
            }
        }
        return values.mapValues { it.value.get() }
    }

    class Area(val width: Int, val height: Int, private val offset: Point = Point()) {
        private val area = Array(width) { IntArray(height) }

        fun print() {
            (0 until height).forEach { y ->
                (0 until width).forEach { x ->
                    print("${area[x][y]}\t")
                }
                println()
            }
        }

        fun valueNoOffset(x: Int, y: Int): Int = area[x][y]

        fun setAt(x: Int, y: Int, value: Int) {
            val areaX = x - offset.x
            val areaY = y - offset.y
            area[areaX][areaY] = value
        }

        fun countFields(condition: (Int) -> Boolean): Int {
            return area.sumOf { it.count(condition) }
        }

    }

    data class Position(val id: Int, val point: Point)

    private fun Point.distance(other: Point): Int = abs(other.x - x) + abs(other.y - y)
}
