package kryszt.aoc2018

import kryszt.tools.IoUtil
import java.util.*
import java.util.concurrent.LinkedBlockingQueue

object Main8 {

    val testValues = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"
    @JvmStatic
    fun main(args: Array<String>) {
//        val line = testValues
        val line = IoUtil.readTaskFile(this)
        val values = fromLine(line)
        val topNode = readNode(values)
//        println(topNode)
//        println(sumMeta(topNode))
        println(nodeValue(topNode))
    }

    private fun fromLine(line: String): Queue<Int> {
        return line.split(" ").map { it.toInt() }.let { LinkedBlockingQueue(it) }
    }

    private fun readNode(input: Queue<Int>): Node {
        val childCount = input.poll()
        val metaCount = input.poll()
        val children = (0 until childCount).map { readNode(input) }
        val meta = (0 until metaCount).map { input.poll() }
        return Node(meta, children)
    }

    private fun sumMeta(node: Node): Int {
        return node.metas.sum() + node.children.sumBy { sumMeta(it) }
    }

    private fun nodeValue(node: Node): Int {
        println("Node value for: $node")

        val value = if (node.children.isEmpty()) {
            println("No children, ${node.metas.sum()} for $node")
            node.metas.sum()
        } else {
            println("has ${node.children.size} children for $node")
            node.metas.sumBy { meta -> node.children.getOrNull(meta - 1)?.let(::nodeValue) ?: 0 }
        }
        println("Value: $value for $node")

        return value
    }

    data class Node(val metas: List<Int>, val children: List<Node>)

}
