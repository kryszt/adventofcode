package kryszt.aoc2018

object Main14 {

    const val steps = 540561

    @JvmStatic
    fun main(args: Array<String>) {
        solution2()
    }

    fun solution1() {
        val generator = RecipesGenerator(listOf(3, 7))
        generator.print()
        generator.generate(steps + 10)
        generator.print()
        println(generator.valueAt(steps))
    }

    fun solution2() {
        val generator = RecipesGenerator(listOf(3, 7))
        val foundAt = findSequence(generator, "540561")
        println("found at: $foundAt")
    }

    private fun findSequence(generator: RecipesGenerator, sequence: String): Int {
        val len = sequence.length
        var lastChecked = 0
        while (true) {
            generator.generate(10000)
            val startAt = lastChecked
            println("Starting at $startAt")
            for (i in startAt until generator.count - len) {
                if (sequence == generator.valueOfLength(i, len)) {
                    return i
                }
                lastChecked = i
            }
        }
    }

    class RecipesGenerator(initial: List<Int>) {

        private val recipes = mutableListOf<Int>().apply { addAll(initial) }
        val count: Int
            get() = recipes.size

        private var elfOne = 0
        private var elfTwo = 1

        fun generate(steps: Int) {
            for (i in 0 until steps) {
                generateNext()
//                print()
            }
        }

        private fun generateNext() {
            val nextScore = recipes[elfOne] + recipes[elfTwo]
            addScore(nextScore)
            elfOne = (elfOne + recipes[elfOne] + 1) % count
            elfTwo = (elfTwo + recipes[elfTwo] + 1) % count
        }

        private fun addScore(score: Int) {
            if (score > 9) {
                recipes.add(1)
            }
            recipes.add(score % 10)
        }

        fun valueAt(first: Int): String {
            return recipes.subList(first, first + 10).joinToString(separator = "")
        }

        fun valueOfLength(first: Int, length: Int): String {
            return recipes.subList(first, first + length).joinToString(separator = "")
        }

        fun print() {
            println("$elfOne,$elfTwo -> ${recipes.joinToString(separator = " ")}")
        }
    }
}
