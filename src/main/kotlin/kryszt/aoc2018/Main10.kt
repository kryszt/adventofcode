package kryszt.aoc2018

import kryszt.tools.IoUtil
import kryszt.tools.Point

object Main10 {


    @JvmStatic
    fun main(args: Array<String>) {
//        val stars = IoUtil.readTaskFileLines(this, "-test").map(this::parseLine)
        val stars = IoUtil.readTaskFileLines(this).map(this::parseLine)

        printStars(stars.map { it.positionAt(10641) })


//        for (i in 0 until 100000) {
//            val positions = stars.map { it.positionAt(i) }
//            if (i % 10000 == 0) {
//                println("Checking $i")
//            }
//            if (canHaveText(positions)) {
//                println("Possible text at $i")
//            }
//            println("can be at $i: ${canHaveText(positions)}")
//        }
    }

    private fun canHaveText(points: List<Point>): Boolean {
        val top = points.maxByOrNull { it.y }!!.y
        val bottom = points.minByOrNull { it.y }!!.y

        return Math.abs(top - bottom) < 10
    }

    private fun printStars(points: List<Point>) {
        println("Printing $points")


        val pointsSet = points.toSet()

        val bottom = points.maxByOrNull { it.y }!!.y
        val top = points.minByOrNull { it.y }!!.y

        val left = points.minByOrNull { it.x }!!.x
        val right = points.maxByOrNull { it.x }!!.x

        println("Printing t=$top, b=$bottom, l=$left, r=$right")
        println()
        for (y in top..bottom) {
            for (x in left..right) {
                val char = if (pointsSet.contains(Point(x, y))) "#" else " "
                print(char)
            }
            println()
        }
        println()
    }

    private fun parseLine(line: String): Star {
        val start = line.substring(line.indexOf("<") + 1, line.indexOf(">")).split(",").let {
            Point(it.first().trim().toInt(), it.last().trim().toInt())
        }
        val velocity = line.substring(line.lastIndexOf("<") + 1, line.lastIndexOf(">")).split(",").let {
            Point(it.first().trim().toInt(), it.last().trim().toInt())
        }
        return Star(start, velocity)
    }

    data class Star(val startPoint: Point, val velocity: Point)

    fun Star.positionAt(time: Int): Point = startPoint + (velocity * time)
}
