package kryszt.aoc2018

import kryszt.tools.Point

object Main22 {

    private val testConfig = Config(510, Point(10, 10))
    //    val testConfig = Config(510, 15, 15)
    private val mainConfig = Config(8112, Point(13, 743))

    @JvmStatic
    fun main(args: Array<String>) {
        solution2()
    }

    private fun solution1() {
        val map = MapHelper(mainConfig)
        println(map.totalRisk())
        map.printMap(15, 15)
    }

    private fun solution2() {
        val config = mainConfig
        val map = MapHelper(config)
        val solver = PathFinder(map, Point(0, 0), Gear.Torch, config.target)
        val bestValue = solver.findPathValue()
        println("Basic = $bestValue")
        println("Checked = ${solver.count}")
        println()
        println("Best = $bestValue")
    }

    class MapHelper(private val config: Config) {

        private val geoIndexes = mutableMapOf<Point, Long>()
        private val types = mutableMapOf<Point, Int>()

        private fun geoIndexAt(x: Int, y: Int): Long {
            return if ((x == 0 && y == 0) || (x == config.target.x && y == config.target.y)) {
                0
            } else if (y == 0) {
                x * 16807L
            } else if (x == 0) {
                y * 48271L
            } else {
                geoIndexes.getOrPut(Point(x, y)) {
                    val left = erosionLevelAt(x - 1, y)
                    val right = erosionLevelAt(x, y - 1)
                    left * right
                }
            }
        }

        private fun erosionLevelAt(x: Int, y: Int): Long {
            return (geoIndexAt(x, y) + config.depth) % 20183
        }

        private fun typeAt(x: Int, y: Int): Int {
            return types.getOrPut(Point(x, y)) { (erosionLevelAt(x, y) % 3).toInt() }
        }

        fun typeAt(point: Point): Int {
            return types.getOrPut(point) { (erosionLevelAt(point.x, point.y) % 3).toInt() }
        }

        fun totalRisk(): Int {
            var sum = 0
            for (y in 0..config.target.y) {
                for (x in 0..config.target.x) {
                    sum += typeAt(x, y)
                }
            }
            return sum
        }

        fun printMap(maxX: Int = config.target.x, maxY: Int = config.target.y) {
            for (y in 0..maxY) {
                for (x in 0..maxX) {
                    when (typeAt(x, y)) {
                        TypeRocky -> print('.')
                        TypeWet -> print('=')
                        TypeNarrow -> print('|')
                    }
                }
                println()
            }
        }
    }

    class PathFinder(private val caveMap: MapHelper, private val startPlace: Point, private val startGear: Gear, private val targetPlace: Point) {

        //        private var maxTime = startPlace.manhattanDistance(targetPlace) * 8 + 7
        private var maxTime = 1020
        private val bestTimes = mutableMapOf<GearPlace, Int>()
        //        val visits = mutableMapOf<Point, AtomicInteger>()
        var count = 0

        fun findPathValue(): Int {
            return findPathValue(startPlace, startGear, targetPlace, 0)
        }

        private fun findPathValue(startPlace: Point, startGear: Gear, targetPlace: Point, currentTime: Int): Int {
//            println("findPathValue $startPlace, $startGear, $targetPlace, $currentTime")
            count++

            if (currentTime >= maxTime) {
                return Int.MAX_VALUE / 2
            }
            if (currentTime + startPlace.manhattanDistance(targetPlace) >= maxTime) {
                return Int.MAX_VALUE / 2
            }

            if (startPlace == targetPlace) {
                val extraSwitch = if (startGear == Gear.Torch) 0 else SwitchTime
                val reachTime = currentTime + extraSwitch
                println("Target entered at $currentTime and reached at $reachTime on step $count with last max at $maxTime")
                if (reachTime < maxTime) {
                    maxTime = reachTime
                }
                return extraSwitch
            }
            if (!updateBestTime(startPlace, startGear, currentTime)) {
                return Int.MAX_VALUE / 2
            }

//            visits.getOrPut(startPlace) { AtomicInteger(0) }.incrementAndGet()

            var shortest = Int.MAX_VALUE / 2
            selectNextPlaces(startPlace).forEach { nextPlace ->
                val nextGear = newGearFor(startPlace, startGear, nextPlace)
                val moveTime = if (nextGear != startGear) SwitchTime + MoveTime else MoveTime
                val totalTime = moveTime + findPathValue(nextPlace, nextGear, targetPlace, currentTime + moveTime)
                if (totalTime < shortest) {
                    shortest = totalTime
                }
            }
            return shortest
        }

        private fun updateBestTime(place: Point, gear: Gear, time: Int): Boolean {
            val key = GearPlace(place, gear)
            val lastBest = bestTimes[key]
            return if (lastBest == null || lastBest > time) {
                bestTimes[key] = time
                true
            } else {
                false
            }
        }

        private fun selectNextPlaces(currentPlace: Point): List<Point> {
            return listOf(
                    currentPlace.move(Point.Left),
                    currentPlace.move(Point.Right),
                    currentPlace.move(Point.Up),
                    currentPlace.move(Point.Down)
            ).filter { it.x >= 0 && it.y >= 0 }
                    .sortedBy { it.manhattanDistance(targetPlace) }
        }

        private fun newGearFor(currentPoint: Point, currentGear: Gear, nextPoint: Point): Gear {
            return newGearFor(caveMap.typeAt(currentPoint), currentGear, caveMap.typeAt(nextPoint))
        }

        private fun newGearFor(currentType: Int, currentGear: Gear, newType: Int): Gear {
            return if (newType == currentType) {
                currentGear
            } else {
                val allowedCurrent = allowedGears[currentType].orEmpty()
                val allowedNext = allowedGears[newType].orEmpty()
                allowedCurrent.intersect(allowedNext).first()
            }
        }
    }

    data class Config(val depth: Int, val target: Point)

    const val TypeRocky: Int = 0
    const val TypeWet = 1
    const val TypeNarrow = 2
    const val SwitchTime = 7
    const val MoveTime = 1

    enum class Gear {
        Torch, Climbing, Neither
    }

    data class GearPlace(val place: Point, val gear: Gear)

    val allowedGears = mapOf(
            TypeRocky to setOf(Gear.Torch, Gear.Climbing),
            TypeWet to setOf(Gear.Climbing, Gear.Neither),
            TypeNarrow to setOf(Gear.Torch, Gear.Neither)
    )

}
