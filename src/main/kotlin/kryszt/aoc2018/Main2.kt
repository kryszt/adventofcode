package kryszt.aoc2018

import kryszt.tools.IoUtil

object Main2 {
    val testLines2 = listOf(
            "abcdef",
            "bababc",
            "abbcde",
            "abcccd",
            "aabcdd",
            "abcdee",
            "ababab"
    )

    @JvmStatic
    fun main(args: Array<String>) {
        val values = IoUtil.readTaskFileLines(this)
        findSimilar(values)?.let { println(it) }
    }

    fun countLetters(lines: List<String>): Int {

        var twos = 0
        var threes = 0

        lines.forEach {
            println(it)
            val counts = it.countChars()
            if (counts.containsValue(2)) {
                println("has twos")
                twos++
            }
            if (counts.containsValue(3)) {
                println("has threes")
                threes++
            }
        }

        println("Twos = $twos, Threes = $threes")
        return twos * threes
    }

    fun findSimilar(lines: List<String>): Pair<String, String>? {
        lines.forEach { outer ->
            lines.forEach { inner ->
                if (outer.countDifferences(inner) == 1) {
                    return Pair(outer, inner)
                }
            }
        }
        return null
    }

    private fun String.countChars(): Map<Char, Int> = this.groupBy { it }.mapValues { (_, value) -> value.size }

    private fun String.countDifferences(other: String): Int {
        var diffs = 0
        forEachIndexed { index, c ->
            if (c != other[index]) {
                diffs++
            }
        }
        return diffs
    }

}
