package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main13 : DayChallenge {

//    private val values: Pair<Int, List<Pair<Int, Int>>> = Pair(939, "7,13,x,x,59,x,31,19".asLines())
    private val values: Pair<Int, List<Pair<Int, Int>>> = readTaskLines().let { lines ->
        Pair(
                lines[0].toInt(),
                lines[1].asLines()
        )
    }

    private fun String.asLines(): List<Pair<Int, Int>> =
            split(",").mapIndexedNotNull { index, s -> s.asLineInfo(index) }

    private fun String.asLineInfo(index: Int): Pair<Int, Int>? =
            toIntOrNull()?.let { Pair(it, index) }

    @JvmStatic
    fun main(args: Array<String>) {
//        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        val time = values.first
        values.second
                .map { line -> Pair(line, (1 + time / line.first) * (line.first) - time) }
                .alsoPrint()
                .minByOrNull { it.second }
                .alsoPrint()
                ?.let { it.first.first * it.second }
                .alsoPrint()
    }

    override fun taskTwo() {
        var currentStep = values.second.first().first.toLong()
        var currentTime = 0L
        val otherLines = values.second.subList(1, values.second.size)

        otherLines.forEach { nextLine ->
            val next = nextTime(currentTime, currentStep, nextLine.first.toLong(), nextLine.second.toLong())
            currentStep = next.newStep
            currentTime = next.time
        }
        println("$currentTime")
    }

    private fun nextTime(currentTime: Long, a: Long, b: Long, expectedDiff: Long): NextRound {
        println("next for: $currentTime")
        val findDiff = expectedDiff % b
        var tmpTime = currentTime

        while (b.distanceFrom(tmpTime) != findDiff) {
            tmpTime += a
        }
        return NextRound(time = tmpTime, newStep = a * b)
    }

    private fun Long.distanceFrom(base: Long): Long =
            (this - (base % this)) % this

    private data class NextRound(val time: Long, val newStep: Long)
}
