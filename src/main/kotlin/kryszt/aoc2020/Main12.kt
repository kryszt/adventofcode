package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import java.lang.IllegalArgumentException
import kotlin.math.absoluteValue

object Main12 : DayChallenge {

        private val values = readTaskLines()
//    private val values = listOf("F10", "N3", "F7", "R90", "F11")

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        val walker = Walker(Point(), Point.Right)
        values.forEach { line ->
            walker.move(line[0], line.substring(1).toInt())
        }
        println(walker.position)
        walker.position.let { it.x.absoluteValue.plus(it.y.absoluteValue) }.also { println(it) }
    }

    override fun taskTwo() {
        val walker = WaypointWalker(Point(), Point.Up.plus(Point.Right.times(10)))
        values.forEach { line ->
            walker.move(line[0], line.substring(1).toInt())
        }
        println(walker.position)
        walker.position.let { it.x.absoluteValue.plus(it.y.absoluteValue) }.also { println(it) }
    }
}

private class Walker(startPoint: Point, startDirection: Point) {

    var position = startPoint
    private var direction = startDirection

    fun move(order: Char, steps: Int) {
        when (order) {
            'F' -> move(direction, steps)
            'N' -> move(Point.Up, steps)
            'S' -> move(Point.Down, steps)
            'E' -> move(Point.Right, steps)
            'W' -> move(Point.Left, steps)
            'L' -> turn(Point::left, steps / 90)
            'R' -> turn(Point::right, steps / 90)
            else -> throw IllegalArgumentException("Unsupported direction: $order")
        }
    }

    private fun turn(change: Point.() -> Point, steps: Int) {
        repeat(steps) {
            direction = direction.change()
        }
    }

    private fun move(moveDirection: Point, steps: Int) {
        position += moveDirection.times(steps)
    }

}

private class WaypointWalker(startPoint: Point, startWaypoint: Point) {

    var position = startPoint
    private var direction = startWaypoint

    fun move(order: Char, steps: Int) {
        when (order) {
            'F' -> position += direction.times(steps)
            'N' -> moveWaypoint(Point.Up, steps)
            'S' -> moveWaypoint(Point.Down, steps)
            'E' -> moveWaypoint(Point.Right, steps)
            'W' -> moveWaypoint(Point.Left, steps)
            'L' -> turnWaypoint(Point::left, steps / 90)
            'R' -> turnWaypoint(Point::right, steps / 90)
            else -> throw IllegalArgumentException("Unsupported direction: $order")
        }
    }

    private fun turnWaypoint(change: Point.() -> Point, steps: Int) {
        repeat(steps) {
            direction = direction.change()
        }
    }

    private fun moveWaypoint(moveDirection: Point, steps: Int) {
        direction += moveDirection.times(steps)
    }

}