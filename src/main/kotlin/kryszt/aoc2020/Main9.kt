package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.IoUtil.readTestTaskLines

object Main9 : DayChallenge {

//        private val values = Triple(readTestTaskLines().map { it.toLong() }, 5, 127L)
    private val values = Triple(readTaskLines().map { it.toLong() }, 25, 1309761972L)

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        println(firstInvalid(values.first, values.second))
    }

    override fun taskTwo() {
        val section = findSection(values.first, values.third)
        println(section)
        val setValues = values.first.subList(section.first, section.second)
        val result = (setValues.minOrNull() ?: 0) + (setValues.maxOrNull() ?: 0)
        println(result)
    }

    private fun firstInvalid(numbers: List<Long>, previous: Int): Long {
        for (index in previous until numbers.size) {
            if (!validate(numbers, index, previous)) {
                return numbers[index]
            }
        }
        return -1
    }

    private fun validate(numbers: List<Long>, index: Int, previous: Int): Boolean {
        val current = numbers[index]
        return containsSum(numbers.subList(index - previous, index), current)
    }

    private fun containsSum(numbers: List<Long>, value: Long): Boolean {
//        println("$value in $numbers")
        for (i in numbers.indices) {
            for (j in i + 1 until numbers.size) {
                if (numbers[i] + numbers[j] == value) {
                    return true
                }
            }
        }
        return false
    }

    private fun findSection(numbers: List<Long>, value: Long): Pair<Int, Int> {
        var from = 0
        var to = 0
        var sum = 0L

        while (sum != value) {
//            println("$from - $to = $sum of $value")
            if (sum < value) {
                //take next number
                sum += numbers[to]
                to++
            } else if (sum > value) {
                //drop last
                sum -= numbers[from]
                from++
            }
        }
        return Pair(from, to)
    }
}
