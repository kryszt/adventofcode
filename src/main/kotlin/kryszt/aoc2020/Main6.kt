package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.GroupsCollector
import kryszt.tools.IoUtil.readTaskLines

object Main6 : DayChallenge {

    private val values = readTaskLines()

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        AnswersParserPartOne(values)
                .parseData()
//                .also(::println)
                .sumBy { it.size }
                .also(::println)
    }

    override fun taskTwo() {
        AnswersParserPartTwo(values)
                .parseData()
//                .also(::println)
                .sumBy { it.size - 1 }
                .also(::println)
    }

    private class AnswersParserPartOne(lines: List<String>) : GroupsCollector<Answers>(lines, { mutableSetOf() }) {

        override fun handleLine(current: Answers, line: String) {
            current.addAll(line.asIterable())
        }
    }

    private class AnswersParserPartTwo(lines: List<String>) : GroupsCollector<Answers>(lines, { mutableSetOf() }) {

        override fun handleLine(current: Answers, line: String) {
            val withMarker = line.asIterable().plus('0')
            if (current.isEmpty()) {
                current.addAll(withMarker)
            } else {
                val intersect = current.intersect(withMarker)
                current.clear()
                current.addAll(intersect)
            }
        }
    }

}

private typealias Answers = MutableSet<Char>