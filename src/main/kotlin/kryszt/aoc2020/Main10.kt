package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.IoUtil.readTestTaskLines

object Main10 : DayChallenge {

    //    private val values = readTestTaskLines().map(String::toInt).sorted()
    private val values = readTaskLines().map(String::toInt).sorted()
//    private val values = listOf(16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4).sorted()

    @JvmStatic
    fun main(args: Array<String>) {
//        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        val counts = IntArray(4)

        //first and last:
        counts[values.first()]++
        counts[3]++

        values
                .zipWithNext()
                .forEach { (smaller, larger) ->
                    val diff = larger - smaller
                    counts[diff]++
                }

        println("${counts[0]}, ${counts[1]}, ${counts[2]}, ${counts[3]}")
        println(counts[1] * counts[3])
    }

    override fun taskTwo() {
        splitParts(values)
                .also { println(it) }
                .map { countOptions(it) }
                .also { println(it) }
                .fold(1L, operation = { a: Long, b: Int -> a * b })
                .also { println(it) }
    }

    private fun countOptions(jolts: List<Int>): Int {
        if (jolts.size < 2) return 1
        return countOptions(jolts.first(), jolts.subList(1, jolts.size))
    }

    private fun countOptions(current: Int, jolts: List<Int>): Int {
        if (jolts.size < 2) return 1
        var sum = 0
        if (jolts[0] - current <= 3) {
            sum += countOptions(jolts[0], jolts.subList(1, jolts.size))
        }
        if (jolts[1] - current <= 3) {
            sum += countOptions(jolts[1], jolts.subList(2, jolts.size))
        }
        if (jolts.size > 2 && jolts[2] - current <= 3) {
            sum += countOptions(jolts[2], jolts.subList(3, jolts.size))
        }
        return sum
    }

    private fun splitParts(jolts: List<Int>): List<List<Int>> {
        val sections = mutableListOf<List<Int>>()
        var currentList = mutableListOf(0, jolts.first())

        jolts.zipWithNext()
                .forEach { (first, second) ->
                    if (second - first == 3) {
                        sections.add(currentList)
                        currentList = mutableListOf(second)
                    } else {
                        currentList.add(second)
                    }
                }
        sections.add(currentList)
        return sections
    }

}