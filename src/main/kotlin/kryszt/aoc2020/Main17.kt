package kryszt.aoc2020

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines

object Main17 : DayChallenge {

    private val input = readTaskLines()

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        val solver = InfiniteBoardSolver(BoardRule()::rule, NeighbourCreatorPartOne()::makeNeighbours)
        iterateBoard(make3dBoard(), solver)
    }

    override fun taskTwo() {
        val solver = InfiniteBoardSolver(BoardRule()::rule, NeighbourCreatorPartTwo()::makeNeighbours)
        iterateBoard(make4dBoard(), solver)
    }

    private fun <T> iterateBoard(startCells: Set<T>, solver: InfiniteBoardSolver<T>) {
        solver.iterateCells(startCells, 6)
            .size
            .alsoPrint()
    }

    private fun make3dBoard(): Set<Point3D> {
        val initialCells = mutableSetOf<Point3D>()
        input.forEachIndexed { y, s ->
            s.forEachIndexed { x, c ->
                if (c == '#') initialCells.add(Point3D(x, y, 0))
            }
        }
        return initialCells
    }

    private fun make4dBoard(): Set<Point4D> {
        val initialCells = mutableSetOf<Point4D>()
        input.forEachIndexed { y, s ->
            s.forEachIndexed { x, c ->
                if (c == '#') initialCells.add(Point4D(x, y, 0, 0))
            }
        }
        return initialCells
    }

    private class BoardRule {
        fun rule(isAlive: Boolean, aliveNeighbours: Int): Boolean {
            if (aliveNeighbours == 3) return true
            return isAlive && aliveNeighbours == 2
        }
    }

    private class NeighbourCreatorPartOne {

        fun makeNeighbours(cell: Point3D): List<Point3D> = neighbourDirections.map { cell.plus(it) }

        private val neighbourDirections: List<Point3D> = buildNeighbours()

        private fun buildNeighbours(): List<Point3D> {
            val neighbors = mutableListOf<Point3D>()
            for (x in -1..1) {
                for (y in -1..1) {
                    for (z in -1..1) {
                        if (x != 0 || y != 0 || z != 0) {
                            neighbors.add(Point3D(x, y, z))
                        }
                    }
                }
            }
            return neighbors
        }

    }

    private class NeighbourCreatorPartTwo {


        fun makeNeighbours(cell: Point4D): List<Point4D> = neighbourDirections.map { cell.plus(it) }

        private val neighbourDirections: List<Point4D> = buildNeighbours()

        private fun buildNeighbours(): List<Point4D> {
            val neighbors = mutableListOf<Point4D>()
            for (x in -1..1) {
                for (y in -1..1) {
                    for (z in -1..1) {
                        for (t in -1..1) {
                            if (x != 0 || y != 0 || z != 0 || t != 0) {
                                neighbors.add(Point4D(x, y, z, t))
                            }
                        }
                    }
                }
            }
            return neighbors
        }

    }
}
