package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.IoUtil.readTestTaskLines

object Main7 : DayChallenge {

    //    private val values: Bags = readTestTaskLines().map { parseLine(it) }
//    private val values: Bags = readTaskLines("-test2").map { parseLine(it) }

    private val values: Bags = readTaskLines().map { parseLine(it) }
    private val myBag = Bag("shiny", "gold")

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        val all = mutableSetOf<Bag>()
        findAllContaining(myBag, all)
        println(all)
        println(all.size)
    }

    private fun findAllContaining(bag: Bag, containing: MutableSet<Bag>) {
        val allContaining = values.filter { it.second.containsKey(bag) }
        allContaining.forEach { group ->
            if (containing.add(group.first)) {
                findAllContaining(group.first, containing)
            }
        }
    }

    override fun taskTwo() {
        println(countTotalContent(myBag).minus(1))
    }

    private fun countTotalContent(bag: Bag): Int {
        return values
                .find { it.first == bag }
                ?.second
                ?.entries
                ?.sumBy { it.value * countTotalContent(it.key) }
                ?.plus(1)
                ?: 0
    }

    private fun parseLine(line: String): BagInfo {
        val left: Bag = line.split(" ").let { split -> Bag(split[0], split[1]) }
        return if (line.endsWith("no other bags.")) {
            BagInfo(left, emptyMap())
        } else {
            BagInfo(left, parseContain(line.split("contain")[1]))
        }
    }

    private fun parseContain(contain: String): Map<Bag, Int> {
        val items = mutableMapOf<Bag, Int>()
        contain
                .split(",")
                .map { it.trim() }
                .forEach { part ->
                    val info = part.split(" ")
                    items[Bag(info[1], info[2])] = info[0].toInt()
                }
        return items
    }

}

private data class Bag(val secondary: String, val main: String)
private typealias BagInfo = Pair<Bag, Map<Bag, Int>>
private typealias Bags = List<BagInfo>