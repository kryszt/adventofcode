package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.LineHandler
import kryszt.tools.alsoPrint

object Main19 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        val task = TaskCollector().buildTask(readTaskLines())

        val validator = Validator(task.rule)
        task.inputs.count { validator.isValid(it) }.alsoPrint()
    }

    override fun taskTwo() {
        val task = TaskCollector().buildTaskPartTwo(readTaskLines())
        val validator = Validator(task.rule)
        task.inputs
            .count { validator.isValid(it) }
            .alsoPrint()
    }


    private class RulesBuilder : LineHandler {
        val rules = mutableMapOf<Int, Rule>()
        val ruleLines = mutableMapOf<Int, String>()

        override fun handleLine(line: String) {
            line.split(":").also {
                ruleLines[it[0].toInt()] = it[1].trim()
            }
        }

        fun getRule(id: Int): Rule {
            return rules.getOrPut(id) { buildRule(id) }
        }

        private fun buildRule(id: Int): Rule {
            return when (val line = ruleLines[id]!!) {
                "\"a\"" -> RuleChar('a')
                "\"b\"" -> RuleChar('b')
                else -> asListRule(line)
            }
        }

        private fun asListRule(line: String): Rule {
            return line
                .split('|')
                .map { asRules(it) }
                .let { RuleList(it) }
        }

        private fun asRules(numbersLine: String): List<Rule> {
            return numbersLine
                .trim()
                .split(' ')
                .map { it.toInt() }
                .map { RuleLink(it) { getRule(it) } }
        }
    }

    private class InputCollector : LineHandler {

        val inputs = mutableListOf<String>()

        override fun handleLine(line: String) {
            inputs.add(line)
        }
    }

    private data class Task(val rule: Rule, val inputs: List<String>)

    private class TaskCollector : LineHandler {

        val rulesBuilder = RulesBuilder()
        val inputCollector = InputCollector()

        var currentHandler: LineHandler = rulesBuilder

        fun buildTask(lines: List<String>): Task {
            lines.forEach(this::handleLine)
            return Task(rulesBuilder.getRule(0), inputCollector.inputs)
        }

        fun buildTaskPartTwo(lines: List<String>): Task {
            lines.forEach(this::handleLine)
            rulesBuilder.handleLine("8: 42 | 42 8")
            rulesBuilder.handleLine("11: 42 31 | 42 11 31")
            return Task(rulesBuilder.getRule(0), inputCollector.inputs)
        }

        override fun handleLine(line: String) {
            when {
                line.isEmpty() -> currentHandler = inputCollector
                else -> currentHandler.handleLine(line)
            }
        }
    }

    private class Validator(val rule: Rule) {
        fun isValid(input: String): Boolean {
            val result = rule.check(input, 0)
            return result.contains(input.length)
        }
    }

    private interface Rule {
        fun check(input: String, position: Int): List<Int>
    }

    private data class RuleChar(val requiredChar: Char) : Rule {

        override fun check(input: String, position: Int): List<Int> {
            return if (input.getOrNull(position) == requiredChar) {
                listOf(position + 1)
            } else {
                emptyList()
            }
        }
    }

    private class RuleLink(val id: Int, val linkedRule: () -> Rule) : Rule {
        override fun toString(): String = "Rule $id"

        override fun check(input: String, position: Int): List<Int> {
            return linkedRule().check(input, position)
        }
    }

    private class RuleList(val rules: List<List<Rule>>) : Rule {

        override fun check(input: String, position: Int): List<Int> {
            return rules.map { allRules(it, input, position) }.flatten().toSet().toList()
        }

        private fun allRules(rules: List<Rule>, input: String, position: Int): List<Int> {
            var stepMatched: Set<Int> = setOf(position)
            rules.forEach { rule ->
                if (stepMatched.isEmpty()) {
                    return emptyList()
                }
                val nextStepsCandidates = mutableSetOf<Int>()
                stepMatched.toList().forEach { checkPosition ->
                    rule.check(input, checkPosition)
                        .also { nextStepsCandidates.addAll(it) }
                }
                stepMatched = nextStepsCandidates
            }
            return stepMatched.toList()
        }
    }
}

