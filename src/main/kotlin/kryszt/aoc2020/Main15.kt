package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.alsoPrint

object Main15 : DayChallenge {

    private val initialNumbers = intArrayOf(0, 20, 7, 16, 1, 18, 15)

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        runGame(initialNumbers, 2020).alsoPrint()
//        runGame(intArrayOf(1, 3, 2), 20)
//        checkRun(intArrayOf(0, 3, 6), 2020, 436)
//        checkRun(intArrayOf(1, 3, 2), 2020, 1)
//        checkRun(intArrayOf(2, 1, 3), 2020, 10)
//        checkRun(intArrayOf(1, 2, 3), 2020, 27)
//        checkRun(intArrayOf(2, 3, 1), 2020, 78)
//        checkRun(intArrayOf(3, 2, 1), 2020, 438)
//        checkRun(intArrayOf(3, 1, 2), 2020, 1836)
    }

    private fun checkRun(initialNumbers: IntArray, repeats: Int, expectedResult: Int) {
        val result = runGame(initialNumbers, repeats)
        if (result != expectedResult) {
            System.err.println("Expected $expectedResult but got $result")
        }
    }

    override fun taskTwo() {
        runGame(initialNumbers, 30_000_000).alsoPrint()

//        checkRun(intArrayOf(0, 3, 6), 30_000_000, 175594)
//        checkRun(intArrayOf(1, 3, 2), 30_000_000, 2578)
//        checkRun(intArrayOf(2, 1, 3), 30_000_000, 3544142)
//        checkRun(intArrayOf(1, 2, 3), 30_000_000, 261214)
//        checkRun(intArrayOf(2, 3, 1), 30_000_000, 6895259)
//        checkRun(intArrayOf(3, 2, 1), 30_000_000, 18)
//        checkRun(intArrayOf(3, 1, 2), 30_000_000, 362)
    }

    private fun runGame(initialNumbers: IntArray, repeats: Int): Int {
        //make it 1-based, for simpler checking with task examples
        val turnCounter = mutableMapOf<Int, IntArray>()
        initialNumbers.forEachIndexed { index, i ->
            turnCounter[i] = newCache(index + 1)
        }
        var lastNumber = initialNumbers.last()
        var currentTurn = initialNumbers.size + 1

        while (currentTurn <= repeats) {
            val lastSpokenTurns = turnCounter[lastNumber]!!
            val nextValue = if (lastSpokenTurns.first() == 0) 0 else lastSpokenTurns.last() - lastSpokenTurns.first()
            if (currentTurn == repeats || currentTurn % 10_000_000 == 0) {
                println("turn $currentTurn, $lastNumber was seen in ${lastSpokenTurns.asString()}, next will be $nextValue")
            }

            turnCounter.getOrPut(nextValue, { newCache() }).placeLast(currentTurn)
            lastNumber = nextValue
            currentTurn++
        }
        return lastNumber
    }

    private fun newCache() = IntArray(2)
    private fun newCache(value: Int) = IntArray(2).placeLast(value)
    private fun IntArray.placeLast(value: Int): IntArray = apply {
        this[0] = this[1]
        this[1] = value
    }

    private fun IntArray.first() = this[0]
    private fun IntArray.last() = this[1]
    private fun IntArray.asString() = "${this[0]},${this[1]}"

}
