package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point

object Main11 : DayChallenge {

    private const val EMPTY = 'L'
    private const val TAKEN = '#'
    private const val FLOOR = '.'

    private val baseBoard: List<CharArray>
        //        get() = readTestTaskLines().map { it.toCharArray() }
        get() = readTaskLines().map { it.toCharArray() }

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        solve(this::nextStatePartOne)
    }

    override fun taskTwo() {
        solve(this::nextStatePartTwo)
    }

    private fun solve(next: (Board) -> Board) {
        var lastBoardString = baseBoard.asString()
        var currentBoard = baseBoard
        while (true) {
            val nextState = next(currentBoard)
            val nextStateString = nextState.asString()
            if (lastBoardString == nextStateString) {
                break
            } else {
                currentBoard = nextState
                lastBoardString = nextStateString
            }
        }
        println(lastBoardString.count { it == TAKEN })
    }

    private fun nextStatePartOne(board: List<CharArray>): List<CharArray> {
        val nextBoard = mutableListOf<CharArray>()
        board.forEachIndexed { y, line ->
            val nextLine = CharArray(line.size)
            nextBoard.add(nextLine)
            for (x in line.indices) {
                nextLine[x] = this.nextValuePartOne(board, x, y)
            }
        }
        return nextBoard
    }

    private fun nextValuePartOne(board: List<CharArray>, x: Int, y: Int): Char =
            when (val place = board[y][x]) {
                EMPTY -> if (countTakenAround(board, x, y) == 0) TAKEN else EMPTY
                TAKEN -> if (countTakenAround(board, x, y) >= 4) EMPTY else TAKEN
                else -> place
            }

    private fun countTakenAround(board: List<CharArray>, x: Int, y: Int): Int =
            Point
                    .neighbours
                    .count { TAKEN == board.getOrNull(y + it.y)?.getOrNull(x + it.x) }

    private fun nextStatePartTwo(board: List<CharArray>): List<CharArray> {
        val nextBoard = mutableListOf<CharArray>()
        board.forEachIndexed { y, line ->
            val nextLine = CharArray(line.size)
            nextBoard.add(nextLine)
            for (x in line.indices) {
                nextLine[x] = this.nextValuePartTwo(board, x, y)
            }
        }
        return nextBoard
    }

    private fun nextValuePartTwo(board: List<CharArray>, x: Int, y: Int): Char =
            when (val place = board[y][x]) {
                EMPTY -> if (countTakenInLineOfSight(board, x, y) == 0) TAKEN else EMPTY
                TAKEN -> if (countTakenInLineOfSight(board, x, y) >= 5) EMPTY else TAKEN
                else -> place
            }

    private fun countTakenInLineOfSight(board: List<CharArray>, x: Int, y: Int): Int =
            Point
                    .neighbours
                    .count { TAKEN == firstInDirection(board, x, y, it) }

    private fun firstInDirection(board: List<CharArray>, x: Int, y: Int, direction: Point): Char {
        var step = 1
        while (true) {
            when (val space = board.getOrNull(y + direction.y * step)?.getOrNull(x + direction.x * step)) {
                EMPTY, TAKEN -> return space
                null -> return FLOOR
            }
            step++
        }
    }
}

private typealias Board = List<CharArray>

private fun List<CharArray>.asString() = joinToString(separator = "\n", transform = { String(it) })