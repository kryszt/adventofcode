package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import java.math.BigInteger

object Main3 : DayChallenge {

    //    val values = readTaskLines("-test")
    val values = readTaskLines()

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        solveForDirections(values, listOf(Point(3, 1)))
                .also { println(it) }
    }

    override fun taskTwo() {
        val directions = listOf(
                Point(1, 1),
                Point(3, 1),
                Point(5, 1),
                Point(7, 1),
                Point(1, 2)
        )
        solveForDirections(values, directions)
                .also { println(it) }
    }

    private fun solveForDirections(forest: List<String>, directions: List<Point>): BigInteger {
        return directions.map { countTrees(forest, it) }
                .also { println(it) }
                .fold(BigInteger.ONE, operation = { acc: BigInteger, i: Int -> acc.multiply(i.toBigInteger()) })
    }

    private fun countTrees(forest: List<String>, direction: Point): Int {
        var count = 0
        var column = 0

        forest.forEachIndexed { index, line ->
            if (index % direction.y == 0) {
                if (line[column % line.length] == '#') {
                    count++
                }
                column += direction.x
            }
        }
        return count
    }
}
