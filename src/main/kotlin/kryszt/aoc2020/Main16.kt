package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.LineHandler
import kryszt.tools.alsoPrint

object Main16 : DayChallenge {

    //    private val content = TicketsReader().parseContent(readTestTaskLines())
//    private val content = TicketsReader().parseContent(readTaskLines("-test2"))
    private val content = TicketsReader().parseContent(readTaskLines())

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        println()
        taskTwo()
    }

    override fun taskOne() {
        content
                .nearbyTickets
                .flatten()
                .filterNot { value -> content.conditions.any { condition -> condition.contains(value) } }
                .alsoPrint()
                .sum()
                .alsoPrint()
    }

    override fun taskTwo() {
        findColumns()
                .map { Pair(it.key, it.value) }
                .filter { it.first.name.startsWith("departure") }
//                .alsoPrintOnEach()
                .map { content.myTicket[it.second] }
                .alsoPrint()
                .fold(1L, operation = { a, b -> a * b })
                .alsoPrint()
    }

    private fun findColumns(): Map<Condition, Int> {
        val candidates: MutableMap<Condition, MutableSet<Int>> =
                findColumnCandidates(content.conditions, content.validTickets)
                        .mapValues { it.value.toMutableSet() }
                        .toMutableMap()

        val result = mutableMapOf<Condition, Int>()

        while (candidates.isNotEmpty()) {
            candidates
                    .entries
                    .find { it.value.size == 1 }
                    ?.let { Pair(it.key, it.value.first()) }
                    ?.also {
                        result[it.first] = it.second
                    }
                    ?.also {
                        candidates.remove(it.first)
                    }
                    ?.also {
                        candidates.forEach { (_, values) -> values.remove(it.second) }
                    }
                    ?: throw IllegalStateException("No single column candidate in $candidates")
        }
        return result
    }


    private fun findColumnCandidates(conditions: List<Condition>, tickets: List<Ticket>): Map<Condition, Set<Int>> {
        val conditionMapping = mutableMapOf<Condition, Set<Int>>()
        conditions.forEach { condition ->
            findConditionColumnCandidates(condition, tickets)
                    .also { conditionMapping[condition] = it }
        }
        return conditionMapping
    }

    private fun findConditionColumnCandidates(condition: Condition, tickets: List<Ticket>): Set<Int> {
        return tickets
                .first()
                .indices
                .filter { column -> tickets.all { ticket -> condition.contains(ticket[column]) } }
                .toSet()
                .takeUnless { it.isEmpty() }
                ?: throw IllegalStateException("Cannot find column for $condition")
    }
}

private class MyTicketLineHandler : LineHandler {
    var myNumbers: Ticket = emptyList()

    override fun handleLine(line: String) {
        myNumbers = line.split(',').map { it.toInt() }
    }
}

private class ConditionsLineHandler : LineHandler {

    val linePattern = "([a-z ]+): (\\d+)-(\\d+) or (\\d+)-(\\d+)".toPattern()

    val conditions = mutableListOf<Condition>()

    override fun handleLine(line: String) {
        linePattern.matcher(line)
                .takeIf { it.find() }
                ?.let {
                    Condition(name = it.group(1),
                            firstRange = it.group(2).toInt().rangeTo(it.group(3).toInt()),
                            secondRange = it.group(4).toInt().rangeTo(it.group(5).toInt()))
                }
                ?.also { conditions.add(it) }
                ?: throw IllegalArgumentException("Cannot parse $line")
    }
}

private class NearbyTicketsLineHandler : LineHandler {

    val tickets = mutableListOf<Ticket>()

    override fun handleLine(line: String) {
        line
                .split(',')
                .map { it.toInt() }
                .also { tickets.add(it) }
    }
}

private data class Condition(val name: String, val firstRange: IntRange, val secondRange: IntRange) {
    fun contains(value: Int): Boolean =
            firstRange.contains(value)
                    || secondRange.contains(value)
}

private data class Task(val conditions: List<Condition>, val myTicket: Ticket, val nearbyTickets: List<Ticket>) {

    val validTickets: List<Ticket> by lazy {
        nearbyTickets.filter { it.isValid(conditions) }
    }

    private fun Ticket.isValid(conditions: List<Condition>): Boolean =
            this.all { value -> conditions.any { condition -> condition.contains(value) } }
}

private class TicketsReader : LineHandler {
    val conditions = ConditionsLineHandler()
    val myTicket = MyTicketLineHandler()
    val nearbyTickets = NearbyTicketsLineHandler()

    private var currentHandler: LineHandler = conditions

    fun parseContent(lines: List<String>): Task {
        lines.forEach(this::handleLine)
        return Task(conditions.conditions, myTicket.myNumbers, nearbyTickets.tickets)
    }

    override fun handleLine(line: String) {
        when {
            line.isEmpty() -> Unit
            line == "your ticket:" -> currentHandler = myTicket
            line == "nearby tickets:" -> currentHandler = nearbyTickets
            else -> currentHandler.handleLine(line)
        }
    }
}

private typealias Ticket = List<Int>
