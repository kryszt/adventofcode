package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.IoUtil.readTestTaskLines
import kryszt.tools.alsoPrint
import java.lang.IllegalArgumentException

object Main14 : DayChallenge {

    private val maskLine = "mask = ([X10]{36})".toPattern()
    private val memLine = "mem\\[([0-9]+)] = ([0-9]+)".toPattern()

    private val values: List<Order> = readTaskLines().map(this::parseLine)
//    private val values: List<Order> = readTestTaskLines().map(this::parseLine)
//    private val values: List<Order> = readTaskLines("-test2")

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        runTask(MemInputPartOne())
    }

    override fun taskTwo() {
        runTask(MemInputPartTwo())
    }

    private fun runTask(memInput: MemInput) {
        values.forEach { order ->
            memInput.order(order)
        }
//        println(memInput.memory)
        memInput.memory.values.sum().alsoPrint()
    }

    private fun parseLine(line: String): Order =
            line.parseMaskString()
                    ?: line.parseWriteLine()
                    ?: throw IllegalArgumentException("Unparsable line: $line")

    private fun String.parseMaskString(): Order.Mask? =
            maskLine
                    .matcher(this)
                    .takeIf { it.find() }
                    ?.group(1)
                    ?.asMask()

    private fun String.asMask(): Order.Mask {
        return Order.Mask(stringMask = this)
    }

    private fun String.parseWriteLine(): Order.Write? =
            memLine
                    .matcher(this)
                    .takeIf { it.find() }
                    ?.let { Order.Write(it.group(2).toLong(), it.group(1).toLong()) }
}

private abstract class MemInput {

    private var writeMask: Order.Mask = Order.Mask("")
    val memory = mutableMapOf<Long, Long>()

    fun order(order: Order) {
        when (order) {
            is Order.Mask -> writeMask = order
            is Order.Write -> write(order)
        }
    }

    private fun write(order: Order.Write) {
        val value = writeValue(order, writeMask)
        writeAddresses(order, writeMask).forEach { address ->
            memory[address] = value
        }
    }

    protected fun maskValue(value: Long, mask: ValueMask): Long =
            value.or(mask.orMask).and(mask.andMask)

    abstract fun writeValue(order: Order.Write, writeMask: Order.Mask): Long
    abstract fun writeAddresses(order: Order.Write, writeMask: Order.Mask): List<Long>
}

private class MemInputPartOne : MemInput() {

    override fun writeValue(order: Order.Write, writeMask: Order.Mask): Long =
            maskValue(order.value, writeMask.asValueMask())

    override fun writeAddresses(order: Order.Write, writeMask: Order.Mask): List<Long> = listOf(order.memAddress)

    private fun Order.Mask.asValueMask(): ValueMask {
        val orValue = stringMask.replace('X', '0').toLong(2)
        val andValue = stringMask.replace('X', '1').toLong(2)
        return ValueMask(orMask = orValue, andMask = andValue)
    }
}

private class MemInputPartTwo : MemInput() {

    override fun writeValue(order: Order.Write, writeMask: Order.Mask): Long = order.value

    override fun writeAddresses(order: Order.Write, writeMask: Order.Mask): List<Long> {
//        println("write ${order.value} to ${order.memAddress} with mask ${writeMask.stringMask}")
        return generateValues(mergeAddress(order.memAddress, writeMask.stringMask))
    }

    private fun mergeAddress(address: Long, mask: String): String {
        val addressString = address
                .toInt()
                .let { Integer.toBinaryString(it) }
                .padStart(mask.length, '0')

        val merged = CharArray(mask.length)
        for (i in mask.indices) {
            merged[i] = mergeBit(addressString[i], mask[i])
        }
        return String(merged)
    }

    private fun mergeBit(addressBit: Char, maskBit: Char): Char =
            if (maskBit == '0') addressBit else maskBit

    private fun generateValues(mapString: String): List<Long> {
        val nextX = mapString.indexOf('X')
        if (nextX < 0) return listOf(mapString.toLong(2))
        val withZero = mapString.replaceFirst('X', '0')
        val withOne = mapString.replaceFirst('X', '1')
        return generateValues(withZero) + generateValues(withOne)
    }
}

private class ValueMask(val orMask: Long, val andMask: Long)

private sealed class Order {
    data class Mask(val stringMask: String) : Order()
    data class Write(val value: Long, val memAddress: Long) : Order()
}

