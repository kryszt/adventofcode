package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import java.util.regex.Matcher

object Main2 : DayChallenge {
    private val linePattern = "([0-9]+)-([0-9]+) ([a-z]): ([a-z]+)".toPattern()

    private val values: List<Entry> = readTaskLines().mapNotNull { splitLine(it) }
//    private val values: List<Entry> = listOf(
//            Entry(1, 3, 'a', "abcde"),
//            Entry(1, 3, 'b', "cdefg"),
//            Entry(2, 9, 'c', "ccccccccc")
//    )

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        values.count { it.isValidPartOne() }.also { println(it) }
    }

    override fun taskTwo() {
        values.count { it.isValidPartTwo() }.also { println(it) }
    }


    private fun splitLine(line: String): Entry? {
        return linePattern
                .matcher(line)
                .takeIf { it.find() }
                ?.let { parse(it) }
    }

    private fun parse(matcher: Matcher): Entry =
            Entry(
                    from = matcher.group(1).toInt(),
                    to = matcher.group(2).toInt(),
                    c = matcher.group(3).first(),
                    pass = matcher.group(4)
            )

    private data class Entry(val from: Int, val to: Int, val c: Char, val pass: String) {
        fun isValidPartOne(): Boolean {
            return pass.count { it == c } in from..to
        }

        fun isValidPartTwo(): Boolean {
            return (pass[from - 1] == c).xor(pass[to - 1] == c)
        }
    }
}