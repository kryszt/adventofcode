package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.alsoPrint

object Main23 : DayChallenge {

    //        private val values: List<Int> = "389125467".map { it - '0' }
    private val values: List<Int> = "318946572".map { it - '0' }

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        val nodes = buildNodes(values.asSequence())
        doMoves(nodes, 100)
        nodes.allNodes[1]
            ?.toList()
            ?.minus(1)
            ?.joinToString("")
            .alsoPrint()
    }

    override fun taskTwo() {
        val extraNodes = (10..1_000_000).asSequence()
        val nodes = buildNodes(values.asSequence() + extraNodes)
        doMoves(nodes, 10_000_000)

        nodes.allNodes[1]!!.toSequence().take(3).fold(1L, { acc, i -> acc * i }).alsoPrint()
    }

    private fun doMoves(nodes: Nodes, steps: Int) {
        var current = nodes.mainNode
        repeat(steps) {
            current = step(current, nodes.allNodes)
        }
    }

    private fun step(node: Node, nodes: Map<Int, Node>): Node {
        //remove 3:
        val cut = takeThree(node)
        //find new place
        val target = findTarget(node, nodes, cut)
        //place
        placeAfter(target, cut)
        //go to next
        return node.next!!
    }

    private fun takeThree(node: Node): Node {
        val fourth = node.next?.next?.next?.next ?: throw IllegalStateException("No 4th item?")

        val firstCut = node.next!!

        node.next!!.prev = fourth.prev
        fourth.prev!!.next = node.next

        node.next = fourth
        fourth.prev = node

        return firstCut
    }

    private fun findTarget(node: Node, nodes: Map<Int, Node>, sideNodes: Node): Node {
        var targetValue = node.value - 1
        while (targetValue != node.value) {
            if (targetValue < 1) {
                targetValue = nodes.size
            }
            if (sideNodes.contains(targetValue)) {
                targetValue--
                continue
            }
            nodes[targetValue]?.run { return this }
        }
        throw IllegalStateException("No target for ${node.value}")
    }

    private fun placeAfter(node: Node, insert: Node) {
        node.next!!.prev = insert.prev
        insert.prev!!.next = node.next

        node.next = insert
        insert.prev = node
    }

    private fun buildNodes(values: Sequence<Int>): Nodes {
        val startNode = Node(values.first())
        val allNodes = mutableMapOf<Int, Node>()
        allNodes[startNode.value] = startNode

        var lastNode = startNode
        values.drop(1).forEach { v ->
            val nextNode = Node(v)
            allNodes[v] = nextNode
            lastNode.next = nextNode
            nextNode.prev = lastNode
            lastNode = nextNode
        }

        lastNode.next = startNode
        startNode.prev = lastNode

        return Nodes(startNode, allNodes)
    }

    private class Nodes(val mainNode: Node, val allNodes: Map<Int, Node>)

    private fun Node.find(value: Int): Node? = NodeIterator(this).asSequence().find { it.value == value }

    private fun Node.contains(value: Int): Boolean = find(value) != null

    private fun Node.toSequence(): Sequence<Int> = NodeIterator(this).asSequence().map { it.value }

    private fun Node.toList(): List<Int> = NodeIterator(this).asSequence().map { it.value }.toList()

    private class Node(val value: Int, var prev: Node? = null, var next: Node? = null)

    private class NodeIterator(private val startNode: Node) : Iterator<Node> {

        private var currentNode: Node? = startNode

        override fun hasNext(): Boolean = currentNode != null

        override fun next(): Node =
            checkNotNull(currentNode)
                .also {
                    currentNode = it.next?.takeUnless { nexNode -> nexNode == startNode }
                }
    }
}
