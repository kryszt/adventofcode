package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.alsoPrint

object Main25 : DayChallenge {

    //test
//    private const val publicKeyDoors = 17807724L
//    private const val publicKeyCard = 5764801L

    //task
    private const val publicKeyDoors = 7573546L
    private const val publicKeyCard = 17786549L

    private const val divisor = 20201227L

    private const val baseSubject = 7L

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
    }


    override fun taskOne() {
        val loopSizeDoors = findRepeats(publicKeyDoors).alsoPrint()
        val loopSizeCard = findRepeats(publicKeyCard).alsoPrint()

        println(loop(loopSizeDoors, publicKeyCard))
        println(loop(loopSizeCard, publicKeyDoors))
    }

    private fun findRepeats(expected: Long): Int {
        var value = 1L
        var count = 0
        while (value != expected) {
            count++
            value = (value * baseSubject) % divisor
        }
        return count
    }

    private fun loop(repeats: Int, subject: Long = 1): Long {
        var value = 1L
        repeat(repeats) {
            value = (value * subject) % divisor
        }
        return value
    }

    override fun taskTwo() {
        TODO("Not yet implemented")
    }
}