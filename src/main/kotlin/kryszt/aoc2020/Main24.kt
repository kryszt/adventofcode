package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.InfiniteBoardSolver
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.IoUtil.readTestTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint

object Main24 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        findBlackTiles().size.alsoPrint()
    }

    override fun taskTwo() {
        val solver = InfiniteBoardSolver(BoardRule()::rule, BoardNeighbours()::neighbours)
        val initialBoard = findBlackTiles()
        solver
            .iterateCells(initialBoard, 100)
            .size
            .alsoPrint()
    }

    private fun findBlackTiles(): Set<Point> {
        val blackTiles = mutableSetOf<Point>()

        readTaskLines()
            .map { traverseLine(it) }
            .forEach { point ->
                if (!blackTiles.add(point)) {
                    blackTiles.remove(point)
                }
            }

        return blackTiles
    }

    private fun traverseLine(line: String): Point {
        return replaceAll(line)
            .mapNotNull { directions[it] }
            .reduce { acc, point -> acc.plus(point) }
    }

    private fun replaceAll(string: String): String {
        var updated = string
        replacements.forEach {
            updated = updated.replace(it.first, it.second)
        }
        return updated
    }

    private val replacements = listOf(
        "ne" to "1",
        "se" to "3",
        "sw" to "4",
        "nw" to "6",
        "e" to "2",
        "w" to "5"
    )

    private val directions = mapOf(
        '1' to Point(1, -1),
        '2' to Point(2, 0),
        '3' to Point(1, 1),
        '4' to Point(-1, 1),
        '5' to Point(-2, 0),
        '6' to Point(-1, -1)
    )

    private class BoardRule {
        fun rule(isAlive: Boolean, aliveNeighbours: Int): Boolean =
            when (isAlive) {
                true -> !(aliveNeighbours == 0 || aliveNeighbours > 2)
                false -> aliveNeighbours == 2
            }
    }

    private class BoardNeighbours {
        fun neighbours(point: Point): List<Point> {
            return listOf(
                point.move(1, -1),
                point.move(2, 0),
                point.move(1, 1),
                point.move(-1, 1),
                point.move(-2, 0),
                point.move(-1, -1)
            )
        }
    }
}