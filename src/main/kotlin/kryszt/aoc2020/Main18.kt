package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import java.util.*

object Main18 : DayChallenge {

    private val input = readTaskLines()

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        check("2 * 3 + (4 * 5)", 26, precedenceTaskOne)
        check("5 + (8 * 3 + 9 + 3 * 4 * 3)", 437, precedenceTaskOne)
        check("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 12240, precedenceTaskOne)
        check("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 13632, precedenceTaskOne)
        println()

        input.map { evaluate(it, precedenceTaskOne) }
                .sum()
                .alsoPrint()
    }

    override fun taskTwo() {
        check("1 + (2 * 3) + (4 * (5 + 6))", 51, precedenceTaskTwo)
        check("2 * 3 + (4 * 5)", 46, precedenceTaskTwo)
        check("5 + (8 * 3 + 9 + 3 * 4 * 3)", 1445, precedenceTaskTwo)
        check("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 669060, precedenceTaskTwo)
        check("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 23340, precedenceTaskTwo)

        input.map { evaluate(it, precedenceTaskTwo) }
                .sum()
                .alsoPrint()

    }

    private fun evaluate(input: String, precedence: Map<Char, Int>): Long {
        return evaluateOnp(buildOnp(input, precedence))
    }

    private fun check(input: String, expected: Long, precedence: Map<Char, Int>) {
        val result = evaluate(input, precedence)
        if (result != expected) {
            System.err.println("$expected expected, but got $result for '$input'")
        } else {
            println("OK: $expected for '$input'")
        }
    }

    private fun buildOnp(input: String, precedence: Map<Char, Int>): List<Char> {

        val operatorStack = Stack<Char>()
        val output = mutableListOf<Char>()

        fun operatorPrecedence(operator: Char): Int =
                precedence[operator] ?: 0

        fun popUntilParenthesis() {
            while (operatorStack.peek() != '(') {
                output.add(operatorStack.pop())
            }
            //remove '('
            operatorStack.pop()
        }

        fun handleOperator(operator: Char) {
            val operatorPrecedence = operatorPrecedence(operator)
            while (operatorStack.isNotEmpty() && operatorStack.peek() != '(' && operatorPrecedence <= operatorPrecedence(operatorStack.peek())) {
                output.add(operatorStack.pop())
            }
            operatorStack.push(operator)
        }

        input.forEach { c ->
            when {
                c.isDigit() -> output.add(c)
                c == '(' -> operatorStack.push(c)
                c == ')' -> popUntilParenthesis()
                c == '*' || c == '+' -> handleOperator(c)
            }
        }
        while (operatorStack.isNotEmpty()) {
            output.add(operatorStack.pop())
        }

        return output
    }

    private fun evaluateOnp(onp: List<Char>): Long {
        val valueStack = Stack<Long>()

        onp.forEach { c ->
            when {
                c.isDigit() -> valueStack.push((c - '0').toLong())
                c == '+' -> valueStack.push(valueStack.pop() + valueStack.pop())
                c == '*' -> valueStack.push(valueStack.pop() * valueStack.pop())
            }
        }
        return valueStack.pop()
    }

    @Suppress("unused")
    private val precedenceNormal = mapOf(
            '*' to 1,
            '+' to 0
    )
    private val precedenceTaskOne = mapOf(
            '*' to 0,
            '+' to 0
    )
    private val precedenceTaskTwo = mapOf(
            '*' to 0,
            '+' to 1
    )
}
