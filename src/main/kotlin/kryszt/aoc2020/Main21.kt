package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import kryszt.tools.alsoPrintOnEach
import kryszt.tools.parseWith
import java.util.regex.Matcher

object Main21 : DayChallenge {

    private val menuPattern = "([a-z ]+) \\(contains ([a-z, ]+)\\)".toPattern()

    //        private val menu: List<MenuItem> = readTestTaskLines().parseWith(menuPattern, this::parseItem)
    private val menu: List<MenuItem> = readTaskLines().parseWith(menuPattern, this::parseItem)

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        val productToAllergen = findAllergens().productToAllergen
        val allIngredients = menu.map { it.ingredients }.flatten().toMutableList()

        allIngredients.removeAll {
            productToAllergen.containsKey(it)
        }
        allIngredients.size.alsoPrint()
    }

    override fun taskTwo() {
        findAllergens()
                .productToAllergen
                .map { Pair(it.key, it.value) }
                .sortedBy { it.second }
                .alsoPrintOnEach()
                .joinToString(",") { it.first }
                .alsoPrint()
    }

    private fun findAllergens(): AllergensMapping {
        val allAllergens = menu.map { it.allergens }.flatten().toSet()
        val possibleAllergens = mutableMapOf<String, MutableSet<String>>()

        allAllergens.forEach { allergen ->
            menu
                    .filter { it.allergens.contains(allergen) }
                    .map { it.ingredients.toSet() }
                    .reduce { acc, set -> acc.intersect(set) }
                    .also { possibleAllergens[allergen] = it.toMutableSet() }
        }

        val productToAllergen = mutableMapOf<String, String>()
        while (possibleAllergens.isNotEmpty()) {

            fun removeIngredient(name: String) {
                possibleAllergens
                        .forEach { (_, u) -> u.remove(name) }
            }

            possibleAllergens
                    .entries
                    .find { it.value.size == 1 }
                    ?.let { Pair(it.key, it.value.first()) }
                    ?.also { productToAllergen[it.second] = it.first }
                    ?.also { possibleAllergens.remove(it.first) }
                    ?.also { removeIngredient(it.second) }
                    ?: throw IllegalStateException("No 1-ingredient option in $possibleAllergens")
        }
        return AllergensMapping(productToAllergen)
    }

    private fun parseItem(matcher: Matcher): MenuItem = MenuItem(
            ingredients = matcher.group(1).split(" "),
            allergens = matcher.group(2).split(", ")
    )

    private data class AllergensMapping(
            val productToAllergen: Map<String, String>
    )

    private data class MenuItem(val ingredients: List<String>, val allergens: List<String>)
}
