package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.alsoPrint
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

object Main22 : DayChallenge {

    //    private val playerOne = listOf(9, 2, 6, 3, 1)
//    private val playerTwo = listOf(5, 8, 4, 7, 10)
    private val playerOne = listOf(20, 28, 50, 36, 35, 15, 41, 22, 39, 45, 30, 19, 47, 38, 25, 6, 2, 27, 5, 4, 37, 24, 42, 29, 21)
    private val playerTwo = listOf(23, 43, 34, 49, 13, 48, 44, 18, 14, 9, 12, 31, 16, 26, 33, 3, 10, 1, 46, 17, 32, 11, 40, 7, 8)

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1")
        taskOne()
        println("Task 2")
        taskTwo()
    }

    override fun taskOne() {
        playGame()
                .let { if (it.first.isNotEmpty()) it.first else it.second }
                .alsoPrint()
                .reversed()
                .mapIndexed { index, i -> (index + 1) * i }
                .sum()
                .alsoPrint()
    }

    override fun taskTwo() {
        playRecursiveGame(playerOne, playerTwo)
                .let { if (it.first.isNotEmpty()) it.first else it.second }
                .alsoPrint()
                .reversed()
                .mapIndexed { index, i -> (index + 1) * i }
                .sum()
                .alsoPrint()

        recursiveTotalSteps.get().also { println("Done in $it steps") }
    }

    private fun playGame(): Pair<List<Int>, List<Int>> {
        val deckOne = LinkedList(playerOne)
        val deckTwo = LinkedList(playerTwo)
        var counter = 0

        while (deckOne.isNotEmpty() && deckTwo.isNotEmpty()) {
            counter++
            val cardOne = deckOne.pop()
            val cardTwo = deckTwo.pop()

            if (cardOne > cardTwo) {
                deckOne.add(cardOne)
                deckOne.add(cardTwo)
            }
            if (cardTwo > cardOne) {
                deckTwo.add(cardTwo)
                deckTwo.add(cardOne)
            }
        }
        println("Done in $counter steps")
        return Pair(deckOne, deckTwo)
    }

    private val recursiveTotalSteps = AtomicInteger()

    private fun playRecursiveGame(playerOne: List<Int>, playerTwo: List<Int>): Pair<List<Int>, List<Int>> {
        val deckOne = LinkedList(playerOne)
        val deckTwo = LinkedList(playerTwo)
        var counter = 0

        val seenRounds = mutableSetOf<String>()

        fun snapshot(): String = deckOne.joinToString(",") + ":" + deckTwo.joinToString(",")

        while (deckOne.isNotEmpty() && deckTwo.isNotEmpty()) {
            recursiveTotalSteps.incrementAndGet()
            val roundSnapshot = snapshot()
            if (!seenRounds.add(roundSnapshot)) {
                //already visited, win for p1
                return Pair(deckOne + deckTwo, emptyList())
            }

            counter++
            val cardOne = deckOne.pop()
            val cardTwo = deckTwo.pop()

            val pOneWins: Boolean

            pOneWins = if (cardOne <= deckOne.size && cardTwo <= deckTwo.size) {
                val result = playRecursiveGame(deckOne.take(cardOne), deckTwo.take(cardTwo))
                result.first.size > result.second.size
            } else {
                cardOne > cardTwo
            }


            if (pOneWins) {
                deckOne.add(cardOne)
                deckOne.add(cardTwo)
            } else {
                deckTwo.add(cardTwo)
                deckTwo.add(cardOne)
            }
        }
        return Pair(deckOne, deckTwo)
    }


}
