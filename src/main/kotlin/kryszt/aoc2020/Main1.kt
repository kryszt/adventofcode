package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil

object Main1 : DayChallenge {

    private val values = IoUtil.readTaskFileLines(this).map { it.toInt() }

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        values
                .find { first -> values.contains(2020 - first) }
                ?.let { it * (2020 - it) }
                .also { println(it) }
    }

    override fun taskTwo() {
        for (index1 in values.indices) {
            for (index2 in (index1 + 1) until values.size) {
                for (index3 in (index2 + 1) until values.size) {
                    val v1 = values[index1]
                    val v2 = values[index2]
                    val v3 = values[index3]
                    val sum = v1 + v2 + v3
                    if (sum == 2020) {
                        println("$v1 + $v2 + $v3 = $sum -> ${v1 * v2 * v3}")
                        return
                    }
                }
            }
        }
    }
}