package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.IoUtil.readTestTaskLines
import java.lang.IllegalStateException

object Main8 : DayChallenge {

        private val values = readTaskLines()
//    private val values = readTestTaskLines()


    @JvmStatic
    fun main(args: Array<String>) {
//        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        executeOrders(values.map(this::parseOrder))
                .also { println(it) }
    }

    override fun taskTwo() {
        for (index in values.indices) {
            val line = values[index]
            if (line.startsWith("jmp") || line.startsWith("nop")) {
                val orders = switched(index)
                val result = executeOrders(orders)
                if (result.finished) {
                    println("replaced $index")
                    println(result)
                    return
                }
            }
        }
    }

    private fun switched(index: Int): List<BasicOrder> {
        return values
                .toMutableList()
                .also { it[index] = switchLine(it[index]) }
                .map(this::parseOrder)
    }

    private fun switchLine(line: String): String =
            if (line.startsWith("jmp")) line.replace("jmp", "nop") else line.replace("nop", "jmp")

    private fun executeOrders(orders: List<BasicOrder>): End {
        val machine = BasicMachine(orders)
        val executes = mutableSetOf<Int>()

        while (true) {
            when {
                machine.isFinished() -> return End(machine.accumulator, true)
                executes.add(machine.address) -> machine.step()
                else -> return End(machine.accumulator, false)
            }
        }
    }

    private fun parseOrder(line: String): BasicOrder =
            line.split(" ").let { BasicOrder(it[0], it[1].toInt()) }
}

data class End(val endValue: Int, val finished: Boolean)

private data class BasicOrder(val order: String, val value: Int)

private class BasicMachine(private val orders: List<BasicOrder>) {

    var accumulator: Int = 0
    var address: Int = 0

    fun step() {
        val order = orders[address]
//        println("execution $address -> $order")
        when (order.order) {
            "acc" -> accumulator += order.value
            "jmp" -> address += (order.value - 1)
            "nop" -> Unit
        }
        address++
    }

    fun isFinished(): Boolean = address >= orders.size
}