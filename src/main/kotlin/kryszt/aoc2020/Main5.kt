package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines

object Main5 : DayChallenge {

    private val values = readTaskLines()

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        values
                .map { it.asSeatId() }
                .maxOrNull()
                ?.also(::println)
    }

    override fun taskTwo() {
        values
                .map { it.asSeatId() }
                .sorted()
                .zipWithNext()
                .find { it.second == it.first + 2 }
                ?.also { println(it) }
                ?.also { println(it.first + 1) }
    }

    private fun String.asSeatId(): Int =
            replace('F', '0')
                    .replace('B', '1')
                    .replace('L', '0')
                    .replace('R', '1')
                    .toInt(2)
}
