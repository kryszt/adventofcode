package kryszt.aoc2020

import kryszt.tools.*
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.util.concurrent.atomic.AtomicInteger

object Main20 : DayChallenge {

    private val TILE_PATTERN = "Tile ([0-9]+):".toPattern()

    private val MONSTER_PATTERN = listOf(
        "                  # ",
        "#    ##    ##    ###",
        " #  #  #  #  #  #   "
    )

    private val tiles: List<Tile> = readTiles(IoUtil.readTaskFile(this))
//    private val tiles: List<Tile> = readTiles(IoUtil.readTaskFile(this, "-test"))

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        prepareTiles()
            .corners
            .fold(1L) { acc, tile -> acc * tile.id }
            .alsoPrint()
    }

    override fun taskTwo() {
        val puzzleData = prepareTiles()
        val borderSides = puzzleData.cornerSides.toSet()

        val topLeftCandidate = puzzleData.corners.first()
        val allTiles = puzzleData.corners.minus(topLeftCandidate).plus(puzzleData.rest).toMutableList()

        val topLeft = topLeftCandidate.toTopLeftCorner(borderSides)
        val puzzle = buildPuzzle(topLeft, allTiles, borderSides).map {
            it.map(Tile::stripFrame)
        }

        val image = joinToImage(puzzle)

        val imagePoints = asPoints(image)
        val imagePointsSet = imagePoints.toSet()

        val monsterPoints = generateMonsters()
            .map { findMonsterPoints(it, imagePointsSet) }
            .flatten()
            .toSet()

        imagePointsSet.minus(monsterPoints).size.alsoPrint()
    }

    private fun findMonsterPoints(monster: MonsterPattern, mapPoints: Set<Point>): Set<Point> {
        val maxX = mapPoints.maxByOrNull { it.x }?.x ?: 0
        val maxY = mapPoints.maxByOrNull { it.y }?.y ?: 0

        val pointsWithMonsters = mutableSetOf<Point>()

        repeat(maxX) { dx ->
            repeat(maxY) { dy ->
                val translated = monster.translate(dx, dy)
                if (translated.matches(mapPoints)) {
                    pointsWithMonsters.addAll(translated.points)
                }
            }
        }
        return pointsWithMonsters
    }

    private fun joinToImage(puzzle: List<List<Tile>>): List<String> =
        puzzle
            .map { joinRowToImage(it) }
            .flatten()

    private fun joinRowToImage(row: List<Tile>): List<String> =
        (row.first().data.indices).map { rowIndex ->
            row.joinToString("") { it.data[rowIndex] }
        }

    private fun Side.isBorder(cornerSides: Collection<Side>): Boolean {
        return cornerSides.contains(this)
    }

    private fun Tile.toTopLeftCorner(borderSides: Collection<Side>): Tile {
        var finalTile = this
        if (sideRight.isBorder(borderSides)) {
            finalTile = flipLeftRight()
        }
        if (finalTile.sideBottom.isBorder(borderSides)) {
            finalTile = finalTile.flipTopBottom()
        }
        return finalTile
    }


    private fun buildPuzzle(topLeft: Tile, otherTiles: MutableList<Tile>, borders: Set<Side>): List<List<Tile>> {
        val rows = mutableListOf<List<Tile>>()
        rows.add(buildRow(topLeft, otherTiles, borders))
        var expectedTopSide = topLeft.sideBottom

        while (!borders.contains(expectedTopSide)) {
            val nextTile = otherTiles.find { it.sides.contains(expectedTopSide) }
                ?: throw IllegalStateException("No tile matching top = $expectedTopSide")
            otherTiles.remove(nextTile)

            val nextAdjusted = nextTile.adjustTop(expectedTopSide)
            rows.add(buildRow(nextAdjusted, otherTiles, borders))
            expectedTopSide = nextAdjusted.sideBottom
        }
        return rows
    }

    private fun buildRow(startTile: Tile, otherTiles: MutableList<Tile>, borders: Set<Side>): List<Tile> {
        val tiles = mutableListOf(startTile)
        var expectedLeftSide = startTile.sideRight
        while (!borders.contains(expectedLeftSide)) {
            val nextTile = otherTiles.find { it.sides.contains(expectedLeftSide) }
                ?: throw IllegalStateException("No tile matching left = $expectedLeftSide")
            otherTiles.remove(nextTile)
            val nextAdjusted = nextTile.adjustLeft(expectedLeftSide)
            tiles.add(nextAdjusted)
            expectedLeftSide = nextAdjusted.sideRight
        }

        return tiles
    }

    private fun Tile.adjustLeft(expectedLeft: Side): Tile =
        rotateToLeftSide(expectedLeft)
            .flipLeftToDirection(expectedLeft)

    private fun Tile.adjustTop(expectedTop: Side): Tile =
        rotateToTopSide(expectedTop)
            .flipTopToDirection(expectedTop)

    private fun Tile.rotateToLeftSide(expectedLeft: Side): Tile {
        return when (expectedLeft) {
            sideLeft -> this
            sideRight -> this.flipLeftRight()
            sideTop -> this.rotateLeft()
            sideBottom -> this.rotateRight()
            else -> throw IllegalStateException("No side $expectedLeft in $this")
        }
    }

    private fun Tile.rotateToTopSide(expectedTop: Side): Tile {
        return when (expectedTop) {
            sideTop -> this
            sideBottom -> this.flipTopBottom()
            sideRight -> this.rotateLeft()
            sideLeft -> this.rotateRight()
            else -> throw IllegalStateException("No side $expectedTop in $this")
        }
    }

    private fun Tile.flipLeftToDirection(expectedLeft: Side): Tile {
        return if (expectedLeft.isReversedOf(sideLeft)) this.flipTopBottom() else this
    }

    private fun Tile.flipTopToDirection(expectedTop: Side): Tile {
        return if (expectedTop.isReversedOf(sideTop)) this.flipLeftRight() else this
    }

    private fun prepareTiles(): PuzzleData {
        val sideCounts = mutableMapOf<Side, AtomicInteger>()
        tiles.forEach { tile ->
            tile.sides.forEach { side ->
                sideCounts.getOrPut(side) { AtomicInteger(0) }.incrementAndGet()
            }
        }

        fun isCorner(tile: Tile): Boolean {
            return tile.sides.count { side -> sideCounts[side]?.get() == 1 } == 2
        }

        val cornerSides: List<Side> = sideCounts.filter { it.value.get() == 1 }.map { it.key }
        return tiles
            .partition { isCorner(it) }
            .let {
                PuzzleData(
                    corners = it.first,
                    rest = it.second,
                    cornerSides = cornerSides
                )
            }
    }

    private fun readTiles(content: String): List<Tile> {
        return content.split("\n\n").map { readTile(it) }
    }

    private fun readTile(tileString: String): Tile {
        val lines = tileString.split("\n")
        val tileId = TILE_PATTERN.matcher(lines[0])
            .takeIf { it.find() }
            ?.group(1)
            ?.toInt()
            ?: throw IllegalArgumentException("Not a tile line: ${lines[0]}")
        return Tile(tileId, lines.subList(1, lines.size))
    }

    private class Tile(val id: Int, val data: List<String>) {
        val sideTop: Side = Side(data.first())
        val sideBottom: Side = Side(data.last())
        val sideLeft: Side = Side(data.map { it[0] }.joinToString(""))
        val sideRight: Side = Side(data.map { it.last() }.joinToString(""))

        fun printTile() {
            id.alsoPrint()
            data.alsoPrintOnEach()
        }

        val sides: List<Side> = listOf(sideTop, sideRight, sideBottom, sideLeft)

        fun flipTopBottom(): Tile = Tile(id, data.reversed())

        fun flipLeftRight(): Tile = Tile(id, data.map { it.reversed() })

        fun rotateLeft(): Tile {
            val rowsToColumns = data.first().indices.reversed().map { column ->
                data.map { it[column] }.joinToString("")
            }
            return Tile(id, rowsToColumns)
        }

        fun rotateRight(): Tile {
            val rowsToColumns = data.first().indices.map { column ->
                data.map { it[column] }.reversed().joinToString("")
            }
            return Tile(id, rowsToColumns)
        }

        fun stripFrame(): Tile {
            return Tile(
                id,
                data.subList(1, data.size - 1)
                    .map { it.substring(1, it.length - 1) }
            )
        }
    }

    private data class PuzzleData(val corners: List<Tile>, val rest: List<Tile>, val cornerSides: List<Side>)

    class Side(val base: String) {
        private val reversed = base.reversed()

        override fun toString(): String = base

        override fun hashCode(): Int =
            (base.hashCode() + reversed.hashCode()) / 2

        fun isReversedOf(other: Side): Boolean =
            reversed == other.base

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Side

            return (base == other.base || reversed == other.base)
        }
    }

    private fun generateMonsters(): List<MonsterPattern> {
        val base = buildMonster()
        val allMonsters = mutableListOf<MonsterPattern>()
        var current = base
        repeat(4) {
            allMonsters.add(current)
            current = current.rotateRight()
        }
        current = base.flipTopBottom()
        repeat(4) {
            allMonsters.add(current)
            current = current.rotateRight()
        }
        return allMonsters
    }

    private fun asPoints(data: List<String>): List<Point> {
        val points = mutableListOf<Point>()
        data.forEachIndexed { y, row ->
            row.forEachIndexed { x, c ->
                if (c == '#') points.add(Point(x, y))
            }
        }
        return points
    }

    private fun buildMonster(): MonsterPattern {
        return MonsterPattern(asPoints(MONSTER_PATTERN))
    }

    private class MonsterPattern(val points: List<Point>) {

        fun normalized(): MonsterPattern {
            val minX = points.minByOrNull { it.x }?.x ?: 0
            val minY = points.minByOrNull { it.y }?.y ?: 0
            return MonsterPattern(points.map { it.move(-minX, -minY) })
        }

        fun rotateRight(): MonsterPattern =
            MonsterPattern(points.map { it.right() }).normalized()

        fun flipTopBottom(): MonsterPattern =
            MonsterPattern(points.map { it.copy(y = -it.y) }).normalized()

        fun translate(x: Int, y: Int): MonsterPattern =
            MonsterPattern(points.map { it.move(x, y) })

        fun matches(mapPoints: Collection<Point>): Boolean =
            mapPoints.containsAll(this.points)
    }
}
