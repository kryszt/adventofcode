package kryszt.aoc2020

import kryszt.tools.DayChallenge
import kryszt.tools.GroupsCollector
import kryszt.tools.IoUtil.readTaskLines

object Main4 : DayChallenge {

    private val passports = PassportCollector(readTaskLines()).parseData()

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        println(passports.count { isValidPartOne(it) })
    }

    override fun taskTwo() {
        passports.filter { isValidPartTwo(it) }
                .also { println(it.size) }
    }

    private class PassportCollector(lines: List<String>) :
            GroupsCollector<Passport>(lines, { mutableMapOf() }) {

        override fun handleLine(current: Passport, line: String) {
            line
                    .split(" ")
                    .forEach { it.split(":").also { entry -> updateValue(current, entry.first(), entry.last()) } }
        }

        private fun updateValue(current: Passport, key: String, value: String) {
            current[key] = value
        }
    }

    private val requiredFields = listOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")

    private val validEyeColors = setOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")

    private val fieldValidators: Map<String, (String) -> Boolean> = mapOf(
            "byr" to { value -> value.toIntOrNull()?.let { it in 1920..2002 } ?: false },
            "iyr" to { value -> value.toIntOrNull()?.let { it in 2010..2020 } ?: false },
            "eyr" to { value -> value.toIntOrNull()?.let { it in 2020..2030 } ?: false },
            "hgt" to { value -> validateHeightString(value) },
            "hcl" to { value -> "#[0-9a-f]{6}".toRegex().matches(value) },
            "ecl" to { value -> validEyeColors.contains(value) },
            "pid" to { value -> "[0-9]{9}".toRegex().matches(value) }
    )

    private fun validateHeightString(text: String): Boolean {
        return when {
            text.endsWith("cm") -> text.take(text.length - 2).toIntOrNull()?.let { it in 150..193 } ?: false
            text.endsWith("in") -> text.take(text.length - 2).toIntOrNull()?.let { it in 59..76 } ?: false
            else -> false
        }
    }

    private fun isValidPartOne(passport: Passport): Boolean =
            requiredFields.all { key -> passport[key]?.isNotEmpty() ?: false }

    private fun isValidPartTwo(passport: Passport): Boolean =
            fieldValidators.all { passport[it.key]?.let(it.value) ?: false }
}

private typealias Passport = MutableMap<String, String>
