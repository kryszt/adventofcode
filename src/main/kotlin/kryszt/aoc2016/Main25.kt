package kryszt.aoc2016

object Main25 {

    const val input = "cpy a d\n" +
            "cpy 11 c\n" +
            "cpy 231 b\n" +
            "inc d\n" +
            "dec b\n" +
            "jnz b -2\n" +
            "dec c\n" +
            "jnz c -5\n" +
            "cpy d a\n" +
            "jnz 0 0\n" +
            "cpy a b\n" +
            "cpy 0 a\n" +
            "cpy 2 c\n" +
            "jnz b 2\n" +
            "jnz 1 6\n" +
            "dec b\n" +
            "dec c\n" +
            "jnz c -4\n" +
            "inc a\n" +
            "jnz 1 -7\n" +
            "cpy 2 b\n" +
            "jnz c 2\n" +
            "jnz 1 4\n" +
            "dec b\n" +
            "dec c\n" +
            "jnz 1 -4\n" +
            "jnz 0 0\n" +
            "out b\n" +
            "jnz a -19\n" +
            "jnz 1 -21"

    @JvmStatic
    fun main(args: Array<String>) {
        val instructions = input.split("\n").map { it.split(" ").toMutableList() }

        javaClass.classLoader.getResource("")

        val processor = Main25Processor(instructions)

        processor.write("a", 189)

        println("Starting")
        while (processor.canStep() && processor.steps < 100000) {
            processor.step()
        }

        println("At a: ${processor.valueOrRegister("a")}")
    }

    private fun emulate(startA: Int) {
        var a = startA
        var b = 0
        val d = (a + (11 * 231))


        do {
            a = d//9
            do {
                b = a//11
                a = b / 2
                b %= 2
                println(b)//28
            } while (a != 0)//29
        } while (true)//30


    }

    class Main25Processor(instructions: List<MutableList<String>>) : Main23.Main23Processor(instructions) {

        override fun executeInstruction(order: String, args: List<String>): Boolean {
            when (order) {
                "out" -> println("Signal ${valueOrRegister(args[0])} at $steps")
            }

            return super.executeInstruction(order, args)
        }
    }


}
