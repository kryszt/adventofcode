package kryszt.aoc2016

import kryszt.tools.IoUtil
import java.util.regex.Pattern

object Main8 {

    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\2016\\8.txt"

    const val input = "rect 3x2\n" +
            "rotate column x=1 by 1\n" +
            "rotate row y=0 by 4\n" +
            "rotate column x=1 by 1"

    val PatternRec = "rect ([0-9]+)x([0-9]+)".toPattern()
    val PatternRotateRow = "rotate row y=([0-9]+) by ([0-9]+)".toPattern()
    val PatternRotateColumn = "rotate column x=([0-9]+) by ([0-9]+)".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {
        val lines = IoUtil.readTaskFileLines(this)
//        val lines = input.split("\n")
//        val lines = input2.split("\n")
        val parts = lines.map { splitLine(it) }
        val screen = Screen(50, 6)
//        val screen = Screen(7, 3)
        parts.forEach {
            println(it)
            execute(it, screen)
            println(screen.toString())
        }

        println("On are: ${screen.countOn()}")

    }

    fun execute(instruction: Instruction, screen: Screen) {
        when (instruction.action) {
            "rec" -> screen.fill(instruction.val1, instruction.val2)
            "rotX" -> screen.rotateColumn(instruction.val1, instruction.val2)
            "rotY" -> screen.rotateRow(instruction.val1, instruction.val2)
        }
    }

    fun splitLine(line: String): Instruction {
        return splitLine(line, "rec", PatternRec)
                ?: splitLine(line, "rotX", PatternRotateColumn)
                ?: splitLine(line, "rotY", PatternRotateRow)
                ?: throw IllegalArgumentException("Cannot parse $line")
    }

    fun splitLine(line: String, action: String, pattern: Pattern): Instruction? {
        val m = pattern.matcher(line)
        if (m.find()) {
            return Instruction(action, m.group(1).toInt(), m.group(2).toInt())
        }
        return null
    }

    data class Instruction(val action: String, val val1: Int, val val2: Int)

    class Screen(val width: Int, val height: Int) {
        val content: Array<Array<Int>> = Array(height, { Array(width, { 0 }) })

        fun fill(columns: Int, rows: Int) {
            for (r in 0 until rows) {
                for (c in 0 until columns) {
                    content[r][c] = 1
                }
            }
        }

        fun rotateRow(row: Int, move: Int) {
            val rowValues = content[row].copyOf()
            for (c in 0 until rowValues.size) {
                content[row][(c + move) % width] = rowValues[c]
            }
        }

        fun rotateColumn(column: Int, move: Int) {
            val columnValues = content.map { it[column] }
            for (r in 0 until columnValues.size) {
                content[(r + move) % height][column] = columnValues[r]
            }
        }

        override fun toString(): String {
            return content.joinToString("\n", transform = { it.joinToString("", transform = { if (it == 0) " " else "#" }) })
        }

        fun countOn(): Int {
            return content.sumBy { it.sumBy { if (it == 0) 0 else 1 } }
        }
    }
}
