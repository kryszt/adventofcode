package kryszt.aoc2016

object Main16 {

    @JvmStatic
    fun main(args: Array<String>) {
//        println(expand("11100010111110100", 272))
        println(checksum(expand("11100010111110100", 272)))
        println(checksum(expand("11100010111110100", 35651584)))
    }

    fun expand(base: String, size: Int): String {
        val expanded = base + '0' + base.reversed().replace('1', '2').replace('0', '1').replace('2', '0')
        return if (expanded.length < size) {
            expand(expanded, size)
        } else {
            expanded.substring(0, size)
        }
    }

    fun checksum(value: String): String {
        return if (value.length % 2 == 1) {
            value
        } else {
            val buffer = StringBuffer()
            for (i in 0 until value.length step 2) {
                val c = if (value[i] == value[i + 1]) '1' else '0'
                buffer.append(c)
            }
            checksum(buffer.toString())
        }
    }

}
