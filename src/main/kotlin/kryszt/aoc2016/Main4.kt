package kryszt.aoc2016

import kryszt.tools.IoUtil
import kryszt.tools.SummerValue

object Main4 {
    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\2016\\4.txt"

    @JvmStatic
    fun main(args: Array<String>) {
        var sum = 0

        IoUtil.forEachLine(this) {
            val room = parseLine(it)
            if (room.checksum == checksum(room.name)) {
                sum += room.sector
                println("valid $room")
            }
        }
        println("sum $sum")
    }

    private fun parseLine(text: String): Room {
        val lastDash = text.lastIndexOf("-")
        val sumOpen = text.lastIndexOf("[")
        val name = text.substring(0, lastDash)
        val sector = text.substring(lastDash + 1, sumOpen).toInt()
        val sum = text.substring(sumOpen + 1, text.length - 1)


        return Room(name, sector, sum, decryptName(name, sector))
    }

    private fun decryptName(name: String, code: Int): String {
        return name.map {
            when (it) {
                '-' -> ' '
                else -> 'a' + (((it - 'a') + code) % 26)
            }
        }.joinToString("")
    }

    private fun checksum(text: String): String {
        val chars = mutableMapOf<Char, SummerValue>()
        text.replace("-", "").forEach {
            chars.getOrPut(it, { SummerValue(it) }).count++
        }
        return chars.values.sorted().subList(0, 5).joinToString(separator = "", transform = { it.char.toString() })
    }

    data class Room(val name: String, val sector: Int, val checksum: String, val decryptedName: String)

}
