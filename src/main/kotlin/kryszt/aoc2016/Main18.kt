package kryszt.aoc2016

object Main18 {

    //    const val FirstRow = "..^^."
//    const val FirstRow = ".^^.^.^^^^"
    const val FirstRow = ".^^^^^.^^.^^^.^...^..^^.^.^..^^^^^^^^^^..^...^^.^..^^^^..^^^^...^.^.^^^^^^^^....^..^^^^^^.^^^.^^^.^^"

    @JvmStatic
    fun main(args: Array<String>) {
        var row = FirstRow
        println(row)
        var safeCount = row.count { it == '.' }
        for (i in 1 until 400000) {
            row = nextRow(row)
            safeCount += row.count { it == '.' }
        }
        println(row)

        println()
        println("safe $safeCount")
    }

    fun nextRow(row: String): String {
        val buffer = StringBuffer()
        val expanded = ".$row."
        for (i in 0 until row.length) {
            val above = expanded.substring(i, i + 3)
            buffer.append(asChar(willBeTrap(above)))
        }
        return buffer.toString()
    }

    fun asChar(isTrap: Boolean) = if (isTrap) '^' else '.'

    fun willBeTrap(above: String): Boolean =
            when (above) {
                "^^." -> true
                ".^^" -> true
                "^.." -> true
                "..^" -> true
                else -> false
            }
}
