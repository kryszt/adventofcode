package kryszt.aoc2016

import kryszt.tools.IoUtil

object Main9 {

    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\2016\\9.txt"

    const val input = "ADVENT\n" +
            "A(1x5)BC\n" +
            "(3x3)XYZ\n" +
            "A(2x2)BCD(2x2)EFG\n" +
            "(6x1)(1x3)A\n" +
            "X(8x2)(3x3)ABCY\n" +
            "(27x12)(20x12)(13x14)(7x10)(1x12)A\n" +
            "(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN"

    @JvmStatic
    fun main(args: Array<String>) {
        val lines = IoUtil.readTaskFileLines(this)
//        val lines = input.split("\n")

        lines.forEach {
            //            val decompressed = decompress(it)
//            val decompressed2 = decompress2(it)
            val size2 = decompressLength(it)
//            println("$it -> l = ${decompressed.length}")
//            println("$it -> l2 = ${decompressed2.length}")
            println("$it -> l2 = $size2")
//            if (size2 != decompressed2.length) {
//                throw IllegalStateException("Wrong size")
//            }
        }
    }

    fun decompress(text: String): String {
        val builder = StringBuilder()
        var index = 0


        do {
            val nextBracket = text.indexOf('(', index)
            if (nextBracket < 0) {
                builder.append(text.substring(index, text.length))
                index = text.length
            } else {
                builder.append(text.substring(index, nextBracket))
                val endBracket = text.indexOf(')', nextBracket)
                val marker = parseRepetition(text.substring(nextBracket + 1, endBracket))
                val repeatString = text.substring(endBracket + 1, endBracket + 1 + marker.charCount)
                builder.append(repeatString.repeat(marker.repeats))
                index = endBracket + 1 + marker.charCount
            }

        } while (index < text.length)
        return builder.toString()
    }

    fun decompress2(text: String): String {
        val builder = StringBuilder()
        var index = 0


        do {
            val nextBracket = text.indexOf('(', index)
            if (nextBracket < 0) {
                builder.append(text.substring(index, text.length))
                index = text.length
            } else {
                builder.append(text.substring(index, nextBracket))
                val endBracket = text.indexOf(')', nextBracket)
                val marker = parseRepetition(text.substring(nextBracket + 1, endBracket))
                val repeatString = text.substring(endBracket + 1, endBracket + 1 + marker.charCount)
                val decompressedRepeatString = decompress2(repeatString)
                builder.append(decompressedRepeatString.repeat(marker.repeats))
                index = endBracket + 1 + marker.charCount
            }

        } while (index < text.length)
        return builder.toString()
    }

    fun decompressLength(text: String): Long {
        var size: Long = 0
        var index = 0


        do {
            val nextBracket = text.indexOf('(', index)
            if (nextBracket < 0) {
                size += (text.length - index)
//                builder.append(text.substring(index, text.length))
                index = text.length
            } else {
                size += nextBracket - index
//                builder.append(text.substring(index, nextBracket))
                val endBracket = text.indexOf(')', nextBracket)
                val marker = parseRepetition(text.substring(nextBracket + 1, endBracket))
                val repeatString = text.substring(endBracket + 1, endBracket + 1 + marker.charCount)
                val decompressedRepeatLength = decompressLength(repeatString)
                size += (decompressedRepeatLength * marker.repeats)
//                builder.append(decompressedRepeatString.repeat(marker.repeats))
                index = endBracket + 1 + marker.charCount
            }

        } while (index < text.length)
        return size
    }

    fun parseRepetition(marker: String): Marker {
        return marker.split("x").let { Marker(it[0].toInt(), it[1].toInt()) }
    }

    data class Marker(val charCount: Int, val repeats: Int)
}
