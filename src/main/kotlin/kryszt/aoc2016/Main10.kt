package kryszt.aoc2016

import kryszt.tools.IoUtil

object Main10 {

    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\2016\\10.txt"

    const val input = "value 5 goes to bot 2\n" +
            "bot 2 gives low to bot 1 and high to bot 0\n" +
            "value 3 goes to bot 1\n" +
            "bot 1 gives low to output 1 and high to bot 0\n" +
            "bot 0 gives low to output 2 and high to output 0\n" +
            "value 2 goes to bot 2"

    const val groupNumber = "([0-9]+)"
    const val groupText = "([a-zA-Z]+)"

    val PatternInput = "value $groupNumber goes to $groupText $groupNumber".toPattern()
    val PatternBot = "bot $groupNumber gives low to $groupText $groupNumber and high to $groupText $groupNumber".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {
        val lines = IoUtil.readTaskFileLines(this)
//        val lines = input.split("\n")

        val world = World()


        lines.forEach { line ->
            parseInput(line)?.let { world.inputs.add(it) }
            parseBot(line)?.let { world.bots.put(it.botId, it) }
        }

        println(world.inputs)
        println(world.bots)

        world.inputs.forEach {
            world.passValue(it)
        }
        println("Outputs: ${world.outputs}")
    }

    fun parseInput(line: String): Input? =
            PatternInput.matcher(line).takeIf { it.matches() }?.let {
                Input(target = Main10.Target(it.group(2), it.group(3).toInt()), value = it.group(1).toInt())
            }

    fun parseBot(line: String): Bot? =
            PatternBot.matcher(line).takeIf { it.matches() }?.let {
                Bot(botId = it.group(1).toInt(),
                        lowTarget = Main10.Target(it.group(2), it.group(3).toInt()),
                        highTarget = Main10.Target(it.group(4), it.group(5).toInt()))
            }

    data class Input(val target: Target, val value: Int)

    data class Target(val type: String, val id: Int)

    data class Bot(val botId: Int, val lowTarget: Target, val highTarget: Target) {
        val hold = mutableListOf<Int>()

        fun addValue(value: Int, world: World) {
            hold.add(value)
            if (hold.size == 2) {
                println("Bot $botId : ${hold[0]}, ${hold[1]}")
                world.passValue(lowTarget, hold.minOrNull()!!)
                world.passValue(highTarget, hold.maxOrNull()!!)
            }
        }
    }

    class World {

        val inputs = mutableListOf<Input>()
        val outputs = sortedMapOf<Int, MutableList<Int>>()
        val bots = sortedMapOf<Int, Bot>()

        fun passValue(input: Input) {
            passValue(input.target, input.value)
        }

        fun passValue(target: Target, value: Int) {
            when (target.type) {
                "bot" -> bots[target.id]!!.addValue(value, this)
                "output" -> outputs.getOrPut(target.id, { mutableListOf() }).add(value)
            }
        }

    }

}
