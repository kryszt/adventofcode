package kryszt.aoc2016

import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter

object Main5 {

//    const val base = "abc"
    const val base = "ffykfhsq"


    @JvmStatic
    fun main(args: Array<String>) {
        val digest = MessageDigest.getInstance("MD5")

        var index = 0

        val buffer = arrayOfNulls<Char>(8)

        do {
            if ((index % 1000000) == 0) {
                println("at $index")
            }
            val text = "$base$index"

            val digestText = digest.digest(text.toByteArray()).let { DatatypeConverter.printHexBinary(it) }
            if (digestText.startsWith("00000")) {
                println("at $index ${digestText[5]} -> $digestText")
                val position = digestText[5].toString().toIntOrNull()
                val value = digestText[6]
                if (position != null && position > -1 && position < 8) {
                    if (buffer[position] == null) {
                        buffer[position] = value
                    }
                    println("buffer: ${buffer.joinToString(separator = "", transform = { it?.toString() ?: "_" })}")

                    if (buffer.all { it != null }) {
                        break
                    }
                }
            }
            index++

        } while (true)
        println("pass: ${buffer.joinToString(separator = "").toLowerCase()}")
    }

    @JvmStatic
    fun main2(args: Array<String>) {
        val digest = MessageDigest.getInstance("MD5")

        var index = 0
        var count = 0

        val buffer = StringBuffer()

        do {
            if ((index % 1000000) == 0) {
                println("at $index")
            }
            val text = "$base$index"

            val digestText = digest.digest(text.toByteArray()).let { DatatypeConverter.printHexBinary(it) }
            if (digestText.startsWith("00000")) {
                count++
                println("at $index ${digestText[5]} -> $digestText")
                buffer.append(digestText[5])
            }
            index++

        } while (count < 8)
        println(buffer.toString().toLowerCase())
    }

}
