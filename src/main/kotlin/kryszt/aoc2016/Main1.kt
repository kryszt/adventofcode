package kryszt.aoc2016

import kryszt.tools.Point
import kotlin.math.absoluteValue

object Main1 {

    val text = "L2, L3, L3, L4, R1, R2, L3, R3, R3, L1, L3, R2, R3, L3, R4, R3, R3, L1, L4, R4, L2, R5, R1, L5, R1, R3, L5, R2, L2, R2, R1, L1, L3, L3, R4, R5, R4, L1, L189, L2, R2, L5, R5, R45, L3, R4, R77, L1, R1, R194, R2, L5, L3, L2, L1, R5, L3, L3, L5, L5, L5, R2, L1, L2, L3, R2, R5, R4, L2, R3, R5, L2, L2, R3, L3, L2, L1, L3, R5, R4, R3, R2, L1, R2, L5, R4, L5, L4, R4, L2, R5, L3, L2, R4, L1, L2, R2, R3, L2, L5, R1, R1, R3, R4, R1, R2, R4, R5, L3, L5, L3, L3, R5, R4, R1, L3, R1, L3, R3, R3, R3, L1, R3, R4, L5, L3, L1, L5, L4, R4, R1, L4, R3, R3, R5, R4, R3, R3, L1, L2, R1, L4, L4, L3, L4, L3, L5, R2, R4, L2"
//    val text = "R2, L3"
//    val text = "R2, R2, R2"
//    val text = "R5, L5, R5, R3"
//    val text = "R8, R4, R4, R8"

    @JvmStatic
    fun main(args: Array<String>) {
        val moves = parseMoves(text)
        val visited: MutableSet<Point> = mutableSetOf()

        var position = Point()
        var direction = Point.Up

        println("starting")
        moves.forEach {
            direction = if (it.turn == 'L') direction.left() else direction.right()
            (1..it.steps).forEach {
                position = position.move(direction)
                if (!visited.add(position)) {
                    println("Visited again $position -> ${distance(position)}")
                }
            }
        }

        println("sum ${distance(position)}")
    }

    fun parseMoves(txt: String): List<Move> {
        return txt.split(",").map { parseMove(it.trim()) }
    }

    fun parseMove(txt: String): Move {
        return Move(
                txt[0],
                steps = txt.substring(1).toInt()
        )
    }

    fun distance(p: Point): Int = p.x.absoluteValue + p.y.absoluteValue

    data class Move(val turn: Char, val steps: Int)
}
