package kryszt.aoc2016

import kryszt.tools.BasicProcessor

object Main12 {

    const val testInput = "cpy 41 a\n" +
            "inc a\n" +
            "inc a\n" +
            "dec a\n" +
            "jnz a 2\n" +
            "dec a"

    const val input = "cpy 1 a\n" +
            "cpy 1 b\n" +
            "cpy 26 d\n" +
            "jnz c 2\n" +
            "jnz 1 5\n" +
            "cpy 7 c\n" +
            "inc d\n" +
            "dec c\n" +
            "jnz c -2\n" +
            "cpy a c\n" +
            "inc a\n" +
            "dec b\n" +
            "jnz b -2\n" +
            "cpy c b\n" +
            "dec d\n" +
            "jnz d -6\n" +
            "cpy 16 c\n" +
            "cpy 17 d\n" +
            "inc a\n" +
            "dec d\n" +
            "jnz d -2\n" +
            "dec c\n" +
            "jnz c -5"

    @JvmStatic
    fun main(args: Array<String>) {
        val instructions = input.split("\n").map { it.split(" ").toMutableList() }
//        val instructions = testInput.split("\n").map { it.split(" ") }

        val processor = Main12Processor(instructions)

        processor.write("c", 1)

        while (processor.canStep()) {
            processor.step()
        }

        println("At a: ${processor.valueOrRegister("a")}")
    }

    class Main12Processor(instructions: List<MutableList<String>>) : BasicProcessor(instructions) {

        override fun executeInstruction(order: String, args: List<String>): Boolean {
            when (order) {
                "cpy" -> write(args[1], valueOrRegister(args[0]))
                "inc" -> write(args[0], valueOrRegister(args[0]) + 1)
                "dec" -> write(args[0], valueOrRegister(args[0]) - 1)
                "jnz" -> if (valueOrRegister(args[0]) != 0) {
                    movePointer(valueOrRegister(args[1]))
                    return false
                }
            }

            return super.executeInstruction(order, args)
        }
    }
}
