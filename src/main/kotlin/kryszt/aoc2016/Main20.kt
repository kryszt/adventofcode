package kryszt.aoc2016

import kryszt.tools.IoUtil

object Main20 {

    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\2016\\20.txt"

    val input = "5-8\n" +
            "0-2\n" +
            "4-7"

    @JvmStatic
    fun main(args: Array<String>) {
//        val rules = input.split("\n").map { splitLine(it) }.sortedBy { it.from }
        val rules = IoUtil.readTaskFileLines(this).map { splitLine(it) }.sortedBy { it.from }

//        println(rules)

        println(findLowest(rules))
//        println(countAllowed(rules, 9))
        println(countAllowed(rules, 4294967295))
    }

    private fun findLowest(rules: List<Range>): Long {
        var lowestPossible = 0L
        rules.forEach {
            if (lowestPossible < it.from) {
                return lowestPossible
            } else {
                lowestPossible = Math.max(it.to + 1, lowestPossible)
            }
        }
        return lowestPossible
    }

    private fun countAllowed(rules: List<Range>, max: Long): Long {
        var count = 0L
        var lowestPossible = 0L
        rules.forEach {
            if (lowestPossible < it.from) {
                println("possible $lowestPossible through ${it.from - 1} (${it.from - lowestPossible})")
                count += (it.from - lowestPossible)
            }
            lowestPossible = Math.max(it.to + 1, lowestPossible)
        }
        if (lowestPossible <= max) {
            println("possible $lowestPossible through $max")
            count += (max - lowestPossible + 1)
        }
        return count
    }

    fun splitLine(line: String) = line.split("-").let {
        Range(it[0].toLong(), it[1].toLong())
    }

    data class Range(val from: Long, val to: Long)
}
