package kryszt.aoc2016

import kryszt.tools.IoUtil

object Main7 {

    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\2016\\7.txt"

    const val input = "abba[mnop]qrst\n" +
            "abcd[bddb]xyyx\n" +
            "aaaa[qwer]tyui\n" +
            "ioxxoj[asdfgh]zxcvbn"
    const val input2 = "aba[bab]xyz\n" +
            "xyx[xyx]xyx\n" +
            "aaa[kek]eke\n" +
            "zazbz[bzb]cdb"

    @JvmStatic
    fun main(args: Array<String>) {
        val lines = IoUtil.readTaskFileLines(this)
//        val lines = input.split("\n")
//        val lines = input2.split("\n")
        val parts = lines.map { splitLine(it) }

        var countTLS = 0
        var countSSL = 0
        parts.forEach { p ->
            if (supportsTLS(p)) {
                countTLS++
                println("Found TLS in $p")
            }
            if (supportsSSL(p)) {
                println("Found SSL in $p")
                countSSL++
            }
        }
        println("Found TLS $countTLS")
        println("Found SSL $countSSL")
    }

    fun supportsTLS(p: Parts): Boolean {
        if (p.inside.any { isAbba(it) }) {
//            println("no, found inside")
        } else if (p.outside.any { isAbba(it) }) {
//            println("yes, found outside")
            return true
        } else {
//            println("Not Found")
        }
        return false
    }

    fun supportsSSL(p: Parts): Boolean {

        p.outside.forEach {
            val babs = findBaBs(it)
//            println("in $it -> $babs")

            if (babs.any { bab ->
                        p.inside.any { inside -> inside.contains(bab) }
                    }) {
                return true
            }
        }
        return false

    }

    fun isAbba(text: String): Boolean {
        for (i in 0 until text.length - 3) {
            if ((text[i] == text[i + 3]) && (text[i + 1] == text[i + 2]) && (text[i] != text[i + 1])) {
                return true
            }
        }
        return false
    }

    fun findBaBs(text: String): List<String> {
        val babs = mutableListOf<String>()
        for (i in 0 until text.length - 2) {
            if ((text[i] == text[i + 2]) && (text[i] != text[i + 1])) {
                babs.add("${text[i + 1]}${text[i]}${text[i + 1]}")
            }
        }
        return babs
    }

    fun splitLine(line: String): Parts {
        val split: Map<Int, List<String>> = line.split("[\\[|\\]]".toRegex()).withIndex()
                .groupBy { it.index % 2 }
                .mapValues { it.value.map { it.value } }
        return Parts(split[0] ?: emptyList(), split[1] ?: emptyList())
    }

    data class Parts(val outside: List<String>, val inside: List<String>)
}
