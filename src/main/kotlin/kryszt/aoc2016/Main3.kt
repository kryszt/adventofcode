package kryszt.aoc2016

import kryszt.tools.IoUtil

object Main3 {

    @JvmStatic
    fun main(args: Array<String>) {
        val values: List<List<Int>> = IoUtil.readTaskFileLines(this).map { it.trim().split("\\s+".toRegex()).map { it.toInt() } }

        var count = 0
        for (y in 0 until values.size step 3) {
            for (x in 0 until values[y].size) {
                val lengths = listOf(values[y][x], values[y + 1][x], values[y + 2][x])
                if (isTriangle(lengths)) {
                    count++
                }


            }
        }
        println("triangles: " + count)

//        var count = 0
//        println("is triangle: ${isTriangle(listOf(5, 10, 25))}")
//        IoUtil.forEachLine(this) {
//            val lengths = it.trim().split("\\s+".toRegex()).map { it.toInt() }
//            if (isTriangle(lengths)) {
//                count++
//            }
//        }
//        println("triangles: " + count)
    }

    @JvmStatic
    fun main1(args: Array<String>) {
        var count = 0
        println("is triangle: ${isTriangle(listOf(5, 10, 25))}")
        IoUtil.forEachLine(this) {
            val lengths = it.trim().split("\\s+".toRegex()).map { it.toInt() }
            if (isTriangle(lengths)) {
                count++
            }
        }
        println("triangles: " + count)
    }

    fun isTriangle(lengths: List<Int>): Boolean {
        return if (lengths.size == 3) {
            val sum = lengths.sum()
            val max = lengths.maxOrNull() ?: 0
            max < (sum - max)
        } else {
            false
        }
    }

}
