package kryszt.aoc2016

import kryszt.tools.BasicProcessor

object Main23 {

    const val testInput = "cpy 2 a\n" +
            "tgl a\n" +
            "tgl a\n" +
            "tgl a\n" +
            "cpy 1 a\n" +
            "dec a\n" +
            "dec a"

    const val input = "cpy a b\n" +
            "dec b\n" +
            "cpy a d\n" +
            "cpy 0 a\n" +
            "cpy b c\n" +
            "inc a\n" +
            "dec c\n" +
            "jnz c -2\n" +
            "dec d\n" +
            "jnz d -5\n" +
            "dec b\n" +
            "cpy b c\n" +
            "cpy c d\n" +
            "dec d\n" +
            "inc c\n" +
            "jnz d -2\n" +
            "tgl c\n" +
            "cpy -16 c\n" +
            "jnz 1 c\n" +
            "cpy 98 c\n" +
            "jnz 86 d\n" +
            "inc a\n" +
            "inc d\n" +
            "jnz d -2\n" +
            "inc c\n" +
            "jnz c -5"

    @JvmStatic
    fun main(args: Array<String>) {
        val instructions = input.split("\n").map { it.split(" ").toMutableList() }
//        val instructions = testInput.split("\n").map { it.split(" ").toMutableList() }

        val processor = Main23Processor(instructions)

        processor.write("a", 12)

        while (processor.canStep()) {
            processor.step()
        }

        println("At a: ${processor.valueOrRegister("a")}")
    }

    open class Main23Processor(instructions: List<MutableList<String>>) : BasicProcessor(instructions) {

        fun toggleAt(index: Int) {
            instructions.getOrNull(index)?.apply {
                if (size == 2) {
                    if (get(0) == "inc") {
                        set(0, "dec")
                    } else {
                        set(0, "inc")
                    }
                }
                if (size == 3) {
                    if (get(0) == "jnz") {
                        set(0, "cpy")
                    } else {
                        set(0, "jnz")
                    }
                }
            }
        }

        override fun executeInstruction(order: String, args: List<String>): Boolean {
            when (order) {
                "cpy" -> if (isRegister(args[1])) {
                    write(args[1], valueOrRegister(args[0]))
                }
                "inc" -> write(args[0], valueOrRegister(args[0]) + 1)
                "dec" -> write(args[0], valueOrRegister(args[0]) - 1)
                "jnz" -> if (valueOrRegister(args[0]) != 0) {
                    movePointer(valueOrRegister(args[1]))
                    return false
                }
                "tgl" -> toggleAt(nextInstruction.toInt() + valueOrRegister(args[0]))

            }

            return super.executeInstruction(order, args)
        }
    }
}
