package kryszt.aoc2016

import kryszt.tools.MazeSolver
import kryszt.tools.Point

object Main13 {

    @JvmStatic
    fun main(args: Array<String>) {
//        val solver = MazeSolver13(1364, Point(31, 39))
//        val path = solver.solve(listOf(MazeSolver.Path("", Point(1, 1))))
//        val solver = MazeSolver13(10, Point(7, 4))
//        val path = solver.solve(listOf(MazeSolver.Path("", Point(1, 1))))
//        val path = solver.checkAllMovesDeep(MazeSolver.Path("", Point(1, 1)))

        val solver = MazeSolver13(1364, Point(50, 50), 50)
//        val path = solver.solve(listOf(MazeSolver.Path("", Point(1, 1))))
//        val solver = MazeSolver13(10, Point(50, 50), 2)
        val path = solver.solve(listOf(MazeSolver.Path("", Point(1, 1))))

        println(path)
        println(solver.visited)
        println(solver.visited.size)
    }

    class MazeSolver13(val addValue: Int, val end: Point, val maxSteps: Int = Integer.MAX_VALUE / 2) : MazeSolver() {

        override fun isEnd(path: Path): Boolean {
            return path.place == end
        }

        override fun allowed(path: Path): Boolean {
            return when {
                path.lenght > maxSteps -> false
                wasVisited(path.place) -> false
                path.place.x < 0 || path.place.y < 0 -> false
                else -> with(path.place) {
                    x * x + 3 * x + 2 * x * y + y + y * y + addValue
                }.let {
                    Integer.bitCount(it) % 2 == 0
                }
            }
        }

    }

}
