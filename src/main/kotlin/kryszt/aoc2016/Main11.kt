package kryszt.aoc2016

import kryszt.tools.Solver

object Main11 {

    const val Floors = 4

    @JvmStatic
    fun main(args: Array<String>) {

//        val endState = State(3, listOf(3, 3, 3, 3))
//        val startState = State(0, listOf(1, 0, 2, 0))
//        val endState = State(3, Array(10, { 3 }).toList())
//        val startState = State(0, listOf(0, 0, 0, 0, 1, 2, 1, 1, 1, 1))
        val endState = State(3, Array(14, { 3 }).toList())
        val startState = State(0, listOf(0, 0, 0, 0, 1, 2, 1, 1, 1, 1, 0, 0, 0, 0))

        println("Start ${startState.stateText}")
        println("End   ${endState.stateText}")
        val solver = Solver11(endState)
        (0 until Floors).forEach {
            println("$it : ${solver.isFloorValid(startState.parts, it)}")
        }
//
        val found = solver.solve(listOf(startState))

        println("Found $found in ${solver.stepsCount}")
//        println("Start ${startState.stateText}")
//        solver.selectMoves(startState).forEach {
//            println("Possible next: ${it.stateText}")
//        }

    }

    class Solver11(val endState: State) : Solver<State>() {

        val visited = mutableSetOf<String>()

        override fun visiting(state: State) {
            visited.add(state.stateText)
        }

        override fun wasVisited(state: State): Boolean = visited.contains(state.stateText)

        override fun isEnd(state: State): Boolean = endState.stateText == state.stateText

        override fun selectMoves(state: State): List<State> {
            val collector = mutableListOf<State>()

            (state.elevator - 1).takeIf { inFloors(it) }?.let { nextFloor -> generateMoves(state, nextFloor, collector) }
            (state.elevator + 1).takeIf { inFloors(it) }?.let { nextFloor -> generateMoves(state, nextFloor, collector) }

            return collector.filter { allowed(it) }
        }

        fun generateMoves(state: State, toFloor: Int, into: MutableList<State>) {
            val fromFloor: Int = state.elevator
            val onFloor = findAllOnLevel(state, fromFloor)
            val floorArray = state.parts.toIntArray()

            onFloor.forEach { first ->
                onFloor.forEach { second ->
                    floorArray[first] = toFloor
                    floorArray[second] = toFloor
                    into.add(Main11.State(toFloor, floorArray.toList()))
                    floorArray[first] = fromFloor
                    floorArray[second] = fromFloor
                }
            }
        }

        fun findAllOnLevel(state: State, floor: Int): List<Int> {
            val test = state.parts
            return test.mapIndexedNotNull { index, f -> index.takeIf { f == floor } }
        }

        override fun allowed(state: State): Boolean {
            return when {
                wasVisited(state) -> false
                else -> isValidState(state)
            }
        }

        fun inFloors(floor: Int): Boolean = ((floor >= 0) and (floor < Floors))

        fun isValidState(state: State): Boolean {
            return inFloors(state.elevator) and (state.parts.all { inFloors(it) }) and (0 until Floors).all { f -> isFloorValid(state.parts, f) }
        }

        fun isFloorValid(parts: List<Int>, floor: Int): Boolean {
            return (!hasGenerators(parts, floor) || !hasUnshieldedChip(parts, floor))
        }

        private fun hasUnshieldedChip(parts: List<Int>, floor: Int): Boolean {
            for (i in 1 until parts.size step 2) {
                if (parts[i] == floor && parts[i - 1] != floor) {
                    return true
                }
            }
            return false
        }

        private fun hasGenerators(parts: List<Int>, floor: Int): Boolean {
            for (i in 0 until parts.size step 2) {
                if (parts[i] == floor) {
                    return true
                }
            }
            return false
        }
    }

    data class State(val elevator: Int, val parts: List<Int>) {

        val stateText: String by lazy { parts.joinToString(separator = "-", prefix = "$elevator:") }

    }

}
