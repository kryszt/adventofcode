package kryszt.aoc2016

import kryszt.tools.IoUtil
import kryszt.tools.MazeSolver
import kryszt.tools.Point
import java.util.*

object Main24 {

    const val testInput = "###########\n" +
            "#0.1.....2#\n" +
            "#.#######.#\n" +
            "#4.......3#\n" +
            "###########"

    @JvmStatic
    fun main(args: Array<String>) {
//        val maze = testInput.split("\n")
        val maze = IoUtil.readTaskFileLines(this)


        val points = findPoints(maze)
        val distances = buildDistances(maze)

        println(distances)

        val shortest = findShortestWithReturn(distances, points.size)
        println(shortest)
    }

    fun buildDistances(maze: List<String>): Map<Point, Int> {
        val points = findPoints(maze)
        println(points)

        val distances = mutableMapOf<Point, Int>()

        points.forEach { start ->
            points.forEach { end ->

                val solver = MazeSolver24(maze, end.value)
                val path = solver.checkAllMovesWide(start.value)

                distances[Point(start.key.toString().toInt(), end.key.toString().toInt())] = path!!.lenght
                println("Path ${start.key} -> ${end.key} = ${path.lenght}")
            }
        }
        return distances
    }

    fun findShortest(distances: Map<Point, Int>, pointCount: Int): Road {
        val visits = linkedSetOf<Int>()
        visits.add(0)
        return findShortest(distances, pointCount, visits, 0)
    }

    fun findShortestWithReturn(distances: Map<Point, Int>, pointCount: Int): Road {
        val visits = linkedSetOf<Int>()
        visits.add(0)
        return findShortestWithReturn(distances, pointCount, visits, 0)
    }

    fun findShortest(distances: Map<Point, Int>, pointsCount: Int, visits: LinkedHashSet<Int>, currentDistance: Int): Road {
        if (visits.size == pointsCount) {
            return Road(visits.joinToString(""), currentDistance).also {
                println("Found road $it")
            }
        } else {
            val lastPoint = visits.last()
            var shortestRoad = Road("", Int.MAX_VALUE)

            for (i in 0 until pointsCount) {
                if (visits.add(i)) {
                    val newDistance = currentDistance + distances[Point(lastPoint, i)]!!
                    val found = findShortest(distances, pointsCount, visits, newDistance)
                    if (found.lenght < shortestRoad.lenght) {
                        shortestRoad = found
                    }
                    visits.remove(i)
                }
            }
            return shortestRoad
        }
    }

    fun findShortestWithReturn(distances: Map<Point, Int>, pointsCount: Int, visits: LinkedHashSet<Int>, currentDistance: Int): Road {
        val lastPoint = visits.last()
        if (visits.size == pointsCount) {
            return Road(visits.joinToString("") + '0', currentDistance + distances[Point(lastPoint, 0)]!!).also {
                println("Found road $it")
            }
        } else {
            var shortestRoad = Road("", Int.MAX_VALUE)

            for (i in 0 until pointsCount) {
                if (visits.add(i)) {
                    val newDistance = currentDistance + distances[Point(lastPoint, i)]!!
                    val found = findShortestWithReturn(distances, pointsCount, visits, newDistance)
                    if (found.lenght < shortestRoad.lenght) {
                        shortestRoad = found
                    }
                    visits.remove(i)
                }
            }
            return shortestRoad
        }
    }

    data class Road(val waypoints: String, val lenght: Int)

    fun findPoints(maze: List<String>): Map<Char, Point> {
        val points = sortedMapOf<Char, Point>()

        maze.forEachIndexed { y, line ->

            line.forEachIndexed { x, c ->
                if (c.isDigit()) {
                    points[c] = Point(x, y)
                }
            }
        }

        return points
    }

    class MazeSolver24(val maze: List<String>, val end: Point) : MazeSolver() {

//        override fun selectMoves(path: Path): List<Path> {
//            return super.selectMoves(path).also {
//                println("for $path found $it")
//            }
//        }

        override fun isEnd(state: Path): Boolean {
//            println("isEnd ${path.place} end: $end ? ${path.place == end}")
            return (end == state.place)
        }

        override fun allowed(state: Path): Boolean {
            return maze[state.place.y][state.place.x] != '#' && !wasVisited(state.place)
        }

    }

}