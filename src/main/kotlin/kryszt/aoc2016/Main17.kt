package kryszt.aoc2016

import kryszt.tools.MD5Util
import kryszt.tools.MazeSolver
import kryszt.tools.Point

object Main17 {

    val Start = Point(0, 0)
    val End = Point(3, 3)

    //    val PassBase = "hijkl"
//    val PassBase = "ihgpwlah"
//    val PassBase = "kglvqrro"
//    val PassBase = "ulqzkmiv"
    val PassBase = "veumntbg" // input

    @JvmStatic
    fun main(args: Array<String>) {
        val solver = MazeSolver17()
//        val moves = solver.solve(listOf(MazeSolver.Path("", Start)))
        val moves = solver.checkAllMovesDeepLongest(MazeSolver.Path("", Start))
        println(moves)
        println(moves?.path?.length)
    }

    class MazeSolver17 : MazeSolver() {
        override fun isEnd(path: Path): Boolean {
            return path.place == End
        }

        fun checkAllMovesDeepLongest(currentPath: MazeSolver.Path): MazeSolver.Path? {
            if (currentPath.place == End) {
                return currentPath
            }
            return selectMoves(currentPath).map { checkAllMovesDeepLongest(it) }.maxByOrNull { it?.path?.length ?: 0 }
        }

        override fun selectMoves(path: Path): List<Path> {
            val doors = MD5Util.hashedHexString(PassBase + path.path).substring(0, 4)
//        println("doors for $path (${PassBase + path.path}}: $doors")
            val possibleMoves = mutableListOf<Path>()
            //up
            (path.place + Point.Up).let { Path(path.path + "U", it) }.takeIf { allowed(it) && isOpened(doors[0]) }?.let { possibleMoves.add(it) }
            (path.place + Point.Down).let { Path(path.path + "D", it) }.takeIf { allowed(it) && isOpened(doors[1]) }?.let { possibleMoves.add(it) }
            (path.place + Point.Left).let { Path(path.path + "L", it) }.takeIf { allowed(it) && isOpened(doors[2]) }?.let { possibleMoves.add(it) }
            (path.place + Point.Right).let { Path(path.path + "R", it) }.takeIf { allowed(it) && isOpened(doors[3]) }?.let { possibleMoves.add(it) }
//        println("Possible: ${possibleMoves.joinToString { it.path }}")
            return possibleMoves
        }

        fun isOpened(c: Char): Boolean {
            return c.isLetter() && c != 'a'
        }

        override fun allowed(path: Path): Boolean {
            val point = path.place
            return ((point.x >= 0) && (point.x < 4) && (point.y >= 0) && (point.y < 4))
        }

    }

}
