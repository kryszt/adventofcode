package kryszt.aoc2016

import kryszt.tools.Point
import kryszt.tools.IoUtil

object Main22 {

    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\2016\\22.txt"

    const val input = "0 0 10 8 2 80\n" +
            "0 1 11 6 5 54\n" +
            "0 2 32 28 4 87\n" +
            "1 0 9 7 2 77\n" +
            "1 1 8 0 8 0\n" +
            "1 2 11 7 4 63\n" +
            "2 0 10 6 4 60\n" +
            "2 1 9 8 1 88\n" +
            "2 2 9 6 3 66"

    @JvmStatic
    fun main(args: Array<String>) {
//        val nodes = input.split("\n").map { parseLine(it) }
        val nodes = IoUtil.readTaskFileLines(this).map { parseLine(it) }
//        nodes.forEach { println(it) }

        val nodeMap: Map<Point, Node> = nodes.associateBy { it.place }

        var count = 0
        val maxX = nodes.maxByOrNull { it.place.x }!!.place.x
        val maxY = nodes.maxByOrNull { it.place.y }!!.place.y

        println("Size: " + countBy(nodes, { it.size }))
        println("Used: " + countBy(nodes, { it.used }))
        println("Avai: " + countBy(nodes, { it.avail }))


        println("Max $maxX-$maxY")

        val viables = mutableSetOf<Point>()
        nodes.forEach { a ->
            nodes.forEach { b ->
                if (isVialbePair(a, b)) {
                    viables.add(a.place)
//                    println("viable $a and $b")
                    count++
                }
            }
        }
        println("viable: $count")

        for (y in 0..maxY) {
            for (x in 0..maxX) {
                val place = Point(x, y)
                print(nodeChar(nodeMap[place]!!, viables.contains(place)))
            }
            println()
        }
    }

    fun countBy(nodes: List<Node>, value: (Node) -> Int): Map<Int, Int> {
        val map = sortedMapOf<Int, Int>()
        nodes.forEach {
            val key = value(it)
            map[key] = (map[key] ?: 0) + 1
        }
        return map
    }

    fun nodeChar(node: Node, isViable: Boolean): Char {
        return when {
            isViable -> '.'
            node.isEmpty -> '_'
            else -> '#'
        }
    }

    fun isVialbePair(nodeA: Node, nodeB: Node): Boolean {
        return (nodeA.used > 0) && nodeA !== nodeB && (nodeA.used <= nodeB.avail)
    }

    fun parseLine(line: String): Node = line.split(" ").let {
        Node(place = Point(it[0].toInt(), it[1].toInt()),
                size = it[2].toInt(),
                used = it[3].toInt(),
                avail = it[4].toInt(),
                use = it[5].toInt())
    }

    data class Node(val place: Point, val size: Int, val used: Int, val avail: Int, val use: Int) {
        val isEmpty: Boolean = used == 0
    }


}
