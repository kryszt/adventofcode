package kryszt.aoc2016

import kryszt.tools.Summer
import kryszt.tools.IoUtil
import java.util.*

object Main6 {

    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\2016\\6.txt"

    const val input = "eedadn\n" +
            "drvtee\n" +
            "eandsr\n" +
            "raavrd\n" +
            "atevrs\n" +
            "tsrnev\n" +
            "sdttsa\n" +
            "rasrtv\n" +
            "nssdts\n" +
            "ntnada\n" +
            "svetve\n" +
            "tesnvt\n" +
            "vntsnd\n" +
            "vrdear\n" +
            "dvrsen\n" +
            "enarar"

    @JvmStatic
    fun main(args: Array<String>) {
        val lines = IoUtil.readTaskFileLines(this)
//        val lines = input.split("\n")

        val summers = sortedMapOf<Int, Summer>()

        lines.forEach { line ->
            line.forEachIndexed { index, c ->
                summers.getOrPut(index, { Summer() }).nextChar(c)
            }
        }
        summers.forEach { index, summer ->
            println("$index -> ${summer.sorted().first()}")
            println("$index -> ${summer.sorted().last()}")
        }

        println(toMessage2(summers))
    }

    fun toMessage(summers: SortedMap<Int, Summer>): String {
        return summers.values.joinToString(separator = "", transform = { it.sorted().first().char.toString() })
    }

    fun toMessage2(summers: SortedMap<Int, Summer>): String {
        println(summers.values.first().sorted())
        return summers.values.joinToString(separator = "", transform = { it.sorted().last().char.toString() })
    }


}
