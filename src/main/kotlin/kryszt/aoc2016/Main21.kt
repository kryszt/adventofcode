package kryszt.aoc2016

import kryszt.tools.IoUtil
import java.util.regex.Pattern

object Main21 {

    const val path = "C:\\Users\\Tomasz\\IdeaProjects\\Toys\\files\\2016\\21.txt"

    val testInput = "swap position 4 with position 0\n" +
            "swap letter d with letter b\n" +
            "reverse positions 0 through 4\n" +
            "rotate left 1 step\n" +
            "move position 1 to position 4\n" +
            "move position 3 to position 0\n" +
            "rotate based on position of letter b\n" +
            "rotate based on position of letter d"

    const val groupNumber = "([0-9]+)"
    const val groupText = "([a-zA-Z]+)"

    val PSwapPosition = ActionPattern("swapPositions", "swap position $groupNumber with position $groupNumber".toPattern())
    val PSwapLetter = ActionPattern("swapLetters", "swap letter $groupText with letter $groupText".toPattern())
    val PRotateLeft = ActionPattern("rotateLeft", "rotate left $groupNumber step[s]*".toPattern())
    val PRotateRight = ActionPattern("rotateRight", "rotate right $groupNumber step[s]*".toPattern())
    val PRotateBased = ActionPattern("rotateBased", "rotate based on position of letter $groupText".toPattern())
    val PReverse = ActionPattern("reverse", "reverse positions $groupNumber through $groupNumber".toPattern())
    val PMove = ActionPattern("move", "move position $groupNumber to position $groupNumber".toPattern())

    val allPatterns = listOf(PSwapPosition, PSwapLetter, PRotateLeft, PRotateRight, PRotateBased, PReverse, PMove)

    @JvmStatic
    fun main2(args: Array<String>) {

        val actions = IoUtil.readTaskFileLines(this).map { findAction(it) }.reversed()
        var input = "fbgdceah".toMutableList()
//        val actions = testInput.split("\n").map { findAction(it) }.reversed()
//        var input = "decab".toMutableList()

        println(input.joinToString(""))
        actions.forEach {
//            input = unmutate(input, it)
            input = mutate(input, it)
            println(it)
            println(input.joinToString(""))
        }

//        testInput.split("\n").map { findAction(it) }.forEach { println(it) }
//        IoUtil.readTaskFileLines(this).map { findAction(it) }.forEach { println(it) }
    }

    @JvmStatic
    fun main(args: Array<String>) {

        val actions = IoUtil.readTaskFileLines(this).map { findAction(it) }
//        var input = "abcdefgh".toMutableList()
        var input = "gcehdbfa".toMutableList()
//        val actions = testInput.split("\n").map { findAction(it) }
//        var input = "abcde".toMutableList()

        println(input.joinToString(""))
        actions.forEach {
            input = mutate(input, it)
            println(it)
            println(input.joinToString(""))
        }

//        testInput.split("\n").map { findAction(it) }.forEach { println(it) }
//        IoUtil.readTaskFileLines(this).map { findAction(it) }.forEach { println(it) }
    }


    fun mutate(input: MutableList<Char>, action: Action): MutableList<Char> {
        return when (action.action) {
            "swapPositions" -> {
                swap(input, action.args[0].toInt(), action.args[1].toInt())
            }
            "swapLetters" -> {
                swap(input, input.indexOf(action.args[0].first()), input.indexOf(action.args[1].first()))
            }
            "rotateLeft" -> {
                val steps = action.args[0].toInt()
                (input.subList(steps, input.size) + input.subList(0, steps)).toMutableList()
            }
            "rotateRight" -> {
                val steps = action.args[0].toInt()
                rotateRight(input, steps)
            }
            "rotateBased" -> {
                val baseIndex = input.indexOf(action.args[0].first())
                val moves = baseIndex + 1 + if (baseIndex > 3) 1 else 0
                rotateRight(input, moves % input.size)
//                input.let { rotateRight(it, 1) }.let { rotateRight(it, baseIndex) }.let { if (baseIndex > 3) rotateRight(it, 1) else it }
//                val steps = baseIndex + if (baseIndex > 3) 2 else 1
//                (input.subList(input.size - steps, input.size) + input.subList(0, input.size - steps)).toMutableList()
            }
            "reverse" -> {
                val from = action.args[0].toInt()
                val to = action.args[1].toInt()
                (input.subList(0, from) + input.subList(from, to + 1).reversed() + input.subList(to + 1, input.size)).toMutableList()
            }
            "move" -> {
                input.apply {
                    val removed = input.removeAt(action.args[0].toInt())
                    input.add(action.args[1].toInt(), removed)
                }
            }

            else -> throw IllegalArgumentException("Unsupported action $action")
        }
    }

    fun unmutate(input: MutableList<Char>, action: Action): MutableList<Char> {
        return when (action.action) {
            "swapPositions" -> {
                swap(input, action.args[0].toInt(), action.args[1].toInt())
            }
            "swapLetters" -> {
                swap(input, input.indexOf(action.args[0].first()), input.indexOf(action.args[1].first()))
            }
            "rotateLeft" -> {
                val steps = action.args[0].toInt()
                rotateRight(input, steps)
            }
            "rotateRight" -> {
                val steps = action.args[0].toInt()
                rotateLeft(input, steps)
            }
            "rotateBased" -> {
                val baseIndex = input.indexOf(action.args[0].first())
                val steps = unrotateBase(baseIndex)
                rotateRight(input, steps)
//                val steps = baseIndex + if (baseIndex > 3) 2 else 1
//                (input.subList(input.size - steps, input.size) + input.subList(0, input.size - steps)).toMutableList()
            }
            "reverse" -> {
                val from = action.args[0].toInt()
                val to = action.args[1].toInt()
                (input.subList(0, from) + input.subList(from, to + 1).reversed() + input.subList(to + 1, input.size)).toMutableList()
            }
            "move" -> {
                input.apply {
                    val removed = input.removeAt(action.args[1].toInt())
                    input.add(action.args[0].toInt(), removed)
                }
            }

            else -> throw IllegalArgumentException("Unsupported action $action")
        }

    }

    fun unrotateBase(index: Int): Int = when (index) {
        0 -> 7
        1 -> 7
        2 -> 2
        3 -> 6
        4 -> 1
        5 -> 5
        6 -> 0
        7 -> 4
        else -> throw IllegalArgumentException("Invalid index $index")
    }

    fun rotateRight(input: MutableList<Char>, steps: Int): MutableList<Char> {
        return (input.subList(input.size - steps, input.size) + input.subList(0, input.size - steps)).toMutableList()
    }

    fun rotateLeft(input: MutableList<Char>, steps: Int): MutableList<Char> {
        return (input.subList(steps, input.size) + input.subList(0, steps)).toMutableList()
    }

    fun swap(input: MutableList<Char>, index1: Int, index2: Int): MutableList<Char> {
        val first = input[index1]
        input[index1] = input[index2]
        input[index2] = first
        return input
    }

    fun findAction(line: String): Action {
        allPatterns.forEach {
            parsePattern(line, it)?.let { return it }
        }
        throw IllegalArgumentException("Cannot parse [$line]")
    }

    fun parsePattern(line: String, pattern: ActionPattern): Action? {
        return pattern.pattern.matcher(line).takeIf { it.find() }?.let {
            val args = mutableListOf<String>()
            for (i in 1..it.groupCount()) {
                args.add(it.group(i))
            }
            Action(pattern.action, args)
        }
    }


    data class ActionPattern(val action: String, val pattern: Pattern)
    data class Action(val action: String, val args: List<String>)
}
