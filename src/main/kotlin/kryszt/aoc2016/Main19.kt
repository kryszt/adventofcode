package kryszt.aoc2016

object Main19 {

        const val elves = 3017957
//    const val elves = 301795
//    const val elves = 9

    @JvmStatic
    fun main(args: Array<String>) {

        val elfs = Array(elves, { i -> i + 1 })

        var workCopy = elfs
        var left = workCopy.size
        while (true) {

            val size = workCopy.size
//            if (size != left) {
//                throw IllegalStateException("Size: $size but left: $left")
//            }

            println("Left $left")
            var victim = left / 2

            for (i in 0 until size) {

                if (i % 10000 == 0) {
                    println(i)
                }
                if (workCopy[i] < 0) {
                    continue
                }

                workCopy[victim % size] *= -1
                left--
                victim++
                if (left % 2 == 0) {
                    victim++
                }
                if (left == 1) {

                    val winner = workCopy.find { it > 0 }
                    println("Winner is $winner")
                    return
                }
            }

            workCopy = removeEmpty(workCopy)
        }

    }

    @JvmStatic
    fun main1(args: Array<String>) {

        val elfs = Array(elves, { i -> i + 1 })

        var workCopy = elfs
        var left = workCopy.size
        while (true) {

            val size = workCopy.size
//            if (size != left) {
//                throw IllegalStateException("Size: $size but left: $left")
//            }

            println("Left $left")

            for (i in 0 until size) {

                if (i % 10000 == 0) {
                    println(i)
                }
                if (workCopy[i] < 0) {
                    continue
                }

//                val victim = findVictim1(i, workCopy)
                val victim = findVictim2(i, workCopy, left)
                workCopy[victim] *= -1
                left--
                if (left == 1) {

                    val winner = workCopy.find { it > 0 }
                    println("Winner is $winner")
                    return
                }
            }

            workCopy = removeEmpty(workCopy)
        }

    }

    private fun removeEmpty(workCopy: Array<Int>): Array<Int> {
        return workCopy.filter { it > 0 }.toTypedArray()
    }

    fun findVictim1(index: Int, arr: Array<Int>): Int {
        val size = arr.size
        for (i in 1 until size) {
            val checkIndex = (index + i) % size
            if (arr[checkIndex] > 0) {
                return checkIndex
            }
        }
        return -1
    }

    fun findVictim2(index: Int, arr: Array<Int>, left: Int): Int {
        val size = arr.size
        var skips = left / 2

        var victim = index
        while (skips > 0) {
            victim = (victim + 1) % size
            if (arr[victim] > 0) {
                skips--
            }
        }

        println("For ${arr[index]} at $index victim is ${arr[victim]} at index $victim")

        return victim

    }
}
