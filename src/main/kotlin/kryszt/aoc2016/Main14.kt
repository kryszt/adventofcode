package kryszt.aoc2016

import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter

object Main14 {

    val Triplet = "([a-z|0-9])\\1{2}".toPattern()
    val Quintet = "([a-z|0-9])\\1{4}".toPattern()
    val digest = MessageDigest.getInstance("MD5")

    @JvmStatic
    fun main(args: Array<String>) {
        val g = Generator("ihaygndm")
//        val g = Generator("abc")
        val found = g.find(64)

        println()
        println()
        println(found.last())
    }

    class Generator(val base: String) {
        val keys = mutableListOf<Candidate>()
        val candidates = mutableListOf<Candidate>()

        fun find(keysCount: Int): List<Candidate> {
            var index = 0

            while (keys.size < keysCount) {
                val hash = hashedHexString2(base, index)
                asConfirmation(hash, index)?.let { confirm(it) }
                asCandidate(hash, index)?.let {
                    println("Candidate: $it")
                    candidates.add(it)
                }

                checkCandidates()

                index++
                cleanup(index)
            }

            return keys.subList(0, keysCount)
        }

        private fun cleanup(index: Int) {
            candidates.firstOrNull()?.takeIf { it.index < index - 1000 }?.let {
                println("Candidate dropped: $it")
                candidates.removeAt(0)
            }
        }

        private fun checkCandidates() {
            candidates.firstOrNull()?.takeIf { it.confirmed }?.let {
                keys.add(it)
                candidates.removeAt(0)
                println("Candidate made key: $it, we have ${keys.size}")
                checkCandidates()
            }
        }

        fun confirm(confirmation: Candidate) {
            println("confirmation: $confirmation")
            candidates.forEach {
                if (it.letter == confirmation.letter) {
                    it.confirmed = true
                }
            }
        }

        fun asCandidate(hash: String, index: Int): Candidate? {
            return Triplet.matcher(hash).takeIf { it.find() }?.let { Candidate(hash, it.group(1)[0], index) }
        }

        fun asConfirmation(hash: String, index: Int): Candidate? {
            return Quintet.matcher(hash).takeIf { it.find() }?.let { Candidate(hash, it.group(1)[0], index) }
        }

        fun hashedHexString1(base: String, index: Int): String {
            return digest.digest("$base$index".toByteArray()).let { DatatypeConverter.printHexBinary(it).toLowerCase() }
        }

        fun hashedHexString2(base: String, index: Int): String {
            var result = digest.digest("$base$index".toByteArray()).let { DatatypeConverter.printHexBinary(it).toLowerCase() }
            for (i in 1..2016) {
                result = digest.digest(result.toByteArray()).let { DatatypeConverter.printHexBinary(it).toLowerCase() }
            }
            return result
        }


    }

    data class Candidate(val hash: String, val letter: Char, val index: Int, var confirmed: Boolean = false)
}
