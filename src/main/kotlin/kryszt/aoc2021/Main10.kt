package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import kryszt.tools.isEven
import java.util.*

object Main10 : DayChallenge {

    private val corruptionScores: Map<Char, Int> =
        mapOf(
            ')' to 3,
            ']' to 57,
            '}' to 1197,
            '>' to 25137,
        )
    private val completionScores: Map<Char, Int> =
        mapOf(
            '(' to 1,
            '[' to 2,
            '{' to 3,
            '<' to 4,
        )
    private val relatedClosing: Map<Char, Char> =
        mapOf(
            '(' to ')',
            '[' to ']',
            '{' to '}',
            '<' to '>'
        )

    private val openings = setOf('(', '[', '{', '<')
    private val closings = setOf(')', ']', '}', '>')

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines()
            .mapNotNull { it.findCorrupted().corrupted }
            .sumOf { corruptionScores[it]!! }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines()
            .map { it.findCorrupted() }
            .filter { it.corrupted == null }
            .map { it.left.completionScore() }
            .sorted()
            .midElement()
            .alsoPrint()
    }

    private fun String.findCorrupted(): Validation {
        val openings = Stack<Char>()
        this.forEach {
            if (it.isOpening()) {
                openings.push(it)
            }
            if (it.isClosing()) {
                if (openings.isEmpty()) return Validation(it, openings)
                val opened = openings.pop()
                if (opened.closingOf() != it) return Validation(it, openings)
            }
        }
        return Validation(null, openings)
    }

    private fun Stack<Char>.completionScore(): Long {
        var score = 0L
        while (this.isNotEmpty()) {
            score *= 5
            score += completionScores[pop()]!!
        }
        return score
    }

    data class Validation(val corrupted: Char?, val left: Stack<Char>)

    private fun Char.isOpening(): Boolean = openings.contains(this)

    private fun Char.isClosing(): Boolean = closings.contains(this)

    private fun Char.closingOf(): Char = relatedClosing[this]!!

    private fun <T> List<T>.midElement(): T {
        if (isEmpty()) throw Exception("Empty")
        val size = this.size
        if (size.isEven()) throw Exception("Even number of elements")
        val midIndex = size / 2
        return this[midIndex]
    }
}
