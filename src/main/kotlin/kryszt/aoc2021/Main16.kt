package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskFile
import kryszt.tools.alsoPrint

object Main16 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskFile()
            .trim()
            .mapToBinary()
            .let { BinaryInput(it) }
            .readNextSection()
            .versionNumberSum()
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskFile()
            .trim()
            .mapToBinary()
            .let { BinaryInput(it) }
            .readNextSection()
            .evaluate()
            .alsoPrint()
    }

    private fun Section.versionNumberSum(): Int =
        when (this) {
            is Section.LiteralValue -> this.version
            is Section.Operator -> this.version + this.subsections.sumOf { it.versionNumberSum() }
        }

    private fun Section.evaluate(): Long = when (this) {
        is Section.LiteralValue -> this.value
        is Section.Operator -> this.evaluate()
    }

    private fun Section.Operator.evaluate(): Long = when (this.id) {
        TYPE_OPERATOR_SUM -> this.subsections.sumOf { it.evaluate() }
        TYPE_OPERATOR_PRODUCT -> this.subsections.fold(1L) { acc, section -> acc * section.evaluate() }
        TYPE_OPERATOR_MINIMUM -> this.subsections.minOf { it.evaluate() }
        TYPE_OPERATOR_MAXIMUM -> this.subsections.maxOf { it.evaluate() }
        TYPE_OPERATOR_GREATER_THAN -> if (first.evaluate() > second.evaluate()) 1 else 0
        TYPE_OPERATOR_LESS_THAN -> if (first.evaluate() < second.evaluate()) 1 else 0
        TYPE_OPERATOR_EQUAL -> if (first.evaluate() == second.evaluate()) 1 else 0
        else -> throw IllegalArgumentException("Unknown operator type ${this.id}")
    }


    sealed class Section(open val version: Int, open val id: Int) {
        data class LiteralValue(override val version: Int, override val id: Int, val value: Long) : Section(version, id)
        data class Operator(override val version: Int, override val id: Int, val subsections: List<Section>) :
            Section(version, id) {
            val first: Section get() = subsections[0]
            val second: Section get() = subsections[1]
        }
    }

    private fun BinaryInput.readNextSection(): Section {
        val version = readVersion()
        return when (val type = readType()) {
            TYPE_LITERAL_VALUE -> Section.LiteralValue(version, type, readLiteralValue())
            else -> Section.Operator(version, type, readSubsections())
        }
    }

    private fun BinaryInput.readLiteralValue(): Long {
        var value = 0L
        while (true) {
            val marker = readNext()
            value *= 16
            value += readInt(4)
            if (marker == '0') {
                return value
            }
        }
    }

    private fun BinaryInput.readSubsections(): List<Section> {
        return when (val sizeType = readNext()) {
            '0' -> readSubsectionsOfLength(readInt(15))
            '1' -> readSubsectionsOfCount(readInt(11))
            else -> throw Exception("Illegal type $sizeType")
        }
    }

    private fun BinaryInput.readSubsectionsOfLength(contentLength: Int): List<Section> {
        val partialInput = partialInput(contentLength)
        val sections = mutableListOf<Section>()

        while (partialInput.hasMore()) {
            sections.add(partialInput.readNextSection())
        }
        return sections
    }

    private fun BinaryInput.readSubsectionsOfCount(count: Int): List<Section> =
        (0 until count).map {
            this.readNextSection()
        }

    class BinaryInput(private val input: String) {
        var index = 0

        fun hasMore(): Boolean = index < input.length

        fun readNext(): Char {
            return input[index++]
        }

        fun readVersion(): Int = readInt(3)

        fun readType(): Int = readInt(3)

        fun partialInput(size: Int): BinaryInput = BinaryInput(takePart(size))

        fun readInt(size: Int): Int {
            return takePart(size).toInt(2)
        }

        private fun takePart(size: Int): String {
            val part = input.substring(index, index + size)
            index += size
            return part
        }
    }


    private const val TYPE_OPERATOR_SUM = 0
    private const val TYPE_OPERATOR_PRODUCT = 1
    private const val TYPE_OPERATOR_MINIMUM = 2
    private const val TYPE_OPERATOR_MAXIMUM = 3
    private const val TYPE_LITERAL_VALUE = 4
    private const val TYPE_OPERATOR_GREATER_THAN = 5
    private const val TYPE_OPERATOR_LESS_THAN = 6
    private const val TYPE_OPERATOR_EQUAL = 7

    private fun String.mapToBinary(): String =
        this
            .map { hexToBinary[it]!! }
            .joinToString(separator = "")

    private val hexToBinary: Map<Char, String> = mapOf(
        '0' to "0000",
        '1' to "0001",
        '2' to "0010",
        '3' to "0011",
        '4' to "0100",
        '5' to "0101",
        '6' to "0110",
        '7' to "0111",
        '8' to "1000",
        '9' to "1001",
        'A' to "1010",
        'B' to "1011",
        'C' to "1100",
        'D' to "1101",
        'E' to "1110",
        'F' to "1111",
    )
}