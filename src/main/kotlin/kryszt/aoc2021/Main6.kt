package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskFile

object Main6 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        doRepeats(readTaskFile(), 80)
    }

    override fun taskTwo() {
        doRepeats(readTaskFile(), 256)
    }

    private fun doRepeats(task: String, repeats: Int) {
        val initialState = task.loadInitial()

        var currentState = initialState
        repeat(repeats) {
            currentState = currentState.nextState()
        }
        println(currentState.sum())
    }

    private fun String.loadInitial(): LongArray {
        val state = LongArray(9)
        this.trim().split(",").forEach {
            state[it.toInt()]++
        }
        return state
    }

    private fun LongArray.nextState(): LongArray {
        val nextState = LongArray(9)
        nextState[0] = this[1]
        nextState[1] = this[2]
        nextState[2] = this[3]
        nextState[3] = this[4]
        nextState[4] = this[5]
        nextState[5] = this[6]
        nextState[6] = Math.addExact(this[7],this[0])
        nextState[7] = this[8]
        nextState[8] = this[0]
        return nextState
    }
}
