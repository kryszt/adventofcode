package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main14 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solve(readTaskLines(), 10)
    }


    override fun taskTwo() {
        solve(readTaskLines(), 40)
    }

    private fun solve(input: List<String>, repeats: Int) {
        val (startString, replacements) = input
            .parseInput()

        val solver = Solver(replacements)
        solver.solve(startString, repeats)
            .score()
            .alsoPrint()
    }

    private fun Map<Char, Long>.score(): Long {
        val min = values.minOrNull() ?: 0
        val max = values.maxOrNull() ?: 0
        return max - min
    }

    private fun List<String>.parseInput(): Pair<String, Map<String, Char>> {
        val initialString = first()
        val substitutes = drop(2)
            .associate { it.asSubstitute() }
        return initialString to substitutes
    }

    private fun String.asSubstitute(): Pair<String, Char> =
        this.split(" -> ")
            .let { it[0] to it[1][0] }

    private class Solver(inclusions: Map<String, Char>) {
        private val cache = mutableMapOf<CacheKey, CacheValue>()
        private val parts: Map<String, SplitInfo> = inclusions.mapValues { (k, v) ->
            k.asSplit(v)
        }

        fun solve(initialString: String, rounds: Int): Map<Char, Long> {
            return initialString
                .windowed(2, 1)
                .map { findValue(CacheKey(it, rounds)) }
                .fold(mutableMapOf<Char, Long>()) { acc, cacheValue ->
                    acc.addCounts(cacheValue.counts)
                }
                .also { counts ->
                    initialString.drop(1).dropLast(1)
                        .forEach { c ->
                            counts.addCount(c, -1)
                        }
                }
        }

        private fun findValue(key: CacheKey): CacheValue {
            return cache.getOrPut(key) { makeForKey(key) }
        }

        private fun makeForKey(key: CacheKey): CacheValue {
            if (key.level <= 0) return CacheValue(
                if (key.key[0] == key.key[1])
                    mapOf(key.key[0] to 2L)
                else
                    mapOf(
                        key.key[0] to 1L,
                        key.key[1] to 1L,
                    )
            )
            val split = parts[key.key]!!
            val keyLeft = CacheKey(split.left, key.level - 1)
            val keyRight = CacheKey(split.right, key.level - 1)
            return addCounts(findValue(keyLeft), findValue(keyRight), split.connector)
        }

        private fun addCounts(value1: CacheValue, value2: CacheValue, joinChar: Char): CacheValue {
            val sum = mutableMapOf<Char, Long>()
            sum.addCounts(value1.counts)
            sum.addCounts(value2.counts)
            sum.addCount(joinChar, -1)
            return CacheValue(sum)
        }

        private fun MutableMap<Char, Long>.addCounts(other: Map<Char, Long>): MutableMap<Char, Long> = apply {
            other.forEach { (c, count) ->
                addCount(c, count)
            }
        }

        private fun MutableMap<Char, Long>.addCount(c: Char, value: Long): MutableMap<Char, Long> = apply {
            val newCount = this.getOrDefault(c, 0) + value
            this[c] = newCount
        }

        private fun String.asSplit(insert: Char): SplitInfo {
            val left = "${this[0]}$insert"
            val right = "$insert${this[1]}"
            return SplitInfo(left, right, insert)
        }
    }

    private data class SplitInfo(val left: String, val right: String, val connector: Char)
    private data class CacheKey(val key: String, val level: Int)
    private data class CacheValue(val counts: Map<Char, Long>)
}
