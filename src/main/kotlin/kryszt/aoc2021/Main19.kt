package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point3D
import kryszt.tools.Vector3D
import kryszt.tools.alsoPrint
import kotlin.math.abs
import kotlin.math.absoluteValue
import kotlin.math.max

object Main19 : DayChallenge {

    private const val MAX_FOR_SAME = 2

    private val fromX: (Vector3D) -> Int = { it.x }
    private val fromXNegated: (Vector3D) -> Int = { -it.x }
    private val fromY: (Vector3D) -> Int = { it.y }
    private val fromYNegated: (Vector3D) -> Int = { -it.y }
    private val fromZ: (Vector3D) -> Int = { it.z }
    private val fromZNegated: (Vector3D) -> Int = { -it.z }

    data class VectorTransformation(
        val selectX: (Vector3D) -> Int,
        val selectY: (Vector3D) -> Int,
        val selectZ: (Vector3D) -> Int,
    ) {
        fun transform(vector: Vector3D): Vector3D =
            Vector3D(
                x = selectX(vector),
                y = selectY(vector),
                z = selectZ(vector)
            )
    }

    private fun Vector3D.findTransformationFor(other: Vector3D): VectorTransformation? {
        return VectorTransformation(
            selectX = this.x.findTransformationFor(other) ?: return null,
            selectY = this.y.findTransformationFor(other) ?: return null,
            selectZ = this.z.findTransformationFor(other) ?: return null
        )

    }

    private fun Int.findTransformationFor(other: Vector3D): ((Vector3D) -> Int)? =
        when (this) {
            other.x -> fromX
            -other.x -> fromXNegated
            other.y -> fromY
            -other.y -> fromYNegated
            other.z -> fromZ
            -other.z -> fromZNegated
            else -> null
        }

    private fun Vector3D.canFindTransformation(): Boolean {
        if (x == 0 || y == 0 || z == 0) return false
        if (x.absoluteValue == y.absoluteValue) return false
        if (x.absoluteValue == z.absoluteValue) return false
        if (y.absoluteValue == z.absoluteValue) return false
        return true
    }

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val sectors = readTaskLines()
            .parse()
            .map { (name, stars) ->
                SensorConstellations(name, buildConstellations(stars))
            }

        findAllStars(sectors)
            .also {
                it.stars.size.alsoPrint()
            }
    }

    override fun taskTwo() {
        val sectors = readTaskLines()
            .parse()
            .map { (name, stars) ->
                SensorConstellations(name, buildConstellations(stars))
            }

        val result = findAllStars(sectors)
        result.sensors.plus(Point3D())
            .maxDistance()
            .alsoPrint()
    }

    private fun List<Point3D>.maxDistance(): Int {
        var maxDistance = 0
        forEach { start ->
            forEach { other ->
                maxDistance = max(maxDistance, start.distance(other))
            }
        }
        return maxDistance
    }

    data class MergeResult(val sensors: List<Point3D>, val stars: Set<Point3D>)

    private fun findAllStars(sensorGroups: List<SensorConstellations>): MergeResult {
        val allStars = mutableSetOf<Point3D>().apply {
            addAll(sensorGroups.first().constellations.map { it.star })
        }
        val sensors = mutableListOf<Point3D>()

        var constellationsToCheck = sensorGroups.first().constellations
        var distancesIds: Set<String> = constellationsToCheck.distanceIdsSet()
        var constellationsLeft = sensorGroups.drop(1)

        while (constellationsLeft.isNotEmpty()) {
            val nextToMerge = constellationsLeft.maxByOrNull { it.countCommonWith(distancesIds) }
                ?: throw IllegalStateException("Cannot find? why?")
            val normalizedResult = nextToMerge.normalizeTo(constellationsToCheck)

            allStars.addAll(normalizedResult.second)
            sensors.add(normalizedResult.first)

            constellationsToCheck = buildConstellations(allStars.toList())
            distancesIds = constellationsToCheck.distanceIdsSet()

            constellationsLeft = constellationsLeft.minus(nextToMerge)
        }

        return MergeResult(sensors, allStars)
    }

    private fun SensorConstellations.normalizeTo(normalizedConstellations: List<StarConstellation>): Pair<Point3D, List<Point3D>> {
        val transformation = findTransformation(normalizedConstellations)
        val sensorVector = transformation.equivalentPoint.vectorTo(Point3D())
        val normalizedSensorVector = transformation.transformation.transform(sensorVector)
        val normalizedSensor = transformation.normalizedPoint.plus(normalizedSensorVector)

        val newPoints = this.constellations
            .map { transformation.equivalentPoint.vectorTo(it.star) }
            .map { transformation.transformation.transform(it) }
            .map { transformation.normalizedPoint.plus(it) }

        return normalizedSensor to newPoints
    }

    private data class NormalizationData(
        val normalizedPoint: Point3D,
        val equivalentPoint: Point3D,
        val transformation: VectorTransformation
    )

    private fun SensorConstellations.findTransformation(normalizedConstellations: List<StarConstellation>): NormalizationData {
        normalizedConstellations.forEach { normalized ->
            val toNormalize = this.constellations.find { it.distancesId == normalized.distancesId }
            val transformation = toNormalize?.findTransformation(normalized)
            if (transformation != null) return NormalizationData(normalized.star, toNormalize.star, transformation)
        }

        throw IllegalStateException("Cannot find transformation for sensor ${this.sensor}")
    }

    private fun StarConstellation.findTransformation(normalized: StarConstellation): VectorTransformation? {
//        println("try find between $this and $normalized")

        val vOriginal1 = normalized.star.vectorTo(normalized.distancedNeighbours[0].star)
        val vOriginal2 = normalized.star.vectorTo(normalized.distancedNeighbours[1].star)
//        println("$vOriginal1, $vOriginal2")


        val vOther1 = this.star.vectorTo(this.distancedNeighbours[0].star)
        val vOther2 = this.star.vectorTo(this.distancedNeighbours[1].star)
//        println("$vOther1, $vOther2")

        if (!vOriginal1.canFindTransformation() || !vOriginal2.canFindTransformation()) return null
//        println("can find")

        val transformation1 = vOriginal1.findTransformationFor(vOther1) ?: return null
        val transformation2 = vOriginal2.findTransformationFor(vOther2) ?: return null

//        println("found for both")

        if (transformation1 != transformation2) return null

        return transformation1
    }

    private fun List<String>.parse(): Map<String, List<Point3D>> {
        val parsed = mutableMapOf<String, List<Point3D>>()
        lateinit var currentScanner: String

        forEach { line: String ->
            if (line.startsWith("---")) {
                currentScanner = line.drop(4).dropLast(4)
            } else if (line.isNotBlank()) {
                val point = line.split(",")
                    .let { Point3D(it[0].toInt(), it[1].toInt(), it[2].toInt()) }
                parsed
                    .getOrPut(currentScanner) { emptyList() }
                    .plus(point)
                    .also { parsed[currentScanner] = it }
            }
        }
        return parsed
    }

    private fun buildConstellations(
        stars: List<Point3D>
    ): List<StarConstellation> {
        return stars.map { it.buildConstellation(stars) }
    }

    private fun Point3D.buildConstellation(
        stars: List<Point3D>
    ): StarConstellation {
        val constellation = stars
            .mapNotNull { this.makeDistanced(it) }
            .sortedBy { it.distance }
            .take(MAX_FOR_SAME)
        return StarConstellation(this, constellation)
    }

    private fun Point3D.makeDistanced(other: Point3D): DistancedStar? {
        if (this == other) return null
        return DistancedStar(
            other,
            this.distance(other),
            this.distanceId(other)
        )
    }

    private fun Point3D.distanceId(other: Point3D): String {
        val dx = abs(other.x - this.x)
        val dy = abs(other.y - this.y)
        val dz = abs(other.z - this.z)

        return listOf(dx, dy, dz).sorted()
            .joinToString(":")
    }

    private class SensorConstellations(
        val sensor: String,
        val constellations: List<StarConstellation>
    ) {
        val distanceIds: Set<String> = constellations.distanceIdsSet()

        fun countCommonWith(otherDistanceIds: Set<String>): Int {
            return otherDistanceIds.count { distanceIds.contains(it) }
        }
    }

    private fun List<StarConstellation>.distanceIdsSet(): Set<String> =
        map { it.distancesId }.toSet()

    private class StarConstellation(
        val star: Point3D,
        val distancedNeighbours: List<DistancedStar>
    ) {
        val distancesId = distancedNeighbours.joinToString(separator = ",") { it.directionId }
        override fun toString(): String {
            return "StarConstellation(star=$star, distancesId='$distancesId', neighbour='$distancedNeighbours')"
        }

    }

    private data class DistancedStar(val star: Point3D, val distance: Int, val directionId: String)
}
