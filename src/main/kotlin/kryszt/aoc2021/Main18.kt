package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import kotlin.math.max

object Main18 : DayChallenge {

    private val numberPattern = "(\\d+)".toRegex()
    private val notSingleDigitPattern = "(\\d\\d+)".toRegex()
    private val explodingPattern = "\\[(\\d+),(\\d+)]".toRegex()
    private val lastNumberPattern = ".*[\\[|,](\\d+)".toRegex()

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines()
            .reduce { acc, s ->
                acc.addNumber(s)
                    .reduce()
            }
            .alsoPrint()
            .magnitude()
            .alsoPrint()
    }

    override fun taskTwo() {
        var maxMagnitude = 0L

        val lines = readTaskLines()
        lines.indices.forEach { i ->
            lines.indices.forEach { j ->
                if (i != j) {
                    val left = lines[i]
                    val right = lines[j]
                    val magnitude = sumMagnitude(left, right)
                    maxMagnitude = max(maxMagnitude, magnitude)
                }
            }
        }
        maxMagnitude.alsoPrint()
    }

    private fun String.addNumber(other: String): String =
        "[$this,$other]"

    private fun String.explode(): String? {
        val match = findDeepPair(5) ?: return null
        val leftValue = match.groupValues[1].toInt()
        val rightValue = match.groupValues[2].toInt()

        val leftString = this.take(match.range.first).addToLast(leftValue)
        val rightString = this.drop(match.range.last + 1).addToFirst(rightValue)
        return leftString + "0" + rightString
    }

    private fun String.addToLast(value: Int): String {
        val match = lastNumberPattern.find(this)?.groups?.get(1) ?: return this
        val newValue = match.value.toInt() + value
        return this.replacePart(match.range, newValue.toString())
    }

    private fun String.addToFirst(value: Int): String {
        val match = numberPattern.find(this) ?: return this
        val newValue = match.value.toInt() + value
        return this.replacePart(match.range, newValue.toString())
    }

    private fun String.findDeepPair(requiredDepth: Int): MatchResult? {
        val depths = depthCounts()
        return explodingPattern
            .findAll(this)
            .firstOrNull { depths[it.range.first] >= requiredDepth }
    }

    private fun String.split(): String? {
        val found = notSingleDigitPattern.find(this) ?: return null
        val intValue = found.value.toInt()
        val left = intValue / 2
        val right = (intValue + 1) / 2
        val newPart = "[$left,$right]"
        return this.replacePart(found.range, newPart)
    }

    private fun String.replacePart(range: IntRange, newString: String): String {
        val left = this.take(range.first)
        val right = this.drop(range.last + 1)
        return left + newString + right
    }

    private fun String.reduce(): String {
        var nextString: String = this

        while (true) {
            var nextRound = nextString.explode()
            if (nextRound == null) {
                nextRound = nextString.split()
            }
            if (nextRound == null)
                return nextString
            else
                nextString = nextRound
        }
    }

    private fun String.depthCounts(): IntArray {
        val depths = IntArray(this.length)
        var depth = 0
        this.indices.forEach { index ->
            if (this[index] == ']') {
                depths[index] = depth
                depth--
            } else if (this[index] == '[') {
                depth++
                depths[index] = depth
            } else {
                depths[index] = depth
            }
        }
        return depths
    }

    private fun String.magnitude(): Long {
        var simplified = this

        while (true) {
            val nextToReplace = explodingPattern.find(simplified) ?: return simplified.extractNumber()
            val leftValue = nextToReplace.groupValues[1].toLong()
            val rightValue = nextToReplace.groupValues[2].toLong()
            val magnitude = (3 * leftValue + 2 * rightValue)
            simplified = simplified.replacePart(nextToReplace.range, magnitude.toString())
        }
    }

    private fun String.extractNumber(): Long = toLong()

    private fun sumMagnitude(left: String, right: String): Long = left.addNumber(right)
        .reduce()
        .magnitude()
}
