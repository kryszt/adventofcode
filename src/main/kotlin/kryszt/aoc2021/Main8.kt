package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main8 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines()
            .map { it.parseLine() }
            .sumOf { line -> line.display.count { it.length == 2 || it.length == 3 || it.length == 4 || it.length == 7 } }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines()
            .map { it.parseLine() }
            .sumOf { it.decode() }
            .alsoPrint()
    }

    private fun String.parseLine(): Display {
        val (digits, display) = this.split("|").let {
            it[0].trim().split(" ") to it[1].trim().split(" ")
        }
        return Display(digits, display)
    }

    private data class Display(val digits: List<String>, val display: List<String>)

    private fun Display.decode(): Int {
        val rules = DisplayRules(this.digits)
        return this.display
            .map { rules.findValue(it) }
            .joinToString(separator = "")
            .toInt()
    }

    private class DisplayRules(val digits: List<String>) {

        fun findValue(sections: String): Char =
            when (sections.length) {
                2 -> '1'
                3 -> '7'
                4 -> '4'
                5 -> findForLength5(sections)
                6 -> findForLength6(sections)
                7 -> '8'
                else -> throw IllegalArgumentException("Cannot handle $sections")
            }

        private fun findForLength5(sections: String): Char {
            if (sections.hasAllFrom(find('1'))) return '3'
            if (sections.hasFrom(find('4'), 3)) return '5'
            return '2'
        }

        private fun findForLength6(sections: String): Char {
            if (sections.hasAllFrom(find('4'))) return '9'
            if (sections.hasAllFrom(find('7'))) return '0'
            return '6'
        }

        private fun find(digit: Char): String {
            val sections = when (digit) {
                '1' -> digits.find { it.length == 2 }
                '4' -> digits.find { it.length == 4 }
                '7' -> digits.find { it.length == 3 }
                '8' -> digits.find { it.length == 7 }
                else -> null
            }
            return sections!!
        }

        private fun String.hasAllFrom(other: String) =
            other.all { this.contains(it) }

        private fun String.hasFrom(other: String, expected: Int) =
            other.count { this.contains(it) } == expected
    }
}