package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point

object Main15 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val solver = Solver(readTaskLines())
        solver.solve()
        println(solver.finalCost())
    }

    override fun taskTwo() {
        val solver = Solver(readTaskLines(), 5)
        solver.solve()
        println(solver.finalCost())
    }

    class Solver(private val costs: List<String>, repeats: Int = 1) {
        private val originalSize = costs.size
        private val size = costs.size * repeats
        private val lastPoint = Point(size - 1, size - 1)
        private val distances: Array<IntArray> = Array(size) {
            IntArray(size) { Int.MAX_VALUE / 2 }
        }

        private fun distanceAt(point: Point): Int = distances[point.y][point.x]
        private fun setDistanceAt(point: Point, value: Int) {
            distances[point.y][point.x] = value
        }

        fun finalCost(): Int = distanceAt(lastPoint)

        private fun cost(point: Point): Int? {
            if (point.x < 0 || point.y < 0 || point.x >= size || point.y >= size) return null
            val normalizedX = point.x % originalSize
            val normalizedY = point.y % originalSize

            val dx = point.x / originalSize
            val dy = point.y / originalSize
            val d = dx + dy


            return costs[normalizedY][normalizedX].minus('0')
                .minus(1)
                .plus(d)
                .mod(9)
                .plus(1)
        }


        fun solve() {
            var nextMoves: List<Point> = listOf(Point(0, 0))
            setDistanceAt(Point(0, 0), 0)
            while (nextMoves.isNotEmpty()) {
//                println("checking $nextMoves")
                nextMoves = selectNext(nextMoves)
            }
        }

        private fun selectNext(moves: Collection<Point>): List<Point> {
            val nextMoves: MutableList<Point> = mutableListOf()
            moves.forEach {
                neighbours(it) { found ->
//                    println("adding: $found")
                    nextMoves.add(found)
                }
            }
            return nextMoves
        }

        private fun neighbours(point: Point, onFound: (Point) -> Unit) {
            val currentValue: Int = distanceAt(point)
            point.crossNeighbours().forEach { nextPoint ->
                val moveCost = cost(nextPoint)
                if (moveCost != null) {
                    val foundCost = distanceAt(nextPoint)
                    val newCost = currentValue + moveCost
                    if (newCost < foundCost) {
                        setDistanceAt(nextPoint, newCost)
                        onFound(nextPoint)
                    }
                }
            }
        }
    }
}