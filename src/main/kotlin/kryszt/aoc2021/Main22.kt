package kryszt.aoc2021

import kryszt.tools.*
import kryszt.tools.IoUtil.readTaskLines
import java.util.regex.Matcher

object Main22 : DayChallenge {

    private val smallSpace = Rect3dStace(-50..50, -50..50, -50..50)
    private val lineRegex = "([a-z]+) x=(-?\\d+)..(-?\\d+),y=(-?\\d+)..(-?\\d+),z=(-?\\d+)..(-?\\d+)".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines()
            .map { it.parse() }
            .filter { smallSpace.fullyContains(it.space) }
            .removeOverdraws()
            .advancedTotal()
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines()
            .map { it.parse() }
            .removeOverdraws()
            .advancedTotal()
            .alsoPrint()
    }

    private fun List<SpaceAction>.advancedTotal(): Long {
        var allAreas: List<CountedArea> = emptyList()
        forEach {
            allAreas = if (it.isAdd) {
                allAreas.add(it.space)
            } else {
                allAreas.remove(it.space)
            }
        }
        return allAreas.sumOf { it.volumeValue }
    }

    private data class CountedArea(val weight: Int, val space: Rect3dStace) {
        val volumeValue: Long = space.volume * weight
    }

    private fun List<CountedArea>.add(space: Rect3dStace): List<CountedArea> =
        this
            .flatMap { current ->
                current.add(space)
            }.plus(CountedArea(1, space))

    private fun List<CountedArea>.remove(space: Rect3dStace): List<CountedArea> =
        this
            .flatMap { current ->
                current.add(space)
            }

    private fun CountedArea.add(space: Rect3dStace): List<CountedArea> {
        val intersection = this.space.intersection(space) ?: return listOf(this)
        val intersectionArea = CountedArea(-this.weight, intersection)
        return listOf(this, intersectionArea)
    }

    private fun List<SpaceAction>.removeOverdraws(): List<SpaceAction> {
        var allSeparate: List<SpaceAction> = emptyList()
        this.forEach { currentAction ->
            val newAll = allSeparate.filterNot { currentAction.space.fullyContains(it.space) }
            allSeparate = newAll + currentAction
        }
        return allSeparate
    }

    private data class Rect3dStace(val x: IntRange, val y: IntRange, val z: IntRange) {
        fun fullyContains(other: Rect3dStace): Boolean {
            return x.contains(other.x) && y.contains(other.y) && z.contains(other.z)
        }

        fun intersection(other: Rect3dStace): Rect3dStace? {
            val ix = x.intersection(other.x)?.asIntRange() ?: return null
            val iy = y.intersection(other.y)?.asIntRange() ?: return null
            val iz = z.intersection(other.z)?.asIntRange() ?: return null

            return Rect3dStace(ix, iy, iz)
        }

        val volume: Long =
            x.width() * y.width() * z.width()
    }

    private data class SpaceAction(val action: String, val space: Rect3dStace) {
        val isAdd: Boolean = action == "on"
    }

    private fun String.parse(): SpaceAction =
        lineRegex
            .matcher(this)
            .takeIf { it.matches() }
            ?.parse()
            ?: throw IllegalArgumentException("Cannot parse: $this")

    private fun Matcher.parse(): SpaceAction = SpaceAction(
        action = this.group(1),
        Rect3dStace(
            x = this.group(2).toInt()..this.group(3).toInt(),
            y = this.group(4).toInt()..this.group(5).toInt(),
            z = this.group(6).toInt()..this.group(7).toInt()
        )
    )
}
