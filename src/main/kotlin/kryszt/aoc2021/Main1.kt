package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main1 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        taskOne()
        taskTwo()
    }

    override fun taskOne() {
        readTaskLines()
            .map { it.toInt() }
            .also { println(it.size) }
            .zipWithNext()
            .also { println(it.size) }
            .count { it.second > it.first }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines()
            .map { it.toInt() }
            .also { println(it.size) }
            .windowed(3, 1)
            .map { it.sum() }
            .zipWithNext()
            .count { it.second > it.first }
            .alsoPrint()
    }
}