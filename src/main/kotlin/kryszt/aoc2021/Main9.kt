package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint

object Main9 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val board = readTaskLines()
        board.findLowPoints()
            .mapNotNull { board.findValue(it) }
            .sumOf { it - '0' + 1 }
            .alsoPrint()
    }

    override fun taskTwo() {
        val board = readTaskLines()
        val lowPoints = board.findLowPoints()
        lowPoints
            .map { board.findBasin(it).size }
            .sortedDescending()
            .take(3)
            .fold(1, Int::times)
            .alsoPrint()
    }

    private fun List<String>.findLowPoints(): List<Point> {
        val lowPoints = mutableListOf<Point>()

        this.forEachIndexed { y, line ->
            line.forEachIndexed { x, localValue ->
                val localPoint = Point(x, y)
                val adjacent = findValues(localPoint.crossNeighbours())
                if (adjacent.all { it > localValue }) {
                    lowPoints.add(localPoint)
                }
            }
        }
        return lowPoints
    }

    private fun List<String>.findValues(points: List<Point>): List<Char> =
        points.mapNotNull { this.findValue(it) }

    private fun List<String>.findValue(point: Point): Char? =
        this.getOrNull(point.y)?.getOrNull(point.x)

    private fun List<String>.findBasin(from: Point): Set<Point> {
        var nextToCheck = setOf(from)
        val allInBasin = mutableSetOf<Point>()

        while (nextToCheck.isNotEmpty()) {
            allInBasin.addAll(nextToCheck)

            nextToCheck = nextToCheck
                .asSequence()
                .map { it.crossNeighbours() }
                .flatten()
                .toSet()
                .filter { point: Point ->
                    val c = this.findValue(point)
                    c != null && c !='9' && !allInBasin.contains(point)
                }
                .toSet()
        }
        return allInBasin
    }
}
