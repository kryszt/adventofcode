package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main4 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val bingo = readBingo(readTaskLines())

        bingo.numbers.forEach { nextNumber ->
            bingo.boards.forEach { it.markValue(nextNumber) }
            bingo.boards
                .find { it.hasLine() }
                ?.also { it.printBoard() }
                ?.leftSum()
                ?.let { it * nextNumber }
                ?.alsoPrint()
                ?.also { return }
        }
    }

    override fun taskTwo() {
        val bingo = readBingo(readTaskLines())

        var boardsLeft = bingo.boards

        bingo.numbers.forEach { nextNumber ->
            boardsLeft.forEach { it.markValue(nextNumber) }
            val nextBoards = boardsLeft.filterNot { it.hasLine() }
            if (nextBoards.isEmpty()) {
                boardsLeft.first()
                    .also { it.printBoard() }
                    .leftSum()
                    .alsoPrint()
                    .let { it * nextNumber }
                    .alsoPrint()
                return
            }
            boardsLeft = nextBoards
        }
    }

    private fun readBingo(lines: List<String>): BingoData {
        val numbers = lines[0].split(",").map { it.toInt() }
//        val numbers = listOf(68, 14, 63, 79, 41)
        val boards = lines
            .drop(2)
            .windowed(5, 6, false, this::asBoard)
        return BingoData(numbers, boards)
    }

    private fun asBoard(lines: List<String>): BingoBoard {
        check(lines.size == 5) { "Required 5 lines" }
        val bingoBoard = lines.map {
            it.asBingoLine().toIntArray()
        }.toTypedArray()
        return BingoBoard(bingoBoard)
    }

    private fun String.asBingoLine(): List<Int> =
        this
            .trim()
            .split("\\s+".toRegex())
            .map { it.toInt() }
            .also { check(it.size == 5) { "Requires 5 items in line" } }

    data class BingoData(
        val numbers: List<Int>,
        val boards: List<BingoBoard>
    )

    class BingoBoard(
        val values: Array<IntArray>
    ) {
        fun markValue(value: Int) {
            val markedValue = if (value == 0) -111 else -value
            values.indices.forEach { x ->
                values[x].indices.forEach { y ->
                    if (values[x][y] == value) {
                        values[x][y] = markedValue
                    }
                }
            }
        }

        fun leftSum(): Int =
            values.sumOf { row ->
                row.sumOf { if (it > 0) it else 0 }
            }

        fun hasLine(): Boolean {
            return hasHorizontalLine() || hasVerticalLine()
        }

        private fun hasHorizontalLine(): Boolean =
            values.any { line -> line.all { it < 0 } }

        private fun hasVerticalLine(): Boolean =
            values.indices.any { column ->
                values.indices.all { row ->
                    values[row][column] < 0
                }
            }

        fun printBoard() {
            values.forEach { line ->
                line.forEach { print("\t$it") }
                println()
            }
        }
    }
}