package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint
import java.util.*

object Main24 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readConditions()
            .buildMaxNumber()
            .alsoPrint()
    }

    override fun taskTwo() {
        readConditions()
            .buildMinNumber()
            .alsoPrint()
    }

    private fun Map<Int, Group>.buildMaxNumber(): String =
        this.buildNumber(Group::maxForCurrent, Group::maxForSource)

    private fun Map<Int, Group>.buildMinNumber(): String =
        this.buildNumber(Group::minForCurrent, Group::minForSource)

    private fun Map<Int, Group>.buildNumber(selectCurrent: Group.() -> Int, selectSource: Group.() -> Int): String {
        val digits = IntArray(14)
        forEach { (targetIndex, condition) ->
            digits[targetIndex] = condition.selectCurrent()
            digits[condition.inputIndex] = condition.selectSource()
        }
        return digits.joinToString("")
    }

    private fun readIterations(): List<Iteration> =
        readTaskLines()
            .windowed(18, 18)
            .map { it.readIteration() }

    private fun readConditions(): Map<Int, Group> =
        readIterations()
            .buildConditions()

    private fun List<Iteration>.buildConditions(): Map<Int, Group> {
        val digitStack = Stack<Group>()
        val conditions = mutableMapOf<Int, Group>()

        this.forEachIndexed { index, iteration ->
            if (iteration.dixZ == 1) {
                digitStack.push(Group(index, iteration.plusY))
            }
            if (iteration.dixZ == 26) {
                val lastGroup = digitStack.pop()
                val condition = Group(lastGroup.inputIndex, lastGroup.plus + iteration.plusX)
                conditions[index] = (condition)
            }
        }
        return conditions
    }

    private fun List<String>.readIteration(): Iteration =
        Iteration(
            dixZ = this[4].requireStart("div z").lastNumber(),
            plusX = this[5].requireStart("add x").lastNumber(),
            plusY = this[15].requireStart("add y").lastNumber()
        )

    private fun String.requireStart(beginning: String) = apply {
        check(this.startsWith(beginning))
    }

    private fun String.lastNumber(): Int =
        this.takeLastWhile { it != ' ' }
            .toInt()

    //w -7
    //w +2

    private data class Group(val inputIndex: Int, val plus: Int) {

        fun maxForCurrent(): Int = if (plus > 0) 9 else 9 + plus
        fun maxForSource(): Int = maxForCurrent() - plus
        fun minForCurrent(): Int = if (plus > 0) 1 + plus else 1
        fun minForSource(): Int = minForCurrent() - plus

    }

    private data class Iteration(val dixZ: Int, val plusX: Int, val plusY: Int)
}
