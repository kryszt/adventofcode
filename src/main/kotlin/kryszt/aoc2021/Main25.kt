package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.alsoPrint

object Main25 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val board = readTaskLines()
            .parse()
        board.print()
        var movedCount = 0
        do {
            movedCount++
        } while (board.move())

        println()
        board.print()
        println(movedCount)
    }

    override fun taskTwo() {
    }

    private fun Array<CharArray>.print(): Array<CharArray> = apply {
        forEach { line -> String(line).alsoPrint() }
    }

    private fun Array<CharArray>.move(): Boolean {
        val movedEast = moveEast()
        val movedSouth = moveSouth()
        return movedEast || movedSouth
    }

    private fun Array<CharArray>.moveEast(): Boolean {
        var moved = false
        forEach { line ->
            line.forEachIndexed { index, c ->
                val nextIndex = (index + 1).mod(line.size)
                if (c == '>' && line[nextIndex] == '.') {
                    moved = true
                    line[index] = '#'
                }
            }
        }
        forEach { line ->
            line.forEachIndexed { index, c ->
                if (c == '#') {
                    val nextIndex = (index + 1).mod(line.size)
                    line[index] = '.'
                    line[nextIndex] = '>'
                }
            }
        }
        return moved
    }

    private fun Array<CharArray>.moveSouth(): Boolean {
        var moved = false
        forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                val nextY = (y + 1).mod(size)
                if (c == 'v' && this[nextY][x] == '.') {
                    moved = true
                    line[x] = '#'
                }
            }
        }
        forEachIndexed { y, line ->
            line.forEachIndexed { x, c ->
                if (c == '#') {
                    val nextY = (y + 1).mod(size)
                    line[x] = '.'
                    this[nextY][x] = 'v'
                }
            }
        }
        return moved
    }

    private fun List<String>.parse(): Array<CharArray> =
        this.map {
            it.toCharArray()
        }.toTypedArray()
}