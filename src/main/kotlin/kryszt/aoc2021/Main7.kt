package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskFile
import kotlin.math.abs

object Main7 : DayChallenge {

    private val input: IntArray =
        readTaskFile()
            .trim()
            .split(",")
            .map { it.toInt() }
            .toIntArray()

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solve { it }
    }

    override fun taskTwo() {
        val costFunction: (Int) -> Int = {
            ((1 + it) * it) / 2
        }
        solve(costFunction)
    }

    private fun solve(costFunction: (Int) -> Int) {
        println(findSmallestFuelCost(costFunction))
    }

    private fun findSmallestFuelCost(fuelCost: (Int) -> Int): Int {
        val from: Int = input.minOrNull() ?: 0
        val to: Int = input.maxOrNull() ?: 0

        return (from..to).minOf { input.costTo(it, fuelCost) }
    }

    private fun IntArray.costTo(target: Int, fuelCost: (Int) -> Int) =
        sumOf { fuelCost(abs(it - target)) }

}
