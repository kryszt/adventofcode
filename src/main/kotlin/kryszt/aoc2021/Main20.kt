package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint
import kryszt.tools.printBoard

object Main20 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solve(readTaskLines(), 2)
    }

    override fun taskTwo() {
        solve(readTaskLines(), 50)
    }

    private fun solve(input: List<String>, times: Int) {
        val data = input.parse()
        val solver = Enhancer(data.first, data.second, data.third.x, data.third.y)

        repeat(times) {
            solver.nextState()
        }
        solver.count().alsoPrint()
    }

    private fun List<String>.parse(): Triple<BooleanArray, Set<Point>, Point> {
        val replacement = first().map { it == '#' }.toBooleanArray()
        val sizeY = this.lastIndex - 2
        val sizeX = this.last().lastIndex
        val points = this.drop(2)
            .flatMapIndexed { y: Int, line: String ->
                line.asPoints(y)
            }
            .toSet()
        return Triple(replacement, points, Point(sizeX, sizeY))
    }

    private fun String.asPoints(y: Int): List<Point> =
        this.mapIndexedNotNull { index, c ->
            index.takeIf { c == '#' }?.let { Point(index, y) }
        }

    private class Enhancer(
        private val replacements: BooleanArray,
        initialPoints: Set<Point>,
        initialX: Int,
        initialY: Int
    ) {

        private var outsideValue = false
        private var points = initialPoints

        private var minX = 0
        private var maxX = initialX
        private var minY = 0
        private var maxY = initialY

        fun printBoard() {
            points.printBoard('#', '.')
        }

        fun nextState() {
            points = makeNext()
            outsideValue = nextOutside()
            expandBoard()
        }

        private fun makeNext(): Set<Point> {
            val nextPoints = mutableSetOf<Point>()
            (minX - 1..maxX + 1).forEach { x ->
                (minY - 1..maxY + 1).forEach { y ->
                    val p = Point(x, y)
                    if (nextValueFor(p)) {
                        nextPoints.add(p)
                    }
                }
            }
            return nextPoints
        }

        private fun expandBoard() {
            minX--
            maxX++
            minY--
            maxY++
        }

        private fun nextValueFor(point: Point): Boolean {
            return point.aroundPoints()
                .joinToString("") { if (isSet(it)) "1" else "0" }
                .toInt(2)
                .let { replacements[it] }
        }

        private fun nextOutside(): Boolean =
            if (outsideValue) replacements.last() else replacements.first()

        private fun isSet(point: Point): Boolean =
            if (isInArea(point)) points.contains(point) else outsideValue

        private fun isInArea(point: Point): Boolean = point.x in minX..maxX && point.y in minY..maxY

        private fun Point.aroundPoints(): List<Point> =
            around.map { this.plus(it) }

        fun count(): Int = points.size

        val around = listOf(
            Point(-1, -1),
            Point(0, -1),
            Point(1, -1),
            Point(-1, 0),
            Point(0, 0),
            Point(1, 0),
            Point(-1, 1),
            Point(0, 1),
            Point(1, 1),
        )
    }
}