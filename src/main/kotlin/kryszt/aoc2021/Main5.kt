package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.addCount
import kryszt.tools.alsoPrint
import java.lang.Integer.max
import java.lang.Integer.signum
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.absoluteValue

object Main5 : DayChallenge {

    private val linePattern = "([0-9]+),([0-9]+) -> ([0-9]+),([0-9]+)".toPattern()

    @JvmStatic
    fun main(args: Array<String>) {
//        taskOne()
        printTasks()
    }

    override fun taskOne() {
        readTaskLines()
            .parseVents(this::verticalOrHorizontal)
            .markVents()
            .also { println(it.size) }
            .count { (_, v) ->
                v.get() > 1
            }
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines()
            .parseVents()
            .markVents()
//            .also { it.printBoard { it?.get()?.toString() ?: "." } }
            .also { println(it.size) }
            .count { (_, v) ->
                v.get() > 1
            }
            .alsoPrint()
    }


    private fun List<Vents>.markVents(): Map<Point, AtomicInteger> {
        val counts = mutableMapOf<Point, AtomicInteger>()
        this.forEach { vents ->
            vents.addTo(counts)
        }
        return counts
    }

    private fun Vents.addTo(counts: MutableMap<Point, AtomicInteger>) {
        val steps = longerSide
        repeat(steps) { step ->
            val x = from.x + step * stepX
            val y = from.y + step * stepY
            val place = Point(x, y)
            counts.addCount(place)
        }
    }

    private fun verticalOrHorizontal(vents: Vents): Boolean =
        vents.width == 1 || vents.height == 1

    private fun List<String>.parseVents(predicate: (Vents) -> Boolean = { true }): List<Vents> =
        this.map { it.asVents() }.filter(predicate)

    private fun String.asVents(): Vents {
        return linePattern
            .matcher(this)
            .takeIf { it.matches() }
            ?.let {
                Vents(
                    Point(it.group(1).toInt(), it.group(2).toInt()),
                    Point(it.group(3).toInt(), it.group(4).toInt())
                )
            } ?: throw IllegalArgumentException("Cannot parse: $this")
    }

    data class Vents(val from: Point, val to: Point) {
        val width = (to.x - from.x).absoluteValue + 1
        val height = (to.y - from.y).absoluteValue + 1

        val longerSide = max(width, height)

        val stepX: Int = signum(to.x - from.x)
        val stepY: Int = signum(to.y - from.y)
    }
}
