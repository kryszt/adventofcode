package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint
import kryszt.tools.printBoard

object Main13 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        readTaskLines()
            .parseData()
            .let { it.first.executeFold(it.second.first()) }
            .size
            .alsoPrint()
    }

    override fun taskTwo() {
        readTaskLines()
            .parseData()
            .let { it.first.executeFolds(it.second) }
            .also { it.printBoard(onEmpty = ' ') }
    }

    private fun List<String>.parseData(): Pair<Set<Point>, List<Fold>> {
        val points = this
            .mapNotNull { it.asPoint() }
            .toSet()
        val folds = this
            .mapNotNull { it.asFold() }
        return points to folds
    }

    private fun Set<Point>.executeFolds(folds: List<Fold>): Set<Point> =
        folds.fold(this, operation = { acc, fold -> acc.executeFold(fold) })

    private fun Set<Point>.executeFold(fold: Fold): Set<Point> = when (fold.line) {
        'x' -> foldWith(foldSelection = { it.x > fold.at }, { it.foldByX(fold.at) })
        'y' -> foldWith(foldSelection = { it.y > fold.at }, { it.foldByY(fold.at) })
        else -> throw IllegalArgumentException("cannot fold by $fold")
    }

    private fun Set<Point>.foldWith(foldSelection: (Point) -> Boolean, foldFunction: (Point) -> Point): Set<Point> {
        val (toFold, notFolded) = this.partition(foldSelection)
        val folded = toFold.map(foldFunction)

        return notFolded.toSet().plus(folded)
    }

    private fun Point.foldByX(line: Int): Point = this.copy(x = line - (this.x - line))

    private fun Point.foldByY(line: Int): Point = this.copy(y = line - (this.y - line))

    data class Fold(val line: Char, val at: Int)

    private fun String.asPoint(): Point? =
        pointPattern
            .matcher(this)
            .takeIf { it.matches() }
            ?.let {
                Point(it.group(1).toInt(), it.group(2).toInt())
            }

    private fun String.asFold(): Fold? =
        foldPattern
            .matcher(this)
            .takeIf { it.matches() }
            ?.let {
                Fold(it.group(1).first(), it.group(2).toInt())
            }

    private val pointPattern = "([0-9]+),([0-9]+)".toPattern()
    private val foldPattern = "fold along ([x|y])=([0-9]+)".toPattern()
}
