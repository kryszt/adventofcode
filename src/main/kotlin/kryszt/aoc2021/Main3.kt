package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines

object Main3 : DayChallenge {

    private val input: List<String> = readTaskLines()

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val first = input
            .buildFromMajority(char = '1')
            .toInt(2)
        val second = input
            .buildFromMajority(char = '0')
            .toInt(2)

        println(first * second)
    }

    override fun taskTwo() {
        val first = input.findUsing { mostCommon(it) }
            .toInt(2)

        val second = input.findUsing { leastCommon(it) }
            .toInt(2)

        println(first * second)
    }

    private fun List<String>.buildFromMajority(char: Char): String {
        val size = this.first().length
        return (0 until size).map { index ->
            if (hasMajority(char, index)) '1' else '0'
        }
            .joinToString("")
    }

    private fun List<String>.findUsing(charSelector: List<String>.(Int) -> Char): String {
        var currentList = this
        var filterIndex = 0

        while (currentList.size > 1) {
            currentList = currentList.filterUsing(filterIndex, charSelector)
            filterIndex++
        }
        return currentList.first()
    }

    private fun List<String>.filterUsing(
        atIndex: Int,
        charSelector: List<String>.(Int) -> Char
    ): List<String> {
        val selected = this.charSelector(atIndex)
        return this.filter { it[atIndex] == selected }
    }

    private fun List<String>.mostCommon(atIndex: Int): Char =
        if (hasMajority('1', atIndex)) '1' else '0'

    private fun List<String>.leastCommon(atIndex: Int): Char =
        if (hasMajority('1', atIndex)) '0' else '1'

    private fun List<String>.hasMajority(char: Char, atIndex: Int): Boolean {
        val count = countCharsAt(char, atIndex)
        val other = size - count
        return count >= other
    }

    private fun List<String>.countCharsAt(char: Char, atIndex: Int): Int =
        this.count { it[atIndex] == char }
}
