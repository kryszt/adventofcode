package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint
import java.util.*
import kotlin.math.max
import kotlin.math.min

object Main23 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solve(readTaskLines())
    }

    override fun taskTwo() {
        solve(readTaskLines("-2"))
    }

    private fun solve(input:List<String>) {
        val board = input
            .buildBoard()

        val (cost, moves) = Solver().solve(board)
        cost.alsoPrint()
    }

    private fun debugResult(boardState: BoardState, moves: List<Move>) {
        var currentBoard = boardState
        currentBoard.printBoard()

        moves.forEach {
            println(it)
            currentBoard = currentBoard.applyMove(it)
            currentBoard.printBoard()
            currentBoard.totalCost.alsoPrint()
            println()
        }
    }

    private class Solver {
        var bestScore = Int.MAX_VALUE
        var bestMoves: List<Move> = emptyList()
        val moves: Stack<Move> = Stack()

        fun solve(initialBoard: BoardState): Pair<Int, List<Move>> {
            val moves = initialBoard.findMoves()
            moves.forEach {
                execute(initialBoard, it)
            }
            return bestScore to bestMoves
        }

        private fun finishFound(boardState: BoardState) {
            if (boardState.totalCost < bestScore) {
                bestScore = boardState.totalCost
                println("New best ${boardState.totalCost} for moves $moves")
                bestMoves = moves.toList()
            }
        }

        private fun execute(currentBoard: BoardState, move: Move) {
            moves.push(move)
            val changedBoard = currentBoard.applyMove(move)

            if (changedBoard.isFinished()) {
                finishFound(changedBoard)
            } else if (changedBoard.possibleCost() < bestScore) {
                val nextMoves = changedBoard.findMoves()
                nextMoves.forEach { nextMove ->
                    execute(changedBoard, nextMove)
                }
            }


            moves.pop()
        }
    }

    private data class BoardState(
        val items: List<Item>,
        val targets: Map<ItemType, ItemTarget>,
        val stageAreas: Set<Point>,
        val totalCost: Int = 0,
    ) {

        fun printBoard() {
            (1..6).forEach { y ->
                repeat(13) { x ->
                    val typeString = items.find { it.point == Point(x, y) }?.type?.toString() ?: "."
                    print(typeString)
                }
                println()
            }
        }

        private val stageY = stageAreas.first().y

        fun possibleCost(): Int =
            totalCost + missingCost()

        fun missingCost(): Int {
            return items.sumOf { missingCost(it) } + targets.values.sumOf { missingCost(it) }
        }

        private fun missingCost(item: Item): Int {
            if (item.state == ItemState.FINISHED) return 0
            val target = targets[item.type]!!
            val penalty = if (item.point.x == target.entryPoint.x) 2 else 0
            val toEntrance = item.point.manhattanDistance(target.entryPoint)
            return (penalty + toEntrance) * item.type.cost
        }

        private fun missingCost(target: ItemTarget): Int {
            val inTarget = ((1 + target.points.size) * target.points.size) / 2
            return inTarget * target.type.cost
        }

        fun findMoves(): List<Move> {
            return outToTarget() + initialToOut()
        }

        private fun outToTarget(): List<Move> {
            return items
                .filter { it.state == ItemState.OUT }
                .mapNotNull { tryMakeMoveToTarget(it) }
                .sortedBy { it.cost }
        }

        private fun tryMakeMoveToTarget(item: Item): Move? {
            val target = targets[item.type]!!
            if (!canMoveTo(item, target.entryPoint)) return null
            if (!canEnterTarget(target)) return null
            return Move(item, target.lastPlace!!, ItemState.FINISHED)
        }

        private fun canEnterTarget(target: ItemTarget): Boolean =
            target.points.none { hasItemAt(it) }

        private fun isEmptyAt(point: Point): Boolean = !hasItemAt(point)

        private fun hasItemAt(point: Point): Boolean = items.any { it.point == point }

        private fun initialToOut(): List<Move> {
            return items
                .filter { it.state == ItemState.INITIAL && canMoveOut(it) }
                .flatMap { findOutMoves(it) }
                .sortedBy { it.cost }
        }

        private fun findOutMoves(item: Item): List<Move> {
            val entrance = item.point.copy(y = stageY)
            return stageAreas
                .filter { canMoveBetween(it, entrance) }
                .map { Move(item, it, ItemState.OUT) }
        }

        private fun canMoveOut(item: Item): Boolean {
            return !hasItemAt(item.point.oneUp())
        }

        fun isFinished(): Boolean {
            return targets.all { it.value.fullyOccupied }
        }

        fun finished(item: Item, at: Point): BoardState {
            val newItems = this.items.minus(item).plus(item.copy(state = ItemState.FINISHED, point = at))
            val cost = item.point.manhattanDistance(at) * item.type.cost
            val newTotalCost = totalCost + cost
            val newTarget = targets[item.type]!!.removeLast()
            val newTargets = targets.plus(item.type to newTarget)
            return this.copy(
                items = newItems,
                totalCost = newTotalCost,
                targets = newTargets
            )
        }

        fun canMoveBetween(from: Point, to: Point): Boolean {
            val y = to.y
            val fromX = min(from.x, to.x)
            val toX = max(from.x, to.x)
            return (fromX..toX).all { x ->
                isEmptyAt(Point(x, y))
            }
        }

        fun canMoveTo(item: Item, to: Point): Boolean {
            val y = item.point.y
            val fromX = min(item.point.x, to.x)
            val toX = max(item.point.x, to.x)
            return (fromX..toX).none { x ->
                items.any { it.point == Point(x, y) && it != item }
            }
        }

        fun applyMove(move: Move): BoardState {
            if (move.targetState == ItemState.FINISHED) {
                return finished(move.item, move.to)
            }
            val toMove =
                items.find { it.point == move.item.point } ?: throw IllegalStateException("Not found ${move.item}")
            val moved = toMove.copy(point = move.to, state = move.targetState)
            val changedItems = items.minus(toMove).plus(moved)
            return copy(
                items = changedItems,
                totalCost = totalCost + move.cost
            )
        }
    }

    private data class Move(
        val item: Item,
        val to: Point,
        val targetState: ItemState,
        val cost: Int = item.type.cost * item.point.manhattanDistance(to)
    )

    private data class Item(val type: ItemType, val point: Point, val state: ItemState = ItemState.INITIAL)

    private data class ItemTarget(val type: ItemType, val entryPoint: Point, val points: List<Point>) {
        val fullyOccupied: Boolean = points.isEmpty()

        val lastPlace: Point? = points.maxByOrNull { it.y }

        fun removeLast(): ItemTarget = copy(points = lastPlace?.let { points.minus(it) } ?: points)
    }

    private enum class ItemType(val cost: Int) {
        A(1),
        B(10),
        C(100),
        D(1000);
    }

    private enum class ItemState {
        INITIAL, OUT, FINISHED
    }

    private fun List<String>.buildBoard(): BoardState {
        val stayAreas = mutableListOf<Point>()
        val items = mutableListOf<Item>()
        forEachIndexed { y, line ->
            if (line.contains("....")) {
                stayAreas.addAll(line.findStayPoints(y))
            } else {
                line.findItems(y).also { items.addAll(it) }
            }
        }
        val targets = buildTargets(items)
        val stageWithoutEntries = stayAreas.minus(targets.values.map { it.entryPoint }.toSet())

        return BoardState(
            items = items,
            targets = targets,
            stageAreas = stageWithoutEntries.toSet()
        ).checkInitialOnTarget()

    }

    private fun BoardState.checkInitialOnTarget(): BoardState {
        this.items.filter { it.state == ItemState.INITIAL }.forEach { item ->
            val target = targets[item.type]?.takeIf { it.lastPlace == item.point }
            if (target != null) {
                println("found finished: $item")
                return this.finished(item, target.lastPlace!!).checkInitialOnTarget()
            }
        }
        return this
    }

    private fun buildTargets(items: List<Item>): Map<ItemType, ItemTarget> {
        val startX = items.minOf { it.point.x }
        val startY = items.minOf { it.point.y }
        val endY = items.maxOf { it.point.y }

        return mapOf(
            ItemType.A to ItemTarget(ItemType.A, Point(startX, startY - 1), makeTargetPoints(startX, startY, endY)),
            ItemType.B to ItemTarget(
                ItemType.B,
                Point(startX + 2, startY - 1),
                makeTargetPoints(startX + 2, startY, endY)
            ),
            ItemType.C to ItemTarget(
                ItemType.C,
                Point(startX + 4, startY - 1),
                makeTargetPoints(startX + 4, startY, endY)
            ),
            ItemType.D to ItemTarget(
                ItemType.D,
                Point(startX + 6, startY - 1),
                makeTargetPoints(startX + 6, startY, endY)
            ),
        )
    }

    private fun makeTargetPoints(x: Int, firstY: Int, lastY: Int): List<Point> = (firstY..lastY).map { Point(x, it) }

    private fun String.findStayPoints(y: Int): List<Point> =
        mapIndexedNotNull { x, c ->
            if (c == '.') Point(x, y) else null
        }

    private fun String.findItems(y: Int): List<Item> {
        val items = mutableListOf<Item>()
        forEachIndexed { x, c ->
            c
                .itemType()
                ?.let { Item(it, Point(x, y)) }
                ?.also { items.add(it) }
        }
        return items
    }

    private fun Char.itemType(): ItemType? = when (this) {
        'A' -> ItemType.A
        'B' -> ItemType.B
        'C' -> ItemType.C
        'D' -> ItemType.D
        else -> null
    }

}