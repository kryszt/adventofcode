package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines

object Main2 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        println("Task 1:")
        taskOne()
        println("\n\nTask 2:")
        taskTwo()
    }

    override fun taskOne() {
        val moves = readTaskLines()
            .groupBy({ it.split(" ")[0] }, { it.split(" ")[1].toInt() })
            .mapValues { e -> e.value.sum() }

        val f = moves["forward"]!!
        val d = moves["down"]!!
        val u = moves["up"]!!

        println("f=$f, d=$d, u=$u, -> $f * ${d - u} -> ${f * (d - u)}")
    }

    override fun taskTwo() {

        val moves = readTaskLines()
            .map { it.split(" ")[0] to it.split(" ")[1].toInt() }

        var aim = 0L
        var depth = 0L
        var f = 0L

        moves.forEach {
            when (it.first) {
                "forward" -> {
                    f += it.second
                    depth += (aim * it.second)
                }
                "down" -> aim += it.second
                "up" -> aim -= it.second
            }
        }
        println("f=$f, a=$aim, d=$depth, result = ${f * depth}")
    }
}
