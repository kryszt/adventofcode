package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines

object Main12 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        solveWith(readTaskLines().parse(), SmallOnceFilter())
    }

    override fun taskTwo() {
        val graph = readTaskLines()
            .parse()
        val smallCaves = graph.keys
            .filter { it.isSmall() && it != "start" && it != "end" }

        solveWith(graph, OneSmallTwice(smallCaves))
    }

    private fun solveWith(graph: Graph, filter: (String, List<String>) -> Boolean) {
        val solver = Pathfinder(graph, filter)
        solver.solve()
        println(solver.foundPaths)
    }

    private fun List<String>.parse(): Graph {
        val elements = mutableMapOf<String, Set<String>>()
        this
            .forEach {
                val (from, to) = it.split("-")
                elements.addConnection(from, to)
                elements.addConnection(to, from)
            }
        return elements
    }

    private fun MutableMap<String, Set<String>>.addConnection(from: String, to: String) {
        val newValue = get(from)?.plus(to) ?: setOf(to)
        put(from, newValue)
    }

    private class SmallOnceFilter : (String, List<String>) -> Boolean {

        override fun invoke(cave: String, visited: List<String>): Boolean =
            !(cave.isSmall() && visited.contains(cave))
    }

    private class OneSmallTwice(private val allSmall: List<String>) : (String, List<String>) -> Boolean {

        override fun invoke(cave: String, visited: List<String>): Boolean {
            if (cave == "start") return false
            if (cave == "end") return true
            if (!cave.isSmall()) return true

            if (!visited.contains(cave)) return true

            return allSmall.none { smallCave -> visited.count { smallCave == it } > 1 }
        }
    }

    private fun String.isSmall() = this[0].isLowerCase()

    class Pathfinder(
        private val graph: Graph,
        private val nextCaveFilter: (String, List<String>) -> Boolean
    ) {
        private val startCave = "start"
        private val endCave = "end"

        private var visitedPath: List<String> = emptyList()
        var foundPaths: Int = 0
            private set

        fun solve() {
            visit(startCave)
        }

        private fun visit(cave: String) {
            visitedPath = visitedPath + cave
            if (cave == endCave) {
//                println("found path: $visitedPath")
                foundPaths++
                visitedPath = visitedPath.dropLast(1)
                return
            }

            val nextToVisit = cave.nextToVisit()
            nextToVisit.forEach { visit(it) }

            visitedPath = visitedPath.dropLast(1)
        }

        private fun String.nextToVisit(): List<String> {
            return graph[this]
                ?.filter {
                    nextCaveFilter(it, visitedPath)
                }
                .orEmpty()
        }

    }
}
private typealias Graph = Map<String, Set<String>>
