package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.IoUtil.readTaskLines
import kryszt.tools.Point
import kryszt.tools.alsoPrint

object Main11 : DayChallenge {

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val board = readTaskLines()
            .asBoard()
        val flashes = countFlashesInSteps(board, 100)
        board.printBoard()
        println()
        println(flashes)
    }

    override fun taskTwo() {
        readTaskLines()
            .asBoard()
            .let { findSyncStep(it) }
            .alsoPrint()
    }

    private fun List<String>.asBoard(): Array<IntArray> =
        this
            .map { row ->
                row.map { it - '0' }.toIntArray()
            }
            .toTypedArray()


    @Suppress("SameParameterValue")
    private fun countFlashesInSteps(board: Array<IntArray>, steps: Int): Int {
        var totalFlashes = 0
        repeat(steps) {
            board.nextStep()
            totalFlashes += board.countFlashes()
        }
        return totalFlashes
    }

    private fun findSyncStep(board: Array<IntArray>): Int {
        var step = 0
        while (!board.allFlashed()) {
            step++
            board.nextStep()
        }
        return step
    }

    private fun Array<IntArray>.nextStep(): Array<IntArray> {
        forEachIndexed { y, row ->
            row.indices.forEach { x ->
                increase(Point(x, y))
            }
        }
        normalize()
        return this
    }

    private fun Array<IntArray>.countFlashes(): Int =
        this.sumOf { row -> row.count { it == 0 } }

    private fun Array<IntArray>.allFlashed(): Boolean =
        this.all { row -> row.all { it == 0 } }

    private fun Array<IntArray>.normalize() {
        forEach { row ->
            row.indices.forEach { x ->
                if (row[x] > 9) row[x] = 0
            }
        }
    }

    private fun Array<IntArray>.increase(point: Point) {
        getOrNull(point.y)?.let { row ->
            if (row.indices.contains(point.x)) {
                row[point.x]++
                if (row[point.x] == 10) {
                    point.aroundNeighbours().forEach {
                        this.increase(it)
                    }
                }
            }
        }

    }

    private fun Array<IntArray>.printBoard(): Array<IntArray> {
        forEach { row ->
            row.forEach {
                val toPrint = if (it > 9) 0 else it
                print(toPrint)
            }
            println()
        }
        return this
    }
}