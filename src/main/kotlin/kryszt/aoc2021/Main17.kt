package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.Point
import kotlin.math.absoluteValue

object Main17 : DayChallenge {

    private val targetArea = TargetArea(138..184, -125..-71)
//    private val targetArea = TargetArea(20..30, -10..-5)

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val maxYSpeed = findMaxYVelocity()
        val maxHeight = ((1 + maxYSpeed) * maxYSpeed) / 2
        println(maxHeight)
    }

    override fun taskTwo() {
        val minx = findMinXVelocity()
        val maxx = targetArea.x.last
        val miny = targetArea.y.first
        val maxy = findMaxYVelocity()

        println("$minx,$miny -> $maxx,$maxy")

        var totalHit = 0
        for (x in minx..maxx) {
            for (y in miny..maxy) {
                if (hits(Point(x, y))) {
                    totalHit++
                }
            }
        }
        println(totalHit)
    }

    private fun hits(startV: Point): Boolean {
        var step = 1

        while (true) {
            val x = startV.x.xDistance(step)
            val y = startV.y.yDistance(step)
            if (hit(x, y)) return true
            if (missed(x, y)) return false
            step++
        }
    }

    private fun hit(x: Int, y: Int): Boolean =
        targetArea.x.contains(x)
                && targetArea.y.contains(y)

    private fun missed(x: Int, y: Int): Boolean =
        x > targetArea.x.last || y < targetArea.y.first

    private fun findMaxYVelocity(): Int =
        targetArea.y.first.absoluteValue.minus(1)

    private fun findMinXVelocity(): Int {
        val range = targetArea.x.first
        var vx = 0
        while (vx.maxDistance() < range) {
            vx++
        }
        return vx
    }

    private fun Int.xDistance(steps: Int): Int {
        val s = steps.coerceAtMost(this)
        return (((2 * this) + 1 - s) * s) / 2
    }

    private fun Int.yDistance(steps: Int): Int {
        return (((2 * this) + 1 - steps) * steps) / 2
    }

    private fun Int.maxDistance(): Int = ((1 + this) * this) / 2

    class TargetArea(val x: IntRange, val y: IntRange) {
        fun contains(point: Point) = x.contains(point.x) && y.contains(point.y)
    }
}
