package kryszt.aoc2021

import kryszt.tools.DayChallenge
import kryszt.tools.alsoPrint
import java.util.*
import kotlin.math.max

object Main21 : DayChallenge {

    //real:
    //Player 1 starting position: 9
    //Player 2 starting position: 6

    // test
    //Player 1 starting position: 4
    //Player 2 starting position: 8

    @JvmStatic
    fun main(args: Array<String>) {
        printTasks()
    }

    override fun taskOne() {
        val dice = DeterministicDice()
        val gameState = GameState(PlayerState(9), PlayerState(6), 1000)
        val result = play(gameState, dice)

        val score = result.looser.score * dice.totalRolls
        println(score)
    }

    override fun taskTwo() {
        val gameState = GameState(PlayerState(9), PlayerState(6), 21)
        playDiracDice(gameState).alsoPrint()
    }

    private fun play(_gameState: GameState, dice: Dice): GameState {
        var gameState = _gameState

        while (!gameState.isFinished) {
            gameState = gameState.moveNextPlayer(dice.rollMultiple(3))
        }
        return gameState
    }

    private fun playDiracDice(startingState: GameState): Long {
        val activeGameStates = LinkedList(listOf(startingState))

        var firstWins: Long = 0
        var secondWins: Long = 0

        fun addFinished(gameState: GameState) {
            if (gameState.firstWon) {
                firstWins += gameState.worldCount
            } else {
                secondWins += gameState.worldCount
            }
        }

        while (activeGameStates.isNotEmpty()) {

            val nextToEvaluate = activeGameStates.pollFirst()
            val nextStates = cubeResults.map {
                nextToEvaluate.moveNextPlayer(it.steps, it.times)
            }
            nextStates.forEach { gameState ->
                if (gameState.isFinished) {
                    addFinished(gameState)
                } else {
                    activeGameStates.add(gameState)
                }
            }
        }
        return max(firstWins, secondWins)
    }

    interface Dice {
        fun next(): Int
        val totalRolls: Int
    }

    class DeterministicDice : Dice {
        override var totalRolls: Int = 0
            private set

        override fun next(): Int {
            val nextValue = totalRolls.mod(100).plus(1)
            totalRolls++
            return nextValue
        }
    }

    private fun Dice.rollMultiple(times: Int): Int {
        var sum = 0
        repeat(times) {
            sum += next()
        }
        return sum
    }

    private data class PlayerState(val position: Int, val score: Int = 0) {
        fun moveBy(steps: Int): PlayerState {
            val newPosition = position.minus(1).plus(steps).mod(10).plus(1)
            return moveTo(newPosition)
        }

        private fun moveTo(newPosition: Int): PlayerState {
            return copy(
                position = newPosition,
                score = score + newPosition
            )
        }
    }

    private data class GameState(
        val playerOne: PlayerState,
        val playerTwo: PlayerState,
        val scoreLimit: Int,
        val worldCount: Long = 1,
        val firstGoesNext: Boolean = true
    ) {

        fun moveNextPlayer(steps: Int, inWorlds: Long = 1): GameState =
            if (firstGoesNext) {
                movePlayerOne(steps, inWorlds)
            } else {
                movePlayerTwo(steps, inWorlds)
            }

        private fun movePlayerOne(steps: Int, inWorlds: Long = 1): GameState =
            copy(playerOne = playerOne.moveBy(steps), firstGoesNext = false, worldCount = worldCount * inWorlds)

        private fun movePlayerTwo(steps: Int, inWorlds: Long = 1): GameState =
            copy(playerTwo = playerTwo.moveBy(steps), firstGoesNext = true, worldCount = worldCount * inWorlds)

        val isFinished: Boolean
            get() = playerOne.score >= scoreLimit || playerTwo.score >= scoreLimit

        val looser: PlayerState
            get() {
                check(isFinished) { "Game must be finished" }
                return playerOne.takeIf { it.score < scoreLimit } ?: playerTwo
            }

        val firstWon: Boolean
            get() {
                check(isFinished) { "Game must be finished" }
                return playerOne.score >= scoreLimit
            }
    }

    private data class CubeResult(val steps: Int, val times: Long)

    private val cubeResults = listOf(
        CubeResult(3, 1),
        CubeResult(4, 3),
        CubeResult(5, 6),
        CubeResult(6, 7),
        CubeResult(7, 6),
        CubeResult(8, 3),
        CubeResult(9, 1),
    )
}
