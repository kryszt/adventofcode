plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "1.9.10"
}

group "org.kryszt.adventofcode"
version "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
}
